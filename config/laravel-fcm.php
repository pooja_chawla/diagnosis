<?php

return [

    /**
     * Set your FCM Server Key
     * Change to yours
     */

    'server_key' => env('FCM_SERVER_KEY', ''),
    'fcm_sound' => env('FCM_SOUND', 'default'),

];
