<form class="form-horizontal" id="csv-form" enctype="multipart/form-data" >
   
    <div class="form-group">
        <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

        <div class="col-md-6">
            <input type="file" id="csv_file" class="form-control"  name="csv_file" onchange="uploadCsv(this)" >
        </div>
    </div>
	
 
</form>
<script>
	function uploadCsv(imp){ 
	    let formData = new FormData();           
    	formData.append("file", imp.files[0]);
		   $.ajax({
		   	   	headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		       },
		        url: '/admin/importCsv',
		       type: 'POST',
		       data:  formData,
		       enctype: 'multipart/form-data',
		       processData: false,
		       success: function (response) {
		         console.log(response);
		       }
		   });
	   return false;
	 }
</script>