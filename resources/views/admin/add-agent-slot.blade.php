
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<div class="row">
	<div class="col-md-12"></div>
	<div class="col-md-12"></div>
	<div class="col-md-12"></div>
 
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
<?php foreach($days as $day){

?>
 <div class="col-md-12" style="margin-top: 40px;">
 	<div class="col-md-3"></div>
 	<div class="col-md-2">
 		 <label><?php echo $day->day;?> </label>
 	</div>
 	<div class="col-md-2">
 		 <label>Start </label>
 		 <input class="form" type="time" id="opening_hour<?php echo $day->id;?>"  value="<?php echo isset($day->opening_hour) ? $day->opening_hour : '' ?>" onchange="saveSlots('<?php echo $day->id;?>')">
 	</div>
 	<div class="col-md-2">
 		 <label>End </label>
 		 <input class="form" type="time" id="closing_hour<?php echo $day->id;?>"  value="<?php echo isset($day->closing_hour) ? $day->closing_hour : '' ?>" onchange="saveSlots('<?php echo $day->id;?>')">
 	</div>
 	<?php if($day->opening_hour!='' && $day->closing_hour!=''){?>
 		<button class="btn btn-primary" onclick="showSlot('<?php echo $day->id;?>');">Slots</button>
    <?php }?>		
 	<input type="hidden" id="day_id<?php echo $day->id;?>" value="<?php echo $day->id;?>">
 </div>	
  <div  id="showSlots<?php echo $day->id;?>" class="col-md-12" style="margin-top: 10px; display:none">
  	<div class="col-md-3"></div>
  	<?php 
  		$start = strtotime($day->opening_hour);
        $end = strtotime($day->closing_hour);
        $minutes  = ($end - $start) / 60;
        $start1 = strtotime('01:00');
        $end1 = strtotime('00:00');
        $duration  = ($start1 - $end1) / 60;
        $slots = $minutes /$duration;
        $s =  $day->opening_hour;
        $now = \Carbon\Carbon::today();
        for ($i = 01; $i <= $slots; $i++){	
        $checkSlot=DB::table('agent_slot_not_available')->where('day_id', $day->id)->where('slot_id',$i)->where('status',0)->get();
        $checkBlockSlot=DB::table('agent_slot_not_available')->where('day_id', $day->id)->where('slot_id',$i)->where('status',1)->whereDate('created_at',$now)->get();
  	?>

  	<div class="col-md-1">
    <?php if(count($checkSlot)==0 && count($checkBlockSlot) ==0){?>		
  	  <button id="slot<?php echo $i;?>_<?php echo $day->id;?>" onclick="notAvailable('<?php echo $day->id;?>','<?php echo $i;?>')" class="btn btn-success"><h6><span id="slotText<?php echo $i;?>_<?php echo $day->id;?>"></span> Available</h6><?php }elseif(count($checkSlot)!=0){?>
  	  <button onclick="notAvailable('<?php echo $day->id;?>','<?php echo $i;?>')" class="btn btn-danger"><h6>Not Available</h6>
  	  <?php }elseif(count($checkBlockSlot)!=0){?>
  	  	<button  class="btn btn-warning"><h6>Booked</h6>
  	  <?php }?>	
  	  	<?php echo $s; ?>-<?php 
  	  		if($duratn == 1){
  	  			$timestamp = strtotime($s) + 60*60;
  	  		}else{
  	  			
  	  			$timestamp = strtotime($s) + 60*60*$duratn;	
  	  		}
              $time = date('h:i', $timestamp);
              echo $time;
              $s = $time;
  	  ?>
  	  	
  	  </button>
   </div>
   <?php }?>
   
   
  </div>
<?php }?>
</div>

<script>
	function getAgent(id){
		var id = $('#agent_id').val();
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/agentSlots',
	           data: {id:id},
	           success: function( msg ) {
	           		 //alert(msg);
	           		 
	            	//toastr.success('Duration Updated Successfully')
	           }
	       });

	}
	function notAvailable(day_id,slot_id){
		
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/agentSlotNotAvailable',
	           data: {day_id:day_id,slot_id:slot_id},
	           success: function( msg ) {
	           	  if(msg == 1 ){

	           	  	 $("#slot"+slot_id+'_'+day_id).removeClass("btn-success");  
                     $("#slot"+slot_id+'_'+day_id).addClass("btn-danger");	
                     $('#slotText'+slot_id+'_'+day_id).html('Not')
	           	  }else{
	           	  	
	           	  	 $("#slot"+slot_id+'_'+day_id).removeClass("btn-danger");  
                     $("#slot"+slot_id+'_'+day_id).addClass("btn-success");
	           		 //alert(msg);
	           	  }
	           		
	            	//toastr.success('Duration Updated Successfully')
	           }
	       });
	}


	function saveSlots(day){
		
		var day_id  = $('#day_id'+day).val();
		var opening_hour  = $('#opening_hour'+day).val();
		var closing_hour  = $('#closing_hour'+day).val();
		var agent_id = $('#agent_id').val();
		
		
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/updateAgentSlot',
	           data: {day_id:day_id,opening_hour:opening_hour,closing_hour:closing_hour,agent_id:agent_id},
	           success: function( msg ) {
	           		toastr.success('Time Updated Successfully')	
	            	
	           }
	       });	
	}
	function showSlot(day){
		$('#showSlots'+day).show();
	}
</script>