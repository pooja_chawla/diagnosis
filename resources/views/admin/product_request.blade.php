
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="csrf-token" content="FcC2Ainm7K04HkBKjDGxHZARmiMnxTpUd1teXYfM">
    <title>Admin  | Inventory</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/iCheck/all.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/shortcut-buttons-flatpickr@0.3.0/dist/themes/light.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-fileinput/css/fileinput.min.css?v=4.5.2">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap-duallistbox/dist/bootstrap-duallistbox.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/dist/css/skins/skin-blue-light.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/laravel-admin/laravel-admin.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/nprogress/nprogress.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/sweetalert2/dist/sweetalert2.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/nestable/nestable.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/toastr/build/toastr.min.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/google-fonts/fonts.css">
    <link rel="stylesheet" href="/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css">


    <script src="/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/vendor/laravel-admin-ext/grid-sortable/jquery-ui.min.js"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition skin-blue-light sidebar-mini sidebar-collapse">


<div class="wrapper">

    <!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>La</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Laravel</b> admin</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <ul class="nav navbar-nav hidden-sm visible-lg-block">
        
        </ul>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li>
    <a href="javascript:void(0);" class="container-refresh">
        <i class="fa fa-refresh"></i>
    </a>
</li>

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">Administrator</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                Administrator
                                <small>Member since admin 2020-12-08 09:52:16</small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/admin/auth/setting" class="btn btn-default btn-flat">Setting</a>
                            </div>
                            <div class="pull-right">
                                <a href="/admin/auth/logout" class="btn btn-default btn-flat">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                
                    
                
            </ul>
        </div>
    </nav>
</header>
    <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/vendor/laravel-admin/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Administrator</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

                <!-- search form (Optional) -->
        <form class="sidebar-form" style="overflow: initial;" onsubmit="return false;">
            <div class="input-group">
                <input type="text" autocomplete="off" class="form-control autocomplete" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                <ul class="dropdown-menu" role="menu" style="min-width: 210px;max-height: 300px;overflow: auto;">
                                        <li>
                        <a href="/admin/slots"><i class="fa fa-bars"></i>lab Slots</a>
                    </li>
                                        <li>
                        <a href="/admin/customer-prescriptions"><i class="fa fa-download"></i>Customer Prescription</a>
                    </li>
                                        <li>
                        <a href="/admin/customer_booking_product"><i class="fa fa-product-hunt"></i>Customer Booking Product</a>
                    </li>
                                        <li>
                        <a href="/admin/lab-collection"><i class="fa fa-object-group"></i>Lab Collection</a>
                    </li>
                                        <li>
                        <a href="/admin/home-collection"><i class="fa fa-object-ungroup"></i>Home Collection</a>
                    </li>
                                        <li>
                        <a href="/admin/work-days"><i class="fa fa-clock-o"></i>Available Timings</a>
                    </li>
                                        <li>
                        <a href="/admin"><i class="fa fa-bar-chart"></i>Dashboard</a>
                    </li>
                                        <li>
                        <a href="/admin/settings"><i class="fa fa-cogs"></i>Settings</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/users"><i class="fa fa-users"></i>Users</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/roles"><i class="fa fa-user"></i>Roles</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/permissions"><i class="fa fa-ban"></i>Permission</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/menu"><i class="fa fa-bars"></i>Menu</a>
                    </li>
                                        <li>
                        <a href="/admin/auth/logs"><i class="fa fa-history"></i>Operation log</a>
                    </li>
                                        <li>
                        <a href="/admin/inventories"><i class="fa fa-briefcase"></i>Inventories</a>
                    </li>
                                        <li>
                        <a href="/admin/categories"><i class="fa fa-bars"></i>Categories</a>
                    </li>
                                        <li>
                        <a href="/admin/tags"><i class="fa fa-anchor"></i>Tags</a>
                    </li>
                                        <li>
                        <a href="/admin/products"><i class="fa fa-shopping-bag"></i>Products</a>
                    </li>
                                        <li>
                        <a href="/admin/customers"><i class="fa fa-users"></i>Customers</a>
                    </li>
                                        <li>
                        <a href="/admin/bookings"><i class="fa fa-th-list"></i>Bookings</a>
                    </li>
                                        <li>
                        <a href="/admin/vendor-products"><i class="fa fa-bars"></i>Vendor Products</a>
                    </li>
                                        <li>
                        <a href="/admin/price-caps"><i class="fa fa-cc-visa"></i>Price Cap</a>
                    </li>
                                        <li>
                        <a href="/admin/agents"><i class="fa fa-user-secret"></i>Agents</a>
                    </li>
                                        <li>
                        <a href="/admin/agent-slots"><i class="fa fa-bars"></i>Agent Slots</a>
                    </li>
                                    </ul>
            </div>
        </form>
        <!-- /.search form -->
        
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>

            <li>
                             <a href="/admin/slots">
                            <i class="fa fa-bars"></i>
                                    <span>lab Slots</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/customer-prescriptions">
                            <i class="fa fa-download"></i>
                                    <span>Customer Prescription</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/customer_booking_product">
                            <i class="fa fa-product-hunt"></i>
                                    <span>Customer Booking Product</span>
                            </a>
        </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-th-list"></i>
                                    <span>Customer Booking</span>
                                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                    <li>
                             <a href="/admin/lab-collection">
                            <i class="fa fa-object-group"></i>
                                    <span>Lab Collection</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/home-collection">
                            <i class="fa fa-object-ungroup"></i>
                                    <span>Home Collection</span>
                            </a>
        </li>
                                </ul>
        </li>
    <li>
                             <a href="/admin/work-days">
                            <i class="fa fa-clock-o"></i>
                                    <span>Available Timings</span>
                            </a>
        </li>
    <li>
                             <a href="/admin">
                            <i class="fa fa-bar-chart"></i>
                                    <span>Dashboard</span>
                            </a>
        </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-tasks"></i>
                                    <span>Admin</span>
                                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                    <li>
                             <a href="/admin/settings">
                            <i class="fa fa-cogs"></i>
                                    <span>Settings</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/users">
                            <i class="fa fa-users"></i>
                                    <span>Users</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/roles">
                            <i class="fa fa-user"></i>
                                    <span>Roles</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/permissions">
                            <i class="fa fa-ban"></i>
                                    <span>Permission</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/menu">
                            <i class="fa fa-bars"></i>
                                    <span>Menu</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/auth/logs">
                            <i class="fa fa-history"></i>
                                    <span>Operation log</span>
                            </a>
        </li>
                                </ul>
        </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-medkit"></i>
                                    <span>Products</span>
                                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                    <li>
                             <a href="/admin/inventories">
                            <i class="fa fa-briefcase"></i>
                                    <span>Inventories</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/categories">
                            <i class="fa fa-bars"></i>
                                    <span>Categories</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/tags">
                            <i class="fa fa-anchor"></i>
                                    <span>Tags</span>
                            </a>
        </li>
                                        <li>
                             <a href="/admin/products">
                            <i class="fa fa-shopping-bag"></i>
                                    <span>Products</span>
                            </a>
        </li>
                                </ul>
        </li>
    <li>
                             <a href="/admin/customers">
                            <i class="fa fa-users"></i>
                                    <span>Customers</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/bookings">
                            <i class="fa fa-th-list"></i>
                                    <span>Bookings</span>
                            </a>
        </li>
    <li class="treeview">
            <a href="#">
                <i class="fa fa-user-plus"></i>
                                    <span>Vendors</span>
                                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                                    <li>
                             <a href="/admin/vendor-products">
                            <i class="fa fa-bars"></i>
                                    <span>Vendor Products</span>
                            </a>
        </li>
                                </ul>
        </li>
    <li>
                             <a href="/admin/price-caps">
                            <i class="fa fa-cc-visa"></i>
                                    <span>Price Cap</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/agents">
                            <i class="fa fa-user-secret"></i>
                                    <span>Agents</span>
                            </a>
        </li>
    <li>
                             <a href="/admin/agent-slots">
                            <i class="fa fa-bars"></i>
                                    <span>Agent Slots</span>
                            </a>
        </li>
    
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
    <div class="content-wrapper" id="pjax-container">
        <style type="text/css"> .column-selector { margin-right: 10px; } .column-selector .dropdown-menu { padding: 10px; height: auto; max-height: 500px; overflow-x: hidden; } .column-selector .dropdown-menu ul { padding: 0; } .column-selector .dropdown-menu ul li { margin: 0; } .column-selector .dropdown-menu label { width: 100%; padding: 3px; } </style>

        <div id="app">
            <section class="content-header">
        <h1>
            Lab Slots
            <small>List</small>
        </h1>

        <!-- breadcrumb start -->
                <ol class="breadcrumb" style="margin-right: 30px;">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li>
                Lab Slots
                </li>
                    </ol>
        
        <!-- breadcrumb end -->

    </section>

 <section class="content">

                        
                    <div class="row"><div class="col-md-12"><div class="box grid-box">
    
        <div class="box-header with-border">
        <div class="pull-right">
            <div class="dropdown pull-right column-selector">
    <button type="button" class="btn btn-sm btn-instagram dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-table"></i>
        &nbsp;
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <ul>
                                
                <li class="checkbox icheck">
                    <label>
                        <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="column-select-item" value="product_id" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;&nbsp;&nbsp;Product
                    </label>
                </li>
                                
                <li class="checkbox icheck">
                    <label>
                        <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="column-select-item" value="vendor" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;&nbsp;&nbsp;Vendor
                    </label>
                </li>
                                
                <li class="checkbox icheck">
                    <label>
                        <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="column-select-item" value="price" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;&nbsp;&nbsp;Price
                    </label>
                </li>
                                
                <li class="checkbox icheck">
                    <label>
                        <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="column-select-item" value="discount" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;&nbsp;&nbsp;Discount
                    </label>
                </li>
                                
                <li class="checkbox icheck">
                    <label>
                        <div class="icheckbox_minimal-blue checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="column-select-item" value="status" checked="" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;&nbsp;&nbsp;Status
                    </label>
                </li>
                            </ul>
        </li>
        <li class="divider">
        </li><li class="text-right">
            <button class="btn btn-sm btn-default column-select-all">All</button>&nbsp;&nbsp;
            <button class="btn btn-sm btn-primary column-select-submit">Submit</button>
        </li>
    </ul>
</div>
            
<div class="btn-group pull-right" style="margin-right: 10px">
    <a href="http://localhost:8000/admin/product-request?_export_=all" target="_blank" class="btn btn-sm btn-twitter" title="Export"><i class="fa fa-download"></i><span class="hidden-xs"> Export</span></a>
    <button type="button" class="btn btn-sm btn-twitter dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="http://localhost:8000/admin/product-request?_export_=all" target="_blank">All</a></li>
        <li><a href="http://localhost:8000/admin/product-request?_export_=page%3A1" target="_blank">Current page</a></li>
        <li><a href="http://localhost:8000/admin/product-request?_export_=selected%3A__rows__" target="_blank" class="export-selected">Selected rows</a></li>
    </ul>
</div>
            
        </div>
                <div class="pull-left">
            <div class="btn-group grid-select-all-btn" style="display:none;margin-right: 5px;">
    <a class="btn btn-sm btn-default hidden-xs"><span class="selected"></span></a>
    <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
        <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" class="grid-batch-0">Batch delete </a></li>
                        </ul>
    </div> <div class="btn-group" style="margin-right: 5px" data-toggle="buttons">
    <label class="btn btn-sm btn-dropbox 6113bf4ea5146-filter-btn " title="Filter">
        <input type="checkbox"><i class="fa fa-filter"></i><span class="hidden-xs">&nbsp;&nbsp;Filter</span>
    </label>

    </div> <button type="button" class="btn btn-sm btn-info grid-save-order-btn" style="margin-left: 10px;display: none;">
    <i class="fa fa-save"></i><span class="hidden-xs">&nbsp;&nbsp;Save order</span>
</button>
        </div>
            </div>
    
    <div class="box-header with-border hide filter-box" id="filter-box">
    <form action="http://localhost:8000/admin/product-request" class="form-horizontal" pjax-container="" method="get">

        <div class="row">
                        <div class="col-md-12">
                <div class="box-body">
                    <div class="fields-group">
                                                    <div class="form-group">
    <label class="col-sm-2 control-label"> ID</label>
    <div class="col-sm-8">
        <div class="input-group input-group-sm">
            <div class="input-group-addon">
            <i class="fa fa-pencil"></i>
        </div>

    <input type="text" class="form-control id" placeholder="ID" name="id" value="">
</div>    </div>
</div>
                                            </div>
                </div>
            </div>
                    </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="btn-group pull-left">
                            <button class="btn btn-info submit btn-sm"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </div>
                        <div class="btn-group pull-left " style="margin-left: 10px;">
                            <a href="http://localhost:8000/admin/product-request" class="btn btn-default btn-sm"><i class="fa fa-undo"></i>&nbsp;&nbsp;Reset</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>


    

    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover grid-table" id="grid-table6113bf4e69b67">
            <thead>
                <tr>
                                        <th class="column-__row_selector__"> <div class="icheckbox_minimal-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="grid-select-all" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>&nbsp;</th>
                                        <th class="column-__sortable__"> </th>
                                        <th class="column-product_id">Product</th>
                                        <th class="column-vendor">Vendor</th>
                                        <th class="column-price">Price</th>
                                        <th class="column-discount">Discount</th>
                                        <th class="column-status">Status</th>
                                    </tr>
            </thead>

            
            <tbody class="ui-sortable">

                
                                <tr data-key="1">
                                        <td class="column-__row_selector__">
                        <div class="icheckbox_minimal-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="grid-row-checkbox" data-id="1" autocomplete="off" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                                        <td class="column-__sortable__">
                        <a class="grid-sortable-handle ui-sortable-handle" style="cursor: move;" data-key="1" data-sort="">
    <i class="fa fa-ellipsis-v"></i>
    <i class="fa fa-ellipsis-v"></i>
  </a>
                    </td>
                                        <td class="column-product_id">
                        5-HYDROXY INDOLE ACETIC ACID (5-HIAA)-RANDOM URINE
                    </td>
                                        <td class="column-vendor">
                        preet_vendor
                    </td>
                                        <td class="column-price">
                        6000
                    </td>
                                        <td class="column-discount">
                        100
                    </td>
                                        <td class="column-status">
                        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-mini bootstrap-switch-animate" style="width: 115.222px;"><div class="bootstrap-switch-container" style="width: 169px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 56px;">Accepted</span><span class="bootstrap-switch-label" style="width: 56px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 56px;">Rejected</span><input type="checkbox" class="grid-switch-status" checked="" data-key="1"></div></div>
                    </td>
                                    </tr>
                                <tr data-key="2">
                                        <td class="column-__row_selector__">
                        <div class="icheckbox_minimal-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="grid-row-checkbox" data-id="2" autocomplete="off" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    </td>
                                        <td class="column-__sortable__">
                        <a class="grid-sortable-handle ui-sortable-handle" style="cursor: move;" data-key="2" data-sort="">
    <i class="fa fa-ellipsis-v"></i>
    <i class="fa fa-ellipsis-v"></i>
  </a>
                    </td>
                                        <td class="column-product_id">
                        5-HYDROXY INDOLE ACETIC ACID (5-HIAA)-RANDOM URINE
                    </td>
                                        <td class="column-vendor">
                        preet_vendor
                    </td>
                                        <td class="column-price">
                        6000
                    </td>
                                        <td class="column-discount">
                        100
                    </td>
                                        <td class="column-status">
                        <div class="bootstrap-switch bootstrap-switch-wrapper bootstrap-switch-on bootstrap-switch-mini bootstrap-switch-animate" style="width: 115.222px;"><div class="bootstrap-switch-container" style="width: 169px; margin-left: 0px;"><span class="bootstrap-switch-handle-on bootstrap-switch-success" style="width: 56px;">Accepted</span><span class="bootstrap-switch-label" style="width: 56px;">&nbsp;</span><span class="bootstrap-switch-handle-off bootstrap-switch-danger" style="width: 56px;">Rejected</span><input type="checkbox" class="grid-switch-status" checked="" data-key="2"></div></div>
                    </td>
                                    </tr>
                            </tbody>

            

        </table>

    </div>
</div>
        
    </section>
        </div>
       <script>
    function notAvailable(day_id,slot_id){
        
        $.ajax({
                headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
               type: "POST",
               url: '/admin/slotNotAvailable',
               data: {day_id:day_id,slot_id:slot_id},
               success: function( msg ) {
                     //alert(msg);
                     $("#slot"+slot_id+'_'+day_id).removeClass("btn-success");  
                     $("#slot"+slot_id+'_'+day_id).addClass("btn-danger");  
                     $('#slotText'+slot_id+'_'+day_id).html('Not')
                    //toastr.success('Duration Updated Successfully')
               }
           });
    }

    function saveDuration(){
        var duration_hour  = $('#duration_hour').val();
        var duration_minute  = $('#duration_minute').val();
        var duration = duration_hour+':'+duration_minute;
        $.ajax({
                headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
               type: "POST",
               url: '/admin/updateDuration',
               data: {duration:duration},
               success: function( msg ) {
                    toastr.success('Duration Updated Successfully')
               }
           });  
    }
    function saveSlots(day){
        
        var day_id  = $('#day_id'+day).val();
        var opening_hour  = $('#opening_hour'+day).val();
        var closing_hour  = $('#closing_hour'+day).val();
        
        
        $.ajax({
                headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
               type: "POST",
               url: '/admin/updateslots',
               data: {day_id:day_id,opening_hour:opening_hour,closing_hour:closing_hour},
               success: function( msg ) {
                    toastr.success('Time Updated Successfully') 
                    
               }
           });  
    }
    function showSlot(day){
        $('#showSlots'+day).show();
    }
</script>

    

    <!-- /.box-body -->
</div></div></div>
        
    </section>
        </div>
        <script data-exec-on-popstate>$(function () { $('.grid-row-checkbox').iCheck({checkboxClass:'icheckbox_minimal-blue'}).on('ifChanged', function () {

    var id = $(this).data('id');

    if (this.checked) {
        $.admin.grid.select(id);
        $(this).closest('tr').css('background-color', '#ffffd5');
    } else {
        $.admin.grid.unselect(id);
        $(this).closest('tr').css('background-color', '');
    }
}).on('ifClicked', function () {

    var id = $(this).data('id');

    if (this.checked) {
        $.admin.grid.unselect(id);
    } else {
        $.admin.grid.select(id);
    }

    var selected = $.admin.grid.selected().length;

    if (selected > 0) {
        $('.grid-select-all-btn').show();
    } else {
        $('.grid-select-all-btn').hide();
    }

    $('.grid-select-all-btn .selected').html("{n} items selected".replace('{n}', selected));
});
  ;(function () {
    $('.grid-switch-status').bootstrapSwitch({
        size:'mini',
        onText: 'Active',
        offText: 'Inactive',
        onColor: 'success',
        offColor: 'danger',
        onSwitchChange: function(event, state){

            $(this).val(state ? 1 : 0);

            var key = $(this).data('key');
            var value = $(this).val();
            var _status = true;

            $.ajax({
                url: "/admin/inventories/" + key,
                type: "POST",
                async:false,
                data: {
                    "status": value,
                    _token: LA.token,
                    _method: 'PUT'
                },
                success: function (data) {
                    if (data.status)
                        toastr.success(data.message);
                    else
                        toastr.warning(data.message);
                },
                complete:function(xhr,status) {
                    if (status == 'success')
                        _status = xhr.responseJSON.status;
                }
            });

            return _status;
        }
    });
})();  var actionResolver = function (data) {

            var response = data[0];
            var target   = data[1];

            if (typeof response !== 'object') {
                return $.admin.swal({type: 'error', title: 'Oops!'});
            }

            var then = function (then) {
                if (then.action == 'refresh') {
                    $.admin.reload();
                }

                if (then.action == 'download') {
                    window.open(then.value, '_blank');
                }

                if (then.action == 'redirect') {
                    $.admin.redirect(then.value);
                }

                if (then.action == 'location') {
                    window.location = then.value;
                }

                if (then.action == 'open') {
                    window.open(then.value, '_blank');
                }
            };

            if (typeof response.html === 'string') {
                target.html(response.html);
            }

            if (typeof response.swal === 'object') {
                $.admin.swal(response.swal);
            }

            if (typeof response.toastr === 'object' && response.toastr.type) {
                $.admin.toastr[response.toastr.type](response.toastr.content, '', response.toastr.options);
            }

            if (response.then) {
              then(response.then);
            }
        };

        var actionCatcher = function (request) {
            if (request && typeof request.responseJSON === 'object') {
                $.admin.toastr.error(request.responseJSON.message, '', {positionClass:"toast-bottom-center", timeOut: 10000}).css("width","500px")
            }
        };  
(function ($) {
    $('.grid-row-action-60be0743bdc964436').off('click').on('click', function() {
        var data = $(this).data();
        var target = $(this);
        Object.assign(data, {"_model":"App_Models_Inventory"});
        
                var process = $.admin.swal({
            "type": "question",
    "showCancelButton": true,
    "showLoaderOnConfirm": true,
    "confirmButtonText": "Submit",
    "cancelButtonText": "Cancel",
    "title": "Are you sure to delete this item ?",
    "text": "",
    "confirmButtonColor": "#d33",
            preConfirm: function(input) {
                return new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: 'Encore_Admin_Grid_Actions_Delete',
                        _input: input,
                    });

                    $.ajax({
                        method: 'POST',
                        url: '/admin/_handle_action_',
                        data: data,
                        success: function (data) {
                            resolve(data);
                        },
                        error:function(request){
                            reject(request);
                        }
                    });
                });
            }
        }).then(function(result) {
            if (typeof result.dismiss !== 'undefined') {
                return Promise.reject();
            }
            
            if (typeof result.status === "boolean") {
                var response = result;
            } else {
                var response = result.value;
            }

            return [response, target];
        });
        process.then(actionResolver).catch(actionCatcher);
    });
})(jQuery);
  ;(function () {
    $('.table-responsive').on('shown.bs.dropdown', function(e) {
        var t = $(this),
            m = $(e.target).find('.dropdown-menu'),
            tb = t.offset().top + t.height(),
            mb = m.offset().top + m.outerHeight(true),
            d = 20;
        if (t[0].scrollWidth > t.innerWidth()) {
            if (mb + d > tb) {
                t.css('padding-bottom', ((mb + d) - tb));
            }
        } else {
            t.css('overflow', 'visible');
        }
    }).on('hidden.bs.dropdown', function() {
        $(this).css({
            'padding-bottom': '',
            'overflow': ''
        });
    });
})();  
(function ($) {
    $('.grid-row-action-60be0743be51b1326').off('click').on('click', function() {
        var data = $(this).data();
        var target = $(this);
        Object.assign(data, {"_model":"App_Models_Inventory"});
        
                var process = $.admin.swal({
            "type": "question",
    "showCancelButton": true,
    "showLoaderOnConfirm": true,
    "confirmButtonText": "Submit",
    "cancelButtonText": "Cancel",
    "title": "Are you sure to delete this item ?",
    "text": "",
    "confirmButtonColor": "#d33",
            preConfirm: function(input) {
                return new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: 'Encore_Admin_Grid_Actions_Delete',
                        _input: input,
                    });

                    $.ajax({
                        method: 'POST',
                        url: '/admin/_handle_action_',
                        data: data,
                        success: function (data) {
                            resolve(data);
                        },
                        error:function(request){
                            reject(request);
                        }
                    });
                });
            }
        }).then(function(result) {
            if (typeof result.dismiss !== 'undefined') {
                return Promise.reject();
            }
            
            if (typeof result.status === "boolean") {
                var response = result;
            } else {
                var response = result.value;
            }

            return [response, target];
        });
        process.then(actionResolver).catch(actionCatcher);
    });
})(jQuery);
  
(function ($) {
    $('.grid-row-action-60be0743beb263728').off('click').on('click', function() {
        var data = $(this).data();
        var target = $(this);
        Object.assign(data, {"_model":"App_Models_Inventory"});
        
                var process = $.admin.swal({
            "type": "question",
    "showCancelButton": true,
    "showLoaderOnConfirm": true,
    "confirmButtonText": "Submit",
    "cancelButtonText": "Cancel",
    "title": "Are you sure to delete this item ?",
    "text": "",
    "confirmButtonColor": "#d33",
            preConfirm: function(input) {
                return new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: 'Encore_Admin_Grid_Actions_Delete',
                        _input: input,
                    });

                    $.ajax({
                        method: 'POST',
                        url: '/admin/_handle_action_',
                        data: data,
                        success: function (data) {
                            resolve(data);
                        },
                        error:function(request){
                            reject(request);
                        }
                    });
                });
            }
        }).then(function(result) {
            if (typeof result.dismiss !== 'undefined') {
                return Promise.reject();
            }
            
            if (typeof result.status === "boolean") {
                var response = result;
            } else {
                var response = result.value;
            }

            return [response, target];
        });
        process.then(actionResolver).catch(actionCatcher);
    });
})(jQuery);
  
(function ($) {
    $('.grid-row-action-60be0743bef4f3263').off('click').on('click', function() {
        var data = $(this).data();
        var target = $(this);
        Object.assign(data, {"_model":"App_Models_Inventory"});
        
                var process = $.admin.swal({
            "type": "question",
    "showCancelButton": true,
    "showLoaderOnConfirm": true,
    "confirmButtonText": "Submit",
    "cancelButtonText": "Cancel",
    "title": "Are you sure to delete this item ?",
    "text": "",
    "confirmButtonColor": "#d33",
            preConfirm: function(input) {
                return new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: 'Encore_Admin_Grid_Actions_Delete',
                        _input: input,
                    });

                    $.ajax({
                        method: 'POST',
                        url: '/admin/_handle_action_',
                        data: data,
                        success: function (data) {
                            resolve(data);
                        },
                        error:function(request){
                            reject(request);
                        }
                    });
                });
            }
        }).then(function(result) {
            if (typeof result.dismiss !== 'undefined') {
                return Promise.reject();
            }
            
            if (typeof result.status === "boolean") {
                var response = result;
            } else {
                var response = result.value;
            }

            return [response, target];
        });
        process.then(actionResolver).catch(actionCatcher);
    });
})(jQuery);
  
(function ($) {
    $('.grid-row-action-60be0743bf34e5456').off('click').on('click', function() {
        var data = $(this).data();
        var target = $(this);
        Object.assign(data, {"_model":"App_Models_Inventory"});
        
                var process = $.admin.swal({
            "type": "question",
    "showCancelButton": true,
    "showLoaderOnConfirm": true,
    "confirmButtonText": "Submit",
    "cancelButtonText": "Cancel",
    "title": "Are you sure to delete this item ?",
    "text": "",
    "confirmButtonColor": "#d33",
            preConfirm: function(input) {
                return new Promise(function(resolve, reject) {
                    Object.assign(data, {
                        _token: $.admin.token,
                        _action: 'Encore_Admin_Grid_Actions_Delete',
                        _input: input,
                    });

                    $.ajax({
                        method: 'POST',
                        url: '/admin/_handle_action_',
                        data: data,
                        success: function (data) {
                            resolve(data);
                        },
                        error:function(request){
                            reject(request);
                        }
                    });
                });
            }
        }).then(function(result) {
            if (typeof result.dismiss !== 'undefined') {
                return Promise.reject();
            }
            
            if (typeof result.status === "boolean") {
                var response = result;
            } else {
                var response = result.value;
            }

            return [response, target];
        });
        process.then(actionResolver).catch(actionCatcher);
    });
})(jQuery);
  ;(function () {
$('.column-select-submit').on('click', function () {

    var defaults = ["name","status","created_at"];
    var selected = [];

    $('.column-select-item:checked').each(function () {
        selected.push($(this).val());
    });

    if (selected.length == 0) {
        return;
    }

    var url = new URL(location);

    if (selected.sort().toString() == defaults.sort().toString()) {
        url.searchParams.delete('_columns_');
    } else {
        url.searchParams.set('_columns_', selected.join());
    }

    $.pjax({container:'#pjax-container', url: url.toString()});
});

$('.column-select-all').on('click', function () {
    $('.column-select-item').iCheck('check');
    return false;
});

$('.column-select-item').iCheck({
    checkboxClass:'icheckbox_minimal-blue'
});
})();  
$('.export-selected').click(function (e) {
    e.preventDefault();
    
    var rows = $.admin.grid.selected().join();

    if (!rows) {
        return false;
    }
    
    var href = $(this).attr('href').replace('__rows__', rows);
    location.href = href;
});
  
$('.grid-batch-0').on('click', function() {

    swal({
        title: "Are you sure to delete this item ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirm",
        showLoaderOnConfirm: true,
        cancelButtonText: "Cancel",
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.ajax({
                    method: 'post',
                    url: '/admin/inventories/' + $.admin.grid.selected().join(),
                    data: {
                        _method:'delete',
                        _token:'FcC2Ainm7K04HkBKjDGxHZARmiMnxTpUd1teXYfM'
                    },
                    success: function (data) {
                        $.pjax.reload('#pjax-container');

                        resolve(data);
                    }
                });
            });
        }
    }).then(function(result) {
        var data = result.value;
        if (typeof data === 'object') {
            if (data.status) {
                swal(data.message, '', 'success');
            } else {
                swal(data.message, '', 'error');
            }
        }
    });
});
  ;(function () {
$('.grid-select-all').iCheck({checkboxClass:'icheckbox_minimal-blue'});

$('.grid-select-all').on('ifChanged', function(event) {
    if (this.checked) {
        $('.grid-row-checkbox').iCheck('check');
    } else {
        $('.grid-row-checkbox').iCheck('uncheck');
    }
}).on('ifClicked', function () {
    if (this.checked) {
        $.admin.grid.selects = {};
    } else {
        $('.grid-row-checkbox').each(function () {
            var id = $(this).data('id');
            $.admin.grid.select(id);
        });
    }

    var selected = $.admin.grid.selected().length;

    if (selected > 0) {
        $('.grid-select-all-btn').show();
    } else {
        $('.grid-select-all-btn').hide();
    }

    $('.grid-select-all-btn .selected')
        .html("{n} items selected".replace('{n}', selected));
});
})();  ;(function () {
var $btn = $('.60be0743c0cd8-filter-btn');
var $filter = $('#filter-box');

$btn.unbind('click').click(function (e) {
    if ($filter.is(':visible')) {
        $filter.addClass('hide');
    } else {
        $filter.removeClass('hide');
    }
});
})();  
$('.grid-per-pager').on("change", function(e) {
    $.pjax({url: this.value, container: '#pjax-container'});
});
  ;(function () {
    $('.container-refresh').off('click').on('click', function() {
        $.admin.reload();
        $.admin.toastr.success('Refresh succeeded !', '', {positionClass:"toast-top-center"});
    });
})(); });</script>

        
    </div>

    <!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
                    <strong>Env</strong>&nbsp;&nbsp; local
        
        &nbsp;&nbsp;&nbsp;&nbsp;

                <strong>Version</strong>&nbsp;&nbsp; 1.8.11
        
    </div>
    <!-- Default to the left -->
    <strong>Powered by <a href="https://github.com/z-song/laravel-admin" target="_blank">laravel-admin</a></strong>
</footer>
</div>

<button id="totop" title="Go to top" style="display: none;"><i class="fa fa-chevron-up"></i></button>

<script>
    function LA() {}
    LA.token = "FcC2Ainm7K04HkBKjDGxHZARmiMnxTpUd1teXYfM";
    LA.user = {"id":1,"username":"admin","name":"Administrator","avatar":"http:\/\/localhost:8000\/vendor\/laravel-admin\/AdminLTE\/dist\/img\/user2-160x160.jpg"};
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/dist/js/app.min.js"></script>
<script src="/vendor/laravel-admin/jquery-pjax/jquery.pjax.js"></script>
<script src="/vendor/laravel-admin/nprogress/nprogress.js"></script>
<script src="/vendor/laravel-admin/nestable/jquery.nestable.js"></script>
<script src="/vendor/laravel-admin/toastr/build/toastr.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="/vendor/laravel-admin/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="/vendor/laravel-admin/laravel-admin/laravel-admin.js"></script>
<script src="/laravel-admin.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
<script src="/vendor/laravel-admin/moment/min/moment-with-locales.min.js"></script>
<script src="/vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.jsdelivr.net/npm/shortcut-buttons-flatpickr@0.1.0/dist/shortcut-buttons-flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr@4.6.6/dist/l10n/zh.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/fileinput.min.js?v=4.5.2"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/select2/select2.full.min.js"></script>
<script src="/vendor/laravel-admin/number-input/bootstrap-number-input.js"></script>
<script src="/vendor/laravel-admin/AdminLTE/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="/vendor/laravel-admin/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js"></script>
<script src="/vendor/laravel-admin/bootstrap-fileinput/js/plugins/sortable.min.js?v=4.5.2"></script>
<script src="/vendor/laravel-admin/bootstrap-duallistbox/dist/jquery.bootstrap-duallistbox.min.js"></script>
<script src="/packages/ckeditor/ckeditor.js"></script>
<script src="/packages/ckeditor/adapters/jquery.js"></script>


</body>
</html>
