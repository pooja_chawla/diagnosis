
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<div class="row">
	<div class="col-md-12"></div>
	<div class="col-md-12"></div>
	<div class="col-md-12"></div>
 <div class="col-md-12">
 	<div class="col-md-4"></div>
 	
    <div class="col-md-8">	
	  <label> Select Agent  </label>
 		<span>
        <select id="agent_id" name="duration_minute" onchange ="getAgent();">
	       <option>Select agent</option>
	       <?php foreach($agents as $agent){?>
	       <option value="<?php echo $agent->id; ?>"><?php echo $agent->name; ?></option>
	       
	   <?php } ?>
        </select>
      </span>
 	</div>

	
 </div>	
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
 <div class="col-md-12"></div>
<div id="appent-slot"></div>
<script>
	function getAgent(id){
		var id = $('#agent_id').val();
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/agentSlots',
	           data: {id:id},
	           success: function( msg ) {
	           		 //alert(msg);
	           	$('#appent-slot').append(msg)	 
	            	//toastr.success('Duration Updated Successfully')
	           }
	       });

	}
	function notAvailable(day_id,slot_id){
		
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/agentSlotNotAvailable',
	           data: {day_id:day_id,slot_id:slot_id},
	           success: function( msg ) {
	           		 //alert(msg);
	           		 $("#slot"+slot_id+'_'+day_id).removeClass("btn-success");  
                     $("#slot"+slot_id+'_'+day_id).addClass("btn-danger");	
                     $('#slotText'+slot_id+'_'+day_id).html('Not')
	            	//toastr.success('Duration Updated Successfully')
	           }
	       });
	}


	function saveSlots(day){
		
		var day_id  = $('#day_id'+day).val();
		var opening_hour  = $('#opening_hour'+day).val();
		var closing_hour  = $('#closing_hour'+day).val();
		var agent_id = $('#agent_id').val();
		
		$.ajax({
			 	headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	          },
	           type: "POST",
	           url: '/admin/updateAgentSlot',
	          data: {day_id:day_id,opening_hour:opening_hour,closing_hour:closing_hour,agent_id:agent_id},
	           success: function( msg ) {
	           		toastr.success('Time Updated Successfully')	
	            	
	           }
	       });	
	}
	function showSlot(day){
		$('#showSlots'+day).show();
	}
</script>