export default ({ store, redirect }) => {
	
	let user  = store.getters['auth/user'];
	if(user.login_type == 3){
		 return redirect('/')
	}
  if (!store.getters['auth/check']) {
    return redirect('/')
  }
}