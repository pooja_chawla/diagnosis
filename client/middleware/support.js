export default ({ store, redirect }) => {
	
	let user  = store.getters['auth/user'];
	if(user.login_type == '2' ||   user.login_type == '3' ||  user.login_type == '4'){
		 return redirect('/')
	}
  if (!store.getters['auth/check']) {
    return redirect('/support/login')
  }
}
