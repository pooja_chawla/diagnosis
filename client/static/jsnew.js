$(document).ready(function(){$(".customer-logos").slick({slidesToShow:2,slidesToScroll:1,autoplay:!0,autoplaySpeed:2000,arrows:!1,dots:!1,pauseOnHover:!1,responsive:[{breakpoint:768,settings:{slidesToShow:1}},{breakpoint:520,settings:{slidesToShow:1}}]})}),

$(document).ready(function(){$(".partner-logos").slick({slidesToShow:3,slidesToScroll:1,autoplay:true,autoplaySpeed:2000,arrows:false,dots:false,pauseOnHover:!1,responsive:[{breakpoint:769,settings:{slidesToShow:2}},{breakpoint:520,settings:{slidesToShow:1}}]})}),

$(document).ready(function(){$(".client-sec").slick({slidesToShow:3,slidesToScroll:1,autoplay:!0,autoplaySpeed:1500,arrows:!1,dots:!0,pauseOnHover:!1,responsive:[{breakpoint:768,settings:{slidesToShow:3}},{breakpoint:520,settings:{slidesToShow:1}}]})}),

$(window).scroll(function(){$(window).scrollTop()>=330?$(".sticky-header").addClass("fixed"):$(".sticky-header").removeClass("fixed")});