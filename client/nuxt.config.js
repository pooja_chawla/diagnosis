require('dotenv').config()
const { join } = require('path')
const { copySync, removeSync } = require('fs-extra')

module.exports = {
  ssr: false,

  srcDir: __dirname,

  env: {
    apiUrl: process.env.API_URL || process.env.APP_URL + '/api',
    appName: process.env.APP_NAME || 'Laravel Nuxt',
    appLocale: process.env.APP_LOCALE || 'en',
    githubAuth: !!process.env.GITHUB_CLIENT_ID
  },

  head: {
    title: process.env.APP_NAME,
  //  titleTemplate: 'Diagnosis at the door - ' + process.env.APP_NAME,
    titleTemplate: 'uDaud - ' + 'Diagnosis at the door',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'google-site-verification', content: 'IWWVp7EH0tgUMB9CzAxwa7aIWjd4oxOM0gHcXVROpCw' },
      { name: 'msvalidate.01', content: 'C97CB5A123A9A1B43250D1417A5F1B81' },
      { hid: 'description', name: 'Online Blood Sample Collection from Home: uDAUD', content: 'Looking for the Best blood testing lab near me? Avail our fast Blood test at home service, while sitting at your home. Best Preventive health packages available' }
    ],
    script: [
       // { src: 'js/jquery.min.js' },
       // { src: '~assets/js/bootstrap.min.js' },
       // { src: '~assets/js/jsnew.js' },
       // { src: '~assets/js/gtm.js' },
       { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'},
       { src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA_6IcvK8dOvWRfHDybcjFRr0R6CjGVMUY&libraries=places'},
       { src: 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js'}
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=K2D:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap'},
    ]
  },

  loading: { color: '#3ea8d8',height: '10px' },

  router: {
    middleware: ['locale', 'check-auth']
  },

  css: [

      { src: '~assets/sass/app.scss', lang: 'scss'},
      { src: '~assets/css/style.css'},
      { src: '~assets/css/dashboard.css'},
      { src: '~assets/css/demo.css'},
      { src: '~assets/css/bootstrap.min.css'},
      // src: '~assets/css/bootstrap.css',

     
  ],

  plugins: [
    '~components/global',
    '~plugins/i18n',
    '~plugins/vform',
    '~plugins/axios',
    // '~plugins/fontawesome',
    '~plugins/nuxt-client-init',
    { src: '~plugins/bootstrap', mode: 'client' },
    {src: '~/plugins/splide.client.js', ssr:false}
  ],

  modules: [
    '@nuxtjs/router'
  ],

  build: {
    extractCSS: true
  },

  hooks: {
    generate: {
      done (generator) {
        // Copy dist files to public/_nuxt
        if (generator.nuxt.options.dev === false && generator.nuxt.options.mode === 'spa') {
          const publicDir = join(generator.nuxt.options.rootDir, 'public', '_nuxt')
          removeSync(publicDir)
          copySync(join(generator.nuxt.options.generate.dir, '_nuxt'), publicDir)
          copySync(join(generator.nuxt.options.generate.dir, '200.html'), join(publicDir, 'index.html'))
          removeSync(generator.nuxt.options.generate.dir)
        }
      }
    }
  }
}
