import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'

Vue.use(Router)

const page = path => () => import(`~/pages/${path}`).then(m => m.default || m)

const routes = [
  // { path: '/:location', name: 'index', component: page('index.vue') },
  { path: '/', name: 'index', component: page('index.vue') },
  { path: '/signup', name: 'signup', component: page('signup.vue') },
  { path: '/about-us', name: 'about-us', component: page('static/about.vue') },
  { path: '/city/diagnostic-centre-mumbai', name: 'mumbai', component: page('static/mumbai.vue') },
  { path: '/city/diagnostic-centre-thane', name: 'thane', component: page('static/thane.vue') },
  { path: '/city/diagnostic-centre-navi-mumbai', name: 'thane', component: page('static/navi_mumbai.vue') },
  { path: '/kare-package', name: 'kare-package', component: page('static/kare.vue') },
  { path: '/body-test/:slug', name: 'body-test', component: page('static/body_test.vue') },
  { path: '/healthy-tip', name: 'healthy-tip', component: page('static/healthy.vue') },
  { path: '/services', name: 'services', component: page('static/partner.vue') },
  { path: '/faq', name: 'faq', component: page('static/faq.vue') },
  { path: '/contact', name: 'contact', component: page('static/contact.vue') },
  { path: '/payment', name: 'payment', component: page('static/payment.vue') },
  { path: '/lab_test', name: 'lab_test', component: page('static/lab_test.vue') },
  { path: '/kare-package-details', name: 'kare-package-details', component: page('static/kare_details.vue') },
  { path: '/lab/:slug', name: 'lab', component: page('static/lab_detail.vue') },
  { path: '/healthytip/:slug', name: 'healthytip', component: page('static/healthy_details.vue') },
  { path: '/support/login', name: 'support.login', component: page('support/login.vue') },
  { path: '/support/dashboard', name: 'support.dashboard', component: page('support/dashboard.vue') },
  { path: '/support/booking_request', name: 'support.booking_request', component: page('support/bookings/booking_request.vue') },
  { path: '/support/consultation', name: 'support.consultation', component: page('support/consultation.vue') },

  { path: '/support/booking/create', name: 'support.bookings.create', component: page('support/bookings/create.vue') },
  { path: '/support/bookings', name: 'support.bookings', component: page('support/bookings/index.vue') },

 { path: '/support/profile', name: 'support.profile', component: page('support/profile.vue') },
  { path: '/tests/:slug', name: 'tests', component: page('static/tests.vue') },
  { path: '/product/checkout', name: 'product/checkout', component: page('static/checkout.vue') },
  { path: '/product_detail/:slug/:name', name: 'product_detail', component: page('static/product_detail.vue') },
  { path: '/labs', name: 'labs', component: page('static/labs.vue') },
  { path: '/custom-test-labs', name: 'custom-test-labs', component: page('static/custom_labs.vue') },
  { path: '/custom-package-labs/:slug', name: 'custom-package-labs', component: page('static/custom_labs.vue') },
  { path: '/career', name: 'career', component: page('static/career.vue') },
  { path: '/payment-sucess', name: 'payment-sucess', component: page('static/payment_sucess.vue') },
  { path: '/kare-package', name: 'kare-package', component: page('static/kare.vue') },
  { path: '/diagnostic-centre-mumbai', name: 'mumbai', component: page('static/mumbai.vue') },
  { path: '/diagnostic-centre-thane', name: 'thane', component: page('static/thane.vue') },
  { path: '/diagnostic-centre-navi-mumbai', name: 'thane', component: page('static/navi_mumbai.vue') },
  { path: '/body-test/:slug', name: 'body-test', component: page('static/body_test.vue') },
  { path: '/healthy-tip', name: 'healthy-tip', component: page('static/healthy.vue') },
  { path: '/services', name: 'services', component: page('static/partner.vue') },
  { path: '/faq', name: 'faq', component: page('static/faq.vue') },
  { path: '/contact', name: 'contact', component: page('static/contact.vue') },
  { path: '/payment', name: 'payment', component: page('static/payment.vue') },
  { path: '/lab_test', name: 'lab_test', component: page('static/lab_test.vue') },
  { path: '/kare-package-details', name: 'kare-package-details', component: page('static/kare_details.vue') },
  { path: '/lab/:slug', name: 'lab', component: page('static/lab_detail.vue') },
  { path: '/healthytip/:slug', name: 'healthytip', component: page('static/healthy_details.vue') },
  { path: '/tests/:slug', name: 'tests', component: page('static/tests.vue') },
  { path: '/product/checkout', name: 'product/checkout', component: page('static/checkout.vue') },
  { path: '/product_detail/:slug/:name', name: 'product_detail', component: page('static/product_detail.vue') },
  { path: '/labs', name: 'labs', component: page('static/labs.vue') },
  { path: '/custom-test-labs', name: 'custom-test-labs', component: page('static/custom_labs.vue') },
  { path: '/custom-package-labs/:slug', name: 'custom-package-labs', component: page('static/custom_labs.vue') },
  { path: '/career', name: 'career', component: page('static/career.vue') },
  { path: '/payment-sucess', name: 'payment-sucess', component: page('static/payment_sucess.vue') },
  { path: '/career_inner', name: 'career_inner', component: page('static/career_inner.vue') },
  { path: '/cart/order-summery', name: 'cart/order-summery', component: page('static/cart.vue') },
  { path: '/package/:slug', name: 'package', component: page('static/package_detail.vue') },

  { path: '/custom-package/:slug', name: 'custom-package', component: page('static/custom_package_detail.vue') },
  { path: '/package/:slug/:labSlug', name: 'lab/package', component: page('static/package_detail.vue') },
  { path: '/kare_packages/:slug/:pid', name: 'kare-packages', component: page('static/kare_package_detail.vue') },
  { path: '/lab_packages/:slug', name: 'lab-packages', component: page('static/lab_packages.vue') },
  { path: '/package-lab/:slug', name: 'package-lab', component: page('static/package_labs.vue') },
  { path: '/kare-package-lab/:slug/:pid', name: 'kare-package-lab', component: page('static/kare_package_labs.vue') },
  { path: '/booking-schedule', name: 'booking-schedule', component: page('static/booking_schedule.vue') },
  { path: '/booking-response', name: 'booking-response', component: page('static/booking_response.vue') },
  { path: '/teamlead/login', name: 'teamlead.login', component: page('teamlead/login.vue') },
  { path: '/teamlead/dashboard', name: 'teamlead.dashboard', component: page('team.vue') },
  { path: '/doctor-consultation', name: 'doctor-consultation', component: page('static/consultation.vue') },
  { path: '/vendor/login', name: 'vendor.login', component: page('vendor/login.vue') },
  { path: '/vendor/register', name: 'vendor.register', component: page('vendor/register.vue') },
  { path: '/vendor/profile', name: 'vendor.profile', component: page('vendor/profile.vue') },
  { path: '/vendor/customer-visit', name: 'vendor.customer-visit', component: page('vendor/bookings/customer_visit.vue') },
  { path: '/teamlead/profile', name: 'teamlead.profile', component: page('teamlead/profile.vue') },
  { path: '/user/profile', name: 'user-profile', component: page('static/profile.vue') },
  { path: '/user/login', name: 'user-login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },


  { path: '/vendor/dashboard', name: 'vendor.dashboard', component: page('home.vue') },
  { path: '/teamlead/dashboard', name: 'teamlead.dashboard', component: page('team.vue') },
  {
    path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') },
    ]
  },

  {
    path: '/vendor/products',
    component: page('vendor/products/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'vendor.products', component: page('vendor/products/index.vue') },

    ]
  },

  {
    path: '/teamlead/visit-request',
    component: page('teamlead/bookings/bookingRequest.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.visit.request', component: page('teamlead/bookings/bookingRequest.vue') },

    ]
  },

  {
    path: '/teamlead/unassignedBooking',
    component: page('teamlead/bookings/unassignedBooking.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.unassignedBooking', component: page('teamlead/bookings/unassignedBooking.vue') },

    ]
  },

  {
    path: '/teamlead/query',
    component: page('teamlead/query/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.query', component: page('teamlead/query/index.vue') },

    ]
  },

  {
    path: '/vendor/bookings',
    component: page('vendor/bookings/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'vendor.bookings', component: page('vendor/bookings/index.vue') },

    ]
  },

  {
    path: '/teamlead/agents',
    component: page('teamlead/agent/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.agents', component: page('teamlead/agent/index.vue') },

    ]
  },

  {
    path: '/teamlead/runners',
    component: page('teamlead/runner/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.runners', component: page('teamlead/runner/index.vue') },

    ]
  },


  {
    path: '/teamlead/profile',
    component: page('teamlead/profile.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.profile', component: page('teamlead/profile.vue') },

    ]
  },

  {
    path: '/teamlead/bookings',
    component: page('teamlead/bookings/allbooking.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.bookings', component: page('teamlead/bookings/allbooking.vue') },

    ]
  },

  {
    path: '/teamlead/currentbooking',
    component: page('teamlead/bookings/currentbooking.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'teamlead.currentbooking', component: page('teamlead/bookings/currentbooking.vue') },

    ]
  },

  {
    path: '/vendor/daud_bookings',
    component: page('vendor/bookings/daud_bookings.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'vendor.daud_bookings', component: page('vendor/bookings/daud_bookings.vue') },

    ]
  },
  { path: '/add_customer/:id', name: 'add_customer', component: page('vendor/bookings/add_customer.vue') },
  { path: '/booking_detail/:id', name: 'booking_detail', component: page('vendor/bookings/booking_detail.vue') },
  { path: '/support_booking_detail/:id', name: 'support_booking_detail', component: page('support/bookings/booking_detail.vue') },
  { path: '/product_detail/:id', name: 'admin_product_detail', component: page('vendor/products/product_detail.vue') },
  { path: '/product_edit/:id', name: 'my_product_edit', component: page('vendor/products/product_edit.vue') },
  { path: '/edit_booking/:id', name: 'edit_booking', component: page('vendor/bookings/edit_booking.vue') },
  { path: '/teamlead/booking/booking_detail/:id', name: 'tmbooking_detail', component: page('teamlead/bookings/booking_detail.vue') },

  { path: '/agent_detail/:id', name: 'agent_detail', component: page('teamlead/agent/agent_detail.vue') },

  { path: '/vendor/booking_request', name: 'vendor.booking_request', component: page('vendor/bookings/booking_request.vue') },
  { path: '/vendor/bookings/create', name: 'vendor.bookings.create', component: page('vendor/bookings/create.vue') },
  { path: '/vendor/bookings/charges', name: 'vendor.bookings.charges', component: page('vendor/bookings/charges.vue') },

  { path: '/vendor/products/create', name: 'vendor.products.create', component: page('vendor/products/create.vue') },
  { path: '/vendor/products/edit/:id', name: 'vendor.products.edit', component: page('vendor/products/edit.vue') },
  { path: '/vendor/products/my_product', name: 'vendor.products.my_product', component: page('vendor/products/my_product.vue') },
  { path: '/vendor/charges', name: 'vendor.charges', component: page('vendor/charges.vue') },

  {
    path: '/vendor/bookings',
    component: page('vendor/bookings/index.vue'),
    children: [

      // { path: '', redirect: { name: 'vendor.products' } },
      { path: '', name: 'vendor.bookings', component: page('vendor/bookings/index.vue') },

    ]
  }
]

export function createRouter() {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
