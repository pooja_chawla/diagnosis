<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('vendor_id');
            $table->string('product_id');
            $table->string('customer_id');
            $table->date('booking_date', 0)->nullable();
            $table->string('time_slot')->nullable();
            $table->string('refered_by_doc')->nullable();
            $table->longText('description')->nullable();
            $table->string('order')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
