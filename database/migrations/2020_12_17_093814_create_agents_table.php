<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->longText('qualification')->nullable();
            $table->longText('photo')->nullable();
            $table->string('identity_type')->nullable();
            $table->longText('identity')->nullable();
            $table->string('identity_verification')->nullable();
            $table->longText('driving_license_no')->nullable();
            $table->string('driving_license_expired_at')->nullable();
            $table->longText('driving_proof')->nullable();
            $table->string('driving_proof_verification')->nullable();
            $table->string('status')->nullable();
            $table->string('order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
