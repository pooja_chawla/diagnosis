<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->longText('address')->nullable()->after('password');
            $table->longText('landmark')->nullable()->after('address');
            $table->string('latitude')->nullable()->after('landmark');
            $table->string('longitude')->nullable()->after('latitude');
            $table->longText('image')->nullable()->after('longitude');
            $table->string('phone_code')->nullable()->after('image');
            $table->string('phone')->nullable()->after('phone_code');
            $table->string('otp')->nullable()->after('phone');
            $table->string('login_otp')->nullable()->after('otp');
            $table->string('device_type')->nullable()->after('login_otp');
            $table->string('device_token')->nullable()->after('device_type');
            $table->string('status')->nullable()->after('device_token');
            $table->string('order')->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_code');
            $table->dropColumn('phone');
        });
    }
}
