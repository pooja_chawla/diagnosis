<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRequestToCustomerBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_booking', function (Blueprint $table) {
            $table->string('assigned_agent')->nullable();
            $table->string('request_status')->default(0);
            $table->string('rejected_users')->nullable();
            $table->string('request_sent')->nullable();
            $table->string('vendor_id')->after('id')->nullable(); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_booking', function (Blueprint $table) {
            //
        });
    }
}
