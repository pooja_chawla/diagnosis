<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;

// use App\Http\Controllers\API\AuthController;
// use App\Http\Controllers\API\ForgotPasswordController;
// use App\Http\Controllers\API\CustomerController;
// use App\Http\Controllers\API\AgentController;
// use App\Http\Controllers\API\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);
    Route::get('profile', [AuthController::class, 'getAuthUser']);    
    Route::post('profile/update', [AuthController::class, 'updateProfile']);    
    Route::post('support/update', [SupportController::class, 'updateProfile']);    
    Route::get('support/labs', [SupportController::class, 'labs']);    
    Route::get('support/product/{id}', [SupportController::class, 'productListing']);    
    Route::get('support/getpackage/{id}', [SupportController::class, 'getpackage']);    
    Route::post('support/productDetailId', [SupportController::class, 'productDetailId']);    
    Route::post('support/addBooking', [SupportController::class, 'addBooking']);    
    Route::post('support/productDetailIds', [SupportController::class, 'productDetailIds']);    
    Route::post('refresh', [AuthController::class, 'refresh']);    
    
    Route::post('agent_locations', [RunnerController::class, 'agentLocations']);
    Route::post('scanner', [RunnerController::class, 'scanner']);
    Route::post('agent_scanner', [AgentController::class, 'agentScanner']);
    Route::post('deliver', [RunnerController::class, 'deliver']);
    
     Route::get('old_booking', [AgentController::class, 'oldBooking']);
     Route::post('agent_details', [RunnerController::class, 'agentDetails']);  
       Route::get('visit_listing', [RunnerController::class, 'visitListing']);
 Route::get('old_runner_booking', [RunnerController::class, 'oldRunnerBooking']);

       
   
    Route::post('visit_detail', [RunnerController::class, 'visitDetail']);
    Route::get('vendors', [VendorController::class, 'index']);
    Route::get('vendors/getUser', [VendorController::class, 'getUser']);
    Route::get('vendor/profile', [VendorController::class, 'getProfile']);
    Route::post('vendor/profile_update', [VendorController::class, 'updateProfile']);
    Route::get('vendor/fetchBooking', [VendorController::class, 'vendorTopBooking']);
    Route::get('support/fetchBooking', [SupportController::class, 'vendorTopBooking']);
    Route::get('vendor/bookingCharges', [VendorController::class, 'bookingCharges']);
    Route::get('vendor/fetchDaudBooking', [VendorController::class, 'vendorDaudBooking']);
    Route::get('vendor/getBookingCount', [VendorController::class, 'getBookingCount']);
    Route::get('support/getBookingCount', [SupportController::class, 'getBookingCount']);
    Route::get('support/consultationList', [SupportController::class, 'consultationList']);
    Route::get('teamlead/getTLBookingCount', [VendorController::class, 'getTLBookingCount']);
    Route::get('teamlead/getTLAgentBookingCount/{id}', [AgentController::class, 'getTLAgentBookingCount']);
    Route::get('vendor/getOnGoingBookings', [VendorController::class, 'getOnGoingBookings']);
    Route::get('support/getOnGoingBookings', [SupportController::class, 'getOnGoingBookings']);
    Route::get('vendor/products', [VendorController::class, 'vendorProducts']);
    Route::get('vendor/productListing', [VendorController::class, 'productListing']);
    Route::get('vendor/myProducts', [VendorController::class, 'myProducts']);
    Route::post('vendor/addDoctor', [VendorController::class, 'addDoctor']);
    Route::post('vendor/sendProductRequest', [VendorController::class, 'sendProductRequest']);
    Route::get('vendor/bookingRequestList', [VendorController::class, 'bookingRequestList']);
    Route::get('support/bookingRequestList', [SupportController::class, 'bookingRequestList']);
    Route::get('vendor/doctors', [VendorController::class, 'doctors']);
    Route::get('vendor/showdays', [VendorController::class, 'showdays']);
    Route::get('vendor/showAgents', [VendorController::class, 'showAgents']);
    Route::get('vendor/showdates', [VendorController::class, 'showDates']);
    Route::get('vendor/fetchSlots/{date}/{day}', [VendorController::class, 'fetchSlots']);
    Route::get('teamlead/fetchSlotsTL', [VendorController::class, 'fetchSlotsTL']);
    Route::get('teamlead/fetchTLAgent/{id}', [VendorController::class, 'fetchTLAgent']);
    Route::post('agentPaymentStatus', [AgentController::class, 'agentPaymentStatus']);
    Route::post('agentAddTest', [AgentController::class, 'agentAddTest']);
    Route::get('teamlead/bookingRequest', [VendorController::class, 'bookingRequest']);
    Route::post('teamlead/bookingRequestUpdate', [VendorController::class, 'bookingRequestUpdate']);

    Route::get('vendor/fetchBookingDetails/{id}', [VendorController::class, 'fetchBookingDetails']);
    Route::get('vendor/fetchBookingProduct/{id}', [VendorController::class, 'fetchBookingProduct']);
    Route::get('teamlead/fetchTLBookingDetails/{id}', [VendorController::class, 'fetchTLBookingDetails']);
    Route::get('teamlead/fetchRunnerDetails/{id}', [VendorController::class, 'fetchRunnerDetails']);
    Route::get('teamlead/query', [VendorController::class, 'bookingQuery']);
    Route::get('teamlead/queryCount', [VendorController::class, 'queryCount']);
    Route::post('teamlead/queryMark', [VendorController::class, 'queryMark']);
    
    
    Route::get('vendor/getQuery/{id}', [VendorController::class, 'getQuery']);
    Route::get('teamlead/fetchAgentDetails/{id}', [AgentController::class, 'fetchAgentDetails']);
    Route::post('teamlead/updateslot', [AgentController::class, 'updateAgentslot']);
    Route::post('teamlead/updateAgentLocation', [AgentController::class, 'updateAgentLocation']);
    Route::post('teamlead/getAvailSlots', [AgentController::class, 'getAvailSlots']);
    Route::post('teamlead/deleteAvailability', [AgentController::class, 'deleteAvailability']);
    Route::post('teamlead/addAgentSlot', [AgentController::class, 'addAgentSlot']);
    Route::post('teamlead/removeAgentSlot', [AgentController::class, 'removeAgentSlot']);

    Route::get('teamlead/fetchAgentSlot/{id}', [AgentController::class, 'fetchAgentSlot']);
    Route::get('teamlead/fetchBookingDetails/{id}', [AgentController::class, 'fetchBookingDetails']);
    Route::get('teamlead/fetchAgentLocation/{id}', [AgentController::class, 'fetchAgentLocation']);
    Route::get('vendor/fetchProductDetails/{id}', [VendorController::class, 'fetchProductDetails']);
    Route::get('vendor/getProductData/{id}', [VendorController::class, 'getProductData']);
   
    Route::post('vendor/addNewPatient', [VendorController::class, 'addNewPatient']);
    Route::post('vendor/getPpSlot', [VendorController::class, 'getPpSlot']);
    Route::post('vendor/addProduct', [VendorController::class, 'addProduct']);
    Route::post('vendor/updateProduct', [VendorController::class, 'updateProduct']);
    Route::post('vendor/showProduct', [VendorController::class, 'showProduct']);
    Route::post('vendor/deleteProduct', [VendorController::class, 'deleteProduct']);
    Route::get('vendor/booking', [VendorController::class, 'vendorBooking']);
    Route::post('vendor/addBooking', [VendorController::class, 'addBooking']);
    Route::post('vendor/UpdatePatientBooking', [VendorController::class, 'UpdatePatientBooking']);
    Route::post('vendor/UpdatePaymentBooking', [VendorController::class, 'UpdatePaymentBooking']);
    Route::post('vendor/addNewProductInBooking', [VendorController::class, 'addNewProductInBooking']);
    Route::get('vendor/getCharges', [VendorController::class, 'getCharges']);
    Route::post('vendor/removeProductInBooking', [VendorController::class, 'removeProductInBooking']);
    Route::post('vendor/changeBookingStatus', [VendorController::class, 'changeBookingStatus']);
    Route::post('vendor/getProducPriceList', [VendorController::class, 'getProducPriceList']);
    Route::post('vendor/updateStatus', [VendorController::class, 'updateStatus']);
    Route::post('vendor/sendQuery', [VendorController::class, 'sendQuery']);
    Route::post('teamlead/updatagents', [VendorController::class, 'updatagents']);
    Route::post('teamlead/updatSlot', [VendorController::class, 'updatSlot']);
    Route::post('vendor/uploadReport', [VendorController::class, 'uploadReport']);
    Route::post('vendor/productDetailId', [VendorController::class, 'productDetailId']);
    Route::post('vendor/getProductDetails', [VendorController::class, 'getProductDetails']);
    Route::post('vendor/getAddressDistance', [VendorController::class, 'getAddressDistance']);
    Route::get('vendor/bookingList', [VendorController::class, 'bookingList']);
    Route::get('support/bookingList', [SupportController::class, 'bookingList']);
    Route::get('vendor/chargeBookingList', [VendorController::class, 'chargeBookingList']);
    Route::get('vendor/getTubes', [VendorController::class, 'getTubes']);
    Route::get('teamlead/teambookingList', [VendorController::class, 'teambookingList']);
    Route::get('teamlead/unassignedbooking', [VendorController::class, 'unassignedbooking']);
     Route::get('teamlead/getUnassignedSlots/{id}', [VendorController::class, 'getUnassignedSlots']);
    
    Route::post('teamlead/getBookedSlot', [AgentController::class, 'getBookedSlot']);
    
    Route::post('teamlead/assignAgents', [VendorController::class, 'assignAgents']);
    Route::post('teamlead/assignUdaud', [VendorController::class, 'assignUdaud']);
    
    Route::get('teamlead/unassignedbookingUdaud', [VendorController::class, 'unassignedbookingUdaud']);
    Route::get('teamlead/teamtodaybookingList', [VendorController::class, 'teamtodaybookingList']);
    Route::get('teamlead/agentList', [AgentController::class, 'agentList']);
    Route::get('teamlead/runnerList', [AgentController::class, 'runnerList']);
    
    Route::get('teamlead/agentLocation', [AgentController::class, 'agentLocation']);
    Route::get('teamlead/runnerLocation', [AgentController::class, 'runnerLocation']);
    
    Route::get('vendor/daudBookingList', [VendorController::class, 'daudBookingList']);

    Route::post('agentBookingStatus', [AgentController::class, 'agentBookingStatus']); 
     Route::post('patient_details', [AuthController::class, 'UserDetails']); 
      Route::post('patient_details_test', [AuthController::class, 'UserDetailsTest']); 
       Route::post('bookingComment', [AuthController::class, 'bookingComment']); 
      
    Route::get('agent_pickup_list', [FriendshipController::class, 'getPickup']); 
    Route::post('agentLocationapp', [AgentController::class, 'agentLocationapp']); 
    
    Route::get('agent_profile', [AgentController::class, 'agentProfile']); 
    Route::post('agent_profile_update', [AgentController::class, 'agentProfileUpdate']); 
    Route::post('accept_request', [FriendshipController::class, 'requestAccept']); 
    Route::get('friendship_list', [FriendshipController::class, 'getFriendList']); 

    Route::post('notification_detail', [FriendshipController::class, 'notificationDetail']); 

    Route::get('products', [ProductController::class, 'index']);
    Route::post('product', [ProductController::class, 'show']);
    Route::get('customers', [CustomerController::class, 'index']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('login_api', [LoginController::class, 'login']);
    Route::post('login_phone', [AuthController::class, 'loginWithOtp']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('products', [ProductController::class, 'getProducts']);
    Route::get('load-products', [ProductController::class, 'fetchProducts']);
    Route::get('slot', [AuthController::class, 'slot']);
    //Route::get('labs', [LabController::class, 'index']);
    Route::get('lab_detail/{slug}', [LabController::class, 'labDetails']);
    // Route::post('resend_email_verif', [AuthController::class, 'resendVerificationEmail']);  
    // Route::post('send_otp', [AuthController::class, 'loginWithOtp']);  
    Route::post('otp_verification', [AuthController::class, 'otpVerification']);   
    Route::post('login_otp_verification', [AuthController::class, 'loginOtpVerification']); 
    Route::post('customer_otp_login', [CustomerController::class, 'customerOtpLogin']); 
    Route::post('match_login_otp', [CustomerController::class, 'loginOtpVerification']);
    Route::post('otp_resend', [AuthController::class, 'otpResend']); 


    Route::post('forget_password', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('upload_document', [AuthController::class, 'uploadDocumentslogin']);

 

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
    
});
     Route::get('getProductList', [HomeController::class, 'getProductList']);
      Route::get('getCouponCode', [HomeController::class, 'getCouponCode']);
     
     Route::get('getAllTestLab', [HomeController::class, 'getAllTestLab']);
     Route::get('bookingDates', [HomeController::class, 'bookingDates']);
     Route::get('fetchSlots/{date}/{day}/{time}', [HomeController::class, 'fetchSlots']);
     Route::get('labs', [HomeController::class, 'index']);
     Route::get('labsMob', [HomeController::class, 'indexMob']);
     Route::get('getReports', [HomeController::class, 'getReports']);
     
     Route::post('pickupFilter', [FriendshipController::class, 'pickupFilter']);
     Route::post('call_back', [HomeController::class, 'callBack']);
     Route::get('getProducts', [HomeController::class, 'getProducts']);
     Route::get('getKareProducts', [HomeController::class, 'getKareProducts']);

     Route::post('discountCoupon', [HomeController::class, 'discountCoupon']);
     Route::get('getTestimonial', [HomeController::class, 'getTestimonial']);
     Route::post('getVendorProduct', [AgentController::class, 'getVendorProduct']);
     Route::get('getBlogs', [HomeController::class, 'getBlogs']);
     Route::get('getInternalBlogs', [HomeController::class, 'getInternalBlogs']);
     Route::get('getAgentProduct', [AgentController::class, 'getAgentProduct']);
    
     Route::post('updateUserAgent', [AgentController::class, 'updateUserAgent']);
     Route::post('addCustomTest', [HomeController::class, 'addCustomTest']);
     Route::post('updatePatientDetails', [HomeController::class, 'updatePatientDetails']);
     Route::post('getCustomerLocation', [AgentController::class, 'getCustomerLocation']); 
     Route::get('getProductsmob', [HomeController::class, 'getProductsMob']);
     Route::get('getTest', [HomeController::class, 'getTests']);
     Route::get('showAgents', [HomeController::class, 'showAgents']);
     Route::get('getCustomCartTests/{slug}', [HomeController::class, 'getCustomCartTests']);
     Route::get('getCartPrices', [HomeController::class, 'getCartPrices']);
     Route::get('lab_detail/{slug}', [HomeController::class, 'labDetails']);
     Route::get('healthyTipDetails/{slug}', [HomeController::class, 'healthyTipDetails']);
     Route::get('package/{slug}', [HomeController::class, 'labDetails']);
     Route::get('getLabName/{lab}', [HomeController::class, 'getLabName']);
     Route::get('getLabProducts/{lab}', [HomeController::class, 'getLabProducts']);
     Route::get('labs_listing/{slug}', [HomeController::class, 'getpackageLabs']);
     Route::get('selected_lacation/{loc}', [HomeController::class, 'selectedLacation']);
     Route::get('getBodyTest/{id}', [HomeController::class, 'bodyTest']);
     Route::get('deleteCartPackage/{id}', [HomeController::class, 'deleteCartPackage']);
     Route::get('product_detail/{slug}/{lab}', [HomeController::class, 'productDetail']);
     Route::get('lab_product/{slug}/{lab}/{child}', [HomeController::class, 'labProductDetail']);
     Route::get('replace_product/{slug}/{lab}/{child}', [HomeController::class, 'replaceProductDetail']);
     Route::get('replace_lab_product/{slug}/{lab}/{child}', [HomeController::class, 'replaceLabProductDetail']);
     Route::get('refund/{order}/{price}', [HomeController::class, 'refund']);
     Route::post('creatSessionData', [HomeController::class, 'creatSessionData']);
     Route::post('creatCartSessionData', [HomeController::class, 'creatCartSessionData']);
     Route::post('check_selected_lab', [HomeController::class, 'getSelectedLab']);
     Route::get('add_cartsession/{slug}/{pack}/{child}', [HomeController::class, 'addCartSession']);
     Route::get('profile_test/{slug}', [HomeController::class, 'profileTest']);
     Route::get('userRoles/{slug}', [AgentController::class, 'userRole']);
     Route::get('loginuserRoles/{slug}', [AgentController::class, 'loginuserRoles']);
     
     Route::get('site_info', [HomeController::class, 'siteInfo']);
     Route::get('consultation', [HomeController::class, 'consultation']);
     Route::get('popular_labs', [HomeController::class, 'popularLabs']);
     Route::get('userPackages', [HomeController::class, 'getUserPackages']);
     Route::get('getCartCount', [HomeController::class, 'getCartCount']);
     
     Route::get('getPartners', [HomeController::class, 'getPartners']);
     Route::get('getCounts', [HomeController::class, 'getCounts']);
     Route::get('getUserMembers', [HomeController::class, 'getUserMembers']);
     Route::get('getBodyPart/{type}', [HomeController::class, 'getBodyPart']);
     Route::post('submitPrescription', [HomeController::class, 'submitPrescription']);
     Route::post('getDistance', [HomeController::class, 'getDistance']);
     Route::post('createBooking', [HomeController::class, 'createBooking']);
     Route::post('labsearch', [HomeController::class, 'labsearch']);
     Route::post('submitContactUs', [HomeController::class, 'submitContactUs']);
     Route::post('addPatient', [CustomerController::class, 'addPatient']);
     Route::post('add_child_patient', [AgentController::class, 'addChildPatient']);
     
     Route::post('createChildPatient', [HomeController::class, 'createChildPatient']);
     Route::post('updateChildPatient', [HomeController::class, 'updateChildPatient']);
     Route::post('sendPatientOtp', [HomeController::class, 'sendPatientOtp']);
     Route::post('partners', [HomeController::class, 'partners']);
     
     Route::get('getUserDetails/{id}', [HomeController::class, 'getUserDetails']);
     Route::get('getMemeberUserDetails/{id}', [HomeController::class, 'getMemeberUserDetails']);
     Route::get('getOrderDetails', [HomeController::class, 'getOrderDetails']);
     Route::get('getProfileOrderDetails', [HomeController::class, 'getProfileOrderDetails']);
     
     Route::get('payment/{pay}', [HomeController::class, 'payment']);
     Route::get('consultpayment/{pay}/{user}', [CustomerController::class, 'consultpayment']);
     Route::post('agent_payment', [AgentController::class, 'payment']);
     Route::get('submit_payment/{url}', [AgentController::class, 'submitPayment']);
     Route::post('agent_response', [AgentController::class, 'response']);
     Route::post('response', [HomeController::class, 'response']);
     Route::post('consultresponse', [CustomerController::class, 'consultresponse']);
       Route::post('getVenderProducts', [AgentController::class, 'getVenderProducts']);
     Route::post('venderAddress',[AgentController::class, 'venderAddress']);
     Route::post('updateCart', [HomeController::class, 'updateCart']);

     Route::get('test/email', [HomeController::class, 'updtestEmail']);

//Agent

     Route::post('agentRegister', [AgentAuthController::class, 'agentRegister']);
     Route::post('agentLogin', [AgentAuthController::class, 'agentLogin']);
    Route::post('send_doc_consultation', [CustomerController::class, 'sendDocConsultation']);
