<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\FcmNotification;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\User;
use App\Models\Customer;
use App\Models\AgentSlots;
use App\Models\CustomerBookingProduct;

class ReminderRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'diagnos:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel Booking after 150 secs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
        public function handle()
        {     
            $bookings = Booking::where('reminder', 0)->whereNotNull('assigned_agent')->whereDate('booking_date','=',\Carbon\Carbon::now())->get();
            foreach ($bookings as $key => $booking) { 
                $slot = AgentSlots::find($booking['time_slot']);
                $slotChange = $slot->slot;
                $exp = explode('-', $slotChange);
                $string = substr($exp[0], 0, -3);
                $timeFormat  =date("H:i", strtotime($exp[0]));
                $diffInSeconds = Carbon::parse($booking->booking_date.' '.$timeFormat.':00')->diffInSeconds(now());
                if($diffInSeconds <= 1800) {
                   $user = User::find($booking['user_id']);
                   $vendor = User::find($booking['vendor_id']);
                   $agent = User::find($booking['assigned_agent']);
                   $customerBookingProduct = CustomerBookingProduct::where('booking_id',$booking['id'])->get();
                    foreach ($customerBookingProduct as $key => $value) {
                       $product[] = $value->title;
                    }
                   $product = implode(",",$product);    
                   $customer = Customer::where('user_id',$booking['user_id'])->first();
                    $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$customer->phone.'&message=Dear '. $user->username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Just to remind you for your appointment with '.$vendor->username.' for the blood/other sample collection on '.$product.'. Our scientific Officer '.$agent->username.' ('.$agent->phone.') will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';

                    
                    $smsUrl = str_replace(' ', '%20', $homepage);
                    $curl = curl_init();
                    curl_setopt_array($curl, [
                        CURLOPT_URL => $smsUrl,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                    ]);

                    $response = curl_exec($curl);
                    Booking::where('id',$booking['id'])->update(['reminder' => 1]);
                }
            }
        }
}
