<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\FcmNotification;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\User;
use App\Models\AgentSlots;
use App\Models\CustomerBookingProduct;
use Twilio\Rest\Client;

class AdminRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Admin:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign request to agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $todayDate = Carbon::now()->format('Y-m-d');
        $bookings = Booking::where('request_status','0')->whereNotNull('request_sent')->get();
        foreach ($bookings as $key => $booking) {
            if($booking->assigned_agent){
                $agent = User::find($booking->assigned_agent);
                 $customer = User::find($booking->user_id);
                 $time_slot = AgentSlots::find($booking->time_slot);
                 $test = CustomerBookingProduct::where('booking_id',$booking->id)->get()->pluck('title')->toArray();

                Booking::where('id',$booking['id'])
                ->update(['request_sent'=> NULL ]);
                $recipientsIOS = User::where('id',$agent->id)->where('device_type', 'ios')->get()->pluck('device_token');
                $recipients = User::where('id',$agent->id)->where('device_type', 'Android')->get()->pluck('device_token');

                $payload = [
                'title' =>'New Request',
                'body' => 'Admin assinged you a new job',
                'booking_id' => $booking['id'],
                'sound' => 'default',
                ];
                if(count($recipients) > 0) {
                // Send Push notification
                try {
                FcmNotification::send($recipients, $payload);   
                } catch (\Exception $e) {
                // something went wrong
                }
                } 

                if(count($recipientsIOS) > 0) {
                // Send Push notification
                try {
                FcmNotification::send($recipientsIOS, $payload, $ios = true);   
                } catch (\Exception $e) {
                // something went wrong
                }

                }
            }

        }
    }
}
