<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\FcmNotification;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\AgentBookedSlots;
use App\Models\AgentAvailability;
use App\Models\AgentSlots;
use App\Models\Customer;
use App\Models\User;
use Twilio\Rest\Client;

class CustomerRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send request to agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
   public function handle()
    {
        $bookings = Booking::where('request_status', 0)->whereNull('assigned_agent')->whereNull('tl_call')->whereDate('booking_date','>=',\Carbon\Carbon::now())->get();
        $agent_id = '';
        foreach ($bookings as $key => $booking) {
                $patient = User::find($booking['user_id']);
                $vendor = User::find($booking['vendor_id']);
                $customer = Customer::where('user_id',$booking['user_id'])->first();
                $slot = AgentSlots::find($booking['time_slot']);
                $BookedSlots = AgentBookedSlots::where('slot_id',$slot->id)->where('booking_date',$booking['booking_date'])->get()->pluck('agent_id')->toArray();
                $agents = User::closeTo($booking['lat'], $booking['lang'])->first();
                
                if($agents){
                  if(in_array($agents->id, $BookedSlots)){
                    Booking::where('id',$booking['id'])->update(['tl_Call' => '250']);
                    die();
                  }
                  
                $bookingSlot = explode('-',$slot->slot);
                $avalAgent = AgentAvailability::where('agent_id',$agents->id)->whereDate('date','=',$booking['booking_date'])->first();
                if($avalAgent){
                    $closingTime = strtotime($avalAgent->date.' '.$avalAgent->closing_hours);
                    $bookingTime = strtotime($booking['booking_date'].' '.$bookingSlot[0]);
                       if($closingTime > $bookingTime){
                        $agent_id = $avalAgent->agent_id;
                       }
                if($agent_id){
                $agents = User::find($agent_id);
                $AgentBookedSlots = new AgentBookedSlots();
                $AgentBookedSlots->slot_id = $slot->id;
                $AgentBookedSlots->agent_id = $agents->id;
                $AgentBookedSlots->booking_date = $booking['booking_date'];
                $AgentBookedSlots->save();

                Booking::where('id',$booking['id'])->update(['assigned_agent' => $agents->id]);
                $recipientsIOS = User::where('id',$agents->id)->where('device_type', 'ios')->get()->pluck('device_token');
                $recipients = User::where('id',$agents->id)->where('device_type', 'Android')->get()->pluck('device_token');
                $payload = [
                    'title' =>'New Request',
                    'body' => 'You have new request from customer',
                    'booking_id' => $booking['id'],
                    'sound' => 'default',
                ];

                if(count($recipients) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipients, $payload);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }
                } 

                if(count($recipientsIOS) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipientsIOS, $payload, $ios = true);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }

                }
                $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendor->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);

                $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$customer->phone.'&message=Dear '.$patient->username.', Your Sample collection for '.$labimp.' on Dated '.$booking->booking_date.' at '.$slot->slot.' has been assigned to our scientific Officer '.$agents->username.' '.$agents->phone.'. Please make yourself available on time in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
                $smsUrl = str_replace(' ', '%20', $sms);
                $curl = curl_init();

                curl_setopt_array($curl, [
                CURLOPT_URL => $smsUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                ]);

                $response = curl_exec($curl);
                die();

            }else{
                Booking::where('id',$booking['id'])->update(['tl_Call' => '250']);
            }
            }else{
            Booking::where('id',$booking['id'])->update(['tl_Call' => '250']);
            }
        }else{
            Booking::where('id',$booking['id'])->update(['tl_Call' => '250']);
 
        }
        
        }
    }
}
