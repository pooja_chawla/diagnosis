<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\FcmNotification;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\User;

class CancelBookingRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Cancel:booking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send request to agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bookings = Booking::whereIn('booking_status',[9,10])->whereDate('booking_date','>=',\Carbon\Carbon::now())->get();
        foreach ($bookings as $key => $booking) {
                $assigned_agent = $booking->assigned_agent;
                $agents = User::find($assigned_agent);
                $booking->assigned_agent = NULL;
                $booking->request_status = NULL;
                $booking->save();
                $recipientsIOS = User::where('id',$agents->id)->where('device_type', 'ios')->get()->pluck('device_token');
                $recipients = User::where('id',$agents->id)->where('device_type', 'Android')->get()->pluck('device_token');
                $payload = [
                    'title' =>'Booking Canceled',
                    'body' => 'Booking has been canceled',
                    'status' => 'canceled',
                    'booking_id' => $booking['id'],
                    'sound' => 'https://mysittivacations.com/notification.mp3',
                ];

                if(count($recipients) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipients, $payload);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }
                } 

                if(count($recipientsIOS) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipientsIOS, $payload, $ios = true);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }

                }

        }
    }
}
