<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class ForgotPasswordRequest extends ApiRequest
{
    public function rules()
    {
       return [
            'email'  => 'required|email|exists:users,email',
            // 'email'  => 'sometimes|email|exists:users,email',
            // 'phone'  => 'sometimes|exists:users,phone',
        ];
    }
}
