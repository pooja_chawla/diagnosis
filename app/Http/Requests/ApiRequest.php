<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\ApiResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class ApiRequest extends FormRequest {

  protected function failedValidation(Validator $validator)
  {
      $error = $validator->errors()->first();
      $errors = $validator->errors();
      $response = ApiResponse::error($error,$errors);
      throw new ValidationException($validator, $response);
  }

}
