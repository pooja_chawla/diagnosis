<?php

namespace App\Http\Requests;

class AgentRunnerRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'user_id'  => 'required|exists:users,id',
        ];
    }
}
