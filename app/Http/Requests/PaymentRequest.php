<?php

namespace App\Http\Requests;

class PaymentRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'booking_id'  => 'required|exists:bookings,id',
          'customer_id'  => 'required|exists:users,id',
          'amount'  => 'required',
        ];
    }
}
