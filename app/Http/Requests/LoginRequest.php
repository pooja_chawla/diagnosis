<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class LoginRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'email'        => 'required|email',
            'password'     => 'required',
            'device_type'  => 'sometimes|string',
            'role'    => ['required', 'string', Rule::in([User::RUNNER_ROLE, User::AGENT_ROLE])],
            'device_token' => 'sometimes|string',
            'latitude'     => 'sometimes|string',
            'longitude'    => 'sometimes|string',
        ];
    }
}
