<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;
class AcceptFriendshipRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'status'    => ['required', 'string', Rule::in([User::REQUEST_ACCEPTED, User::REQUEST_REJECTED])],
          'booking_id'  => 'required|exists:bookings,id',
        ]; 
    }
}
