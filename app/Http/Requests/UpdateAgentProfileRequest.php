<?php

namespace App\Http\Requests;

class UpdateAgentProfileRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'username' => 'string|max:255',
          'avatar'   => 'image|mimes:jpeg,png,jpg,gif|max:2048',
          'address'  => 'string',
          'age' => 'string',
          'gender' => 'string',
          'description'  => 'string',
        ];
    }
}
