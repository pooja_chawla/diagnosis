<?php

namespace App\Http\Requests;

class VisitRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'booking_id'  => 'required|exists:bookings,id',
        ];
    }
}
