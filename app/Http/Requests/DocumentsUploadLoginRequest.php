<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class DocumentsUploadLoginRequest extends ApiRequest
{
    public function rules()
    {
       return [
			'id'  => 'required|exists:users,id',
			'vehicle_number'  => 'required',
			'education'  => 'required',
			'vehicle_image'  => 'required',
			'account'  => 'required',
			'adhar_card'  => 'required',
			'pan_card'  => 'required',
        ];
    }
}
