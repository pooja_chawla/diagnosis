<?php

namespace App\Http\Requests;

class UpdateProfileRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'username' => 'required|string|max:255',
          'avatar'   => 'image|mimes:jpeg,png,jpg,gif|max:2048',
          'address'  => 'required|string',
          'phone_code'  => 'required|string',
          'phone'    => 'required|string|unique:users,phone,'.$this->user()->id,
        ];
    }
}
