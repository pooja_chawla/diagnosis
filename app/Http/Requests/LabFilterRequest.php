<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class LabFilterRequest extends ApiRequest
{
    public function rules()
    {
       return [
			 'type'    => ['required', 'string', Rule::in(['a', 'b','c'])],
        ];
    }
}
