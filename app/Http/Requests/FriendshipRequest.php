<?php

namespace App\Http\Requests;

class FriendshipRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'id'  => 'required|exists:users,id',
        ];
    }
}
