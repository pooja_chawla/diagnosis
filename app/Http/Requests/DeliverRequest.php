<?php

namespace App\Http\Requests;

class DeliverRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'qrcode' => 'required',
        ];
    }
}
