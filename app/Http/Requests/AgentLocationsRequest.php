<?php

namespace App\Http\Requests;

class AgentLocationsRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'latitude' => 'required',
          'longitude'  => 'required',
        ];
    }
}
