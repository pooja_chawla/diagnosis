<?php

namespace App\Http\Requests;

class AgentScannerRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'qrcode' => 'required',
          'booking_id'  => 'required|exists:bookings,id',
        ];
    }
}
