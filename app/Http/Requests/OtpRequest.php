<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class OtpRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'otp' => 'required|exists:users,otp',
        ];
    }

    public function messages()
    {
        return [
            'otp.exists' => __('otp_invalid')
        ];
    }
}
