<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;
class NotificationDetailRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'booking_id'  => 'required|exists:bookings,id',
        ];
    }
}
