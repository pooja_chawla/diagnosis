<?php

namespace App\Http\Requests;

class ScannerRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'qrcode' => 'required',
        ];
    }
}
