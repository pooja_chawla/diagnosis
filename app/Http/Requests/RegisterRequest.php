<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class RegisterRequest extends ApiRequest
{
    public function rules()
    {
        return [

            'username'     => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6',
            'phone'        => 'required|string|unique:users|phone',
            'phone_code'   => 'required|string',
            'image'        => 'sometimes|image|mimes:jpeg,png,jpg,gif|max:5120',
            'device_type'  => 'string',
            'device_token' => 'string',
            'latitude'     => 'string',
            'longitude'    => 'string',
            'gender'    => 'string',
            'age'    => 'string',
            'description'    => 'string',
            'role'         => 'required',
        ];
    }
}



            