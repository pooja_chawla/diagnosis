<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;

class LoginOtpRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'phone' => 'required|exists:users,phone',
            'otp' => 'required|exists:users,login_otp',
        ];
    }

    public function messages()
    {
        return [
            'otp.exists' => __('otp_invalid')
        ];
    }
}
