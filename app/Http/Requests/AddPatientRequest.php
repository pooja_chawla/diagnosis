<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Validation\Rule;
class AddPatientRequest extends ApiRequest
{
    public function rules()
    {
        return [
          'username'    => 'required',
          'phone'  => 'required',
          'address'  => 'required',
          'state'  => 'required',
          'zip'  => 'required',
          'city' => 'required',

        ]; 
    }
}
