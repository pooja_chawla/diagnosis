<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Qrcode;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $qrcode = Qrcode::where('booking_id',$this->id)->first();
         if($qrcode){
           $qr_status = '1';
         }else{
          $qr_status = '0';
         }
         if($this->order_id){
            $vendorName = 'Udaud- '.User::byVendor($this->vendor_id);
         }else{
            $vendorName = User::byVendor($this->vendor_id);
         }
        return [
            'id'                  => $this->id,
            'qr_status' =>         $qr_status,
            'user_id'                  => $this->user_id,
            'username'            => User::byName($this->user_id),
            'age'                 =>   User::byAge($this->user_id),
            'gender'                 =>   User::byGender($this->user_id),
            'phone'               =>  User::byPhone($this->user_id),
            'vendor'               =>  $vendorName,
            'comment'              => $this->comment,
            'vendor_id'               =>  $this->vendor_id,
            'medical_condition'      =>  User::byMedical($this->user_id),
            'total_price'      => $this->total_price,
            'phone_code'          => "+91",
            'payment_status'      => User::byId($this->id),
            'booking_status'      => $this->booking_status,
            'medical_tube'   =>  "Yellow",
            'created_at'          => (string) $this->created_at,
        ];
    
    }
}
