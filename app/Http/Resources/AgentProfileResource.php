<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;

use Illuminate\Http\Resources\Json\JsonResource;

class AgentProfileResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'username'            => (string) $this->username,
            'age'                 =>  (string) $this->age,
            'email'                 =>  (string) $this->email,
            'address'                 =>  (string) $this->address,
            'phone'               => (string) $this->phone,
            'phone_code'          => (string) $this->phone_code,
            'gender'              =>(string) $this->gender,
            'description'         => (string)$this->description,
            'avatar'              => ($this->image ? asset('/').'storage/'.$this->image : $this->photo_url) ,
            'created_at'          => (string) $this->created_at,
            'accepted' =>  Booking::where('request_status','1')->where('assigned_agent',$this->id)->get()->count(),
            'request' =>  Booking::where('request_status','0')->where('assigned_agent',$this->id)->get()->count(),
            'rejected' => Booking::where('request_status','0')->whereRaw("find_in_set('".$this->id."',rejected_users)")->get()->count(),
        ];
    
    }
}

