<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;
use App\Models\AgentSlots;
use App\Models\Qrcode;


use Illuminate\Http\Resources\Json\JsonResource;

class RunnerHistoryResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		// $user = auth()->user();
      //       $agent = 'Pending';
      //       $booking = [];

            $booking = Booking::find($this->booking_id);
            $vendor = User::select('username','address')->where('id',$booking->vendor_id)->first();
            $slot = AgentSlots::find($booking->time_slot);
            $agent = User::select('username')->where('id',$booking->assigned_agent)->first();
            $customer = User::select('username')->where('id',$booking->user_id)->first();
    		return [
            'id'                  => $this->booking_id,
            'user'                => $customer->username,
            // 'agent_name'                => $agent->username,
            'booking_status'      =>      $this->booking_status,
            'address'             => $vendor->address,
            'date'             => $booking->booking_date,
            'vendor'         => $vendor->username,
             
        ];
    }
}

