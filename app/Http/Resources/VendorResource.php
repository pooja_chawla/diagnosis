<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            'user_id'            => (string) $this->user_id,
            
            'status'              => (string)$this->status,
        ];
    
    }
}
