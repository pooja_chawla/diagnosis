<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;
use App\Models\ProductRequest;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		$user = auth()->user();
    		$getPrice =  ProductRequest::where('status','!=','3')->where('cancel_status','0')->where('product_id',$this->id)->where('vendor',$user->id)->get();
             if(count($getPrice)!= 0){
             	$price = $getPrice[0]->price;
             	$discount = $getPrice[0]->discount;
             	$status = $getPrice[0]->status;
             }else{
             	$price = 0;
             	$discount = 0;
             	$status = 0;
             }
    		return [
            'id'                  => $this->id,
            'title'            => (string) $this->title,
            'product_code'                 =>  (string) $this->product_code,
            'type'                 =>  (string) $this->type,
             
            'price' => $price,
            'discount' => $discount,
            'priceStatus' => $status
        ];
    }
}

