<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;


use Illuminate\Http\Resources\Json\JsonResource;

class VisitResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		$user = auth()->user();
            $agent = 'Pending';
            if($this->assigned_agent){
                $getAgent =  User::select('username')->where('id', $this->assigned_agent)->get();
                if(count($getAgent)!=0 ){
                    $agent = $getAgent[0]->username;
                }    
            }
    		return [
            'id'                  => $this->id,
            'username'            => (string) $this->username,
            'charges'            => (string) $this->charges,
            'assigned_agent'                 =>  $agent,
            'booking_status'                 =>  (string) $this->booking_status,
            'formatted_date'                 =>  (string) $this->formatted_date,
            'order_id' => $this->order_id,
             
        ];
    }
}

