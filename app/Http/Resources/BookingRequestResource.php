<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;
use App\Models\AgentSlots;


use Illuminate\Http\Resources\Json\JsonResource;

class BookingRequestResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		$user = auth()->user();
            $agent = 'Pending';
            $booking = [];

            $vendor = User::select('username')->where('id',$this->vendor_id)->first();
            $slot = AgentSlots::find($this->time_slot);
    		return [
            'id'                  => $this->id,
            'slot'                => $slot->slot,
            'bookingRequest'      => $this->bookingRequest,
            'vendor'              => $vendor->username,
            'user'                =>$this->username,
            'address'             =>$this->address,
            'date'                =>date('d-m-Y', strtotime($this->booking_date)),
             
        ];
    }
}

