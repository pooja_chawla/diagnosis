<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;
use App\Models\AgentSlots;


use Illuminate\Http\Resources\Json\JsonResource;

class UnassignedBookingResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		// $user = auth()->user();
      //       $agent = 'Pending';
      //       $booking = [];

            $vendor = User::select('username')->where('id',$this->vendor_id)->first();
            $slot = AgentSlots::find($this->time_slot);
            $agent = User::closeToBooking($this->lat, $this->lang)->limit(5)->get()->toArray();
            $customer = User::select('username')->where('id',$this->user_id)->first();
    		return [
            'id'                  => $this->id,
            'vendor'                => $vendor->username,
            'slot'      => $slot->slot,
            'slot_id'              => $this->time_slot,
            'user'                => $customer->username,
            'address'             => $this->address,
            'date'             => $this->booking_date,
            'agents'                =>$agent,
             
        ];
    }
}

