<?php

namespace App\Http\Resources;
use App\Models\User;
use App\Models\Booking;
use App\Models\AgentSlots;


use Illuminate\Http\Resources\Json\JsonResource;

class UpcomingBookingResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    		// $user = auth()->user();
      //       $agent = 'Pending';
      //       $booking = [];

            $vendor = User::select('username')->where('id',$this->vendor_id)->first();
            $slot = AgentSlots::find($this->time_slot);
            $agent = User::select('username')->where('id',$this->assigned_agent)->first();
            $customer = User::select('username')->where('id',$this->user_id)->first();

               if($this->booking_status == '0'){
                  $status = 'Visit Booked';
               }else if($this->booking_status == '1') {
                  $status = 'On the way';
               }else if($this->booking_status == '2') {
                  $status = 'Sample collected';
               }else if($this->booking_status == '3') {
                  $status = 'Sample Dispatched';
               }else if($this->booking_status == '4') {
                  $status = 'Sample Delivered';
               }else if($this->booking_status == '5') {
                  $status = 'Sample Recieved';
               }else if($this->booking_status == '6') {
                  $status = 'Testing in progress ( will require later)';
               }else if($this->booking_status == '7') {
                  $status = 'Report ready(later)';
               }else if($this->booking_status == '8') {
                  $status = 'Completed(later)';
               }else if($this->booking_status == '9') {
                  $status = 'Rebooking';
               }else if($this->booking_status == '10') {
                  $status = 'Cancel booking';
               }
    		return [
            'id'                  => $this->id,
            'user'                => $customer->username,
            'agent_name'                => $agent->username,
            'slot' => $slot->slot,
            'booking_status'      =>          $status,
            'address'             => $this->address,
            'date'             => $this->booking_date,
            'vendor'         => $vendor->username,
            'order_id'      => $this->order_id,
             
        ];
    }
}

