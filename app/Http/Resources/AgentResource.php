<?php

namespace App\Http\Resources;
use App\Models\User;

use Illuminate\Http\Resources\Json\JsonResource;

class AgentResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $userlat = User::find($this->id);
        $trackAddress= '';
        if($userlat){
            $url  = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$userlat->latitude.",".$userlat->longitude."&key=AIzaSyDzVKjCaPkolFgBenWZVdYg3RTHkqNuPJE";
            $json = @file_get_contents($url);
            $data = json_decode($json);
            if($data){
            $status = $data->status;
            $trackAddress = '';
            if($status == "OK")
            {
              $trackAddress = $data->results[0]->formatted_address;
            }
            }
          
        }
        return [
            'id'                  => $this->id,
            'username'            => (string) $this->username,
            'age'                 =>  (string) $this->age,
            'email'                 =>  (string) $this->email,
            'address'                 =>  (string) $this->address,
            'track'                  =>  (string) $trackAddress,
            'avatar'              => ($this->image ? asset('/').'storage/'.$this->image : $this->photo_url) ,
            'phone'               => (string) $this->phone,
            'phone_code'          => (string) $this->phone_code,
            'assigned_location'   => (string) $this->assigned_location,
            'gender'              =>(string) $this->gender,
            'description'              => (string)$this->description,
            'created_at'          => (string) $this->created_at,
        ];
    
    }
}
