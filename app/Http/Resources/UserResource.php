<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    public function __construct($resource, $token = '')
    {
        // Ensure you call the parent constructor
        parent::__construct($resource);
        $this->resource = $resource;        
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'username'            => (string) $this->username,
            'email'               => (string) $this->email,
            'avatar'              => ($this->image ? asset('/').'storage/'.$this->image : $this->photo_url) ,
            'role'                => (string) $this->userRole(),
            'phone'               => (string) $this->phone,
            'phone_code'          => (string) $this->phone_code,
            'address'             => (string) $this->address,
            'device_type'         => (string) $this->device_type,
            'device_token'        => (string) $this->device_token,
            'lat'         => (string) $this->agent_lat,
            'long'        => (string) $this->agent_long,
            'location'        => (string) $this->assigned_location,
            'created_at'          => (string) $this->created_at,
            'token'               => $this->when($this->token != '', $this->token),
            'otp_verified'        => (boolean)is_null($this->otp),
            'login_otp_verified'  => (boolean)is_null($this->login_otp),
            // 'otp_verified'        => true,
             'document'            => (boolean)is_null($this->document_status),
           
            'status'              => (string)$this->status,
        ];
    
    }
}
