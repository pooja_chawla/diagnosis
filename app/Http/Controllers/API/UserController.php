<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ApiResponse;
use DB;
use App\User;
use Validator;
use Illuminate\Validation\Rule;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserprofileResource;
use App\Job;
use App\Http\Requests\API\DocumentsUploadRequest;

class UserController extends Controller {
 
     public function updateDeviceToken(Request $request)
     {
       $this->validate($request, [
          'device_type' => 'required|string',
          'device_type' => 'required|string',
       ]);
       
       $user = auth()->user();
       $user->device_token = $request->input('device_token');
       $user->device_type = $request->input('device_type');
       $user->save();
       return ApiResponse::success($user->toArray(), 'success');
     }

     public function updateCoordinates(Request $request)
     {
      $this->validate($request, [
          'latitude' => 'required|string',
          'longitude' => 'required|string',
       ]);
      
       $user = auth()->user();
       $user->latitude = $request->input('latitude');
       $user->longitude = $request->input('longitude');
       // $user->user_tz = $request->input('user_tz');
       $user->save();
       return ApiResponse::success($user->toArray(), 'success');
     }

     public function cards(Request $request)
     {
         $user = $request->user();
         
         $stripe_id = $user->stripe_id;
         
         if(!$stripe_id)
         {
           return ApiResponse::success('success', []);
         }

         $paymentMethods = $user->listPaymentMethods();

         $cards = $paymentMethods->data;

         return ApiResponse::success('success', $cards);
     }

     public function addCard(Request $request)
     {
          $user = $request->user();

          $validator = Validator::make($request->all(), [
              'token' => 'required|string'
          ]); 

          if ($validator->fails()) {
              $error = $validator->messages()->first();
              return ApiResponse::error($error);
          } 

          $paymentMethod = $request->input('token');

          $user->addPaymentMethod($paymentMethod);
          $user->updateDefaultPaymentMethod($paymentMethod);
    
          return ApiResponse::success('Card has been successfully added');
     }

     public function uploadDocuments(DocumentsUploadRequest $request)
     {
         $user = $request->user();

         $user->addMediaFromRequest('dl_front')->withCustomProperties($request->only([
              'vehicle_type',
              'license_number',
              'valid_vehicle_type',
              'issued_on',
              'exp_date',
         ]))->toMediaCollection('driver_dl_front');
         
         $user->addMediaFromRequest('dl_back')->toMediaCollection('driver_dl_back');
         $user->addMediaFromRequest('proof_of_address')->toMediaCollection('driver_proof_of_address');
         $user->addMediaFromRequest('credit_check')->toMediaCollection('driver_credit_check');
         $user->addMediaFromRequest('crb_check')->toMediaCollection('driver_crb_check');
         $user->addMediaFromRequest('passport')->toMediaCollection('driver_passport');
         $user->addMediaFromRequest('insurance_certificate')->toMediaCollection('driver_insurance_certificate');

         return ApiResponse::success('success');
     }

      public function addispayContacts(Request $request)
      {

         $this->validate($request, [
             'contacts' => 'required|array'
         ]);

         $data['addispay_contacts'] = User::whereIn('phone',$request['contacts'])->where('is_active',1)->pluck('phone')->all();

         $data['others'] = [] ;

         $others = array_values(array_diff($request['contacts'],$data['addispay_contacts']));

         if($others)
         {
           $data['others'] = $others ;
         }

         return ApiResponse::success('success',$data);

      }

}
