<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Resources\UserResource;
use App\Http\Resources\RunnerHistoryResource;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\ScannerRequest;
use App\Models\Qrcode;
use App\Http\Requests\DeliverRequest;
use App\Http\Requests\VisitRequest;
use App\Models\Agent;
use App\Models\User;
use App\Models\AgentSlots;
Use \Carbon\Carbon;
use App\Models\Booking;
use App\Http\Requests\AgentLocationsRequest;
use App\Http\Requests\AgentRunnerRequest;

class RunnerController extends AppBaseController
{
	public function agentLocations(AgentLocationsRequest $request)
	{
		$user_id  =  $request->user()->id;
		User::where('id',$user_id)->update(['latitude' => $request->latitude, 'longitude' => $request->longitude]);
		$assigned_agent = $request->user()->assigned_agent;
		$exp_agent = explode(',', $assigned_agent);

		$users =  User::where('status','1')->whereIn('id',$exp_agent);
		$users =  $users->select("*", DB::raw("6371 * acos(cos(radians(" . $request->user()->latitude . "))
		* cos(radians(agent_lat)) * cos(radians(agent_long) - radians(" . $request->user()->longitude . "))
		+ sin(radians(" .$request->user()->latitude. ")) * sin(radians(agent_lat))) AS distance"));
		$users = $users->having('distance', '<', 20000);

		 $users =  $users->orderBy('distance', 'asc')->get();
		 $user =  UserResource::Collection($users);
        return $this->sendResponse('Success', $user);

	}
	public function scanner(ScannerRequest $request)
	{
		$user_id  =  $request->user()->id;
		$qrcheck = Qrcode::where('code',$request->qrcode)->first();
		if($qrcheck){
			 Qrcode::where('code',$request->qrcode)->update(['runner_id' => $user_id, 'booking_status' => 'recieved by runner', 'qr_status' => '1']);
	        return $this->sendResponse('Success');
		}else{
			  return ApiResponse::error('Qrcode is not valid');
		}
	}

	public function deliver(DeliverRequest $request)
	{
		$user_id  =  $request->user()->id;
		$qrcheck = Qrcode::where('code',$request->qrcode)->first();
		if($qrcheck){
			 Qrcode::where('code',$request->qrcode)->update(['runner_id' => $user_id, 'lab_status' => '1', 'booking_status' => 'delivered to lab']);
	        return $this->sendResponse('Success');
		}else{
			  return $this->sendResponse('Qrcode is not valid');
		}
	}

	public function visitListing(Request $request)
	{
		$data = [];
		$user_id  =  $request->user()->id;
		// $user_id  =  '1132';
		$visit = Qrcode::where('runner_id',$user_id)->get()->pluck('booking_id');
		if($visit){
		$booking = Booking::whereIn('id',$visit)->whereDate('booking_date','=',\Carbon\Carbon::now())->get();
		foreach ($booking as $key => $value) {
			$vendor = User::find($value['vendor_id']);
			$agent = User::find($value['assigned_agent']);
			$slot = AgentSlots::find($value['time_slot']);
			$patient = User::find($value['user_id']);
			$data[$key]['id'] = $value->id;
			$data[$key]['vendor'] = $vendor->username;
			$data[$key]['user'] = $patient->username;
			$data[$key]['date'] = $value->booking_date;
			$data[$key]['slot'] = $slot->slot;


		}
		return ApiResponse::success('success', $data);
	}else{
		return ApiResponse::success('success');

	}
	}

		public function visitDetail(VisitRequest $request)
	{
		 // $booking_id  =  $request;
		 $booking = Booking::find($request->booking_id);
		$qrcode = Qrcode::where('booking_id',$request->booking_id)->first();
		$data = [];

			$vendor = User::find($booking->vendor_id);
			$agent = User::find($booking->assigned_agent);
			$slot = AgentSlots::find($booking->time_slot);
			$patient = User::find($booking->user_id);
			$data['id'] = $booking->id;
			$data['vendor'] = $vendor->username;
			if($qrcode){	
			 $data['lab_status'] = $qrcode->lab_status;
			 $data['qr'] = $qrcode->code;
			}else{
			$data['lab_status'] = "0";
			}
			$data['address'] = $vendor->address;
		
			$data['user'] = $patient->username;
			$data['date'] = $booking->booking_date;
			$data['slot'] = $slot->slot;
		return ApiResponse::success('success', $data);
	}

		public function agentDetails(AgentRunnerRequest $request)
	{
		$user_id  =  $request->user_id;
		$user = User::find($user_id);
		$detail = [];
		$booking = Booking::where('request_status','1')->where('assigned_agent',$user_id)->whereDate('booking_date','=',\Carbon\Carbon::now())->get();
		$agent['id'] = $user_id;
		$agent['name'] = $user->username;
		$agent['email'] = $user->email;
		$agent['phone'] = $user->phone;
		$agent['lat'] = $user->agent_lat;
		$agent['long'] = $user->agent_long;
		$agent['assigned_location'] = $user->assigned_location;
		foreach ($booking as $key => $value) {
			$patient = User::find($value['user_id']);
			$vendor = User::find($value['vendor_id']);
			$qr = Qrcode::where('booking_id',$value['id'])->first();
			if($qr){
			$detail[$key]['status'] = $qr->qr_status;
			}else{
			$detail[$key]['status'] = '0';	
			}
			$detail[$key]['booking_id'] = $value['id'];
			$detail[$key]['patient'] = $patient->username;
			$detail[$key]['vendor'] = $vendor->username;
		}
		

	      return ApiResponse::mulitsuccess('success', $agent, $detail);
	}

  public function oldRunnerBooking(Request $request)
  {
     $user_id  =  $request->user()->id;
     $qrcode = Qrcode::where('runner_id',$user_id)->get();
      $code =  RunnerHistoryResource::Collection($qrcode);
     return ApiResponse::success('success', $code);
  }
}