<?php
namespace App\Http\Controllers\API;
use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Helpers\ApiResponse;
use App\Helpers\PaymentApp;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\SiteInfo;
use App\Models\BackCall;
use App\Helpers\FcmNotification;
use App\Models\Agent;
use App\Helpers\EmailFrontend;
use App\Models\Vendor;
use App\Models\Booking;
use App\Models\Coupon;
use App\Models\ContactUs;
use App\Models\Product;
use App\Models\CustomerPrescription;
use App\Models\WorkDay;
use App\Models\AgentSlots;
use App\Models\VendorProduct;
use App\Http\Resources\UserResource;
use App\Models\Partner;
use App\Models\PartnerWithUs;
use App\Models\Accreditation;
use App\Models\AssignProfile;
use App\Models\Customer;
use App\Models\Profile;
use App\Models\Offer;
use App\Models\AgentSlotNotAvailabil;
use App\Models\CustomerReport;
use App\Models\CustomerBookingRequest;
use App\Models\CustomerBookingProduct;
use App\Models\PackageTests;
use App\Models\ProfileTest;
use Mail;
use App\Mail\TestEmail;
use App\Mail\UdaudEmail;
use App\Mail\PrescriptionEmail;
use App\Models\Testimonial;
use App\Models\Category;
use App\Models\CartSession;
use App\Models\Payment;
use App\Models\Blogs;
Use \Carbon\Carbon;
use Carbon\CarbonPeriod;

class HomeController extends Controller
{

    private $repo;

    public function __construct()
    {

        // $this->repo = $re
    }
    
     /**
     * Fetch Agents
     *
     * @param  Request $request
     * @return JSONp
     */
    public function index(Request $request) 
    {
         /*$request->session()->put('selectedLocation', 'chandigarh');  
          echo $request->session()->get('selectedLocation'); */ 

        $data =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address","users.landmark","users.city","users.zip")
        ->leftJoin("users", "users.id", "=", "vendors.user_id")->where('vendors.partner_lab',1)->get();
                  
        $labs= [];
        foreach($data as $key=>$a){
            $accred = [];
            $labs[$key]['vendorId'] = $a->user_id; 
           
            if(count($a->accreditation)!=0){
                $accreditations = $a->accreditation;
                foreach($accreditations as  $k=>$accreditation){

                  $acc = Accreditation::where('id',$accreditation)->get();
                   if(count($acc)!=0){$accred[$k] = $acc[0]->name;}
                
                }
            }
            $labs[$key]['rating'] = $a->rating;
             $labs[$key]['ratingCount'] = $a->ratingCount;
            $labs[$key]['accreditations'] = $accred; 
            $labs[$key]['end'] = end($accred); 

            $labs[$key]['title'] = $a->title; 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = $a->description;
            $labs[$key]['address'] = $a->address; 
            $labs[$key]['city'] = $a->city;
            $labs[$key]['zip'] = $a->zip;
            $labs[$key]['landmark'] = $a->city.' '.$a->state; 
        }
       /* echo "<pre>"; 
        print_r($labs);*/

         return ApiResponse::success('success',   $labs);
    }
    public function getpackageLabs($pack){
      
      $id = Product::select('id')->where('cost_free_package',0)->where('slug',$pack)->first();
      //print_r($id->id);
      $data =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address","users.landmark","users.id as vendorId")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        ->leftJoin("assign_package_price", "assign_package_price.vendor_id", "=", "vendors.user_id")
                        ->where('assign_package_price.product_id',$id->id)
                       ->where('vendors.partner_lab',1)
                        ->get(); 
         $labs= [];
        foreach($data as $key=>$a){
          if($a->title != ''){
            $labs[$key]['vendorId'] = $a->vendorId; 
                $accred = [];
           
            if(count($a->accreditation)!=0){
                $accreditations = $a->accreditation;
                foreach($accreditations as  $k=>$accreditation){

                  $acc = Accreditation::where('id',$accreditation)->get();
                  if(count($acc)!=0){$accred[$k] = $acc[0]->name;}
                
                }
            }
            $labs[$key]['rating'] = $a->rating;
            $labs[$key]['ratingCount'] = $a->ratingCount;
            $labs[$key]['accreditations'] = $accred; 
            $labs[$key]['end'] = end($accred); 
            $labs[$key]['title'] = $a->title; 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = $a->description;
            $labs[$key]['address'] = $a->address; 
            $labs[$key]['landmark'] = $a->landmark; 
          }  
        } 
         return ApiResponse::success('success',   $labs);
    }
        public function indexMob(Request $request) 
    {
         /*$request->session()->put('selectedLocation', 'chandigarh');  
          echo $request->session()->get('selectedLocation'); */ 

        $data =Vendor::select("vendors.*","users.address","users.username")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        ->get();
                  
        $labs= [];
        foreach($data as $key=>$a){
            $labs[$key]['id'] = $a->user_id;
            if($a->username) {
              $username = $a->username;
            }else{
              $username = "";
            }
            $labs[$key]['title'] = $username; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = strip_tags($a->description); 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['address'] = $a->address; 
           
        } 
         return ApiResponse::success('success', $labs);
    }


    public function getDistance(Request $request){
            $data = $request->all();
      $vendor = auth()->user();
      $vendor_latitude =  $vendor['latitude'];
      $vendor_longitude = $vendor['longitude'];
      $a =round(PaymentApp::distance($vendor_latitude, $vendor_longitude, $data['lat'], $data['lon']),1);
      $price = $a*10;
      //echo  $a;
      return ApiResponse::success('success', $price);
    }
    public function selectedLacation( Request $request, $loc){
          $request->session()->put('selectedLocation', $loc);  
         // echo $request->session()->get('selectedLocation');
    }
    public function getLabProducts($lab){
      $getVendor = Vendor::select('user_id')->where('slug',$lab)->first();
     // print_r($getVendor->user_id);
      $data =Product::select("products.title","products.id","products.profile_ids","products.slug","products.image", "products.price" , "products.discount")
                        //->leftJoin("assign_product", "assign_product.product_id", "=", "products.id")
                        //->whereIn('explode(",",assign_product.vendor_id)',$getVendor->user_id)
                        ->where('products.type', 5)
                        ->where('cost_free_package',0)
                        ->where('products.status', 1)
                        ->get(); 
                                      
        $product= [];
        $test= [];
        foreach($data as $key=>$a){
            $checkLab = AssignProfile::where('product_id',$a->id)->get();
           
           if(count($checkLab) != 0){
             $labLists = explode(',',$checkLab[0]->vendor_id);
             if(in_array($getVendor->user_id,  $labLists)){
            $test = PackageTests::select('name')->where('product_id',$a->id)->limit('3')->get()->toArray();
            $testCount = PackageTests::select('name')->where('product_id',$a->id)->count();
            $count = 0; 
            if(count($a->profile_ids)!=0){
               foreach ($a->profile_ids as  $profile) {
                  $count += ProfileTest::where('profile_id',$profile)->count();
               }
            }
           
            $img = asset('/storage/'.$a->image);
            $product[$key]['title'] = $a->title;
            $product[$key]['slug'] = $a->slug;   
            
            $product[$key]['discount'] = 0 ;
            $product[$key]['price'] = number_format($a->price) ;
            if($a->discount != 0){
              $product[$key]['discount'] = number_format($a->price);
              $product[$key]['price'] = number_format($a->price - $a->discount); 
              $pricePer= $a->discount*100/$a->price;
              $product[$key]['pricePer']= round($pricePer);
            }  
               
            

            $product[$key]['image'] = $img; 
            $product[$key]['tests'] = $test; 
            $product[$key]['count'] = $count +  $testCount; 

           
         }  
        } 
      }
       // print_r($product);
         return ApiResponse::success('success', $product);               
    }

    public function getKareProducts(Request $request){   

        $data = Product::where('status','1')->orderBy('order','desc')->where('type', 5)->where('categories',null)->where('cost_free_package',0)->get();

        
        $product= [];
        $test= [];
        foreach($data as $key=>$a){
            $test = PackageTests::select('name')->where('product_id',$a->id)->limit('3')->get()->toArray();
            $testCount = PackageTests::select('name')->where('product_id',$a->id)->count();
            $bookedCount = CartSession::where('product_id',$a->id)->where('status',1)->count();
            $count = 0; 
            if(count($a->profile_ids)!=0){
               foreach ($a->profile_ids as  $profile) {
                  $count += ProfileTest::where('profile_id',$profile)->count();
               }
            }
           
            $img = asset('/storage/'.$a->image);
            $product[$key]['title'] = $a->title;
            $product[$key]['slug'] = $a->slug;   
            $product[$key]['bookedCount'] = $bookedCount +30;   
            
            $product[$key]['discount'] = 0 ;
            $product[$key]['price'] = $a->price;


            if($a->discount != 0){ 
              $pricePer= $a->discount*100/$a->price;
              $product[$key]['pricePer']= round($pricePer);
              $product[$key]['discount'] = number_format($a->price);
              $product[$key]['price'] = ($a->price - $a->discount); 
            }  
               
            

            $product[$key]['image'] = $img; 
            $product[$key]['tests'] = $test; 
            $product[$key]['count'] = $count +  $testCount; 
           
        } 
        $keys = array_column($product, 'price');
        array_multisort($keys, SORT_ASC, $product);
       // print_r($product);
         return ApiResponse::success('success', $product);
    }

    public function getProducts(Request $request){   

        $data = Product::where('status','1')->orderBy('order','desc')->where('type', 5)->where('categories',null)->where('cost_free_package',0)->get();

        
        $product= [];
        $test= [];
        foreach($data as $key=>$a){
            $test = PackageTests::select('name')->where('product_id',$a->id)->limit('3')->get()->toArray();
            $testCount = PackageTests::select('name')->where('product_id',$a->id)->count();
            $bookedCount = CartSession::where('product_id',$a->id)->where('status',1)->count();
            $count = 0; 
            if(count($a->profile_ids)!=0){
               foreach ($a->profile_ids as  $profile) {
                  $count += ProfileTest::where('profile_id',$profile)->count();
               }
            }
           
            $img = asset('/storage/'.$a->image);
            $product[$key]['title'] = $a->title;
            $product[$key]['slug'] = $a->slug;   
            $product[$key]['bookedCount'] = $bookedCount +30;   
            
            $product[$key]['discount'] = 0 ;
            $product[$key]['price'] = number_format($a->price) ;


            if($a->discount != 0){ 
              $pricePer= $a->discount*100/$a->price;
              $product[$key]['pricePer']= round($pricePer);
              $product[$key]['discount'] = number_format($a->price);
              $product[$key]['price'] = number_format($a->price - $a->discount); 
            }  
               
            

            $product[$key]['image'] = $img; 
            $product[$key]['tests'] = $test; 
            $product[$key]['count'] = $count +  $testCount; 
        } 
        $keys = array_column($product, 'bookedCount');
        array_multisort($keys, SORT_DESC, $product);
       // print_r($product);
         return ApiResponse::success('success', $product);
    }
    public function getCounts(Request $request){
      $data = [];
      $customer= Customer::select('id')->count();
      $data['customerCount'] =  6000 + $customer;
      $tests1 = PackageTests::select('id')->count();
      $tests2 = ProfileTest::select('id')->count();
      $tests3 = CustomerBookingProduct::select('id')->count();
      $data['testCount'] = 26000 + $tests1 + $tests2 + $tests3;
      $data['labCount'] = Vendor::select('id')->count();
      return ApiResponse::success('success', $data);
    }
    public function getPartners(Request $request){
      $partner = [];
      

      $partner = Partner::select('id','name','image')->get();
         foreach($partner as $key=>$a){
             $img = asset('/storage/'.$a->image); 
            $partner[$key]['id'] = $a->id;
            $partner[$key]['name'] = $a->name; 
            $partner[$key]['image'] = $img;   
      } 
      return ApiResponse::success('success', $partner);
    }
    public function getProductsMob(Request $request){   

        $data = Product::select('status','1')->where('cost_free_package',0)->orderBy('order','desc')->get();

        
        $product= [];
        foreach($data as $key=>$a){
             $product[$key]['id'] = $a->id;
            $product[$key]['title'] = $a->title;
            $product[$key]['description'] = $a->description; 
            $product[$key]['slug'] = $a->slug;   
            $product[$key]['price'] = number_format($a->price); 
           
           
        } 
         return ApiResponse::success('success', $product);
    }
     public function getTests(Request $request){   

        $data = Product::where('status','1')->where('cost_free_package',0)->orderBy('order','desc')->where('type', 2)->get();

        
        $test= [];
        foreach($data as $key=>$a){

            $test[$key]['title'] = $a->title;
            $test[$key]['description'] = $a->description; 
            $test[$key]['slug'] = $a->slug;   
            $test[$key]['price'] = number_format($a->price); 
           
           
        } 
         return ApiResponse::success('success', $test);
    }

    public function getLabName($slug){
        
       $lab =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        ->where('slug',$slug)
                        ->first();
        if($lab){
            $data = array('name' => $lab->title);
            return ApiResponse::success('success', $data);
        }                              
    }
    public function labDetails($slug){

       //echo $slug;
       $lab =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address","users.phone")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        ->where('slug',$slug)
                        ->get(); 
       
       $labs=[];
       foreach ($lab as $key => $a) {
            
            $labs[$key]['rating'] = $a->rating;
            $labs[$key]['ratingCount'] = $a->ratingCount;
            $labs[$key]['title'] = $a->title; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = $a->description; 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['phone'] = $a->phone; 
            $labs[$key]['address'] = $a->address; 
            $labs[$key]['landmark'] = $a->landmark; 
            $acc = Accreditation::select('name','position')->whereIn('id',$a->accreditation)->get()->toArray();
            if(count($acc) == 0 ){
               $labs[$key]['accreditations'] = [];
            }else{
              $labs[$key]['accreditations'] = $acc;
            }
            $gallery_img = $a->gallery; 
            $f = [];
            if($gallery_img!=''){
            foreach ($gallery_img as $a => $value) {
                $f[$a]['img'] = asset('/').'storage/'.$value; 
            } 
            }              
            $labs[$key]['gallery'] = $f; 
                
       }
       

        return ApiResponse::success('success', $labs);

    }                       

    public function getProductList(Request $request)
    {

        $product    = Product::where('status','1')->where('cost_free_package',0)->orderBy('order','desc')->where('type', 2)->get();

        if($product){
            return ApiResponse::success('success', $product);
        }
        return ApiResponse::success('error'); 
    }

    public function productDetail($slug,$labname){
        
        $lab =Vendor::where('slug',$labname)->first();
        $products    = Product:: where('slug',$slug)->get();
        $lab ='';
        if($lab){
          $lab = $lab->title;  
        }
        

        $product= [];
        foreach($products as $key=>$a){
            $test = PackageTests::select('name')->where('product_id',$a->id)->get()->toArray();
            $img = asset('/storage/'.$a->image);
            $product[$key]['title'] = $a->title; 
           // $product[$key]['userId'] = $a->id; 
            $product[$key]['id'] = $a->id; 
            $product[$key]['cat'] = $a->categories; 
            $product[$key]['image'] = $img;  
            $product[$key]['description'] = $a->description; 
            $product[$key]['discount'] = 0 ;
            $product[$key]['actualprice'] = number_format($a->price );
            $product[$key]['price'] = number_format($a->price) ;
            $product[$key]['tests'] = $test; 
            if($a->discount != 0){
              $pricePer= $a->discount*100/$a->price;
              $product[$key]['pricePer']= round($pricePer);
              $product[$key]['discount'] = number_format($a->price);
              $product[$key]['price'] = number_format($a->price - $a->discount); 
            } 
            $product[$key]['labName'] = $lab; 
            
        } 
        //print_r($product);
            
        
        if($product){
            return ApiResponse::success('success', $product);
        }
        return ApiResponse::success('error');
    }
    

    public function profileTest($slug){
      $products    = Product:: select('profile_ids')->where('cost_free_package',0)->where('slug',$slug)->first();
      //$products    = ['5','6'];
      /*$profiles =ProfileTest::select('name','profile_id')->whereIn('profile_id', $products->profile_ids)->get();
       */
      $data =[];
      if(count($products->profile_ids)!=0){
        foreach($products->profile_ids as $key=>$product){
          
          if($product){
            $pro = Profile::select('name')->where('id', $product)->first();
            $data[$key]['pname'] =   $pro->name;
            $data[$key]['tests'] =ProfileTest::select('name','profile_id')->where('profile_id', $product)->get();
          }
        }
    }

      return ApiResponse::success('success', $data);
    }
    public function getBodyPart($type){
      
      $two = Category::select('name','id','image')->orderBy('id','desc')->limit(6)->get();
      $d2 = [];
     
        foreach($two as $key=>$a){
          if($type == 1 && $a->id !='9'){
            $d2[$key]['id'] = $a->id; 
            $d2[$key]['image'] = asset('/').'storage/'.$a->image; 
            
            $d2[$key]['name'] = $a->name; 
          }else if($type == 2 && $a->id !='8'){
            $d2[$key]['id'] = $a->id; 
            $d2[$key]['image'] = asset('/').'storage/'.$a->image; 
            
            $d2[$key]['name'] = $a->name; 
          }
        } 
    /* echo "<pre>";
     print_r($d2);
     die;*/
      $data['body2'] = $d2;
      $d1 =[];
      $ones = Category::select('name','id','image')->limit(5)->get();
        foreach($ones as $key=>$one){
            $d1[$key]['id'] = $one->id; 
            $d1[$key]['image'] = asset('/').'storage/'.$one->image; 
            $d1[$key]['name'] = $one->name; 
        } 
       $data['body1'] = $d1;
      return ApiResponse::success('success', $data);
    }
    public function bodyTest($id){
      
      $products    = Product::where('categories',$id)->where('cost_free_package',0)->where('status',1)->get();
    
      $data =[];
      //$testCount = 0;
      //$count = 0; 
      foreach($products as $key=>$product){
        $testCount = 0;
        $count = 0;
         $data[$key]['id'] =  $product->id;
         $data[$key]['cat'] =  $id;
         $data[$key]['pname'] =  $product->title;
         $data[$key]['slug'] =  $product->slug;
         $data[$key]['discount'] = 0 ;
         $data[$key]['price'] = number_format($product->price );
         $data[$key]['p'] = $product->price;
            if($product->discount != 0){
               $pricePer= $product->discount*100/$product->price;
              $data[$key]['pricePer']= round($pricePer);
              $data[$key]['discount'] = number_format($product->price);
              $data[$key]['price'] = number_format($product->price - $product->discount); 
            }  
         $pro = Profile::select('name')->where('id', $product)->first();
         $testCount = PackageTests::select('name')->where('product_id',$product->id)->count();
           
            if(count($product->profile_ids)!=0){
               foreach ($product->profile_ids as  $profile) {
                  $count += ProfileTest::where('profile_id',$profile)->count();
               }
            }
            $data[$key]['count'] = $count +  $testCount;  
       
      }
         uasort($data, function ($item, $compare) {
         return $item['p'] >= $compare['p']; 
      });
      $d = array_values($data);

      return ApiResponse::success('success', $d);
    }

    public function siteInfo(){
      $siteinfo =SiteInfo::find(1);
      $info['number'] = $siteinfo->site_number; 
      $info['logo'] = $siteinfo->site_logo;
      $info['email'] = $siteinfo->site_email; 
      $info['address'] = $siteinfo->site_address;  
      $info['copyright'] = $siteinfo->site_copyright; 
      $info['description'] = $siteinfo->site_description;  
      if($info){
          return ApiResponse::success('success', $info);
      }
      return ApiResponse::success('error'); 
    } 
    public function consultation(){
      $siteinfo =SiteInfo::find(1);
      $info = $siteinfo->cunsltation_fee;  
      if($info){
          return ApiResponse::success('success', $info);
      }
      return ApiResponse::success('error'); 
    }
    
    public function popularLabs(Request $request){

        $data =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        ->get();
                  
        $labs= [];
        foreach($data as $key=>$a){
            $labs[$key]['title'] = $a->title; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = $a->description; 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['address'] = $a->address; 
            
        } 
         return ApiResponse::success('success', $labs);

    }
    public function submitPrescription(Request $request){
        $data = $request->all();
        $file = $request->file('image') ;
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        $fileNameWithExt = str_replace(" ", "_", $fileNameWithExt);
        $filename = preg_replace("/[^a-zA-Z0-9\s]/", "", $fileNameWithExt);
        $filename = urlencode($filename);
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        $destinationPath = public_path().'/prescription';
        $file->move($destinationPath,$fileNameToStore);
       // print_r($fileNameToStore );
        $arr = array(
            'f_name' => $data['f_name'],
            'l_name' => $data['l_name'],
            'number' => $data['number'],
            'image' => 'prescription/'.$fileNameToStore,
        );
            $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number=8595659421,9326537102,9137580450&message=Customer having Mob No '.$data['number'].' has uploaded prescription. Please Call back.Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $sms);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);
              $response = curl_exec($curl);
        CustomerPrescription::create($arr);
        $temp ="<html><body><div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;margin: 0 auto;'>";
        $temp .="<tbody><tr><td style='margin: 0px auto;padding: 0 0 0px;width: 100%;'><img width='100px' src='https://udaudindia.com/_nuxt/img/logo.26c739f.png'></td>";            
        $temp .='</tr></tbody></table>';
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 4px #df1111;margin: 10px 0 0;border-style: double;'>";
        $temp .="<tbody><tr><td style='padding: 15px;'>";
        $temp .="<label style='font-size: 14px;font-weight: 700;'>Person Name</label><p style='margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;'>".$data['f_name']."</p>";
        $temp .="<label style='font-size: 14px;font-weight: 700;'>Mobile No.</label><p style='margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;'>".$data['number']."</p>";
        $temp .='</td></tr></tbody></table></div></body></html>';
        $attach =  url('/prescription/'.$fileNameToStore);
        $sub = 'Prescription';
        // Mail::to('info@udaudindia.com')->send(new PrescriptionEmail($temp,$attach));
        EmailFrontend::sendEmailFrontend('info@udaudindia.com',$sub,$temp);
        $msg = 'We are checking the  details shared by you, We will contact you soon on this number - ('.$data['number'].' )' ;
        return ApiResponse::success($msg, $arr);
    }
    public function bookingDates(Request $request){
      $current_date = Carbon::tomorrow()->format('Y-m-d');
      //$last_date =    \Carbon\Carbon::parse($current_date)->endOfMonth()->toDateString();
      $now = Carbon::now();
     // $last_date = $now->endOfWeek()->format('Y-m-d');
      $last_date = $date = \Carbon\Carbon::today()->addDays(4)->format('Y-m-d');
      /*echo "current: ". $current_date."<br>";
      echo "last: ". $last_date;*/
       $dateRange = CarbonPeriod::create($current_date, $last_date);
       $dates = [];
       foreach($dateRange as $date) {
            $dates[] = $date->format('Y-m-d');
        }
        $currentDates= [];
       
        foreach($dates as $key=>$date) {
             $currentDates[$key]['date'] = \Carbon\Carbon::parse($date)->format('d');
             $currentDates[$key]['month'] = \Carbon\Carbon::parse($date)->format('M');
             $currentDates[$key]['day'] =Carbon::parse($date)->format('l');;
        }
       return ApiResponse::success('Success.',$currentDates);
    }
    public function fetchTimeSlots($slotId,$timeid){

          $one =  AgentSlots::where('status',1)->where('id','>=',$slotId)->where('day_slot',$timeid)->get();
          $a = [];
          if(count($one)!=0){
            foreach($one as $key=>$data){
              $a[$key]['id'] = $data->id;
              $a[$key]['slot'] = $data->slot;
              $a[$key]['day_slot'] = $data->day_slot;
              $a[$key]['status'] = $data->status;
              $a[$key]['show_status'] = 1; // available
            }
          }
          $two =  AgentSlots::where('status',1)->where('id','<=',$slotId)->where('day_slot',$timeid)->get();
           $b = [];
          if(count($two)!=0){
            foreach($two as $key=>$data){
              $b[$key]['id'] = $data->id;
              $b[$key]['slot'] = $data->slot;
              $b[$key]['day_slot'] = $data->day_slot;
              $b[$key]['status'] = $data->status;
              $b[$key]['show_status'] = 2; //not  available
            }
          }
          $slots = array_merge($b,$a);
          return ApiResponse::success('Success.',$slots);
          echo "<pre>";
          print_r($slots);
         

    }
    public function fetchSlots($date,$day,$timeid){
      $currentDate = \Carbon\Carbon::now()->format('d');
      if($date == $currentDate){

        $newDateTime = Carbon::now()->addMinutes('30')->format('g:00 A');
        //$newDateTime = '3:00 PM';
        
        $newTime = \Carbon\Carbon::now()->format('A');
       // $newTime = 'PM';

        $t = AgentSlots::where('status',1)->get();
        $slots = [];

        foreach ($t as $slot){
         
           $timeSlot = explode('-', $slot->slot);
           $slotStr = strtotime($timeSlot[0]);
           $time = date('g:i A',$slotStr);
          //print_r($time);

           if(trim($newDateTime) == trim($time)){
              
            return $this->fetchTimeSlots($slot->id , $timeid);
              
           }
        
            
        }
        
       // print_r($newDateTime);
      }else{
        
        $three = AgentSlots::where('status',1)->where('day_slot',$timeid)->get();
         $slots = [];
          if(count($three)!=0){
            foreach($three as $key=>$data){
              $slots[$key]['id'] = $data->id;
              $slots[$key]['slot'] = $data->slot;
              $slots[$key]['day_slot'] = $data->day_slot;
              $slots[$key]['status'] = $data->status;
              $slots[$key]['show_status'] = 1; //available
            }
          }
        return ApiResponse::success('Success.',$slots);
      }
      
       return $this->fetchNotAvailableTimeSlots( $timeid,$newTime);

       
    }
    public function fetchNotAvailableTimeSlots($time ,$status){
        $three = AgentSlots::where('status',1)->where('day_slot',$time)->get();
         $slots = [];
          if(count($three)!=0){
            foreach($three as $key=>$data){
              $slots[$key]['id'] = $data->id;
              $slots[$key]['slot'] = $data->slot;
              $slots[$key]['day_slot'] = $data->day_slot;
              $slots[$key]['status'] = $data->status;
              if($status == 'PM'){
                 $slots[$key]['show_status'] = 2;
              }else{
                $slots[$key]['show_status'] = 1; //available
              }
              
            }
          }
        return ApiResponse::success('Success.',$slots);
    }
    public function fetchSlotsx($date,$day,$time){
      $currentDate = \Carbon\Carbon::now()->format('d');
      
      $newDateTime = Carbon::now()->addHours(2)->format('h');
             

             
        
        print_r($newDateTime);
      $details = AgentSlots::where('status',1)->where('day_slot',$time)->get();
      foreach ($details as $detail){
        if($date == $currentDate){
            echo $detail;
        }else{
          //echo "string";
        }
      }
     // return ApiResponse::success('Success.',$slots);
    }
/*
    public function fetchSlots($date,$day,$time){

       $day_id = WorkDay::select('id')->where('day', $day)->first();
       $day = $day_id->id;
       $slots =WorkDay::select("work_days.*", "work_slots.opening_hour","work_slots.closing_hour")
                        ->leftJoin("work_slots", "work_slots.day_id", "=", "work_days.id")
                        ->where('work_slots.day_id',$day)
                        ->get();
       $addSlots =[];   
       $data = [];              
      foreach($slots as $key=>$slot){
        $start = strtotime($slot->opening_hour);
        $end = strtotime($slot->closing_hour);
        $minutes  = ($end - $start) / 30;
        //print_r($minutes);
      
          
          $start1 = strtotime('01:00');
          $end1 = strtotime('00:00');
          $duration  = ($start1 - $end1) / 60;
          $slots = $minutes /$duration;
          $s =  $slot->opening_hour;
          $now = \Carbon\Carbon::today();
          //print_r($slots);
           for ($i = 01; $i <= $slots; $i++){
             
            // $data['agent_id'] = '1';
             
             $duratn = 1;
             $timestamp = strtotime($s) + 30*60;
             $time = date('h:i A', $timestamp);
              
             $hour = date('H', $timestamp);
             $dayTerm = ($hour > 17) ? "3" : (($hour > 12) ? "2" : "1");

              
                $data['slot_id'] = $i;
                $data['duration'] = $dayTerm;
                $data['time'] = $s.'-'.$time;
                $addSlots[] = $data;          
 
             
             $s = $time;

           }
        } 
       
        return ApiResponse::success('Success.',$addSlots);    
             
    }*/

    public function createBooking(Request $request){
      
      

      $validator = Validator::make($request->all(), [
        'dateBooking' => 'required',
        'slotBooking' => 'required',
        'name' => 'required',
        'dob' => 'required',
        'number' => 'required',
        'pincode' => 'required',
        'state' => 'required',
        'city' => 'required',
        'address' => 'required',
        'address1' => 'required',
      ]);
      $data = $request->all(); 
      $details = array(
        'dateBooking' => $data['dateBooking'],
        'slotBooking' => $data['slotBooking'],
        'name' => $data['name'],
        'vendor_id' => '37',
        'dob' => $data['dob'],
        'phone' => $data['number'],
        'pincode' => $data['pincode'],
        'state' => $data['state'],
        'city' => $data['city'],
        'lat' => $data['lat'],
        'lon' => $data['lon'],
        'address' => $data['address'],
        'address1' => $data['address1'],
        'subtotal' => $data['subtotal'],
        'amount' => $data['amount'],
        'payment_status' => $data['paymentStatus'],
        'discount' => $data['discount'],
      );
      
    // print_r($details);
      
      if ($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::error($error);
      }

      $insert = CustomerBookingRequest::create($details);
      $booking =  $insert->id; 
      foreach($data['cart'] as $cart){
        $cart_data['booking_id'] = $booking;
        $cart_data['title'] = $cart['title'];
        $cart_data['price'] = $cart['price'];
        $cart_data['labName'] = $cart['labName'];
        CustomerBookingProduct::create($cart_data);
      }
      $request->session()->put('customerBooking', $booking); 
      return ApiResponse::success('Success',$validator);
  }
     
  public function labsearch(Request $request){
        
        $lab  = $request->all();
        $lab_name = $lab['lab']['labName'];
        //print_r($lab_name);
        $data =Vendor::select("vendors.*", "users.latitude","users.longitude","users.address")
                        ->leftJoin("users", "users.id", "=", "vendors.user_id")
                        //->orWhere('vendors.title', 'like', '%'.$lab_name.'%')
                        ->where('vendors.title',$lab_name)
                        ->get();


                  
        $labs= [];
        foreach($data as $key=>$a){

            $labs[$key]['title'] = $a->title; 
            $labs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $labs[$key]['description'] = $a->description; 
            $labs[$key]['slug'] = $a->slug; 
            $labs[$key]['address'] = $a->address; 
           
        } 
         return ApiResponse::success('success', $labs);
  }

  public function submitContactUs(Request $request){
      $data = $request->all();
      ContactUs::create($data);
      $msg = 'We will contact you soon on this number - ('.$data['phone'].' )' ;
        return ApiResponse::success($msg, $data);
  }

  public function getTestimonial(){
      $testimonial = [];
      $details = Testimonial::get();
       foreach($details as $key=>$a){

            $testimonial[$key]['name'] = $a->name; 
            $testimonial[$key]['image'] = asset('/').'storage/'.$a->image; 
            $testimonial[$key]['description'] = $a->description; 
            $testimonial[$key]['age'] = $a->age; 
            $testimonial[$key]['city'] = $a->city; 
           
        } 
      return ApiResponse::success('success', $testimonial);
  }
  public function getBlogs(){
      $blogs = [];
      $details = Blogs::where('status','1')->limit('3')->get();
       foreach($details as $key=>$a){

            $blogs[$key]['title'] = $a->title; 
            $blogs[$key]['slug'] = $a->slug; 
            $blogs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $blogs[$key]['description'] = $a->description; 
            $blogs[$key]['created_at'] = \Carbon\Carbon::parse( $a->created_at)->format('dM Y'); 
           
           
        } 
      return ApiResponse::success('success', $blogs);
  }
  public function healthyTipDetails($slug){
      $blogs = [];
            $details = Blogs::where('slug',$slug)->first();
            $blogs['title'] = $details->title; 
            $blogs['image'] = asset('/').'storage/'.$details->image; 
            $blogs['description'] = $details->description; 
            $blogs['created_at'] = \Carbon\Carbon::parse( $details->created_at)->format('dM Y'); 

      return ApiResponse::success('success', $blogs);
  }
  public function getInternalBlogs(){
      $blogs = [];
      $details = Blogs::where('status','1')->get();
       foreach($details as $key=>$a){

            $blogs[$key]['title'] = $a->title; 
             $blogs[$key]['slug'] = $a->slug; 
            $blogs[$key]['blog'] = $a->blog; 
            $blogs[$key]['image'] = asset('/').'storage/'.$a->image; 
            $blogs[$key]['description'] = $a->description; 
            $blogs[$key]['created_at'] = \Carbon\Carbon::parse( $a->created_at)->format('dM Y'); 
           
           
        } 
      return ApiResponse::success('success', $blogs);
  }
  public function showAgents(Request $request){
      $current_date = Carbon::today()->format('Y-m-d');
      $last_date = $date = \Carbon\Carbon::today()->addDays(4)->format('Y-m-d');
      /*echo "current: ". $current_date."<br>";
      echo "last: ". $last_date;*/
       $dateRange = CarbonPeriod::create($current_date, $last_date);
       $dates = [];
       foreach($dateRange as $date) {
            $dates[] = $date->format('Y-m-d');
        }
        $currentDates= [];
       
        foreach($dates as $key=>$date) {
             $currentDates[$key]['date'] = \Carbon\Carbon::parse($date)->format('d');
             $currentDates[$key]['month'] = \Carbon\Carbon::parse($date)->format('M');
             $currentDates[$key]['day'] =Carbon::parse($date)->format('l');;
        }
      
        return ApiResponse::success('Success',$currentDates);
    }
  public function addPatient(Request $request){
   
      $this->validate($request, [
          'first_name' => 'required|string',
          'email' => 'required|email',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users',
          /*'last_name' => 'required|string',
          'gender' => 'required',
          'age' => 'required',
          'address' => 'required',
          'city' => 'required|string',
          'state' => 'required|string',
          'zip' => 'required|string',*/
          
       ]); 
       $data = $request->all();
       $otp = PaymentApp::otp();
       $detail = array(
        'name' => $data['first_name'],
        'username' => $data['first_name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        /*'address' => $data['address'],
      
        'address1' => (isset($data['address1'])) ? $data['address1'] : '', 
        'address2' => (isset($data['address2'])) ? $data['address2'] : '',  
        'city' => $data['city'],
        'state' => $data['state'],
        'zip' => $data['zip'],
        'neighbourhood' => (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '',
        'landmark' => (isset($data['landmark'])) ? $data['landmark'] : '',
        'latitude'  => $data['lat'],
        'longitude' => $data['lang'],
       
        'age' => $data['age'],
        'gender' => $data['gender'],*/
        'login_otp' => $otp,
        'login_type' => 3,
        'status' => 1,
       );
      
        User::create($detail);  
            
          $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$data['phone'].'&message=Dear '.$data['first_name'].', Welcome to the world of healthy Habits to keeps yourself Fit and aware. You are successfully registered with us. Team Nexx Wave -uDAUD';


          $smsUrl = str_replace(' ', '%20', $homepage);
            $curl = curl_init();

            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);
            
            return ApiResponse::success('Success',1);
    }
    public function getUserDetails($id){
      //echo $id;
      $users = User::where('parent',$id)->get();
      $data= [];
      if(count($users)!=0){
       
        foreach($users as $key=>$user){
            $carts = CartSession::where('user_id',$user->parent)->where('child',$user->id)->where('status',0)->get();
            $data[$key]['userid'] = $user->id; 
            $data[$key]['username'] = ucfirst($user->username);  
            $data[$key]['patient_code'] = $user->patient_code;
            $data[$key]['gender'] = $user->gender;
            $data[$key]['age'] = $user->age;
            $data[$key]['phone'] = $user->phone;
           
            if(count($carts)!= 0){

              $data[$key]['lab_name'] = (isset($carts[0]->lab_name)) ? $carts[0]->lab_name : '';
              $data[$key]['lab_id'] = (isset($carts[0]->lab_id)) ? $carts[0]->lab_id : '';
              $data[$key]['package']= 1;
              $p = [];
              foreach ($carts as $k => $cart) {
                $p[$k]['cartId'] = $cart->id;
                $p[$k]['product_title'] = $cart->product_title;
                $p[$k]['lab_name'] = $cart->lab_name;
                $p[$k]['lab_id'] = $cart->lab_id;
                $pro = Product::select('discount','price')->where('id',$cart->product_id)->first();
                $p[$k]['pricePer'] = 0;
                $p[$k]['price'] = $cart->price;
                $p[$k]['actual_price'] = $cart->actual_price;
                if($pro->discount != 0){
                  $pricePer= $pro->discount*100/$pro->price;
                  $p[$k]['pricePer']= round($pricePer);
                  $p[$k]['discount'] = number_format($pro->price);
                  $p[$k]['price'] = number_format($pro->price - $pro->discount); 
                  $p[$k]['actual_price'] = number_format($pro->price); 
                }  
              }
              $data[$key]['pack'] = $p;

            }else{
              $data[$key]['package']= 0;
              $data[$key]['pack'] = [];
            }
          }
        }
     
      return ApiResponse::success('Successss',$data);

    }

    public function createChildPatient(Request $request){
      $data = $request->all();
       $this->validate($request, [
          'username' => 'required|string',
          'gender' => 'required',
         // 'email' => 'email',
          'age' => 'required',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users',
       ]); 
      $userData = User::where('id',$data['parent'])->first();
      $detail = array(
        'username' => $data['username'],
        'email' => $data['email'],
        'age' => $data['age'],
        'gender' => $data['gender'],
        'login_type' => 3,
        'patient_code' => rand().'-PT-000'.rand(),
        'parent' => $data['parent'],
        'address' => $userData->address,
        'latitude' => $userData->latitude,
        'longitude' => $userData->longitude,
        'phone' => $data['phone'],
        'address1' => $userData->address1,
        'address2' => $userData->address2,
        'city' => $userData->city,
        'state' => $userData->state,
        'zip' => $userData->zip,
        'neighbourhood' => $userData->neighbourhood,
      );
   
      $id = User::create($detail)->id;
      $cust = array(
        'user_id' => $id,
        'age' => $data['age'],
        'phone' => $data['phone'],
        'gender' => $data['gender'],
      );
       Customer::create($cust);
       return ApiResponse::success('Successss',$detail);

    }
    public function updateChildPatient(Request $request){
      $data = $request->all();
       $this->validate($request, [
          'username' => 'required|string',
          'gender' => 'required',
          'age' => 'required',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
       ]); 
     
      $detail = array(
        'username' => $data['username'],
        'email' => (isset($data['email'])) ? $data['email'] : '',
        'age' => $data['age'],
        'gender' => $data['gender'],
        'phone' => $data['phone'],
        
      );

      User::where('id',$data['id'])->update($detail);
       return ApiResponse::success('Successss',$detail);

    }
    public function replaceLabProductDetail($labSlug,$packId,$child){
     $user = auth()->user();
     $lab =Vendor::where('slug',$labSlug)->first();
     $products    = Product:: where('id',$packId)->get();
     if($child == 0){
        
        $check = CartSession::where('user_id',$user->id)->where('status',0)->delete();
      }else{
        $userCid =User::where('patient_code',$child)->first();
        $child = $userCid->id;
        $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->delete();
      } 
      foreach($products as $key=>$a){
                  $product['user_id'] = $user->id; 
                  $product['child'] = $child; 
                  $product['product_id'] = $a->id; 
                  $product['product_title'] = $a->title; 
                  $product['actual_price'] = number_format($a->price) ;
                  $product['price'] = number_format($a->price) ;
                 
                  if($a->discount != 0){
                    $product['price'] = number_format($a->price - $a->discount); 
                  } 
                  $product['lab_id'] = $lab->user_id; 
                  $product['lab_name'] = $lab->title; 
                  $product['status'] = 0; 
                  CartSession::create($product);
                  return ApiResponse::success('success', '1');
      }            
    }
    public function addCartSession($labSlug,$packId,$child){
         $user = auth()->user();
         $lab =Vendor::where('slug',$labSlug)->first();
         $products    = Product:: where('id',$packId)->get();
         if($child == 0){
            
            $check = CartSession::where('user_id',$user->id)->where('status',0)->get();
          }else{
            $userCid =User::where('patient_code',$child)->first();
            $child = $userCid->id;
            $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->get();
            
          } 
         
              foreach($products as $key=>$a){
                  $product['user_id'] = $user->id; 
                  $product['child'] = $child; 
                  $product['product_id'] = $a->id; 
                  $product['product_title'] = $a->title; 
                  $product['actual_price'] = number_format($a->price) ;
                  $product['price'] = number_format($a->price) ;
                 
                  if($a->discount != 0){
                    $product['price'] = number_format($a->price - $a->discount); 
                  } 
                  $product['lab_id'] = $lab->user_id; 
                  $product['lab_name'] = $lab->title; 
                  $product['status'] = 0; 
                  //print_r($product);
                  if(count($check)==0){
                    CartSession::create($product);
                    return ApiResponse::success('success', '2');
                  }else if($check[0]->product_id == $a->id && $check[0]->lab_id == $lab->user_id ){

                      return ApiResponse::success('success', 1);
                  }else{
                     

                    $proData = array(
                        'pro_title' => $check[0]->product_title,
                        'lab_title' => $check[0]->lab_name,
                    );
                    return ApiResponse::success('success', $proData);
                    
                  }
              } 
              
            
    }
    public function creatSessionData(Request $request){
      $data = $request->all();
      $user = auth()->user();
      $carts = $data['cart'];
      $product = [];
      foreach($carts as $key=>$cart){
          $product['user_id'] = $user->id; 
          $product['child'] = $data['child']; 
          $product['product_id'] = $cart['id']; 
          $product['product_title'] = $cart['title']; 
          $product['actual_price'] = number_format($cart['price'] );
          $product['price'] = number_format($cart['price']) ;
          $product['lab_id'] = $data['labsId'] ;
          $product['lab_name'] = $data['labName'] ;
          $product['package'] = 1 ;
          $product['status'] = 0; 
          if($data['child'] == 0){
            $check = CartSession::where('user_id',$user->id)->where('status',0)->get();
          }else{
            $userCid =User::where('patient_code',$data['child'])->first();
            $child = $userCid->id;
            $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->where('package',1)->get();
          } 
           if(count($check)==0){
              CartSession::create($product);
           }
          //print_r($product);
          //CartSession::create($product);
      }
        return ApiResponse::success('success', '2');


    }

    public function getSelectedLab(request $request){
      $data = $request->all();
      $user = auth()->user();
      $carts = $data['cart'];
        if($data['child'] == 0){
            $check = CartSession::where('user_id',$user->id)->where('status',0)->get();
          }else{
            $userCid =User::where('patient_code',$data['child'])->first();
            $child = $userCid->id;
            $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->where('package',1)->get();
          } 
        
      
      print_r($data);

    }

    public function replaceProductDetail($slug,$labId,$child){
      $lab =Vendor::where('user_id',$labId)->first();
      $products    = Product:: where('slug',$slug)->get();
      $user = auth()->user();
      $labName = $lab->title;
      $product= [];

        if(count($products)!=0){
          if($child == 0){
            $check = CartSession::where('user_id',$user->id)->where('status',0)->delete();
          }else{
            $userCid =User::where('patient_code',$child)->first();
            $child = $userCid->id;
            $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->delete();
          } 
          foreach($products as $key=>$a){
                  $product['user_id'] = $user->id; 
                  $product['child'] = $child; 
                  $product['product_id'] = $a->id; 
                  $product['product_title'] = $a->title; 
                  $product['actual_price'] = number_format($a->price );
                  $product['price'] = number_format($a->price) ;
                  if($a->discount != 0){
                    $product['price'] = number_format($a->price - $a->discount); 
                  } 
                  $product['lab_id'] = $labId; 
                  $product['lab_name'] = $labName; 
                  $product['status'] = 0; 
                  CartSession::create($product);
                  return ApiResponse::success('success', '1');
                  }
              } 
          
    }
    public function labProductDetail($slug,$labId,$child){
        
        $lab =Vendor::where('user_id',$labId)->first();
        $products    = Product:: where('slug',$slug)->get();
        $user = auth()->user();
        $labName ='';
        if($lab){
          $labName = $lab->title;  
        }
        $product= [];

        if(count($products)!=0){
          if($child == 0){
            //$check = CartSession::where('user_id',$user->id)->where('product_id',$products[0]->id)->where('status',0)->get();
            $check = CartSession::where('user_id',$user->id)->where('status',0)->get();
          }else{
            $userCid =User::where('patient_code',$child)->first();
            $child = $userCid->id;
            //$check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('product_id',$products[0]->id)->where('status',0)->get();
            $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->get();
            
          }  
          foreach($products as $key=>$a){
                  $product['user_id'] = $user->id; 
                  $product['child'] = $child; 
                  $product['product_id'] = $a->id; 
                  $product['product_title'] = $a->title; 
                  $product['actual_price'] = number_format($a->price) ;
                  $product['price'] = number_format($a->price) ;
                  if($a->discount != 0){
                    $product['price'] = number_format($a->price - $a->discount); 
                  } 
                  $product['lab_id'] = $labId; 
                  $product['lab_name'] = $labName; 
                  $product['status'] = 0; 
                   if(count($check)==0){
                    CartSession::create($product);
                    return ApiResponse::success('success', '2');
                  }else{
                    if($check[0]->product_id == $a->id && $check[0]->lab_id == $labId ){
                     // echo $check[0]->lab_id.'--'.$labId;
                      return ApiResponse::success('success', 1);
                    }else{
                      $proData = array(
                          'pro_title' => $check[0]->product_title,
                          'lab_title' => $check[0]->lab_name,
                      );
                      return ApiResponse::success('success', $proData);
                      
                    }
                   
                  }
              } 
           
          
        }
    }
    public function getCartCount(Request $request){
       $auth = auth()->user();
       if($auth){
          $carts = CartSession::where('user_id',$auth->id)->where('status',0)->get()->count();
          return ApiResponse::success('success', $carts);
       }
    }
    
    public function getUserPackages(Request $request){
       $auth = auth()->user();
       $totalPrice = 0;
       $totalDiscount = 0;
       $totalActualPrice = 0;
       $data= [];
       $product= [];
       $carts = CartSession::where('user_id',$auth->id)->where('child',0)->where('status',0)->get();
       
       if(count($carts)!=0){
       foreach ($carts as $key => $cart) {
            
           $product[$key]['packageType'] = $cart->package;
           $product[$key]['patient_code'] = 0;
          
           $product[$key]['cartId'] = $cart->id;
           $product[$key]['product_title'] = $cart->product_title;
           $pro = Product::select('discount','price')->where('id',$cart->product_id)->first();

           $product[$key]['pricePer'] = 0;
           $product[$key]['price'] = number_format($pro->price);
           $product[$key]['actual_price'] = number_format($pro->price);
           $totalActualPrice += $pro->price;
           if($pro->discount != 0){
              $pricePer= $pro->discount*100/$pro->price;
              $product[$key]['pricePer']= round($pricePer);
              $product[$key]['discount'] = number_format($pro->price);
              $product[$key]['price'] = number_format($pro->price - $pro->discount); 
              $product[$key]['actual_price'] = number_format($pro->price); 
              $p = $pro->price - $pro->discount;
              $totalPrice += $p;
              $totalDiscount += $pro->discount;
            }else{
              $totalPrice += $pro->price;
              $totalDiscount += $pro->discount;
            }
        }
         $data['package'] =  $product;
        }else{
         $data['package'] =  0;
        }
        if($totalPrice <= 700){
          $totalPrice +='200';
        $convenience['userDetails']['convenience']='200';
        }
       $data['userDetails']['packageType'] = (isset($carts[0]->package)) ? $carts[0]->package : '0';
       $data['userDetails']['userid'] = $auth->id;
       $data['userDetails']['username'] = ucfirst($auth->username);
       $data['userDetails']['lab_name'] = (isset($carts[0]->lab_name)) ? $carts[0]->lab_name : '';
       $data['userDetails']['lab_id']   = (isset($carts[0]->lab_id)) ? $carts[0]->lab_id : '';  
    
       $data['userDetails']['totalPrice'] =  number_format($totalPrice);
       $data['userDetails']['totalDiscount'] =  number_format($totalDiscount);
       $data['userDetails']['totalActualPrice'] =  number_format($totalActualPrice);
      
       return ApiResponse::success('Successss',$data);
    }
   
   public function getAllTestLab(Request $request){
       $auth = auth()->user();
       $data = CartSession::where('status',0)->where('user_id',$auth->id)->where('lab_id',0)->get();
       if(count($data)!=0){
           return ApiResponse::success('Success',1);
       }else{
            return ApiResponse::success('Success',2);
       }

   }
   public function getCartPrices(Request $request){
       $auth = auth()->user();
       $totalPrice = 0;
       $convenience = 0;
       $totalDiscount = 0;
       $totalActualPrice = 0;
       $carts = CartSession::where('user_id',$auth->id)->where('status',0)->get();
       foreach($carts as $cart){
         $pro = Product::where('id',$cart->product_id)->first();
         $totalActualPrice += $pro->price;
         $totalDiscount += $pro->discount;
         $p = $pro->price;
         if($pro->discount!=0){
          $p = $pro->price - $pro->discount;
          
         }
         $totalPrice += $p;
       }
        if($totalPrice <= 700){
          $totalPrice +='200';
          $convenience = '200';
        }
       $data= array(
          'totalPrice' => number_format($totalPrice),
          'convenience' => $convenience,
          'totalDiscount' => number_format($totalDiscount),
          'totalActualPrice' => number_format($totalActualPrice),
       );
       return ApiResponse::success('Successss',$data);
       
   }
   public function addCustomTest(Request $request){
      $data = $request->all();
      $auth = auth()->user();
      $product = [];
      $child = $data['child'];
      $details = Product::where('id',$data['productId'])->get();



      if($child == 0){
        CartSession::where('child',0)->where('user_id',$auth->id)->where('status',0)->where('package',0)->delete();
        $check = CartSession::where('product_id',$data['productId'])->where('user_id',$auth->id)->where('child',0)->where('status',0)->get();
        $checkCat = CartSession::where('user_id',$auth->id)->where('child',0)->where('status',0)->where('package',1)->get();
        if(count( $checkCat)!=0){
         

          foreach($checkCat as $cat){
             $pro = Product::where('id',$cat->product_id)->get();
          
            if($details[0]->categories == $pro[0]->categories)
            {
               CartSession::where('id',$cat->id)->delete();
            }

          }
        }

      }else{
        $userCid =User::where('patient_code',$child)->first();
        $child = $userCid->id;
        CartSession::where('child',$child)->where('user_id',$auth->id)->where('status',0)->where('package',0)->delete();
        $check = CartSession::where('product_id',$data['productId'])->where('user_id',$auth->id)->where('child',$child)->where('status',0)->get();
        $checkCat = CartSession::where('user_id',$auth->id)->where('child',$child)->where('status',0)->where('package',1)->get();
        if(count( $checkCat)!=0){
          foreach($checkCat as $cat){
            if($details[0]->categories == $cat->categories)
            {
               CartSession::where('product_id',$data['productId'])->where('child',$child)->where('user_id',$auth->id)->where('status',0)->where('package',1)->delete();
            }

          }
        }
      }
      
      if(count($check)==0){
      foreach($details as $detail){
        $product['user_id'] = $auth->id;
        $product['child'] = $child;
        $product['product_id'] = $detail->id;
        $product['product_title'] = $detail->title;
        $product['price'] = number_format($detail->price - $detail->discount);
        $product['actual_price'] = number_format($detail->price);
        $product['package'] = 1;
        $product['status'] = 0;
        $lab =CartSession::where('user_id',$auth->id)->where('package',1)->where('child', $child)->where('status',0)->first();
        $product['lab_id'] =(isset($lab->lab_id)) ? $lab->lab_id : '' ;
        $product['lab_name'] =(isset($lab->lab_name)) ? $lab->lab_name : '';

         CartSession::create($product);
         return ApiResponse::success('Successss',1);
      }
    }else{
      return ApiResponse::success('Successss',2);
    }

   }
   public function getCustomCartTests($id){
       $auth = auth()->user(); 

       if($id != 0){
          $userCid =User::where('patient_code',$id)->first();
          
          $c = $userCid->id;
          $carts = CartSession::where('package',1)->where('user_id',$auth->id)->where('child',$c)->where('status',0)->get();
       }else{
          $child = 0;
          $carts = CartSession::where('package',1)->where('user_id',$auth->id)->where('child',0)->where('status',0)->get();
       }
     
       $product = [];
       foreach($carts as $key=>$cart){
          $product[$key]['product_title'] = $cart->product_title; 
          $pro = Product::select('price','discount')->where('id',$cart->product_id)->first();
          $product[$key]['price'] = number_format($pro->price);
          if($pro->discount!=0){
            $product[$key]['price'] = number_format($pro->price - $pro->discount);
          } 
          $product[$key]['product_id'] = $cart->product_id; 
          $product[$key]['cart_id'] = $cart->id; 
        }
       return ApiResponse::success('Successss',$product);
   }
    public function deleteCartPackage($id){
      CartSession::where('id',$id)->delete();
      return ApiResponse::success('Successss',$id);

    }
    public function updateCart(Request $request){
      $auth = auth()->user();
      $data = $request->all();
      if($data['offer'] == 1){
        $offerCount = Offer::where('user_id',$auth->id)->get()->count();
        if($offerCount == 0){
          $detail = array(
           'user_id' => $auth->id,
          );
        Offer::create($detail);
        }
      }
     // print_r($data);
      CartSession::where('user_id',$auth->id)->where('status',0)->update(['slot_id' => $data['slotId'],'booking_date' =>$data['BookedDate']]);
      if($data['mode'] == 1){
          return ApiResponse::success('Success',1);
      }
      if($data['mode'] == 2){
        $postOrderId = 'uDAUD-'.rand().'-'.rand();
        $detail = array(
          'orderId' => $postOrderId,
          'orderAmount' => $data['totalPrice'],
          'paymentMode' => 'COD',
          'referenceId' =>  '123mm',
          'txStatus' => 'SUCCESS',
          'txTime' => Carbon::now(),
          'status' => 1,
       );
        Payment::create($detail);
         CartSession::where('status',0)->where('user_id',$auth->id)->update(['status'=>1,'order_id'=> $postOrderId]);
         
          $payment = Payment::where('orderId',$postOrderId)->first();
          $cart_session = CartSession::where('order_id',$postOrderId)->where('child',0)->get();
          if($cart_session){
            $price = 0;
            foreach ($cart_session as $key => $value) {
                $bdate = date_create($value['booking_date']);
                $b =  date_format($bdate, 'Y-m-d');
                 $customer = Customer::where('user_id',$value['user_id'])->first();
                $user = User::find($value['user_id']);
                $product[$key]['title'] = $value['product_title']; 
                $product[$key]['price'] = $value['price']; 
                $booking['vendor_id'] = $value['lab_id'];
                $booking['user_id'] = $value['user_id'];
                $booking['customer_id'] = $customer->id;
                $booking['address'] = $user->address;
                $booking['time_slot'] = $value['slot_id'];
                $booking['payment_mode'] = $payment->paymentMode;
                $booking['payment_status'] = '1';
                $price += intval(preg_replace('/[^\d.]/', '', $value['price']));
                $booking['price'] = $price;
                $booking['sub_total'] = $price;
                $booking['total_price'] = $price;
                $booking['booking_date'] = $b;
                $booking['city'] = $user->city;
                $booking['state'] = $user->state;
                $booking['zip'] = $user->zip;
                $booking['lat'] = $user->latitude;
                $booking['lang'] = $user->longitude;
                $booking['address1'] = (isset($user->address1)) ? $user->address1 : '';
                $booking['address2'] = (isset($user->address2)) ? $user->address2 : '';
                $booking['neighbourhood'] = (isset($user->neighbourhood)) ? $user->neighbourhood : '';
                $booking['landmark'] = (isset($user->landmark)) ? $user->landmark : '';
                $booking['status'] = 1;
                $booking['booking_status'] = 0;
                $booking['order_id'] = $postOrderId;
                $booking['parent_id'] = $auth->id;
              }
            $id = Booking::create($booking)->id;
            $usersms = User::find($booking['user_id']);
            $slosms = AgentSlots::find($booking['time_slot']);
            $vendorSms = User::find($booking['vendor_id']);
            $emailBooking = Booking::find($id);
            $emailCustomer = Customer::where('user_id',$booking['user_id'])->first();
               $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendorSms->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);
            $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$usersms->phone.'&message=Dear '. $usersms->username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$booking['booking_date'].' time '.$slosms->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
          $smsUrl = str_replace(' ', '%20', $homepage);
            $curl = curl_init();

            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);
            foreach ($product as $value) {
              $bookingProduct['booking_id'] = $id;
              $bookingProduct['title'] = $value['title'];
              $bookingProduct['price'] = $value['price'];
              CustomerBookingProduct::create($bookingProduct);
            }
$test = '';
$temp = '';
 foreach($product as $cart){
    $test .= "<tr><th scope='row' style='width: 33%;border-right: 1px solid;'>Test</th><td style='border-right: 1px solid;'>".$cart['title']."</td><td>".$cart['price']."</td></tr>";
  }
  $temp = '<html><head>';
  $temp .= "<style>.table td, .table th {padding: 5px;vertical-align: middle;border-top: 1px solid #000000;font-size: 15px;}</style>";
  $temp .= '</head><body>';
  $temp .= "<div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
  $temp .= "<div style='color:#00008B;font-size:20px;font-weight:600'>Dear Customer, Welcome to uDAUD and the world of healthy Habits to keeps yourself Fit and aware. Your appointment for the blood/other Sample collection is received with the following details.</div>";
  $temp .= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: block;'>";
  $temp .= "<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 90%;'>";
  $temp .= "<h5 style='margin: 0px 0 0px;'>".$vendorSms->username."</h5>";
  $temp .= "<p style='margin: 10px 0 0;font-size: 15px;line-height: 20px;'>".$vendorSms->address."</p>";
  $temp .= '</td>';           
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;width: 10%;'><img width='auto' src='https://udaudindia.com/_nuxt/img/logo.61b22fc.png' style='width: 120px;'></td>";            
  $temp .= '</tr></tbody></table>';
  $temp .= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 20px 0px;display: inline-table;margin: 10px 0 10px;'>";
  $temp .= '<tbody><tr>';
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;'>".$emailBooking->booking_date."</td>";         
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: center;'>Home Collection Slip</td>";         
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: right;'>Page 1 of 1</td>";        
  $temp .= '</tr></tbody></table>';
  $temp .= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;'>";
  $temp .= '<tbody>';
  $temp .= '<tr>';
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
  $temp .= "<table class='table' style='width: 100%;border-spacing: 0px;'>";
  $temp .= '<tbody><tr><th scope="row">Date</th>';
  $temp .= "<td>".$emailBooking->booking_date."</td>";
  $temp .= "<th scope='row' style='border: 0;'>Collection Time</th>";
  $temp .= "<td style='border: 0;'>".$slosms->slot."</td>";
  $temp .= '</tr>';
  $temp .= "<tr><th scope='row'>Name of Patient</th><td>".$usersms->username."</td><th scope='row'>Sex</th><td>".$emailCustomer->gender."</td>";
  $temp .= '</tr>';
  $temp .= "<tr style='position:relative;'>";
  $temp .= '<th scope="row">Age</th>';
  $temp .= "<td>".$emailCustomer->age."Yrs</td>";
  $temp .= '</tr>';
  $temp .= '<tr>';
  $temp .= '<th scope="row">Address</th>';
  $temp .= '<td colspan="3">'.$usersms->address.' '.$usersms->city.'</td>';
  $temp .= '</tr>';
  $temp .= '<tr>';
  $temp .= '<th scope="row">Mobile</th>';
  $temp .= '<td colspan="3">'.$usersms->phone.'</td>';
  $temp .= '</tr>';
  $temp .= '<tr>';
  $temp .= '<th scope="row">Paid Mode</th>';
  $temp .= '<td colspan="3">'.$usersms->payment_mode.'</td>';
  $temp .= '</tr>';
  $temp .= '</tbody>';
  $temp .= '</table>';
  $temp .= '</td>';                    
  $temp .= '</tr>';     
  $temp .= '</tbody>';
  $temp .= '</table>';
  $temp .= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
  $temp .= '<tbody>';
  $temp .= '<tr>';
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
  $temp .= "<table class='table' style='width: 100%;border-spacing: 0px;'>";
  $temp .= '<tbody>'.$test.'<tr>';
  $temp .= "<th scope='row' colspan='2' style='width: 33%;border-right: 1px solid;text-align: right;font-size: 21px;padding: 0px 10px 0px 0px;'>Total</th>";
  $temp .= "<td style='font-weight: bold;font-size: 21px;'>".$emailBooking->total_price."/-</td>";
  $temp .= '</tr>';
  $temp .= '</tbody>';
  $temp .= '</table>';
  $temp .= '</td>';                    
  $temp .= '</tr>';     
  $temp .= '</tbody>';
  $temp .= '</table>';
  $temp .= "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
  $temp .= '<tbody>';
  $temp .= '<tr>';
  $temp .= "<td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
  $temp .= "<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
  $temp .= '<li>You will receive a confirmation SMS after booking an appointment.</li>';
  $temp .= '<li>Reminder SMS will be shared half an hour prior to your confirmed appointment time.</li>';
  $temp .= '</ul>';
  $temp .= "<h4 style='margin: 15px 0 5px;font-size:16px'>Fasting Health Check-up pre-requisites:</h4>";
  $temp .= "<ul style='padding: 0 15px;margin: 0;list-style: auto; font-size:14px'>";
  $temp .= '<li>10-12 hours of overnight fasting is required prior to the day of the blood collection.</li>';
  $temp .= '<li>Avoid alcohol at least 24 hours before the blood collection appointment.</li>';
  $temp .= '<li>Avoid Tea/Coffee/Other beverages before the blood collection. Water intake is allowed.</li>';
  $temp .= '</ul>';
  $temp .= "<h4 style='margin: 15px 0 0px; font-size:16px'>Kindly confirm the list of your tests to be conducted with our scientific officer in one appointment as there will be no repeat visit for pending tests.</h4>";
  $temp .= "<h4 style='margin: 0px 0 5px; font-size:16px'>Disclaimer:</h4>";
  $temp .= "<p style='font-size:14px;margin:0'>The Health Check-up requires you to give your blood sample. The Following associated rare incidences may occur during and after blood collection:</p>";
  $temp .= "<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
  $temp .= '<li>Discomfort, swelling, redness or dizziness while or immediately after blood collection, the phlebotomist would know how to deal with the same. Request not to panic. Inform our technical expert or pathology lab immediately for support.</li>';
  $temp .= '<li>In case your blood sample gets lysed, your selected pathology lab will approach you for a fresh sample collection, the same day or another mutually decided date.</li>';
  $temp .= '<li>All the protocols followed are as per guidelines . Factors such as physiological disturbances, fever, dehydration, haemolysis, etc. can cause variation in reported results.</li>';
  $temp .= '</ul>';
  $temp .= "<h4 style='margin: 15px 0 5px; font-size:16px'>Request you to share a valid identity proof for the medical test. Acceptable identity proofs are: PAN Card, Driving Licence, Passport, Adhaar Card, Voter ID card.</h4>";
  $temp .= "<p style='font-size:14px;margin:0'>In case of any queries, please write to us at info@udaudindia.com (Mon-Sat, 9am to 5pm)</p>";
  $temp .= "<h4 style='margin: 15px 0 5px; font-size:16px'>We hope your experience is positive with us and look forward towards improving our services and serving you always.</h4>";
  $temp .= '</td>';                      
  $temp .= '</tr>';        
  $temp .= '</tbody>';
  $temp .= '</table>';
  $temp .= '</div>';
  $temp .='<p>CONFIDENTIALITY NOTICE: If you have received this email in error,</br>please immediately notify the sender by e-mail at the address shown.</br>This email transmission may contain confidential information.</br>This information is intended only for the use of the individual(s) or entity to whom it is intended even if addressed incorrectly. Please delete it from your files if you are not the intended recipient. Thank you for your compliance.</br>Copyright (c) 2021  Nexx Wave Health Solutions Pvt Ltd .<br> ==================================================-- </p>';
  $temp .= '</body></html>';
  $sub = 'Your Appointment Schedule - Home Sample collection.';
  if($usersms->email){
    EmailFrontend::sendEmailFrontend('info@udaudindia.com',$sub,$temp);
  }
          }
          $childCart = CartSession::where('order_id',$postOrderId)->where('child','!=',0)->get();
          if($childCart){
          foreach ($childCart as $key => $value) 
          {
              $productChild['title'] = $value['product_title']; 
              $productChild['price'] = $value['price']; 
              $productChild['user_id'] = $value['child']; 
              $productChild['order_id'] = $postOrderId;
              $productChild['lab_id'] = $value['lab_id'];
              CustomerBookingProduct::create($productChild);
          }
          $bookingProduct = CustomerBookingProduct::where('order_id',$postOrderId)->groupBy('user_id')->get();
          foreach ($bookingProduct as $key => $value) 
          {
              $cart = CartSession::where('order_id',$postOrderId)->where('child',$value['user_id'])->first();
              $bdate = date_create($cart->booking_date);
              $b =  date_format($bdate, 'Y-m-d');
              $customer = Customer::where('user_id',$value['user_id'])->first();
              $user = User::find($value['user_id']);
              $booking['vendor_id'] = $value['lab_id'];
              $booking['user_id'] = $value['user_id'];
              $booking['customer_id'] = $customer->id;
              $booking['address'] = $user->address;
              $booking['time_slot'] = $cart->slot_id;
              $booking['payment_mode'] = $payment->paymentMode;
              $booking['payment_status'] = '1';
              $price = intval(preg_replace('/[^\d.]/', '', $payment->orderAmount));
              $booking['price'] = $price;
              $booking['sub_total'] = $price;
              $booking['total_price'] = $price;
              $booking['booking_date'] = $b;
              $booking['city'] = $auth->city;
              $booking['state'] = $auth->state;
              $booking['zip'] = $auth->zip;
              $booking['lat'] = $auth->latitude;
              $booking['lang'] = $auth->longitude;
              $booking['address1'] = (isset($auth->address1)) ? $auth->address1 : '';
              $booking['address2'] = (isset($auth->address2)) ? $user->address2 : '';
              $booking['neighbourhood'] = (isset($auth->neighbourhood)) ? $auth->neighbourhood : '';
              $booking['landmark'] = (isset($auth->landmark)) ? $auth->landmark : '';
              $booking['status'] = 1;
              $booking['order_id'] = $postOrderId;
              $booking['parent_id'] = $auth->id;
               $booking['child_id'] = $value['user_id'];
              $booking['booking_status'] = 0;
              $id = Booking::create($booking)->id;
              $usersms = User::find($value['user_id']);
              $slosms = AgentSlots::find($cart->slot_id);
              $vendorSms = User::find($value['lab_id']);
                $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendorSms->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);
              $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$usersms->phone.'&message=Dear '. $usersms->username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$b.' time '.$slosms->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $homepage);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);

              $response = curl_exec($curl);
              $CustomerBookingProduct = CustomerBookingProduct::where('order_id',$postOrderId)->where('user_id',$value['user_id'])->update(['booking_id' => $id]);
          }
        }
           return ApiResponse::success('Success',2); 
      }
      
      
    }

    public function payment ($pay){
       $auth = auth()->user();
       $mode = "PROD"; //<------------ Change to TEST for test server, PROD for production
       
         if ($mode == "PROD") {
          $url = "https://www.cashfree.com/checkout/post/submit";
        } else {
          //echo "string";
          $url = "https://test.cashfree.com/billpay/checkout/post/submit";
        }  
         $secretKey = "66bb9c5c6e199180e7fbff382a877aa71210d93e";
         $postOrderId = 'uDAUD-'.rand().'-'.rand();
          $postData = array( 
          "appId" => '1615949011ad7f6a60afd08388495161', 
           "paymentMode" => 'upi',
          "orderId" => $postOrderId, 
          "orderAmount" => $pay, 
          "orderCurrency" => 'INR', 
          "orderNote" => 'Booking', 
          "customerName" => $auth->username, 
         
          "customerPhone" => $auth->phone, 
          "customerEmail" => $auth->email,
          "returnUrl" => 'https://udaudindia.com/api/response', 
         // "returnUrl" => 'http://localhost:8000/api/response', 
          "notifyUrl" => ''
         
         
        );
        ksort($postData);
        $signatureData = "";
        foreach ($postData as $key => $value){
            $signatureData .= $key.$value;
        }
        

        
        $s = hash_hmac('sha256', $signatureData, $secretKey,true);

        $sing = base64_encode($s); 
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array( 
                  "signature" => $sing, 
                  "paymentMode" => 'upi',
                  "orderNote" => 'Booking',
                  "orderCurrency" => 'INR', 
                  "customerName" => $auth->username,
                  "customerPhone" => $auth->phone, 
                  "customerEmail" => $auth->email,
                  "orderAmount" => $pay, 
                  "notifyUrl" => '', 
                  // "returnUrl" => 'http://localhost:8000/api/response', 
                  "returnUrl" => 'https://udaudindia.com/api/response', 
                  "appId" => '1615949011ad7f6a60afd08388495161', 
                  "orderId" => $postOrderId,
                  "notifyUrl" => '',
                )
        );
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        CartSession::where('status',0)->where('user_id',$auth->id)->update(['status'=>1,'order_id'=> $postOrderId]);
         // $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$auth->phone.'&message=Dear Customer, Welcome to uDAUD. Your booking has been done. Live Healthy with uDAUD. Team Nexx Wave -uDAUD';


         //  $smsUrls = str_replace(' ', '%20', $homepage);
         //    $curls = curl_init();

         //    curl_setopt_array($curls, [
         //    CURLOPT_URL => $smsUrls,
         //    CURLOPT_RETURNTRANSFER => true,
         //    CURLOPT_ENCODING => "",
         //    CURLOPT_MAXREDIRS => 10,
         //    CURLOPT_TIMEOUT => 30,
         //    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         //    CURLOPT_CUSTOMREQUEST => "GET",
         //    ]);

         //    $responses = curl_exec($curls);
        return  $result;

    }

    public function response(Request $request){
       $data = $request->all();
       $auth = auth()->user();
       if($data['txStatus'] == 'SUCCESS'){
         CartSession::where('status',0)->where('order_id',$data['orderId'])->update(['status'=>1]);
         $status = 1;
       }else{

         CartSession::where('status',0)->where('order_id',$data['orderId'])->update(['status'=>3]);
         $status = 3;
       }
       $detail = array(
          
          'orderId' => $data['orderId'],
          'orderAmount' => $data['orderAmount'],
          'referenceId' => $data['referenceId'],
          'paymentMode' => $data['paymentMode'],
          'txStatus' => $data['txStatus'],
          'txTime' => $data['txTime'],
          'status' => $status,
       );
        Payment::create($detail);
        if($data['txStatus'] == 'SUCCESS'){
          $payment = Payment::where('orderId',$data['orderId'])->first();
          $cart_session = CartSession::where('order_id',$data['orderId'])->where('child',0)->get();
          if($cart_session){
            $price = 0;
            foreach ($cart_session as $key => $value) {
                $bdate = date_create($value['booking_date']);
                $b =  date_format($bdate, 'Y-m-d');
                $user = User::find($value['user_id']);
                $customer = Customer::where('user_id',$value['user_id'])->first();
                $product[$key]['title'] = $value['product_title']; 
                $product[$key]['price'] = $value['price']; 
                $booking['vendor_id'] = $value['lab_id'];
                $booking['user_id'] = $value['user_id'];
                $booking['customer_id'] = $customer->id;
                $booking['address'] = $user->address;
                $booking['time_slot'] = $value['slot_id'];
                $booking['payment_mode'] = $payment->paymentMode;
                $booking['payment_status'] = '1';
                $price += intval(preg_replace('/[^\d.]/', '', $value['price']));
                $booking['price'] = $price;
                $booking['sub_total'] = $price;
                $booking['total_price'] = $price;
                $booking['booking_date'] = $b;
                $booking['city'] = $user->city;
                $booking['state'] = $user->state;
                $booking['zip'] = $user->zip;
                $booking['lat'] = $user->latitude;
                $booking['lang'] = $user->longitude;
                $booking['address1'] = (isset($user->address1)) ? $user->address1 : '';
                $booking['address2'] = (isset($user->address2)) ? $user->address2 : '';
                $booking['neighbourhood'] = (isset($user->neighbourhood)) ? $user->neighbourhood : '';
                $booking['landmark'] = (isset($user->landmark)) ? $user->landmark : '';
                $booking['status'] = 1;
                $booking['booking_status'] = 0;
                $booking['order_id'] = $value['order_id'];
                $booking['parent_id'] = $auth->id;
              }
              $id = Booking::create($booking)->id;
              $usersms = User::find($booking['user_id']);
              $slosms = AgentSlots::find($booking['time_slot']);
              $vendorSms = User::find($booking['vendor_id']);
              $emailCustomer = Customer::where('user_id',$booking['user_id'])->first();
              $emailBooking = Booking::find($id);
              $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendorSms->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);
              $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$usersms->phone.'&message=Dear '. $usersms->username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$booking['booking_date'].' time '.$slosms->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $homepage);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);
              $response = curl_exec($curl);
            foreach ($product as $value) {
              $bookingProduct['booking_id'] = $id;
              $bookingProduct['title'] = $value['title'];
              $bookingProduct['price'] = $value['price'];
              CustomerBookingProduct::create($bookingProduct);
            }
            $test = '';
            $temp = '';
 foreach($product as $cart){
    $test .= "<tr><th scope='row' style='width: 33%;border-right: 1px solid;'>Test</th><td style='border-right: 1px solid;'>".$cart['title']."</td><td>".$cart['price']."</td></tr>";
  }

        $temp ="<html><head><style>.table td, .table th {padding: 5px;vertical-align: middle;border-top: 1px solid #000000;font-size: 15px;}</style></head><body>";
        $temp .="<div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
        $temp .="<div style='color:#00008B;font-size:20px;font-weight:600'>Dear Customer, Welcome to uDAUD and the world of healthy Habits to keeps yourself Fit and aware. Your appointment for the blood/other Sample collection is received with the following details.</div>";
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: block;'>";
        $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 90%;'><h5 style='margin: 0px 0 0px;'>".$vendorSms->username."</h5>";
        $temp .="<p style='margin: 10px 0 0;font-size: 15px;line-height: 20px;'>".$vendorSms->address."</p></td>";            
        $temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;width: 10%;'><img width='auto' src='https://udaudindia.com/_nuxt/img/logo.61b22fc.png' style='width: 120px;'></td>";            
        $temp .='</tr> </tbody></table>';
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 20px 0px;display: inline-table;margin: 10px 0 10px;'>";
        $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;'>".$emailBooking->booking_date."</td>";            
        $temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: center;'>Home Collection Slip</td>";            
        $temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: right;'>Page 1 of 1</td>";            
        $temp .='</tr></tbody></table>';
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;'>";
        $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
        $temp .="<table class='table' style='width: 100%;border-spacing: 0px;'>";
        $temp .="<tbody><tr><th scope='row'>Date</th><td>".$emailBooking->booking_date."</td><th scope='row' style='border: 0;'>Collection Time</th><td style='border: 0;'>".$slosms->slot."</td></tr>";
        $temp .='<tr><th scope="row">Name of Patient</th><td>'.$usersms->username.'</td><th scope="row">Sex</th><td>'.$emailCustomer->gender.'</td>';
        $temp .='</tr><tr style="position:relative;"><th scope="row">Age</th><td>'.$emailCustomer->age.'Yrs</td></tr>';
        $temp .='<tr><th scope="row">Address</th><td colspan="3">'.$usersms->address.' '.$usersms->city.'</td></tr><tr><th scope="row">Mobile</th><td colspan="3">'.$usersms->phone.'</td>';
        $temp .='</tr><tr><th scope="row">Paid Mode</th><td colspan="3">'.$usersms->payment_mode.'</td></tr></tbody></table></td></tr></tbody></table>';
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
        $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
        $temp .="<table class='table' style='width: 100%;border-spacing: 0px;'><tbody>".$test."<tr>";
        $temp .="<th scope='row' colspan='2' style='width: 33%;border-right: 1px solid;text-align: right;font-size: 21px;padding: 0px 10px 0px 0px;'>Total</th>";
        $temp .="<td style='font-weight: bold;font-size: 21px;'>".$emailBooking->total_price."/-</td></tr></tbody></table></td></tr></tbody></table>";
        $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
        $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
        $temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'><li>You will receive a confirmation SMS after booking an appointment.</li>";
        $temp .='<li>Reminder SMS will be shared half an hour prior to your confirmed appointment time.</li></ul>';
        $temp .="<h4 style='margin: 15px 0 5px;font-size:16px'>Fasting Health Check-up pre-requisites:</h4><ul style='padding: 0 15px;margin: 0;list-style: auto; font-size:14px'>";
        $temp .='<li>10-12 hours of overnight fasting is required prior to the day of the blood collection.</li><li>Avoid alcohol at least 24 hours before the blood collection appointment.</li>';
        $temp .='<li>Avoid Tea/Coffee/Other beverages before the blood collection. Water intake is allowed.</li></ul>';
        $temp .="<h4 style='margin: 15px 0 0px; font-size:16px'>Kindly confirm the list of your tests to be conducted with our scientific officer in one appointment as there will be no repeat visit for pending tests.</h4>";
        $temp .="<h4 style='margin: 0px 0 5px; font-size:16px'>Disclaimer:</h4>";
        $temp .="<p style='font-size:14px;margin:0'>The Health Check-up requires you to give your blood sample. The Following associated rare incidences may occur during and after blood collection:</p>";
        $temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
        $temp .='<li>Discomfort, swelling, redness or dizziness while or immediately after blood collection, the phlebotomist would know how to deal with the same. Request not to panic. Inform our technical expert or pathology lab immediately for support.</li>';
        $temp .='<li>In case your blood sample gets lysed, your selected pathology lab will approach you for a fresh sample collection, the same day or another mutually decided date.</li>';
        $temp .='<li>All the protocols followed are as per guidelines . Factors such as physiological disturbances, fever, dehydration, haemolysis, etc. can cause variation in reported results.</li></ul>';
        $temp .="<h4 style='margin: 15px 0 5px; font-size:16px'>Request you to share a valid identity proof for the medical test. Acceptable identity proofs are: PAN Card, Driving Licence, Passport, Adhaar Card, Voter ID card.</h4>";
        $temp .="<p style='font-size:14px;margin:0'>In case of any queries, please write to us at info@udaudindia.com (Mon-Sat, 9am to 5pm)</p>";
        $temp .="<h4 style='margin: 15px 0 5px; font-size:16px'>We hope your experience is positive with us and look forward towards improving our services and serving you always.</h4>";
        $temp .='</td></tr></tbody></table></div>';
        $temp .='------------------------------------------------------------------------------<br>';
        $temp .='<p>CONFIDENTIALITY NOTICE: If you have received this email in error,<br>please immediately notify the sender by e-mail at the address shown. <br> This email transmission may contain confidential information. <br> This information is intended only for the use of the individual(s) or entity to whom it is intended even if addressed incorrectly. Please delete it from your files if you are not the intended recipient. Thank you for your compliance.<br>Copyright (c) 2021  Nexx Wave Health Solutions Pvt Ltd .</p><br>';
        $temp .= '==================================================--';
        $temp .='</body></html>';
        $sub = '';
        if($usersms->email){
          // Mail::to($usersms->email)->send(new TestEmail($temp,$usersms->email));
          EmailFrontend::sendEmailFrontend($usersms->email,$sub,$temp);
        }
          }
          $childCart = CartSession::where('order_id',$data['orderId'])->where('child','!=',0)->get();
          if($childCart){
          foreach ($childCart as $key => $value) {
              $productChild['title'] = $value['product_title']; 
              $productChild['price'] = $value['price']; 
              $productChild['user_id'] = $value['child']; 
              $productChild['order_id'] = $data['orderId'];
              $productChild['lab_id'] = $value['lab_id'];
              CustomerBookingProduct::create($productChild);
          }
          $bookingProduct = CustomerBookingProduct::where('order_id',$data['orderId'])->groupBy('user_id')->get();
          foreach ($bookingProduct as $key => $value) {
              $cart = CartSession::where('order_id',$value['order_id'])->where('child',$value['user_id'])->first();
              $bdate = date_create($cart->booking_date);
              $b =  date_format($bdate, 'Y-m-d');
              $customer = Customer::where('user_id',$value['user_id'])->first();
              $user = User::find($value['user_id']);
              $booking['vendor_id'] = $value['lab_id'];
              $booking['user_id'] = $value['user_id'];
              $booking['customer_id'] = $customer->id;
              $booking['address'] = $user->address;
              $booking['time_slot'] = $cart->slot_id;
              $booking['payment_mode'] = $payment->paymentMode;
              $booking['payment_status'] = '1';
              $price = intval(preg_replace('/[^\d.]/', '', $payment->orderAmount));
              $booking['price'] = $price;
              $booking['sub_total'] = $price;
              $booking['total_price'] = $price;
              $booking['booking_date'] = $b;
              $booking['city'] = $auth->city;
              $booking['state'] = $auth->state;
              $booking['zip'] = $auth->zip;
              $booking['lat'] = $auth->latitude;
              $booking['lang'] = $auth->longitude;
              $booking['address1'] = (isset($auth->address1)) ? $auth->address1 : '';
              $booking['address2'] = (isset($auth->address2)) ? $user->address2 : '';
              $booking['neighbourhood'] = (isset($auth->neighbourhood)) ? $auth->neighbourhood : '';
              $booking['landmark'] = (isset($auth->landmark)) ? $auth->landmark : '';
              $booking['status'] = 1;
              $booking['order_id'] = $value['order_id'];
              $booking['parent_id'] = $auth->id;
              $booking['child_id'] = $value['user_id'];
              $booking['booking_status'] = 0;
              $id = Booking::create($booking)->id;
              $usersms = User::find($value['user_id']);
              $slosms = AgentSlots::find($cart->slot_id);
              $vendorSms = User::find($value['lab_id']);
                $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendorSms->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);
              $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$usersms->phone.'&message=Dear '. $usersms->username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$b.' time '.$slosms->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $homepage);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);

              $response = curl_exec($curl);
              $CustomerBookingProduct = CustomerBookingProduct::where('order_id',$value['order_id'])->where('user_id',$value['user_id'])->update(['booking_id' => $id]);
          }
        }
      }
       echo '<script>alert("Payment Successfully Done")
       window.location= "https://udaudindia.com/booking-response"

       </script>';
       

       // echo '<script>alert("Payment Successfully Done")
       // window.location= "http://localhost:8000/chandigarh/booking-response"

       // </script>';
  

    }
    public function getProfileOrderDetails(Request $request){
      $auth = auth()->user();
      $carts = CartSession::select('id','order_id','booking_date')->where('user_id',$auth->id)->orderBy('id','desc')->groupBy('order_id')->get();
      $data = [];
      $order = [];  
      $details = [];
      if($carts){  
      foreach($carts as $key=>$cart){
         $tpay = Payment::where('orderId',$cart->order_id)->first();
        $booking =  Booking::where('order_id',$cart->order_id)->first(); 
         if($tpay){
         $order[$key]['oreder'] = $cart->order_id;
         $order[$key]['status'] = $tpay->status;
         $order[$key]['booking_status'] = $booking->booking_status;
         $order[$key]['total_payment']  = $tpay->orderAmount;
         $bdate = date_create($cart['booking_date']);
         $b =  date_format($bdate, 'Y-m-d');
         $order[$key]['bookingDate']  =  $b;
         $pay = [];  
         $payments = CartSession::where('order_id',$cart->order_id)->where('user_id',$auth->id)->orderBy('id','desc')->groupBy('child')->get();
         foreach($payments as $k=>$pays){
              if($pays->child == 0){
                $users = User::where('id', $pays->user_id)->first();
                $tests = CartSession::where('user_id',$auth->id)->where('order_id',$cart->order_id)->where('child',0)->get();
              }else{
                 $users = User::where('id', $pays->child)->first();
                 $tests = CartSession::where('user_id',$auth->id)->where('order_id',$cart->order_id)
                 ->where('child',$pays->child)->get();
              }
              $pay[$k]['user_id'] = $users->id;
              $pay[$k]['username'] = $users->username; 
              $pay[$k]['booking_date'] = $pays->booking_date; 
              $pay[$k]['labname'] = $pays->lab_name; 
              $slotName = AgentSlots::select('slot')->where('id',$pays->slot_id)->first();
              $pay[$key]['slot'] = $slotName->slot;  
              $t = [];
              /*echo "<pre>";
              print_r($tests->toArray());
              die;*/
               foreach($tests as $ts=>$test){
                    $t[$ts]['product_title'] = $test->product_title; 
                    $t[$ts]['price'] = $test->price; 

               }
               $pay[$k]['tests'] = $t;

         }
         $order[$key]['payments'] = $pay;
      } 
      } 
    }
     return ApiResponse::success('Success',$order); 

      
    }
    public function getOrderDetails(Request $request){
      $auth = auth()->user();
      $payment = CartSession::select('order_id')->where('user_id',$auth->id)->where('status',1)->orderBy('id','desc')->first();
      
      $order = [];  
      $details = [];  
      if($payment){
        $carts = CartSession::where('order_id',$payment->order_id)->groupBy('child')->get();
        foreach($carts as $key=>$cart){
          
          if($cart->child == 0){
            $users = User::where('id', $cart->user_id)->first();
             $tests = CartSession::where('order_id',$payment->order_id)->where('user_id',$auth->id)->where('child',0)->get();
          }else{
             $users = User::where('id', $cart->child)->first();
             $tests = CartSession::where('order_id',$payment->order_id)->where('user_id',$auth->id)
             ->where('child',$cart->child)->get();
          }
          
         
            $order[$key]['user_id'] = $users->id; 
           
            $order[$key]['username'] = $users->username; 
            $order[$key]['booking_date'] = $cart->booking_date; 
            $order[$key]['labname'] = $cart->lab_name; 
            $slotName = AgentSlots::select('slot')->where('id',$cart->slot_id)->first();
            $order[$key]['slot'] = $slotName->slot;  
            $t = [];
             foreach($tests as $k=>$test){
                  $t[$k]['product_title'] = $test->product_title; 
                  $t[$k]['price'] = $test->price; 

             }
             $order[$key]['tests'] = $t;

         }  
         $details['packages']  = $order;          
         $details['payment']  = Payment::where('orderId',$payment->order_id)->first(); 
         
        return ApiResponse::success('Success',$details);    
      }
      foreach($carts as $key=>$cart){
          $order[$key]['cart_id'] = $cart->id; 
          $order[$key]['user_id'] = $cart->user_id; 
          $order[$key]['username'] = $cart->username; 
          $order[$key]['child'] = $cart->child; 
          $order[$key]['product_id'] = $cart->product_id; 
          $order[$key]['product_title'] = $cart->product_title; 
          $order[$key]['price'] = number_format($cart->price); 
          $order[$key]['actual_price'] = number_format($cart->actual_price); 
          $order[$key]['lab_name'] = $cart->lab_name; 
          $order[$key]['booking_date'] = $cart->booking_date; 
          $slotName = AgentSlots::select('slot')->where('id',$cart->slot_id)->first();
          $order[$key]['slot'] = $slotName->slot; 
          $discount = $cart->actual_price - $cart->price;
          if($discount != 0){
              $pricePer= $discount*100/$cart->price;
              $order[$key]['pricePer']= round($pricePer);
              $order[$key]['discount']= number_format($discoun);
            }else{
              $order[$key]['pricePer']= 0;
              $order[$key]['discountt']= 0;
            }
                                    
      }
      $details['packages']  = $order;          
      $details['payment']  = Payment::where('orderId',$payment->order_id)->first(); 
         
      return ApiResponse::success('Success',$details);  

    }

    public function getReports(Request $request){
       $user = auth()->user();
       $bookings = Booking::where('parent_id',$user->id)->where('booking_status',8)->get();
       if($bookings->isEmpty()){
          $bookings = Booking::where('child_id',$user->id)->where('booking_status',8)->get();
       }
       $data= [];
      if($bookings){
        foreach($bookings as $key=>$booking){
            $rpt = CustomerReport::where('booking_id',$booking->id)->first();
            $lab = User::find($booking->vendor_id);
            $userAll = User::find($booking->user_id);
            $data[$key]['report'] = url('/Upload_report/'.$rpt->report);
            $data[$key]['labname'] = $lab->username;
            $data[$key]['username'] = $userAll->username;
            $data[$key]['order_id'] = $booking->order_id;
            $data[$key]['booking_date'] = $booking->booking_date;
            $data[$key]['status'] = 'Completed';
           }
         }
       return ApiResponse::success('Success',$data);  
    }

    public function getUserMembers(Request $request){
       $user = auth()->user();
       $users = User::where('parent',$user->id)->get();
       $data= [];
      if(count($users)!=0){
       
        foreach($users as $key=>$user){
            $data[$key]['userid'] = $user->id; 
            $data[$key]['username'] = ucfirst($user->username);  
            $data[$key]['patient_code'] = $user->patient_code;
            $data[$key]['gender'] = $user->gender;
            $data[$key]['age'] = $user->age;
            $data[$key]['phone'] = $user->phone;
           
           }
         }
       return ApiResponse::success('Success',$data);  
    }
    public function getMemeberUserDetails($id){
       $user = auth()->user();
       $users = User::where('id',$id)->get();
       $data= [];
      if(count($users)!=0){
       
        foreach($users as $key=>$user){
            $data[$key]['userid'] = $user->id; 
            $data[$key]['username'] = ucfirst($user->username);  
            $data[$key]['patient_code'] = $user->patient_code;
            $data[$key]['gender'] = $user->gender;
            $data[$key]['age'] = $user->age;
            $data[$key]['phone'] = $user->phone;
           
           }
         }
       return ApiResponse::success('Success',$data);  
    }
    public function creatCartSessionData(Request $request){
      $data = $request->all();
      $user = auth()->user();
      $child = $data['child'];
      $detail = array(
        'lab_id' => $data['labsId'],
        'lab_name' => $data['labName'],
      );
      if($child == 0){
        $check = CartSession::where('user_id',$user->id)->where('status',0)->update($detail);
      }else{
        $userCid =User::where('patient_code',$child)->first();
        $child = $userCid->id;
        $check = CartSession::where('user_id',$user->id)->where('child',$userCid->id)->where('status',0)->update($detail);
      }
      //$product = [];
      
        /*foreach ($data['cart'] as $key => $a) {
          $product['user_id'] = $user->id; 
          $product['child'] = $child; 
          $product['product_id'] = $a['id']; 
          $product['product_title'] = $a['title']; 
          $product['actual_price'] = $a['price'] ;
          $product['price'] = $a['price'] ;
          $product['lab_id'] = $data['labsId']; 
          $product['lab_name'] = $data['labName']; 
          $product['status'] = 0; 
          $product['package'] = 1; 
          CartSession::create($product);
       }*/
        return ApiResponse::success('Success',1);  
      
    }
   public function partners(Request $request){
      $data = $request->all();
       $this->validate($request, [
          'labName' => 'required|string',
          'patient' => 'required|string',
          'address' => 'required',
          'message' => 'required',
          'email' => 'required|email',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
       ]); 
       $temp = ['message' => '<div style="margin:0 auto;font-family:"Poppins", sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;">
      <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;margin: 0 auto;">
        <tbody>
          <tr>           
            <td style="margin: 0px auto;padding: 0 0 0px;width: 100%;"><img width="100px" src="https://udaudindia.com/_nuxt/img/logo.26c739f.png" style=""></td>            
          </tr>          
        </tbody>
      </table>
      <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border: 4px #df1111;margin: 10px 0 0;border-style: double;">
        <tbody>
          <tr>
            <td style="padding: 15px;">
              <label style="font-size: 14px;font-weight: 700;">Lab Name</label>
              <p style="margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;">'.$data['labName'].'</p>
              <label style="font-size: 14px;font-weight: 700;">Person Name</label>
              <p style="margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;">'.$data['patient'].'</p>
              <label style="font-size: 14px;font-weight: 700;">Mobile No.</label>
              <p style="margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;">'.$data['phone'].'</p>
              <label style="font-size: 14px;font-weight: 700;">Email ID</label>
              <p style="margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;">'.$data['email'].'</p>
              <label style="font-size: 14px;font-weight: 700;">Address</label>
              <p style="margin: 0 0 10px;border-bottom: 1px solid #ccc;padding: 0 0 10px;">'.$data['address'].'</p>
              <label style="font-size: 14px;font-weight: 700;">Message</label>
              <p style="margin: 0 0 0px;">'.$data['message'].'</p>
            </td>                        
          </tr>          
        </tbody>
      </table>
    </div>
  </body>'];
  if($data['email']){
    Mail::to('info@udaudindia.com')->send(new UdaudEmail($temp,$data['email']));
  }
   $datas = array(
          'lab' => $data['labName'],
          'patient' => $data['patient'],
          'email' => $data['email'],
          'phone' => $data['phone'],
          'address' => $data['address'],
          'message' => $data['message'],
       ); 
       PartnerWithUs::create($datas);
       return ApiResponse::success('Success',1); 
   }
   public function updatePatientDetails(Request $request){
      $data = $request->all();
      $user = auth()->user();
       $detail = array(
        'address' => $data['address'],
        'address1' => (isset($data['address1'])) ? $data['address1'] : '', 
        'address2' => (isset($data['address2'])) ? $data['address2'] : '',  
        'city' => $data['city'],
        'state' => $data['state'],
        'zip' => $data['zip'],
        'neighbourhood' => (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '',
        'landmark' => (isset($data['landmark'])) ? $data['landmark'] : '',
        'age' => $data['age'],
        'gender' => $data['gender'],
       );
       if($data['lat']){
        $detail['latitude']  = $data['lat'];
        $detail['longitude'] = $data['lang'];
       }
       if($data['lat']){ 
        $agentCount = User::closeTo($data['lat'], $data['lang'])->get()->count();
       }else{
        $agentCount = User::closeTo($user['latitude'], $user['longitude'])->get()->count();
       }
       if($agentCount == 0){
        $userPhone = User::find($user->id);
         $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number=8595659421,9326537102,9137580450&message= Customer having Mob No '.$userPhone->phone.' could Not book test. Please call back.Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $sms);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);

              $response = curl_exec($curl);
        return ApiResponse::success('Success',0); 
       }
        User::where('id',$user->id)->update($detail);
        $customer = array(
        'age' => $data['age'],
        'gender' => $data['gender'],
       );
        Customer::where('user_id',$user->id)->update($customer);
       return ApiResponse::success('Success',1); 
   }
   public function refund ($order ,$price){
     $user = auth()->user();
    $booking =  Booking::where('order_id',$order)->first(); 
    if($booking->booking_status >= 2){
       return ApiResponse::success('Success',4);
    }
    $payemnt = Payment::where('orderId',$order)->first();
    $patient = Customer::where('user_id',$user->id)->first();
    if($payemnt->paymentMode == 'COD'){
      Booking::where('order_id',$order)->update(['booking_status'=> 9]);
    }else{
    Booking::where('order_id',$order)->update(['booking_status'=> 9]);
    $result = str_replace(',', '', $price);
    $price = $result;
    $client = new \GuzzleHttp\Client();
      $response = $client->request('POST', 'https://api.cashfree.com/pg/orders/'.$order.'/refunds', [
        'body' => '{"refund_amount":'.$price.',"refund_id":"","refund_note":"refund note for reference"}',
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
          'x-api-version' => '2022-01-01',
          'x-client-id' => '1615949011ad7f6a60afd08388495161',
          'x-client-secret' => '66bb9c5c6e199180e7fbff382a877aa71210d93e',
        ],
      ]);
      $response->getBody();
    $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$patient->phone.'&message=Dear '.$user->username.', refund of INR '.$price.' has been initiated to your account. We look forward to your boking again with us. Team Nexx Wave -uDAUD';
    $smsUrl = str_replace(' ', '%20', $sms);
    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => $smsUrl,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    ]);
    $response = curl_exec($curl);
    }
      $data = array(
      'status' => '2'
      );
    Payment::where('orderId',$order)->update($data);
    CartSession::where('order_id',$order)->update($data);
    $cartSession = CartSession::where('order_id',$order)->where('child',0)->first();
    $patient = User::find($cartSession->user_id);
    $slots = AgentSlots::find($cartSession->slot_id);
    $lab =array();
    $arr = preg_split('/[,\ \.;]/', $cartSession->lab_name);
    $keywords = array_unique($arr);
    $i=0;
    foreach ($keywords as $keyword){
    if($i < 3){
      $lab[] = $keyword;
    }
    $i++;
    }
    $labimp = implode(' ', $lab);
    $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$patient->phone.'&message=Dear '.$patient->username.', Your appointment with lab '.$labimp.' dated '.$cartSession->booking_date.' and '.$slots->slot.' has been cancelled by you. Hope all is well with you. Please take care.  We look forward to your booking again with us. Team Nexx Wave -uDAUD';
    $smsUrl = str_replace(' ', '%20', $sms);
    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => $smsUrl,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    ]);
    $response = curl_exec($curl);

    $cancelNoti = Booking::where('order_id',$order)->first();
    if($cancelNoti->assigned_agent){
         $recipientsIOS = User::where('id',$cancelNoti->assigned_agent)->where('device_type', 'ios')->get()->pluck('device_token');
                $recipients = User::where('id',$cancelNoti->assigned_agent)->where('device_type', 'Android')->get()->pluck('device_token');
                $payload = [
                    'title' =>'Booking Cancelled',
                    'body' => ''.$user->username.' has been cancelled the booking',
                    'sound' => 'default',
                ];

                if(count($recipients) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipients, $payload);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }
                } 

                if(count($recipientsIOS) > 0) {
                    // Send Push notification
                    try {
                        FcmNotification::send($recipientsIOS, $payload, $ios = true);   
                    } catch (\Exception $e) {
                    // something went wrong
                    }

                }
    }

    return ApiResponse::success('Success',1);
    }

    public function callBack(Request $request){
      $data = $request->all();
      $details = array(
        'phone' => $data['phone']
      );
      BackCall::create($details);
        $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number=8595659421,9326537102,9137580450&message=Customer having Mob No '.$data['phone'].' is looking for call back.Team Nexx Wave -uDAUD';
              $smsUrl = str_replace(' ', '%20', $sms);
              $curl = curl_init();

              curl_setopt_array($curl, [
              CURLOPT_URL => $smsUrl,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              ]);

              $response = curl_exec($curl);
       $msg = ' We will contact you soon on this number - ('.$data['phone'].' )' ;
      return ApiResponse::success($msg,1);
    }

    public function discountCoupon(Request $request){
      $checkCoupon = Coupon::where('code',$request->cpn)->get()->count();
      if($checkCoupon == 0){
        return ApiResponse::success('Invalid coupon',0);
      }
      $today = Carbon::now()->format('Y-m-d');
      $checkExpiry = Coupon::where('code',$request->cpn)->whereDate('expiry_date','>=',$today)->first()->toArray();
      if(empty($checkExpiry)){
        return ApiResponse::success('Coupon expired',0);
      }
      if($checkExpiry['type'] == '3'){
        $amount = (intval(preg_replace('/[^\d.]/', '', $request->amount)) - $checkExpiry['amount']);
        $data['totalPrice'] =  number_format($amount);
        $data['coupon'] =  number_format($checkExpiry['amount']);
        $data['orginalPrice'] = (intval(preg_replace('/[^\d.]/', '', $request->amount)));
        return ApiResponse::success('Coupon applied',$data);
      }
      $productCoupon = CartSession::where('user_id',$request->id)->where('status',0)->get()->pluck('product_id');
      if($productCoupon){
        $pExplode =explode(',', $checkExpiry['package_ids']);
        foreach ($productCoupon as $key=>$value) {
          if(!in_array($value, $pExplode)){
            return ApiResponse::success('Invalid coupon',0);
          }
        }
      }

      if($checkExpiry['applied_users']){
        $checkApplied[] = $checkExpiry['applied_users'];
        if(in_array($request->id, $checkApplied)){
          return ApiResponse::success('Already applied',0);
        }
      }
      // return $checkExpiry->user_ids;
      if($checkExpiry['type'] == 2){
        if($checkExpiry['user_ids']){
          // return $checkExpiry['user_ids'];
          $explodeUser= explode(',', $checkExpiry['user_ids']);
          if (($key = array_search($request->id, $explodeUser)) !== false) {
            unset($explodeUser[$key]);
            $explodeUser = implode(',', $explodeUser);
            if($checkExpiry['applied_users']){
              $a[]= $checkExpiry['applied_users'];
              array_push($a,$request->id);
              $appliedUsers = implode(',',$a);
            }else{
              $appliedUsers = $request->id;
            }
            Coupon::where('id',$checkExpiry['id'])->update(['user_ids'=>$explodeUser,'applied_users'=>$appliedUsers]);

            $i = 0;
            foreach ($productCoupon as $key => $value) {
              if (in_array($value, explode(',', $checkExpiry['package_ids']))) {
               $i++;
              }
            }

            $amount = (intval(preg_replace('/[^\d.]/', '', $request->amount)) - $checkExpiry['amount'] * $i);

            $data['totalPrice'] =  number_format($amount);
            $data['coupon'] =  number_format($checkExpiry['amount'] * $i);
            $data['orginalPrice'] = '0';
            return ApiResponse::success('Coupon applied',$data);
          }else{
            return ApiResponse::success('Invalid coupon',0);
          }
        }else{
          if($checkExpiry['applied_users']){
            $a[]= $checkExpiry['applied_users'];
            array_push($a,$request->id);
            $appliedUsers = implode(',',$a);
          }else{
            $appliedUsers = $request->id;
          }
          Coupon::where('id',$checkExpiry['id'])->update(['applied_users'=>$appliedUsers]);
          $i = 0;
          foreach ($productCoupon as $key => $value) {
            if (in_array($value, explode(',', $checkExpiry['package_ids']))) {
             $i++;
            }
          }
          $amount = (intval(preg_replace('/[^\d.]/', '', $request->amount)) - $checkExpiry['amount'] * $i);
          $data['totalPrice'] =  number_format($amount);
          $data['coupon'] =  number_format($checkExpiry['amount'] * $i);
          $data['orginalPrice'] = '0';
          return ApiResponse::success('Coupon applied',$data);
        }
      }
    }

    // email test 
    public function updtestEmail()
    {
      $temp ='';
      $temp ='<html><head><style>.table td, .table th { padding: 5px;vertical-align: middle;border-top: 1px solid #000000;font-size: 15px;}</style></head><body>';
      $temp .="<div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
      $temp .="<div style='color:#00008B;font-size:20px;font-weight:600'>Dear Customer, Welcome to uDAUD and the world of healthy Habits to keeps yourself Fit and aware. Your appointment for the blood/other Sample collection is received with the following details.</div>";
      $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: block;'>";
      $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 90%;'><h5 style='margin: 0px 0 0px;''>test1</h5><p style='margin: 10px 0 0;font-size: 15px;line-height: 20px;'>test2</p>";
      $temp .="</td><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 10%;'><img src='https://udaudindia.com/_nuxt/img/logo.61b22fc.png' style='width: 100px;'></td>";            
      $temp .='</tr> </tbody></table>';
      $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 20px 0px;display: inline-table;margin: 10px 0 10px;'>";
      $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;'>test12</td>";            
      $temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: center;'>Home Collection Slip</td>";            
      $temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: right;'>Page 1 of 1</td>";            
      $temp .='</tr></tbody></table>';
      $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;'>";
      $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
      $temp .="<table class='table' style='width: 100%;border-spacing: 0px;'>";
      $temp .="<tbody><tr><th scope='row'>Date</th><td>test13</td><th scope='row' style='border: 0;'>Collection Time</th><td style='border: 0;'>test3</td>";
      $temp .="</tr><tr><th scope='row'>Name of Patient</th><td>test4,test5</td><th scope='row'>Sex</th><td>test6</td></tr>";
      $temp .="<tr style='position:relative;'><th scope='row'>Age</th><td>test 7 Yrs</td></tr><tr><th scope='row'>Address</th>";
      $temp .="<td colspan='3'>test8,test9,test10</td>";
      $temp .="</tr><tr><th scope='row'>Mobile</th><td colspan='3'>test11</td>";
      $temp .="</tr><tr><th scope='row'>Paid Mode</th><td colspan='3'>test11</td></tr></tbody></table></td></tr></tbody></table>";
      $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
      $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
      $temp .="<table class='table' style='width: 100%;border-spacing: 0px;'><tbody>test14<tr>";
      $temp .="<th scope='row' colspan='2' style='width: 33%;border-right: 1px solid;text-align: right;font-size: 21px;padding: 0px 10px 0px 0px;'>Total</th>";
      $temp .="<td style='font-weight: bold;font-size: 21px;'>test12/-</td></tr></tbody></table></td></tr></tbody></table>";
      $temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
      $temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
      $temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
      $temp .='<li>You will receive a confirmation SMS after booking an appointment.</li>';
      $temp .='<li>Reminder SMS will be shared half an hour prior to your confirmed appointment time.</li></ul>';
      $temp .="<h4 style='margin: 15px 0 5px;font-size:16px'>Fasting Health Check-up pre-requisites:</h4>";
      $temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto; font-size:14px'>";
      $temp .='<li>10-12 hours of overnight fasting is required prior to the day of the blood collection.</li>';
      $temp .='<li>Avoid alcohol at least 24 hours before the blood collection appointment.</li>';
      $temp .='<li>Avoid Tea/Coffee/Other beverages before the blood collection. Water intake is allowed.</li></ul>';
      $temp .="<h4 style='margin: 15px 0 0px; font-size:16px'>Kindly confirm the list of your tests to be conducted with our scientific officer in one appointment as there will be no repeat visit for pending tests.</h4>";
      $temp .="<h4 style='margin: 0px 0 5px; font-size:16px'>Disclaimer:</h4>";
      $temp .="<p style='font-size:14px;margin:0'>The Health Check-up requires you to give your blood sample. The Following associated rare incidences may occur during and after blood collection:</p>";
      $temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
      $temp .='<li>Discomfort, swelling, redness or dizziness while or immediately after blood collection, the phlebotomist would know how to deal with the same. Request not to panic. Inform our technical expert or pathology lab immediately for support.</li>';
      $temp .='<li>In case your blood sample gets lysed, your selected pathology lab will approach you for a fresh sample collection, the same day or another mutually decided date.</li>';
      $temp .='<li>All the protocols followed are as per guidelines . Factors such as physiological disturbances, fever, dehydration, haemolysis, etc. can cause variation in reported results.</li></ul>';
      $temp .="<h4 style='margin: 15px 0 5px; font-size:16px'>Request you to share a valid identity proof for the medical test. Acceptable identity proofs are: PAN Card, Driving Licence, Passport, Adhaar Card, Voter ID card.</h4>";
      $temp .="<p style='font-size:14px;margin:0'>In case of any queries, please write to us at info@udaudindia.com (Mon-Sat, 9am to 5pm)</p>";
      $temp .="<h4 style='margin:15px 0 5px; font-size:16px'> We hope your experience is positive with us and look forward towards improving our services and serving you always.</h4>";
      $temp .='</td></tr></tbody></table></div>';
      $temp .='------------------------------------------------------------------------------ <br/>';
      $temp .='<p>CONFIDENTIALITY NOTICE: If you have received this email in error, <br/>  please immediately notify the sender by e-mail at the address shown.<br/> This email transmission may contain confidential information.<br/> This information is intended only for the use of the individual(s) or entity to whom it is intended even if addressed incorrectly. Please delete it from your files if you are not the intended recipient. Thank you for your compliance.<br/> Copyright (c) 2021  Nexx Wave Health Solutions Pvt Ltd .</p>';
      $temp .='==================================================';
      $temp .='</body></html>';

      return EmailFrontend::sendEmailFrontend('chandan.sharma@yopmail.com','demo testing',$temp);                       
    }

    public function getCouponCode(Request $request)
  {
    $auth = auth()->user();
    $carts = CartSession::where('user_id',$auth->id)->where('status',0)->get();
    $cartTotalPrice = 0;
    $productIds = '';
    foreach ($carts as $key => $cart) 
    {
      $result = str_replace(',', '', $cart->price);
      $cartTotalPrice += $result;
      $productIds .= $cart->product_id.',';
    }
    $packageIds = rtrim($productIds,',');
    $packageIds = explode(',',$packageIds);
    $totalPrice = $cartTotalPrice;
    $today = Carbon::now()->format('Y-m-d');
    $couponData = Coupon::where('type','3')->whereDate('expiry_date','>=',$today)
                            ->where(function($q) use($packageIds){
                              foreach($packageIds as $key=>$id)
                              {
                                if ($key == 0) 
                                {
                                  $q = $q->whereRaw('FIND_IN_SET(' . $id . ',package_ids)');
                                } else {
                                  $q = $q->orWhere(function ($query) use ($id){
                                    $query->whereRaw('FIND_IN_SET(' . $id . ',package_ids)');
                                  });
                                }
                              }
                            })->get(['amount','code','name']);
    if($totalPrice <= 700)
    { 
      return ApiResponse::success('Success',$couponData);
    }
  }
}
