<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Controllers\AppBaseController;
use Illuminate\Auth\Events\Registered;
use App\Helpers\EmailFrontend;
use App\Http\Requests\DeliverRequest;
use App\Http\Requests\VisitRequest;
use App\Models\User;
use App\Models\AgentSlotNotAvailabil;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\CustomerBookin;
use App\Http\Resources\VisitResource;
use App\Http\Resources\ConsultationResource;
use App\Models\CustomerBookingProduct;
use App\Models\AssignPrice;
use App\Models\AgentSlots;
use App\Models\Customer;
use App\Models\DocConsultation;
use App\Models\Doctor;
use App\Models\WorkDay;
use App\Models\ProductRequest;
use App\Http\Resources\UserResource;
Use \Carbon\Carbon;

class SupportController extends AppBaseController
{

    public function updateProfile(Request $request)
    {

        $this->validate($request, [
          'email' => 'required|email',
          'phone' => 'required|phone',
       ]);
        $user = $request->user();
        $user->username = $request->username;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if($request->password)
         {
                $user->password = bcrypt($request->password);
         }

        $user->save();
        $user = new UserResource($user);

        // $user['profile'] = $profile;

        return $this->sendResponse(__('Profile udpated Successfully'), $user); 
    }

    public function labs(Request $request){

       $labs =Vendor::select("users.id","users.username")
        ->leftJoin("users", "users.id", "=", "vendors.user_id")->where('users.status','Active')->get();

      return ApiResponse::success('Success',$labs); 
    } 


        public function productListing($id){
      $vendor_products =  Product::select('products.id','products.title','products.include','products.product_code','products.type','product_request.price','product_request.discount')->
         leftJoin('product_request', 'products.id', '=', 'product_request.product_id')
        ->where('product_request.vendor',$id)
        ->where('product_request.status','2')
        ->where('product_request.cancel_status','0')
        ->orderBy('product_request.id','DESC')
        ->get();
        $products = [];
        if($vendor_products){

            foreach ($vendor_products as $key => $product) {
               $vendorData = Vendor::where('user_id',$id)->first();
               if($product->include[0] == '1'){
                  $products[$key]['charges']= $vendorData->fasting_chr ;
                  $products[$key]['charges']= $vendorData->fasting_chr ;
               }else if($product->include[0] == '2'){
                  $products[$key]['charges'] = $vendorData->nfasting_chr ;
               }else if($product->include[0] == '3'){
                  $products[$key]['charges'] = $vendorData->after_meal_chr ;
               }else if($product->include[0] == '4'){
                  $products[$key]['charges'] = $vendorData->other_chr ;
               } 
               $products[$key]['include'] = $product->include[0];
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['support_vendor'] = $id;
               $products[$key]['type']  = $product->type;
               if($product->type == '1'){ 
                  $products[$key]['type'] = 'Package';
               }else{
                $products[$key]['type']        = 'Individual';
               }      
              $products[$key]['price'] = $product->price;      
              $products[$key]['discount'] = $product->discount;      
          }

        }


      // print_r($products);
        return $this->sendResponse('Success.',$products);
    }

     public function productDetailId(Request $request){
       $data = $request->all();
     
       $productDetail = [];
       $total_price = 0;
       $discount = 0;
       $includealert = 0;
       foreach($data['product'] as $keys =>$product){
            $id =  $product['value'];
              $vendor_id = $product['id'];
            //$vendor_products = VendorProduct::where('product_id' , $id)->where('vendor_id' , $vendor_id->id)->get();
            $vendor_products = Product::where('id' , $id)->get();

            $products = [];
            $vendorCharges = [];
            $otherCharges = 0;
            $inc= [];
           
            if($vendor_products){

                  foreach ($vendor_products as $key => $product) {

               // $product  = Product::where('id',$vendor_product->product_id)->first();
                # code...

               $products['id'] = $product->id;
               $products['title'] = $product->title;
               if($product->type == '1'){ 
                  $type        = 'Package';
               }else{
                $type        = 'Individual';
               }
               $products['type']    = $type;
               $products['image']        = $product->image;
              
               $products['discount']    = $product->discount;
               $products['price']    =  $product->price;
               $products['description']    =  $product->description;
               $inc['a'] = $product->include[0];
               $vendorData = Vendor::where('user_id',$vendor_id)->first();
               if($inc['a'] == '1'){
                  $vendorCharges= $vendorData->fasting_chr ;
               }else if($inc['a'] == '2'){
                  $vendorCharges = $vendorData->nfasting_chr ;
               }else if($inc['a'] == '3'){
                  $vendorCharges = $vendorData->after_meal_chr ;
               }else if($inc['a'] == '4'){
                  $vendorCharges = 0;
                  $otherCharges = $vendorData->other_chr ;
               }
               $products['include']    =  $inc;
               $productRequest = ProductRequest::where('product_id',$product->id)->where('vendor',$vendor_id)->where('status',2)->where('cancel_status',0)->orderBy('id','DESC')->limit(1)->get();             
               if(count($productRequest) != 0){
                
                    
                      $products['price'] = $productRequest[0]->price;      
                    
                    
                      $products['discount'] = $productRequest[0]->discount;      
                     
                }                         
            }

               $total_price += $products['price'] ;
               $discount += $products['discount'];
        }     
        $productDetail['product'][]  = $products;
         $heighVal[]  = $vendorCharges;
         $include[]= $inc;
        
       }

       $getVal = max($heighVal);
       $c = $getVal;
       if($otherCharges != 0){
       
         $getVal = (int)$getVal + (int)$otherCharges;
       }
       
       foreach ($include as $incValue) {
            if($incValue['a'] == 1){
               $includealert = 1;
            }
       }
        $productDetail['payment']['subtotal']  = $total_price;
        $productDetail['payment']['chargesVal']  = $getVal;
        $productDetail['payment']['c']  = $c;
        $productDetail['payment']['total_price']  = $total_price - $discount;
        $productDetail['payment']['discount']  = $discount;
        $productDetail['payment']['includealert']  = $includealert;
          
       return $this->sendResponse('Success.',$productDetail);

    }

     public function productDetailIds(Request $request){
       $data = $request->all();
     
       $productDetail = [];
       $total_price = 0;
       $discount = 0;
       $includealert = 0;
            $id =  $data['product']['value'];
              $vendor_id = $data['product']['id'];
            //$vendor_products = VendorProduct::where('product_id' , $id)->where('vendor_id' , $vendor_id->id)->get();
            $product = Product::where('id' , $id)->first();

            $products = [];
            $vendorCharges = [];
            $otherCharges = 0;
            $inc= [];
           
            if($product){

               $products['id'] = $product->id;
               $products['title'] = $product->title;
               if($product->type == '1'){ 
                  $type        = 'Package';
               }else{
                $type        = 'Individual';
               }
               $products['type']    = $type;
               $products['image']        = $product->image;
              
               $products['discount']    = $product->discount;
               $products['price']    =  $product->price;
               $products['description']    =  $product->description;
               $inc['a'] = $product->include[0];
               $vendorData = Vendor::where('user_id',$vendor_id)->first();
               if($inc['a'] == '1'){
                  $vendorCharges= $vendorData->fasting_chr ;
               }else if($inc['a'] == '2'){
                  $vendorCharges = $vendorData->nfasting_chr ;
               }else if($inc['a'] == '3'){
                  $vendorCharges = $vendorData->after_meal_chr ;
               }else if($inc['a'] == '4'){
                  $vendorCharges = 0;
                  $otherCharges = $vendorData->other_chr ;
               }
               $products['include']    =  $inc;
               $productRequest = ProductRequest::where('product_id',$product->id)->where('vendor',$vendor_id)->where('status',2)->where('cancel_status',0)->orderBy('id','DESC')->limit(1)->get();             
               if(count($productRequest) != 0){
                
                    
                      $products['price'] = $productRequest[0]->price;      
                    
                    
                      $products['discount'] = $productRequest[0]->discount;      
                     
                }                         
            

               $total_price += $products['price'] ;
               $discount += $products['discount'];
        }     
        $productDetail['product'][]  = $products;
         $heighVal[]  = $vendorCharges;
         $include[]= $inc;
        
       

       $getVal = max($heighVal);
       $c = $getVal;
       if($otherCharges != 0){
       
         $getVal = (int)$getVal + (int)$otherCharges;
       }
       
       foreach ($include as $incValue) {
            if($incValue['a'] == 1){
               $includealert = 1;
            }
       }
        $productDetail['payment']['subtotal']  = $total_price;
        $productDetail['payment']['chargesVal']  = $getVal;
        $productDetail['payment']['c']  = $c;
        $productDetail['payment']['total_price']  = $total_price - $discount;
        $productDetail['payment']['discount']  = $discount;
        $productDetail['payment']['includealert']  = $includealert;
          
       return $this->sendResponse('Success.',$productDetail);

    }



    public function getpackage($id){
        $assignPrice = AssignPrice::where('vendor_id',$id)->get()->pluck('product_id');
        if($assignPrice){
          $vendor_products = Product::whereIn('id',$assignPrice)->where('status','1')->orderBy('order','desc')->where('type', 5)->where('categories',null)->where('cost_free_package',0)->get();
        $products = [];
        if($vendor_products){

            foreach ($vendor_products as $key => $product) {
               $vendorData = Vendor::where('user_id',$id)->first();
               if($product->include[0] == '1'){
                  $products[$key]['charges']= $vendorData->fasting_chr ;
                  $products[$key]['charges']= $vendorData->fasting_chr ;
               }else if($product->include[0] == '2'){
                  $products[$key]['charges'] = $vendorData->nfasting_chr ;
               }else if($product->include[0] == '3'){
                  $products[$key]['charges'] = $vendorData->after_meal_chr ;
               }else if($product->include[0] == '4'){
                  $products[$key]['charges'] = $vendorData->other_chr ;
               } 
               $products[$key]['include'] = $product->include[0];
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['support_vendor'] = $id;
               $products[$key]['type']  = $product->type;
               if($product->type == '1'){ 
                  $products[$key]['type'] = 'Package';
               }else{
                $products[$key]['type']        = 'Individual';
               }      
              $products[$key]['price'] = $product->price;      
              $products[$key]['discount'] = $product->discount;      
          }

        }
         return ApiResponse::success('success', $products);
    }
         return ApiResponse::success('success');

    }

      public function addBooking(Request $request){
     $this->validate($request, [
          'first_name' => 'required|string',
          'last_name' => 'required|string',
          'gender' => 'required',
          'email' => 'email',
          'age' => 'required',
          'test' => 'required',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
          'referred_by' => 'required',
          'city' => 'required|string',
          'state' => 'required|string',
          'zip' => 'required|string',
          'payment_mode' => 'required',
          'booking_date' => 'required',
          'slot' => 'required',
       ]); 

        $data = $request->all();

        //print_r($data);
        $day_id = WorkDay::select('id')->where('day', $data['day'])->first();
        $day = $day_id->id;
        $vendor = User::find($data['vendor_id']);
        $userData = array(
            'name' => $data['first_name'],
            'username' => $data['first_name'].' '.$data['last_name'],
            'email' => (isset($data['email'])) ? $data['email'] : '',

        );
        $users = User::create($userData);

         $customerData = array(
            'user_id' => $users->id,
            'age' => $data['age'],
            'gender' => $data['gender'],
            'phone' => $data['phone'],
            'medical_condition' => (isset($data['medical_condition'])) ? $data['medical_condition'] : ''
        );
        $customer = Customer::create($customerData);
       
        $now = \Carbon\Carbon::today();
        $date = strtotime($now);
        $new_date = date('y-m-d', $date);
        $bdate = date_create($data['booking_date']);
        $b =  date_format($bdate, 'Y-m-d');
        $vendorCharges = [];
        $otherCharges = 0;
        if($data['selectType'] == 'product'){
         foreach($data['test'] as $cart){
          if($cart['include'] == '1'){
                    $vendorCharges= $cart['charges'] ;
          }else if($cart['include'] == '2'){
                    $vendorCharges = $cart['charges'] ;
          }else if($cart['include'] == '3'){
                    $vendorCharges = $cart['charges'] ;
          }else if($cart['include'] == '4'){
                    $vendorCharges = 0;
                    $otherCharges = $cart['charges'] ;
          }
          $heighVal[]  = $vendorCharges;
        
         }
       }else{
        if($data['test']['include'] == '1'){
                    $vendorCharges= $data['test']['charges'] ;
          }else if($data['test']['include'] == '2'){
                    $vendorCharges = $data['test']['charges'] ;
          }else if($data['test']['include'] == '3'){
                    $vendorCharges = $data['test']['charges'] ;
          }else if($data['test']['include'] == '4'){
                    $vendorCharges = 0;
                    $otherCharges = $data['test']['charges'] ;
          }
          $heighVal[]  = $vendorCharges;
       }
         $getVal = max($heighVal);
         if($otherCharges != 0){
          $getVal = (int)$getVal + (int)$otherCharges;
         }

        $booking = array(
            'vendor_id' => $vendor->id,
            'user_id' => $users->id,
            'customer_id' => $customer->id,  
            'refered_by_doc' => (isset($data['referred_by']['value'])) ? $data['referred_by']['value'] : '',
            'address' =>$data['address'], 
            'time_slot' =>$data['slot'],
            'price' =>$data['price'],
            'discount' =>$data['discount'],
            'payment_mode' =>$data['payment_mode'],
            'total_price' =>$data['total_price'],
            'sub_total' =>$data['subtotal'],
            //'agent_id' =>$data['agent'],
            'day_id' =>$day,
            'booking_date'=> $b,
            'city'=> $data['city'],
            'state'=> $data['state'],
            'zip'=> $data['zip'],
            'lat'=> $data['lat'],
            'lang'=> $data['lang'],
            'address1'=> (isset($data['address1'])) ? $data['address1'] : '',
            'address2'=> (isset($data['address2'])) ? $data['address2'] : '',
            'neighbourhood'=> (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '',
            'landmark'=> (isset($data['landmark'])) ? $data['landmark'] : '',
            'support_booking' => '1',
            'convenience'=> $data['convenience'],
            'bookingRequest'=> $data['bookingRequestVal'],
            'charges'=> $getVal,
            'status' => 1
        );
        $testList= [];
        $booking = CustomerBookin::create($booking);
         if($data['selectType'] == 'product'){
        foreach($data['test'] as $cart){
        $testList[] = $cart['productName'];
        $cart_data['booking_id'] = $booking->id;
        $cart_data['title'] = $cart['productName'];
        $cart_data['price'] = $cart['price'];
        $cart_data['discount'] = $cart['discount'];
        // $cart_data['charges'] = $cart['charges'];
        CustomerBookingProduct::create($cart_data);
      }
    }else{
         $testList[] = $data['test']['productName'];
        $cart_data['booking_id'] = $booking->id;
        $cart_data['title'] = $data['test']['productName'];
        $cart_data['price'] = $data['test']['price'];
        $cart_data['discount'] = $data['test']['discount'];
        // $cart_data['charges'] = $cart['charges'];
        CustomerBookingProduct::create($cart_data);
    }
        $not_avail= array(
          'day_id' => $day,
          'booking_id' => $booking->id,
          'slot' => $data['slot'],
          'status' =>'1',
        );
            $expTest = implode(',', $testList);
            $AgentSlots = AgentSlots::find($data['slot']);
            $Doctor = Doctor::find($data['referred_by']);

            $address1 = (isset($data['address1'])) ? $data['address1'] : '';
            $address2 = (isset($data['address2'])) ? $data['address2'] : '';
            $neighbourhood = (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '';
            $landmark = (isset($data['landmark'])) ? $data['landmark'] : '';

$test = "";
$temp = "";
$vendor_email = $vendor->email;
 if($data['selectType'] == 'product'){
  foreach($data['test'] as $cart){
    $test .= "<tr><th scope='row' style='width: 33%;border-right: 1px solid;'>Test</th><td style='border-right: 1px solid;'>".$cart['productName']."</td><td>".$cart['price']."</td></tr>";
  }
}else{
    $test .= "<tr><th scope='row' style='width: 33%;border-right: 1px solid;'>Test</th><td style='border-right: 1px solid;'>".$data['test']['productName']."</td><td>".$data['test']['price']."</td></tr>";
}

$temp ='<html><head><style>.table td, .table th { padding: 5px;vertical-align: middle;border-top: 1px solid #000000;font-size: 15px;}</style></head><body>';
$temp .="<div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
$temp .="<div style='color:#00008B;font-size:20px;font-weight:600'>Dear Customer, Welcome to uDAUD and the world of healthy Habits to keeps yourself Fit and aware. Your appointment for the blood/other Sample collection is received with the following details.</div>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: block;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 90%;'><h5 style='margin: 0px 0 0px;''>".$vendor->username."</h5><p style='margin: 10px 0 0;font-size: 15px;line-height: 20px;'>".$vendor->address."</p>";
$temp .="</td><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 10%;'><img src='https://udaudindia.com/_nuxt/img/logo.61b22fc.png' style='width: 100px;'></td>";            
$temp .='</tr> </tbody></table>';
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 20px 0px;display: inline-table;margin: 10px 0 10px;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;'>".$b."</td>";            
$temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: center;'>Home Collection Slip</td>";            
$temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: right;'>Page 1 of 1</td>";            
$temp .='</tr></tbody></table>';
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<table class='table' style='width: 100%;border-spacing: 0px;'>";
$temp .="<tbody><tr><th scope='row'>Date</th><td>".$b."</td><th scope='row' style='border: 0;'>Collection Time</th><td style='border: 0;'>".$AgentSlots->slot."</td>";
$temp .="</tr><tr><th scope='row'>Name of Patient</th><td>".$data['first_name']." ".$data['last_name']."</td><th scope='row'>Sex</th><td>".$data['gender']."</td></tr>";
$temp .="<tr style='position:relative;'><th scope='row'>Age</th><td>".$data['age']. "Yrs</td></tr><tr><th scope='row'>Address</th>";
$temp .="<td colspan='3'>".$data['address']." ".$data['city']." ".$address1." ".$address2." ".$neighbourhood." ".$landmark." ".$data['zip']."</td>";
$temp .="</tr><tr><th scope='row'>Mobile</th><td colspan='3'>".$data['phone']."</td>";
$temp .="</tr><tr><th scope='row'>Paid Mode</th><td colspan='3'>".$data['payment_mode']."</td></tr></tbody></table></td></tr></tbody></table>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<table class='table' style='width: 100%;border-spacing: 0px;'><tbody>".$test."<tr>";
$temp .="<th scope='row' colspan='2' style='width: 33%;border-right: 1px solid;text-align: right;font-size: 21px;padding: 0px 10px 0px 0px;'>Total</th>";
$temp .="<td style='font-weight: bold;font-size: 21px;'>".$data['total_price']."/-</td></tr></tbody></table></td></tr></tbody></table>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
$temp .='<li>You will receive a confirmation SMS after booking an appointment.</li>';
$temp .='<li>Reminder SMS will be shared half an hour prior to your confirmed appointment time.</li></ul>';
$temp .="<h4 style='margin: 15px 0 5px;font-size:16px'>Fasting Health Check-up pre-requisites:</h4>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto; font-size:14px'>";
$temp .='<li>10-12 hours of overnight fasting is required prior to the day of the blood collection.</li>';
$temp .='<li>Avoid alcohol at least 24 hours before the blood collection appointment.</li>';
$temp .='<li>Avoid Tea/Coffee/Other beverages before the blood collection. Water intake is allowed.</li></ul>';
$temp .="<h4 style='margin: 15px 0 0px; font-size:16px'>Kindly confirm the list of your tests to be conducted with our scientific officer in one appointment as there will be no repeat visit for pending tests.</h4>";
$temp .="<h4 style='margin: 0px 0 5px; font-size:16px'>Disclaimer:</h4>";
$temp .="<p style='font-size:14px;margin:0'>The Health Check-up requires you to give your blood sample. The Following associated rare incidences may occur during and after blood collection:</p>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
$temp .='<li>Discomfort, swelling, redness or dizziness while or immediately after blood collection, the phlebotomist would know how to deal with the same. Request not to panic. Inform our technical expert or pathology lab immediately for support.</li>';
$temp .='<li>In case your blood sample gets lysed, your selected pathology lab will approach you for a fresh sample collection, the same day or another mutually decided date.</li>';
$temp .='<li>All the protocols followed are as per guidelines . Factors such as physiological disturbances, fever, dehydration, haemolysis, etc. can cause variation in reported results.</li></ul>';
$temp .="<h4 style='margin: 15px 0 5px; font-size:16px'>Request you to share a valid identity proof for the medical test. Acceptable identity proofs are: PAN Card, Driving Licence, Passport, Adhaar Card, Voter ID card.</h4>";
$temp .="<p style='font-size:14px;margin:0'>In case of any queries, please write to us at info@udaudindia.com (Mon-Sat, 9am to 5pm)</p>";
$temp .="<h4 style='margin:15px 0 5px; font-size:16px'> We hope your experience is positive with us and look forward towards improving our services and serving you always.</h4>";
$temp .='</td></tr></tbody></table></div>';
$temp .='------------------------------------------------------------------------------ <br/>';
$temp .='<p>CONFIDENTIALITY NOTICE: If you have received this email in error, <br/>  please immediately notify the sender by e-mail at the address shown.<br/> This email transmission may contain confidential information.<br/> This information is intended only for the use of the individual(s) or entity to whom it is intended even if addressed incorrectly. Please delete it from your files if you are not the intended recipient. Thank you for your compliance.<br/> Copyright (c) 2021  Nexx Wave Health Solutions Pvt Ltd .</p>';
$temp .='==================================================';
$temp .='</body></html>';
   $sub = 'Your Appointment Schedule - Home Sample collection.';
        if($data['email'])
        {
            EmailFrontend::sendEmailFrontend($data['email'],$sub,$temp);
            EmailFrontend::sendEmailFrontend($vendor->email,$sub,$temp);
        }

        if($data['phone']!=''){
        $lab =array();
        $arr = preg_split('/[,\ \.;]/', $vendor->username);
        $keywords = array_unique($arr);
        $i=0;
        foreach ($keywords as $keyword){
        if($i < 3){
        $lab[] = $keyword;
        }
        $i++;
        }
        $labimp = implode(' ', $lab);
    $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$data['phone'].'&message=Dear '. $data['first_name'].' '.$data['last_name'].', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$b.' time '.$AgentSlots->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
          $smsUrl = str_replace(' ', '%20', $homepage);
            $curl = curl_init();

            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);
        }
        AgentSlotNotAvailabil::create($not_avail);
        $bookingNewId = ['booking_id' => $booking->id ];
        return $this->sendResponse('Successfully Added.',$bookingNewId);
        
    }
    public function bookingRequestList(Request $request){
      $user = auth()->user();

      $req = $request->all();
      $filter = $req['filter'];
       
      $data =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.bookingRequest','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')
        ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
        // ->where('vendor_id',$user->id)
        ->where('support_booking',1)
        ->where('bookingRequest','!=', 0)
        ->orderBy('bookings.id','DESC');
        if($filter['booking']){
          if($filter['booking'] == 'pending'){
              $data->where('bookings.bookingRequest', '1');
           }else if($filter['booking'] == 'accept'){
              $data->where('bookings.bookingRequest', '2');
           }else if($filter['booking'] == 'reject'){
              $data->where('bookings.bookingRequest', '3');
           }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
           }
         }
        $booking = $data->paginate(10);
                
        
        return $this->sendResponse('Successfully Added.',$booking);
    }

        public function bookingList(Request $request){
      $user = auth()->user();
      $req = $request->all();
      $filter = $req['filter'];
      $data =  CustomerBookin::select('bookings.id','bookings.assigned_agent','bookings.total_price','bookings.order_id','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->leftJoin('users', 'bookings.user_id', '=', 'users.id')->where('support_booking',1)
        ->whereNotIn('bookingRequest',['1','3']);
      if($filter['booking']!='' || $filter['tDate']!='' || $filter['fDate'] != '' ){
       
          if($filter['fDate']){
            $data->whereDate('bookings.created_at','>=',$filter['fDate']);
          }
          if($filter['tDate']){
            $data->whereDate('bookings.created_at','<=',$filter['tDate']);
          } 
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
          }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
          }
      }
      $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
                
        return VisitResource::collection($booking);
    }
  public function getOnGoingBookings(Request $request){
       $user = auth()->user();
      
      $req = $request->all();
      $filter = $req['filter']; 
      $data =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.order_id','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->
          //leftJoin('customers', 'customer_booking.customer_id', '=', 'customers.id')
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
         //->leftJoin('doctors', 'customer_booking.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('support_booking',1)
        
        ->where('bookings.status','1')
        ->whereNotIn('bookings.booking_status',['8','9','10'])
        ->whereNotIn('bookings.bookingRequest',['1','3']);
        if($filter['booking']){
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
           }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
           }
         }

       $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
      return VisitResource::collection($booking);

        //return $this->sendResponse('Success.',$booking);
         $booking = [];

        if($data){

            foreach ($data as $key => $d) {

               $user  = User::select('username')->where('id',$d->user_id)->first();
               $vendor  = User::select('username')->where('id',$d->vendor_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['venor'] = $vendor->username;
               $booking[$key]['product'] = $user->username;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }
               $booking[$key]['mode'] = $mode;
               $booking[$key]['status'] = $status;
               $booking[$key]['date'] =  date('d-m-Y', strtotime($d->booking_date));
             
            }

        }

        return $this->sendResponse('Success.',$booking);
    }

    public function getBookingCount(Request $request){
       // $user = auth()->user();
       //$completeBookings =[];
       $onGoingBookings =  DB::table('bookings')->where('bookings.support_booking',1)
        ->where('bookings.status','1')
        ->where('bookings.booking_status','0')
        ->whereDate('bookings.booking_date', '>=', Carbon::today())
        ->get();
       $todayBookings =  DB::table('bookings')->where('bookings.support_booking',1)
        ->where('bookings.status','1')
        ->whereDate('bookings.booking_date', '>=', Carbon::today())
        ->get();
       $completeBookings =  DB::table('bookings')->where('bookings.support_booking',1)
        ->where('bookings.status','8')
        ->whereDate('bookings.booking_date', '>=', Carbon::today())
        ->get();
       $data = array(
          'onGoingBookings' => count($onGoingBookings),
          'todayBookings' => count($todayBookings),
          'completeBookings' => count($completeBookings),
       );
       return $this->sendResponse('Success.',$data);
      
    }

     public function vendorTopBooking(Request $request){
      $user = auth()->user();
       
     
      $booking =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->
          //leftJoin('customers', 'customer_booking.customer_id', '=', 'customers.id')
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
         //->leftJoin('doctors', 'customer_booking.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('support_booking',1)
        ->where('bookings.booking_status','8')
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->paginate(8);
        
        return $this->sendResponse('Success.',$booking);        
        
        $booking = [];

        if($data){

            foreach ($data as $key => $d) {

                $user  = User::select('username')->where('id',$d->user_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['product'] = $user->username;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               
               if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               $booking[$key]['mode'] = $mode;
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }else if($d->booking_status == '9') {
                  $status = 'Cancel Booking';
               }else if($d->booking_status == '10') {
                  $status = 'Rebooking';
               }

               $booking[$key]['status'] = $status;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             
            }

        }

      // print_r($booking);

        return $this->sendResponse('Success.',$booking);
    }

    public function consultationList(Request $request){
      $consultation = DocConsultation::where('status',1)->orderBy('id','asc')->get();
      $getConsltation = ConsultationResource::collection($consultation);
      return $this->sendResponse('Success.',$getConsltation);
    }

}