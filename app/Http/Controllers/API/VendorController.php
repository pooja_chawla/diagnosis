<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Resources\UserResource;
use App\Http\Resources\BookingRequestResource;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\API\{ LoginRequest, AgentRegisterRequest };
use App\Http\Requests\API\{ ChangePasswordRequest };
use App\Http\Resources\ProductResource;
use App\Http\Resources\VisitResource;
use App\Http\Resources\UnassignedBookingResource;
use App\Http\Resources\UpcomingBookingResource;
use App\Helpers\PaymentApp;
use App\Helpers\SmsHelper;
use App\Helpers\EmailFrontend;
use App\Models\User;
use App\Models\Customer;
use App\Models\Doctor;
use App\Models\Booking;
use App\Models\CustomerBookin;
use App\Models\CustomerBookingProduct;
use App\Models\Agent;
use App\Models\AgentSlots;
use App\Models\BookingQuery;
use App\Models\Qrcode;
use App\Models\ProductRequest;
use App\Models\Vendor;
use App\Models\NewProduct;
use App\Models\Product;
use App\Models\PackageTests;
use App\Models\WorkDay;
use App\Models\AgentBookedSlots;
use App\Models\Inventory;
use App\Models\ProductInventory;
use App\Models\VendorProduct;
use App\Models\AgentSlotNotAvailabil;
use App\Models\CustomerReport;
Use \Carbon\Carbon;
use Carbon\CarbonPeriod;
use Mail;
use App\Mail\TestEmail;

class VendorController extends AppBaseController
{

    private $repo;

    public function __construct(UserRepository $repo)
    {

        $this->repo = $repo;
    }
    
     /**
     * Fetch Agents
     *
     * @param  Request $request
     * @return JSON
     */
    public function index(Request $request)
    {

        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');

        // $user = User::find(4);
        // // return $user;
        // $page = User::find(3);



        // $user->rate($page, 4);

        if($latitude && $longitude){

            $users = User::whereHas('roles', function($q){ $q->where('name', 'vendor');
                           })->where('status','Active')->where('latitude',$latitude)->where('longitude',$longitude)->with('vendor')->orderBy('order','desc')->get();
        }else{

            $users = User::whereHas('roles', function($q){ $q->where('name', 'vendor');
                            })->where('status','Active')->with('vendor')->orderBy('order','desc')->get();
        }

        $vendors = [] ;
        if($users){

            foreach ($users as $key => $user) {
                # code...
                if($user->vendor){



                    $vendors[$key]['title']       = $user->vendor->title;
                    $vendors[$key]['description'] = $user->vendor->description;
                    $vendors[$key]['latitude']    = $user->latitude;
                    $vendors[$key]['longitude']   = $user->longitude;
                    $vendors[$key]['image']       = $user->vendor->image ? $user->vendor->image : $user->photo_url;
                    $vendors[$key]['rating']      = $user->averageRating(User::class) ? $user->averageRating(User::class) : 0 ;
                }
            }
        }

        // return ApiResponse::success('success');
        return ApiResponse::success('success', $vendors);
    }
    
    
    public function vendorProducts(Request $request)
    {
        $user = auth()->user();

        $req = $request->all();
        $filter = $req['filter'];
        
       // $data = Product::where('status','1')->whereIn('vendor',['0',$user->id]);
        $data = Product::where('status','1')->where('vendor',$user->id);
         if($filter['booking']){
          $data->where('title', 'like', '%' .$filter['booking']. '%');
          }
        $products = $data->orderBy('order','ASC')->paginate(10);
        return ProductResource::collection($products);  

        $productss = [];
       
        if($vendor_products){
          //print_r($vendor_products);
          $productss['s'] = $vendor_products['current_page'];

            foreach ($vendor_products as $key => $product) {
                # code...
               //print_r($product);
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['type']  = $product->type;
               $products[$key]['image'] = $product->image;
               
               if($product->type == '1'){ 
                  $products[$key]['type'] = 'Package';
               }else{
                $products[$key]['type']        = 'Individual';
               }
               
               $productRequest = ProductRequest::where('product_id',$product->id)->where('vendor',$user->id)->where('status', 2)->where('cancel_status',0)->get();

                if(count($productRequest) != 0){
                      $products[$key]['price'] = $productRequest[0]->price;
                      $products[$key]['discount'] = $productRequest[0]->discount;      
                }else{
                    $products[$key]['price'] = $product->price;
                    $products[$key]['discount']    = $product->discount;   
                }                 
            }

        }


       //print_r($vendor_products);
        return $this->sendResponse('Success.',$productss);
    }
    public function getProducPriceList(Request $request){
        $data = $request->all();
        $user = auth()->user();
        $productRequest = ProductRequest::where('product_id',$data['product'])->where('vendor',$user->id)->get();
        return $this->sendResponse('Success.', $productRequest);
    }
    public function productListing(Request $request){
      $user = auth()->user();

        //$vendor_products = $user->product()->get();
     //   $vendor_products = Product::where('status','1')->where('vendor',$user->id)->orwhere('vendor' , '0')->orderBy('order','desc')->get();

      $vendor_products =  Product::select('products.id','products.title','products.include','products.product_code','products.type','product_request.price','product_request.discount')->
          
         leftJoin('product_request', 'products.id', '=', 'product_request.product_id')
        ->where('product_request.vendor',$user->id)
        ->where('product_request.status','2')
        ->where('product_request.cancel_status','0')
        ->orderBy('product_request.id','DESC')
        ->get();
        $products = [];
       //print_r($vendor_pro1ducts);
        if($vendor_products){

            foreach ($vendor_products as $key => $product) {
               $vendorData = Vendor::where('user_id',$user->id)->first();
               if($product->include[0] == '1'){
                  $products[$key]['charges']= $vendorData->fasting_chr ;
                  $products[$key]['charges']= $vendorData->fasting_chr ;
               }else if($product->include[0] == '2'){
                  $products[$key]['charges'] = $vendorData->nfasting_chr ;
               }else if($product->include[0] == '3'){
                  $products[$key]['charges'] = $vendorData->after_meal_chr ;
               }else if($product->include[0] == '4'){
                  $products[$key]['charges'] = $vendorData->other_chr ;
               } 
               $products[$key]['include'] = $product->include[0];
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['type']  = $product->type;
               //$products[$key]['image'] = $product->image;
               if($product->type == '1'){ 
                  $products[$key]['type'] = 'Package';
               }else{
                $products[$key]['type']        = 'Individual';
               }

                          
             
              $products[$key]['price'] = $product->price;      
              $products[$key]['discount'] = $product->discount;      
                 
             
            
          }

        }


      // print_r($products);
        return $this->sendResponse('Success.',$products);
    }
    public function getProductData($id){
      $vendor_id = auth()->user();
     $vendor_products = VendorProduct::where('product_id' , $id)->where('vendor_id' , $vendor_id->id)->get();

        $products = [];
       //print_r($vendor_products);
        if($vendor_products){

            foreach ($vendor_products as $key => $vendor_product) {

                $product  = Product::where('id',$vendor_product->product_id)->first();
                # code...
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['type']    = $product->type;
               $products[$key]['image']        = $product->image;
               if($vendor_product->offered_price == ''){
                  $products[$key]['price']       = $vendor_product->price;
               }else{
                  $products[$key]['price']       = $vendor_product->offered_price;
               }
               
               $products[$key]['discount']    = $vendor_product->discount;
               $products[$key]['created_at']  = $vendor_product->created_at;
                
            }

        }


      // print_r($products);
        $productDetail = $products;
        return $this->sendResponse('Success.',$productDetail);
    }
    public function getAddressDistance(Request $request){
      $data = $request->all();/*
      $vendor = auth()->user();
      $vendor_latitude =  $vendor['latitude'];
      $vendor_longitude = $vendor['longitude'];
      $a =round(PaymentApp::distance($vendor_latitude, $vendor_longitude, $data['lat'], $data['lon']),2);
      $price = $a*10;
      //echo  $a;
      return ApiResponse::success('success', $price);*/
    }
    public function getProductDetails(Request $request){

      $data = $request->all();
      $vendor_products = Product::where('id' , $data['product'])->get();
      $products = [];
      $vendor_id = auth()->user();
      $productDetail = [];
      $inventory = [];
      $total_price = 0;
      $discount = 0;
      $auth = auth()->user();
      $vendor_latitude =  $vendor_id['latitude'];
      $vendor_longitude = $vendor_id['longitude']; 

      if($vendor_products){
        foreach ($vendor_products as $key => $product) {
           
           $inventory =ProductInventory::select( "inventories.name","inventories.id","inventory_product.product_id")
                        ->leftJoin("inventories", "inventories.id", "=", "inventory_product.inventory_id")
                        ->where('inventory_product.product_id',$product->id)
                        ->get()->toArray();
            $productRequest = ProductRequest::where('product_id',$product->id)->where('vendor',$auth->id)->orderBy('id','DESC')->get();             
           $products['id'] = $product->id;
           $products['title'] = $product->title;
           $products['offered_price'] = 0;
           $products['offered_discount'] = 0;
          if(count($product->include)!= 0){
           
           foreach( $product->include as $include ){
              if($include == 1){
                   $includeValue = 1; 
                   $include = 'Fasting';
                 }elseif($include == 2){
                  $includeValue = 2;
                  $include = 'Non-Fasting';
                 }elseif($include == 3){
                  $includeValue = 3;
                  $include = '2 hours after food';
                 }elseif($include == 4){
                  $includeValue = 4;
                  $include = 'Other';
                 }elseif($include == 0){
                   $includeValue = '';
                  $include = '';
                 }
           }
          }else{
            
            $include = '';
                 
          }
           if(count($productRequest) != 0){
            foreach($productRequest as $pro){
              if($pro->status == 2 && $pro->cancel_status == 0 ){
                  
                    $products['offered_price'] = $pro->price;      
                    //$products['offered_price'] = $productRequest[0]->price;      
                  
                  
                    $products['offered_discount'] = $pro->discount;      
                     //$products['offered_discount'] = $productRequest[0]->discount;      
                    $products['price']    =  $pro->price;
                    $products['discount']    = $pro->discount;        
              }
              if($pro->status == 1 && $pro->cancel_status == 0 ){
                  $products['pro_request'] = $pro->status;
              }
            }
           }else{
              $products['pro_request'] = '0';
           }
           
                   
           if($product->type == '1'){ 
              $type        = 'Package';
           }else{
            $type        = 'Individual';
           }
           $products['type']    = $type;
           $products['image']        = $product->image;
           
           $products['product_code']    = $product->product_code;
         
           $products['include']    = $include;
           $products['includeValue']    = $includeValue;
           
           $products['description']    =  $product->description;
           $products['inventories']    =  $inventory;
              
         }
               
         $total_price += $product->price ;
         $discount += $product->discount;
         
        }

        $productDetail['product'][]  = $products;
        $productDetail['payment']['subtotal']  = $total_price;
        $productDetail['payment']['total_price']  = $total_price - $discount;
        $productDetail['payment']['discount']  = $discount;
        return $this->sendResponse('Success.',$productDetail);
    }
    public function sendProductRequest(Request $request){
      $data = $request->all();
      $vendor_id = auth()->user();
      $detail = array(
        'product_id' =>$data['product'],
        'vendor' => $vendor_id->id,
        'price' =>$data['price'],
        'discount' =>$data['discount'],
        'status' =>'1',
      );

      $query = ProductRequest::where('product_id',$data['product'])->where('vendor',$vendor_id->id)->where('status',1)->where('cancel_status', 0)->get();
      if(count($query) != 0){
           ProductRequest::where('product_id',$data['product'])->where('vendor',$vendor_id->id)->update(['price'=> $data['price'] , 'discount' => $data['discount']]);   
      }else{
        ProductRequest::create($detail);
      }

            
      return $this->sendResponse('Success.');
    }
    public function sendQuery(Request $request){
      $data = $request->all();
      $query = BookingQuery::create($data);
 

      return $this->sendResponse('Success.');
    }
    
    public function productDetailId(Request $request){
       $data = $request->all();
       $vendor_id = auth()->user();
       $productDetail = [];
       $total_price = 0;
       $discount = 0;
       $includealert = 0;
       foreach($data['product'] as $keys =>$product){
            $id =  $product['value'];
            //$vendor_products = VendorProduct::where('product_id' , $id)->where('vendor_id' , $vendor_id->id)->get();
            $vendor_products = Product::where('id' , $id)->get();

            $products = [];
            $vendorCharges = [];
            $otherCharges = 0;
            $inc= [];
           
            if($vendor_products){

                  foreach ($vendor_products as $key => $product) {

               // $product  = Product::where('id',$vendor_product->product_id)->first();
                # code...

               $products['id'] = $product->id;
               $products['title'] = $product->title;
               if($product->type == '1'){ 
                  $type        = 'Package';
               }else{
                $type        = 'Individual';
               }
               $products['type']    = $type;
               $products['image']        = $product->image;
              
               $products['discount']    = $product->discount;
               $products['price']    =  $product->price;
               $products['description']    =  $product->description;
               $inc['a'] = $product->include[0];
               $vendorData = Vendor::where('user_id',$vendor_id->id)->first();
               if($inc['a'] == '1'){
                  $vendorCharges= $vendorData->fasting_chr ;
               }else if($inc['a'] == '2'){
                  $vendorCharges = $vendorData->nfasting_chr ;
               }else if($inc['a'] == '3'){
                  $vendorCharges = $vendorData->after_meal_chr ;
               }else if($inc['a'] == '4'){
                  $vendorCharges = 0;
                  $otherCharges = $vendorData->other_chr ;
               }
               $products['include']    =  $inc;
               $productRequest = ProductRequest::where('product_id',$product->id)->where('vendor',$vendor_id->id)->where('status',2)->where('cancel_status',0)->orderBy('id','DESC')->limit(1)->get();             
               if(count($productRequest) != 0){
                
                    
                      $products['price'] = $productRequest[0]->price;      
                    
                    
                      $products['discount'] = $productRequest[0]->discount;      
                     
                }                      
                 
                 
            }

               $total_price += $products['price'] ;
               $discount += $products['discount'];
               


        }     
        $productDetail['product'][]  = $products;
         //$productDetail['vendorCharges'][]  = $vendorCharges;
     
         $heighVal[]  = $vendorCharges;
         $include[]= $inc;
        
       }

       $getVal = max($heighVal);
       $c = $getVal;
       if($otherCharges != 0){
       
         $getVal = (int)$getVal + (int)$otherCharges;
       }
       
       foreach ($include as $incValue) {
            if($incValue['a'] == 1){
               $includealert = 1;
            }
       }

       
        $productDetail['payment']['subtotal']  = $total_price;
        $productDetail['payment']['chargesVal']  = $getVal;
        $productDetail['payment']['c']  = $c;
        //$productDetail['payment']['vendorCharges']  = $vendorCharges;
        $productDetail['payment']['total_price']  = $total_price - $discount;
        $productDetail['payment']['discount']  = $discount;
        $productDetail['payment']['includealert']  = $includealert;
          
       return $this->sendResponse('Success.',$productDetail);

    }
    public function doctors(Request $request){
        $doc = Doctor::get();
        return $this->sendResponse('Success.',$doc);
    }
    public function addDoctor(Request $request){
      $data = $request->all();
      $doc = Doctor::create(['name'=> $data['doc']]);
      return $this->sendResponse('Success.',$doc);
    }

    public function addNewPatient(Request $request){
      $data = $request->all();
      $vendor = auth()->user();
      $booking = CustomerBookin::where('id', $data['booking_id'])->first();
      $userData = array(
            'name' => $data['f_name'],
            'username' => $data['f_name'].' '.$data['l_name'],
            'email' => (isset($data['email'])) ? $data['email'] : '',

        );
        $users = User::create($userData);
        
         $customerData = array(
            'user_id' => $users->id,
            'age' => $data['age'],
            'gender' => $data['gender'],
            'phone' => $data['phone'],
            'medical_condition' => $data['medical_condition']
        );
        $customer = Customer::create($customerData);
        
        $booking = array(
            'vendor_id' => $vendor->id,
            'user_id' => $users->id,
            'customer_id' => $customer->id,  
            'refered_by_doc' => $data['referred_by']['value'],
            'address' =>$booking->address, 
            'time_slot' =>$booking->time_slot,
            'price' =>$data['price'],
            'discount' =>$data['discount'],
            'payment_mode' => $booking->payment_mode,
            'total_price' =>$data['total_price'],
            'sub_total' =>$data['subtotal'],
            //'agent_id' =>$data['agent'],
            'day_id' =>$booking->day_id,
            'booking_date'=> $booking->booking_date,
            'city'=> $booking->city,
            'state'=> $booking->state,
            'zip'=> $booking->zip,
            'lat'=> $booking->lat,
            'lang'=> $booking->lang,
            'address1'=> (isset($booking->address1)) ? $booking->address1 : '',
            'address2'=> (isset($booking->address2)) ? $booking->address2 : '',
            'neighbourhood'=> (isset($booking->neighbourhood)) ? $booking->neighbourhood : '',
            'landmark'=> (isset($booking->landmark)) ? $booking->landmark : '',
            
            'convenience'=> $data['convenience'],
            'status' => 1
        );
        $booking = CustomerBookin::create($booking);
      //print_r($booking);
      //echo $booking->address;
      
        foreach($data['product_id'] as $cart){
        $cart_data['booking_id'] = $booking->id;
        $cart_data['title'] = $cart['productName'];
        $cart_data['price'] = $cart['price'];
        $cart_data['discount'] = $cart['discount'];
        CustomerBookingProduct::create($cart_data);
      }
        $not_avail= array(
          'day' => 0,
          'booking_id' => $booking->id,
          'slot' => $booking->time_slot,
          'status' =>'1',
        );
        AgentSlotNotAvailabil::create($not_avail);
        return $this->sendResponse('Successfully Added.',$booking);
                        
    }
    public function UpdatePaymentBooking(Request $request){
      $data = $request->all();
      $booking = array(
            'discount' => $data['discount'],
            'convenience' => $data['convenience'],
            'total_price' => $data['total_price'],
        );
      $booking = CustomerBookin::where('id',$data['booking_id'])->update($booking);
      return $this->sendResponse('Successfully Updated.');
    }
    public function UpdatePatientBooking(Request $request){
     $this->validate($request, [
          'user_name' => 'required|string',
          'gender' => 'required',
          'email' => 'email',
          'age' => 'required',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
       ]); 
     $data = $request->all();
     
      $userData = array(
            
            'username' => $data['user_name'],
            'email' => (isset($data['email'])) ? $data['email'] : '',
      );
      $users = User::where('id',$data['user_ids'])->update($userData);
      $customerData = array(
            'age' => $data['age'],
            'gender' => $data['gender'],
            'phone' => $data['phone'],
            'medical_condition' => $data['user_condition'],
        );
      $customer = Customer::where('user_id',$data['user_ids'])->update($customerData);
      $booking = array(
            'refered_by_doc' => (isset($data['referred_by']['value'])) ? $data['referred_by']['value'] : '',
        );
      $booking = CustomerBookin::where('id',$data['booking_id'])->update($booking);
      
      return $this->sendResponse('Successfully Updated.'); 
    }
    public function addBookingw(Request $request){
      $data = $request->all();
      $vendorCharges = [];
      $otherCharges = 0;
       foreach($data['test'] as $cart){
        if($cart['include'] == '1'){
                  $vendorCharges= $cart['charges'] ;
        }else if($cart['include'] == '2'){
                  $vendorCharges = $cart['charges'] ;
        }else if($cart['include'] == '3'){
                  $vendorCharges = $cart['charges'] ;
        }else if($cart['include'] == '4'){
                  $vendorCharges = 0;
                  $otherCharges = $cart['charges'] ;
        }
        $heighVal[]  = $vendorCharges;
      
       }
       $getVal = max($heighVal);
       if($otherCharges != 0){
        $getVal = (int)$getVal + (int)$otherCharges;
       }
       print_r($getVal);
    }
   
    public function addBooking(Request $request){
     $this->validate($request, [
          'first_name' => 'required|string',
          'last_name' => 'required|string',
          'gender' => 'required',
          'email' => 'email',
          'age' => 'required',
          'test' => 'required',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
          'referred_by' => 'required',
          'city' => 'required|string',
          'state' => 'required|string',
          'zip' => 'required|string',
          'payment_mode' => 'required',
          'booking_date' => 'required',
          'slot' => 'required',
       ]); 

        $data = $request->all();

        //print_r($data);
        $day_id = WorkDay::select('id')->where('day', $data['day'])->first();
        $day = $day_id->id;
        $vendor = auth()->user();
        $userData = array(
            'name' => $data['first_name'],
            'username' => $data['first_name'].' '.$data['last_name'],
            'email' => (isset($data['email'])) ? $data['email'] : '',

        );
        $users = User::create($userData);

         $customerData = array(
            'user_id' => $users->id,
            'age' => $data['age'],
            'gender' => $data['gender'],
            'phone' => $data['phone'],
            'medical_condition' => (isset($data['medical_condition'])) ? $data['medical_condition'] : ''
        );
        $customer = Customer::create($customerData);
       
        $now = \Carbon\Carbon::today();
        $date = strtotime($now);
        $new_date = date('y-m-d', $date);
        $bdate = date_create($data['booking_date']);
        $b =  date_format($bdate, 'Y-m-d');
        $vendorCharges = [];
        $otherCharges = 0;
         foreach($data['test'] as $cart){
          if($cart['include'] == '1'){
                    $vendorCharges= $cart['charges'] ;
          }else if($cart['include'] == '2'){
                    $vendorCharges = $cart['charges'] ;
          }else if($cart['include'] == '3'){
                    $vendorCharges = $cart['charges'] ;
          }else if($cart['include'] == '4'){
                    $vendorCharges = 0;
                    $otherCharges = $cart['charges'] ;
          }
          $heighVal[]  = $vendorCharges;
        
         }
         $getVal = max($heighVal);
         if($otherCharges != 0){
          $getVal = (int)$getVal + (int)$otherCharges;
         }

        $booking = array(
            'vendor_id' => $vendor->id,
            'user_id' => $users->id,
            'customer_id' => $customer->id,  
            'refered_by_doc' => (isset($data['referred_by']['value'])) ? $data['referred_by']['value'] : '',
            'address' =>$data['address'], 
            'time_slot' =>$data['slot'],
            'price' =>$data['price'],
            'discount' =>$data['discount'],
            'payment_mode' =>$data['payment_mode'],
            'total_price' =>$data['total_price'],
            'sub_total' =>$data['subtotal'],
            //'agent_id' =>$data['agent'],
            'day_id' =>$day,
            'booking_date'=> $b,
            'city'=> $data['city'],
            'state'=> $data['state'],
            'zip'=> $data['zip'],
            'lat'=> $data['lat'],
            'lang'=> $data['lang'],
            'address1'=> (isset($data['address1'])) ? $data['address1'] : '',
            'address2'=> (isset($data['address2'])) ? $data['address2'] : '',
            'neighbourhood'=> (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '',
            'landmark'=> (isset($data['landmark'])) ? $data['landmark'] : '',
            
            'convenience'=> $data['convenience'],
            'bookingRequest'=> $data['bookingRequestVal'],
            'charges'=> $getVal,
            'status' => 1
        );
        $testList= [];
        $booking = CustomerBookin::create($booking);
        foreach($data['test'] as $cart){
        $testList[] = $cart['productName'];
        $cart_data['booking_id'] = $booking->id;
        $cart_data['title'] = $cart['productName'];
        $cart_data['price'] = $cart['price'];
        $cart_data['discount'] = $cart['discount'];
        // $cart_data['charges'] = $cart['charges'];
        CustomerBookingProduct::create($cart_data);
      }
        $not_avail= array(
          'day_id' => $day,
          'booking_id' => $booking->id,
          'slot' => $data['slot'],
          'status' =>'1',
        );
            $expTest = implode(',', $testList);
            $AgentSlots = AgentSlots::find($data['slot']);
            $Doctor = Doctor::find($data['referred_by']);

            $address1 = (isset($data['address1'])) ? $data['address1'] : '';
            $address2 = (isset($data['address2'])) ? $data['address2'] : '';
            $neighbourhood = (isset($data['neighbourhood'])) ? $data['neighbourhood'] : '';
            $landmark = (isset($data['landmark'])) ? $data['landmark'] : '';

$test = "";
$temp = "";
$vendor_email = $vendor->email;
  foreach($data['test'] as $cart){
    $test .= "<tr><th scope='row' style='width: 33%;border-right: 1px solid;'>Test</th><td style='border-right: 1px solid;'>".$cart['productName']."</td><td>".$cart['price']."</td></tr>";
  }

$temp ='<html><head><style>.table td, .table th { padding: 5px;vertical-align: middle;border-top: 1px solid #000000;font-size: 15px;}</style></head><body>';
$temp .="<div style='margin:0 auto;font-family:'Poppins', sans-serif;width: 800px;background: #fff;padding: 30px;border: 1px solid #E5E5E5;'>";
$temp .="<div style='color:#00008B;font-size:20px;font-weight:600'>Dear Customer, Welcome to uDAUD and the world of healthy Habits to keeps yourself Fit and aware. Your appointment for the blood/other Sample collection is received with the following details.</div>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: block;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 90%;'><h5 style='margin: 0px 0 0px;''>".$vendor->username."</h5><p style='margin: 10px 0 0;font-size: 15px;line-height: 20px;'>".$vendor->address."</p>";
$temp .="</td><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 10%;'><img src='https://udaudindia.com/_nuxt/img/logo.61b22fc.png' style='width: 100px;'></td>";            
$temp .='</tr> </tbody></table>';
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 20px 0px;display: inline-table;margin: 10px 0 10px;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;'>".$b."</td>";            
$temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: center;'>Home Collection Slip</td>";            
$temp .="<td style='margin: 0px 0 0px;padding: 0 0 0px;font-size: 18px;font-weight: bold;width: 33%;text-align: right;'>Page 1 of 1</td>";            
$temp .='</tr></tbody></table>';
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<table class='table' style='width: 100%;border-spacing: 0px;'>";
$temp .="<tbody><tr><th scope='row'>Date</th><td>".$b."</td><th scope='row' style='border: 0;'>Collection Time</th><td style='border: 0;'>".$AgentSlots->slot."</td>";
$temp .="</tr><tr><th scope='row'>Name of Patient</th><td>".$data['first_name']." ".$data['last_name']."</td><th scope='row'>Sex</th><td>".$data['gender']."</td></tr>";
$temp .="<tr style='position:relative;'><th scope='row'>Age</th><td>".$data['age']. "Yrs</td></tr><tr><th scope='row'>Address</th>";
$temp .="<td colspan='3'>".$data['address']." ".$data['city']." ".$address1." ".$address2." ".$neighbourhood." ".$landmark." ".$data['zip']."</td>";
$temp .="</tr><tr><th scope='row'>Mobile</th><td colspan='3'>".$data['phone']."</td>";
$temp .="</tr><tr><th scope='row'>Paid Mode</th><td colspan='3'>".$data['payment_mode']."</td></tr></tbody></table></td></tr></tbody></table>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;border: 1px solid #000;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<table class='table' style='width: 100%;border-spacing: 0px;'><tbody>".$test."<tr>";
$temp .="<th scope='row' colspan='2' style='width: 33%;border-right: 1px solid;text-align: right;font-size: 21px;padding: 0px 10px 0px 0px;'>Total</th>";
$temp .="<td style='font-weight: bold;font-size: 21px;'>".$data['total_price']."/-</td></tr></tbody></table></td></tr></tbody></table>";
$temp .="<table border='0' cellspacing='0' cellpadding='0' style='border-collapse:collapse;width: 100%;padding: 10px 20px;display: inline-table;margin:20px 0 0;'>";
$temp .="<tbody><tr><td style='margin: 0px 0 0px;padding: 0 0 0px;width: 100%;text-align: left;position:relative;'>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
$temp .='<li>You will receive a confirmation SMS after booking an appointment.</li>';
$temp .='<li>Reminder SMS will be shared half an hour prior to your confirmed appointment time.</li></ul>';
$temp .="<h4 style='margin: 15px 0 5px;font-size:16px'>Fasting Health Check-up pre-requisites:</h4>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto; font-size:14px'>";
$temp .='<li>10-12 hours of overnight fasting is required prior to the day of the blood collection.</li>';
$temp .='<li>Avoid alcohol at least 24 hours before the blood collection appointment.</li>';
$temp .='<li>Avoid Tea/Coffee/Other beverages before the blood collection. Water intake is allowed.</li></ul>';
$temp .="<h4 style='margin: 15px 0 0px; font-size:16px'>Kindly confirm the list of your tests to be conducted with our scientific officer in one appointment as there will be no repeat visit for pending tests.</h4>";
$temp .="<h4 style='margin: 0px 0 5px; font-size:16px'>Disclaimer:</h4>";
$temp .="<p style='font-size:14px;margin:0'>The Health Check-up requires you to give your blood sample. The Following associated rare incidences may occur during and after blood collection:</p>";
$temp .="<ul style='padding: 0 15px;margin: 0;list-style: auto;font-size:14px'>";
$temp .='<li>Discomfort, swelling, redness or dizziness while or immediately after blood collection, the phlebotomist would know how to deal with the same. Request not to panic. Inform our technical expert or pathology lab immediately for support.</li>';
$temp .='<li>In case your blood sample gets lysed, your selected pathology lab will approach you for a fresh sample collection, the same day or another mutually decided date.</li>';
$temp .='<li>All the protocols followed are as per guidelines . Factors such as physiological disturbances, fever, dehydration, haemolysis, etc. can cause variation in reported results.</li></ul>';
$temp .="<h4 style='margin: 15px 0 5px; font-size:16px'>Request you to share a valid identity proof for the medical test. Acceptable identity proofs are: PAN Card, Driving Licence, Passport, Adhaar Card, Voter ID card.</h4>";
$temp .="<p style='font-size:14px;margin:0'>In case of any queries, please write to us at info@udaudindia.com (Mon-Sat, 9am to 5pm)</p>";
$temp .="<h4 style='margin:15px 0 5px; font-size:16px'> We hope your experience is positive with us and look forward towards improving our services and serving you always.</h4>";
$temp .='</td></tr></tbody></table></div>';
$temp .='------------------------------------------------------------------------------ <br/>';
$temp .='<p>CONFIDENTIALITY NOTICE: If you have received this email in error, <br/>  please immediately notify the sender by e-mail at the address shown.<br/> This email transmission may contain confidential information.<br/> This information is intended only for the use of the individual(s) or entity to whom it is intended even if addressed incorrectly. Please delete it from your files if you are not the intended recipient. Thank you for your compliance.<br/> Copyright (c) 2021  Nexx Wave Health Solutions Pvt Ltd .</p>';
$temp .='==================================================';
$temp .='</body></html>';
   $sub = 'Your Appointment Schedule - Home Sample collection.';
        if($data['email'])
        {
            EmailFrontend::sendEmailFrontend($data['email'],$sub,$temp);
            EmailFrontend::sendEmailFrontend($vendor->email,$sub,$temp);
        }

        if($data['phone']!=''){
        $lab =array();
        $arr = preg_split('/[,\ \.;]/', $vendor->username);
        $keywords = array_unique($arr);
        $i=0;
        foreach ($keywords as $keyword){
        if($i < 3){
        $lab[] = $keyword;
        }
        $i++;
        }
        $labimp = implode(' ', $lab);
    $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$data['phone'].'&message=Dear '. $data['first_name'].' '.$data['last_name'].', Welcome to the world of healthy Habits to keeps yourself Fit and aware. Your appointment with '.$labimp.' for the blood/other Sample collection on date '.$b.' time '.$AgentSlots->slot.' is received. Our scientific Officer will visit you on the requested time to collect the Blood/Other samples. Please be ready in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
          $smsUrl = str_replace(' ', '%20', $homepage);
            $curl = curl_init();

            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);
        }
        AgentSlotNotAvailabil::create($not_avail);
        $bookingNewId = ['booking_id' => $booking->id ];
        return $this->sendResponse('Successfully Added.',$bookingNewId);
        
    }

    public function updateProduct(Request $request){
   
     $auth = auth()->user();
     $this->validate($request, [
          'title' => 'required|string',
          'include' => 'required',
          'price' => 'required',
          'inventory' => 'required',
       ]); 
     $data = $request->all(); 
     $title = preg_replace('/[^A-Za-z0-9\-]/', '', $data['title']);
     $auth = auth()->user();
     $product = array(
        'title' => $data['title'],
        'slug' => $title.'-'.rand(),
        'product_code' => 'PRO-'.rand(),
        'description' => (isset($data['description'])) ? $data['description'] : '',
        'price' => $data['price'],
        'include' => $data['include'],
        'discount' => (isset($data['discount'])) ? $data['discount'] : '0',
        'status' => 1,
        
     );
      $productId = NewProduct::where('id', $data['productId'])->update($product);
       $booking_req = array(
        'price' => $data['price'],
        'discount' => (isset($data['discount'])) ? $data['discount'] : '0',
      );
      ProductRequest::where('product_id', $data['productId'])->update($booking_req);
      $vendor = array(
        'offered_price' => $data['price'],
        'discount' => $data['discount'],
     );
     VendorProduct::where('product_id',$data['productId'])->where('vendor_id',$auth->id)->update($vendor);
     $checkInven = ProductInventory::where('product_id',$data['productId'])->get();

     if(count($checkInven)!=0){
        $inventory =array(
          'inventory_id'=> $data['inventory']
        );
        ProductInventory::where('product_id',$data['productId'])->update($inventory);
      
      }else{
        $inventory =array(
          'inventory_id'=> $data['inventory'],
           'product_id' =>$data['productId'],
        );
        ProductInventory::create($inventory);
      
      }
      return $this->sendResponse('Successfully updated.',$product);

    }

    public function addProduct(Request $request)
    {
     // print_r($request->all());
    $auth = auth()->user();
     $this->validate($request, [
          'title' => 'required|string',
          'include' => 'required',
          'price' => 'required',
          'inventory' => 'required',
       ]); 
     $data = $request->all(); 
     $title = preg_replace('/[^A-Za-z0-9\-]/', '', $data['title']);
     $auth = auth()->user();
     $product = array(
        'title' => $data['title'],
        'slug' => $title.'-'.rand(),
        'product_code' => 'PRO-'.rand(),
        'description' => (isset($data['description'])) ? $data['description'] : '',
        'price' => $data['price'],
        'discount' => (isset($data['discount'])) ? $data['discount'] : '0',
        'include' => $data['include'],
        'type' => $data['type'],
        'status' => 1,
        'vendor' => $auth->id,
     );
     

     $productId = NewProduct::create($product);

     $vendor = array(
        'vendor_id' => $auth->id,
        'product_id' => $productId->id,
        'offered_price' => $data['price'],
        'discount' => $data['discount'],
        'charges' => (isset($data['charges'])) ? $data['charges'] : '0',
        
     );
     VendorProduct::create($vendor);
     if(count($data['inventory']) != 0){
        $inventory =array(
          'product_id' =>$productId->id,
          'inventory_id'=> $data['inventory']['value']
        );
        ProductInventory::create($inventory);
      
      }
      if($data['type'] == 1){
      if(count($data['tests']) != 0){
       foreach($data['tests'] as $test){
          $inpTest =array(
            'product_id' =>$productId->id,
            'name'=> $test['test']
          );
          PackageTests::create($inpTest);
       }
      }
    }
      $booking_req = array(
        'product_id' => $productId->id,
        'vendor' => $auth->id,
        'price' => $data['price'],
        'discount' => (isset($data['discount'])) ? $data['discount'] : '0',
        'status' =>2,
        'cancel_status' => 0 ,
      );
      ProductRequest::create($booking_req);
        return $this->sendResponse('Successfully Added.',$product);

    }

    public function myProducts(Request $request){
      $user = auth()->user();

      $req = $request->all();
      $filter = $req['filter'];

      $data =  Product::select('products.id','products.title','products.product_code','products.type','product_request.price','product_request.discount')->
          
         leftJoin('product_request', 'products.id', '=', 'product_request.product_id')
        ->where('product_request.vendor',$user->id)
        ->where('product_request.status','2')
        ->where('product_request.cancel_status','0');
        if($filter['booking']){
          if($filter['booking'] == 'package'){
              $data->where('products.type', '1');
           }else if($filter['booking'] == 'individual'){
               $data->where('products.type', '2');
           }else{
              $data->where('products.title', 'like', '%' .$filter['booking']. '%'); 
           }
         }
       $vendor_products =  $data->orderBy('product_request.id','DESC')->paginate(10);

        return $this->sendResponse('Success.',$vendor_products);

        $products = [];
       //print_r($vendor_pro1ducts);
        if($vendor_products){

            foreach ($vendor_products as $key => $product) {

                //$product  = Product::where('id',$vendor_product->product_id)->first();
                # code...
               $products[$key]['id'] = $product->id;
               $products[$key]['title'] = $product->title;
               $products[$key]['type']  = $product->type;
               $products[$key]['image'] = $product->image;
               $products[$key]['price'] = $product->price;
               if($product->type == '1'){ 
                  $products[$key]['type'] = 'Package';
               }else{
                $products[$key]['type']        = 'Individual';
               }
               /*if($vendor_product->offered_price == ''){
                  $products[$key]['price']       = $vendor_product->price;
               }else{
                  $products[$key]['price']       = $vendor_product->offered_price;
               }*/
               
               $products[$key]['discount']    = $product->discount;
               //$products[$key]['created_at']  = $vendor_product->created_at;
                
            }

        }


      // print_r($products);
        
    }

    public function getTubes(){
      $inventory = Inventory::where('status' , 1)->get();
      return $this->sendResponse('Success.',$inventory);
    }

    public function getPpSlot(Request $request){
      $data = $request->all();
      
    }

    public function bookingCharges(){
      $user = auth()->user();
      $vendorDetail = Vendor::where('user_id',$user->id)->first(); 
      $data =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.request_status','bookings.lat','bookings.lang', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->
          //leftJoin('customers', 'customer_booking.customer_id', '=', 'customers.id')
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
         //->leftJoin('doctors', 'customer_booking.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('vendor_id',$user->id)
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->get();
                
        $booking = [];
      
        if($data){

            foreach ($data as $key => $d) {

               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $d->username;
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }

               $booking[$key]['booking_status'] = $status;
                $vendor_latitude =  $user['latitude'];
                $vendor_longitude = $user['longitude'];
                $distanceInKm =round(PaymentApp::distance($vendor_latitude, $vendor_longitude, $d->lat, $d->lang),2);
                $booking[$key]['distance'] = round($distanceInKm , 0);
                //$booking[$key]['distance'] = $distanceInKm;
                $km = $vendorDetail->perkm; //mini distance
                $km_price = $vendorDetail->fix_price; //mini price
                $perkm = $vendorDetail->flat_price_perkm; //mini price


                if($km >= $distanceInKm){
                  $booking[$key]['charges']  = round($km_price , 0);; 
                }else{
                  $a = $distanceInKm - $km;
                  $totalKm = $a*$perkm+$km_price;
                  $booking[$key]['charges']  = round($totalKm , 0);
                }

               ///$booking[$key]['payment_status'] = $pay_status;
               $booking[$key]['total_price'] = $d->total_price;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             

            }
            
        }

      // print_r($booking);

      return $this->sendResponse('Success.',$booking);
      
    }

    public function bookingRequestList(Request $request){
      $user = auth()->user();

      $req = $request->all();
      $filter = $req['filter'];
       
      $data =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.bookingRequest','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')
        ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('vendor_id',$user->id)
        ->where('bookingRequest','!=', 0)
        ->orderBy('bookings.id','DESC');
        if($filter['booking']){
          if($filter['booking'] == 'pending'){
              $data->where('bookings.bookingRequest', '1');
           }else if($filter['booking'] == 'accept'){
              $data->where('bookings.bookingRequest', '2');
           }else if($filter['booking'] == 'reject'){
              $data->where('bookings.bookingRequest', '3');
           }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
           }
         }
        $booking = $data->paginate(10);
                
        
        return $this->sendResponse('Successfully Added.',$booking);
    }

    public function bookingList(Request $request){
      $user = auth()->user();
      $req = $request->all();
      $filter = $req['filter'];
      $data =  CustomerBookin::select('bookings.id','bookings.assigned_agent','bookings.total_price','bookings.order_id','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->leftJoin('users', 'bookings.user_id', '=', 'users.id')->where('vendor_id',$user->id)
        ->whereNotIn('bookingRequest',['1','3']);
      if($filter['booking']!='' || $filter['tDate']!='' || $filter['fDate'] != '' ){
       
          if($filter['fDate']){
            $data->whereDate('bookings.created_at','>=',$filter['fDate']);
          }
          if($filter['tDate']){
            $data->whereDate('bookings.created_at','<=',$filter['tDate']);
          } 
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
          }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
          }
      }
      $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
                
        return VisitResource::collection($booking);
    }
    public function chargeBookingList(Request $request){
      $user = auth()->user();
      $req = $request->all();
      $filter = $req['filter'];
      $data =  CustomerBookin::select('bookings.id','bookings.assigned_agent','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.request_status','bookings.charges', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->leftJoin('users', 'bookings.user_id', '=', 'users.id')->where('vendor_id',$user->id)
        ->whereNotIn('bookingRequest',['1','3'])->whereDate('bookings.created_at','>=','2021-12-04');
      if($filter['booking']!='' || $filter['tDate']!='' || $filter['fDate'] != '' ){
       
          if($filter['fDate']){
            $data->whereDate('bookings.created_at','>=',$filter['fDate']);
          }
          if($filter['tDate']){
            $data->whereDate('bookings.created_at','<=',$filter['tDate']);
          } 
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
          }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
          }
      }
      $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
                
        return VisitResource::collection($booking);
    }
    public function bookingLists(Request $request){
      $user = auth()->user();
      $req = $request->all();
      $filter = $req['filter'];

      $data =  CustomerBookin::select('bookings.id','bookings.assigned_agent','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->leftJoin('users', 'bookings.user_id', '=', 'users.id')->where('vendor_id',$user->id)
        ->whereNotIn('bookingRequest',['1','3']);
         if($filter['booking']){
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
           }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
           }
           

         }

       $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
                
        return VisitResource::collection($booking);
        //return $this->sendResponse('Successfully Added.',$booking);

        if($data){

            foreach ($data as $key => $d) {

                // $user  = z::select('username')->where('id',$d->user_id)->first();
               // $doctor  = Doctor::select('name')->where('id',$d->vendor_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $d->username;
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }

               $booking[$key]['booking_status'] = $status;
              if($d->payment_status == '0'){
                  $pay_status = 'Pending';
               }else if($d->payment_status == '1'){
                  $pay_status = 'Already Paid';
               }else if($d->payment_status == '2'){
                  $pay_status = 'Payment Collected by Agent';
               }else if($d->payment_status == '3'){
                  $pay_status = 'Payment Received';
               }else if($d->payment_status == '4'){
                  $pay_status = 'Paid';
               }

               ///$booking[$key]['payment_status'] = $pay_status;
               $booking[$key]['total_price'] = $d->total_price;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             

            }
            
        }

      

      return $this->sendResponse('Success.',$booking);
        
    }

        public function teamtodaybookingList(){
      $user = auth()->user();
      $useragent = $user->assigned_agent;
      if($useragent){
        $expagent = explode(',', $useragent);

  
         $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->whereIn('bookings.assigned_agent',$expagent)
        ->whereDate('bookings.booking_date','=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->paginate(10);
            return UpcomingBookingResource::collection($data);
       }
       return $this->sendResponse('Success.');
    }

    public function bookingRequest(){
       $user = auth()->user();
        $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->where('bookings.bookingRequest',1)
       ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->paginate(10);
        return BookingRequestResource::collection($data);
        /*$booking = [];
                foreach ($data as $key => $d) {
              $vendor = User::select('username')->where('id',$d->vendor_id)->first();
              $slot = AgentSlots::find($d->time_slot);
               $booking[$key]['id'] = $d->id;
               $booking[$key]['slot'] = $slot->slot;
                $booking[$key]['bookingRequest'] = $d->bookingRequest;
               $booking[$key]['vendor'] = $vendor->username;
               $booking[$key]['user'] = $d->username;
               $booking[$key]['address'] = $d->address;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             
            }
          return $this->sendResponse('Success.',$booking);*/
    }

public function bookingRequestUpdate(Request $request)
{
  $user = auth()->user();
  $booking = [];
  $bookingData = array(
  'bookingRequest' => $request->status,
  );
  if($request->status == 3){
    $message = "Booking has been rejected";
  }else{
     $message = "Booking has been accepted";
  }
  Booking::where('id', $request->booking_id)->update($bookingData);
  $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->where('bookings.bookingRequest',1)
       ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->get();
                foreach ($data as $key => $d) {
            $vendor = User::select('username')->where('id',$d->vendor_id)->first();
              $slot = AgentSlots::find($d->time_slot);
               $booking[$key]['id'] = $d->id;
               $booking[$key]['slot'] = $slot->slot;
                $booking[$key]['bookingRequest'] = $d->bookingRequest;
               $booking[$key]['vendor'] = $vendor->username;
               $booking[$key]['user'] = $d->username;
               $booking[$key]['address'] = $d->address;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             

            }
             return $this->sendResponse($message,$booking);

}
        public function unassignedbooking(){
        $user = auth()->user();
          $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->whereNotIn('bookings.bookingRequest',[1,3])
        ->whereNull('order_id')
       ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->paginate(10);

         return UnassignedBookingResource::collection($data);
           }

      public function unassignedbookingUdaud(){
        $user = auth()->user();
          $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->whereNotIn('bookings.bookingRequest',[1,3])
        ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->whereNotNull('order_id')
        ->orderBy('bookings.id','DESC')
        ->paginate(10);

         return UnassignedBookingResource::collection($data);
           }
    
        public function getUnassignedSlots($id){
          $data =  Booking::find($id);
          $result = array();
          $agent = User::closeToBooking($data->lat, $data->lang)->first();
          if($agent){
          $agentBookedSlots = AgentBookedSlots::where('agent_id',$agent->id)->where('booking_date',$data->booking_date)->get()->pluck('slot_id');
          $agentUnbookedSlots = AgentSlots::whereNotIn('id',$agentBookedSlots)->get();
          foreach ($agentUnbookedSlots as $key => $value) {
          $result[$key]['username'] = $agent->username;
          $result[$key]['id'] = $value->id;
          $result[$key]['slot'] = $value->slot;
          }
          }
          $result1 = array();
          $agent2 = User::closeToBooking($data->lat, $data->lang)->skip(1)->first();
          if($agent2){
          $agentBookedSlots = AgentBookedSlots::where('agent_id',$agent2->id)->where('booking_date',$data->booking_date)->get()->pluck('slot_id');
          $agentUnbookedSlots = AgentSlots::whereNotIn('id',$agentBookedSlots)->get();
          foreach ($agentUnbookedSlots as $key => $value) {
          $result1[$key]['username'] = $agent2->username;
          $result1[$key]['id'] = $value->id;
          $result1[$key]['slot'] = $value->slot;
          }
          }
           return ApiResponse::mulitsuccess('success',$result, $result1);
        
          return $this->sendResponse('success');
        }


        public function teambookingList(){
      $user = auth()->user();
      $useragent = $user->assigned_agent;
      if($useragent){
        $expagent = explode(',', $useragent);
        $booking = [];
         $i = 0;


      $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->whereIn('bookings.assigned_agent',$expagent)
        ->whereDate('bookings.booking_date','>',\Carbon\Carbon::now())
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
         ->paginate(10);
       
return UpcomingBookingResource::collection($data);

            foreach ($data as $key => $d) {

               $user  = User::select('username')->where('id',$value)->first();
               // $doctor  = Doctor::select('name')->where('id',$d->vendor_id)->first();
                # code...
               $vendor = User::select('username')->where('id',$d->vendor_id)->first();
               $booking[$i]['id'] = $d->id;
               $booking[$i]['agent_name'] = $user->username;
               $booking[$i]['user'] = $d->username;
               $booking[$i]['vendor'] = $vendor->username;
           if($d->booking_status == '0'){
                  $status = 'Visit Booked';
               }else if($d->booking_status == '1') {
                  $status = 'On the way';
               }else if($d->booking_status == '2') {
                  $status = 'Sample collected';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Dispatched';
               }else if($d->booking_status == '4') {
                  $status = 'Sample Delivered';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Recieved';
               }else if($d->booking_status == '6') {
                  $status = 'Testing in progress ( will require later)';
               }else if($d->booking_status == '7') {
                  $status = 'Report ready(later)';
               }else if($d->booking_status == '8') {
                  $status = 'Completed(later)';
               }else if($d->booking_status == '9') {
                  $status = 'Rebooking';
               }else if($d->booking_status == '10') {
                  $status = 'Cancel booking';
               }

               $booking[$i]['booking_status'] = $status;
              if($d->payment_status == '0'){
                  $pay_status = 'Pending';
               }else if($d->payment_status == '1'){
                  $pay_status = 'Already Paid';
               }else if($d->payment_status == '2'){
                  $pay_status = 'Payment Collected by Agent';
               }else if($d->payment_status == '3'){
                  $pay_status = 'Payment Received';
               }else if($d->payment_status == '4'){
                  $pay_status = 'Paid';
               }

               $booking[$i]['payment_status'] = $pay_status;
               $booking[$i]['address'] = $d->address;
               $booking[$i]['total_price'] = $d->total_price;
               $booking[$i]['date'] = date('d-m-Y', strtotime($d->booking_date));
             
$i++;
            }

        

// }
      // print_r($booking);

        return $this->sendResponse('Success.',$booking);
       }
       return $this->sendResponse('Success.');
    }

    public function daudBookingList(){
      $user = auth()->user();
       
      $data =  DB::table('bookings')->select('bookings.*','customers.user_id','customers.age','customers.gender','doctors.name','products.title')
        ->leftJoin('customers', 'bookings.customer_id', '=', 'customers.id')
        ->leftJoin('doctors', 'bookings.refered_by_doc', '=', 'doctors.id')
        ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('vendor_id',$user->id)
        ->where('bookings.status','0')
        ->orderBy('bookings.id','DESC')
        ->get();
        $booking = [];

        if($data){

            foreach ($data as $key => $d) {

                $user  = User::select('username')->where('id',$d->user_id)->first();
                $vendor  = User::select('username')->where('id',$d->vendor_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['venor'] = $vendor->username;
               $booking[$key]['product'] = $d->title;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               $booking[$key]['date'] = $d->booking_date;
             
            }

        }

      // print_r($booking);

        return $this->sendResponse('Success.',$booking);
        
    }


    public function showProduct(Request $request)
    {
        

        $this->validate($request, [
          'product_id' => 'required',
        ]);

        $user = auth()->user();

        $vendor_product = $user->products()->where('product_id',$request->product_id)->first();

        $product = [];

        if($vendor_product){

              $product  = Product::where('id',$vendor_product->product_id)->first();
              $product['id'] = $product->id;
              $product['title'] = $product->title;
              $product['test_type']    = $product->test_type;
              $product['image']        = $product->image;
              $product['price']       = $vendor_product->price;
              $product['discount']    = $vendor_product->discount;
              $product['created_at']  = $vendor_product->created_at;
                
        }

        return $this->sendResponse('Success.',$product);
    }

    public function deleteProduct(Request $request)
    {
        

        $this->validate($request, [
          'product_id' => 'required',
        ]);
        $user = auth()->user();

        $user->products()->where('product_id',$request->product_id)->delete();

        return $this->sendResponse('Success');
    }



    public function getProfile(Request $request)
    {
       
        $auth_user = $request->user();
        $profile   = $auth_user->vendor()->where('user_id',$auth_user->id)->first();

        // return $profile;
        $user      = new UserResource($auth_user);

        // $user['profile'] = $profile;
        return $this->sendResponse(__('Success'), $profile); 
    } 
    

    public function updateProfile(Request $request)
    {

        $this->validate($request, [
          'email' => 'required|email',
          'phone' => 'required|phone',
          'title' => 'required|string',
          //'fax' => 'required|string',
          //'GST' => 'required|string',
          'address' => 'required|string',
       ]);
        $user = $request->user();
        $user->username = $request->username;
        $user->address = $request->address;
        $user->latitude = $request->lat;
        $user->longitude = $request->lang;
        $user->landmark = $request->landmark;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->email = $request->email;
        // $user->dob = $request->dob;
        // $user->paypal_id = $request->paypal_id;
        if($request->hasFile('avatar')) {
          $oldPic = $user->image;
          $oldPicPath = storage_path('app/public/'.$user->image);
          $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
          $store = $request->avatar->storeAs('public/users', $avatarName);
          if($store && $oldPic) {
            // delete old pic
            if (file_exists($oldPicPath)) {
              @unlink($oldPicPath);
            }
          }
          $user->image = 'users/'.$avatarName;
        }



        if($request->password)
         {
                $user->password = bcrypt($request->password);
         }

        if($request->phone !== $user->phone){

            $otp = '123456' ;

            $message = 'Your '.config('app.name').' OTP is: ' . $otp;
            
            $phone = '+' .$request->phone_code.$request->phone;

            $messageData = [
              ['to' => $phone, 'body' => $message ]
            ];

            $user->phone_code = $request->phone_code;
            $user->phone = $request->phone;
        }

         $profile = $user->vendor()->updateOrCreate([],[
          // 'parking_address' => $request->parking_address,
          'title'  => $request->title,
          'type'   => $request->type,
          'fax'    => $request->fax,
          'GST'    => $request->GST,
          'description'    => $request->description,
          'image'    => $request->image,
          'latitude'    => $request->latitude,
          'longitude'   => $request->longitude,
        ]);


        $user->save();
        $user = new UserResource($user);

        $user['profile'] = $profile;

        return $this->sendResponse(__('Profile udpated Successfully'), $user); 
    } 

    public function showDays(Request $request){
      $days =  WorkDay::all();
       return $this->sendResponse('Success.',$days);
    }
    public function showdates(Request $request){
      echo "dfjgdfk";
    }

    public function showAgents(Request $request){
      $current_date = Carbon::today()->format('Y-m-d');
      $last_date = $date = \Carbon\Carbon::today()->addDays(4)->format('Y-m-d');
      /*echo "current: ". $current_date."<br>";
      echo "last: ". $last_date;*/
       $dateRange = CarbonPeriod::create($current_date, $last_date);
       $dates = [];
       foreach($dateRange as $date) {
            $dates[] = $date->format('Y-m-d');
        }
        $currentDates= [];
       
        foreach($dates as $key=>$date) {
             $currentDates[$key]['date'] = \Carbon\Carbon::parse($date)->format('d');
             $currentDates[$key]['month'] = \Carbon\Carbon::parse($date)->format('M');
             $currentDates[$key]['day'] =Carbon::parse($date)->format('l');;
        }
       return $this->sendResponse('Success.',$currentDates);
    }

    public function fetchSlots($date,$day){
       $day_id = WorkDay::select('id')->where('day', $day)->first();
       $day = $day_id->id;
       $slots =WorkDay::select("work_days.*", "agent_work_slot.opening_hour","agent_work_slot.closing_hour","agent_work_slot.agent_id")
                        ->leftJoin("agent_work_slot", "agent_work_slot.day_id", "=", "work_days.id")
                        ->where('agent_work_slot.day_id',$day)
                        ->get();
       $addSlots =[];                 
      foreach($slots as $key=>$slot){
          $start = strtotime($slot->opening_hour);
          $end = strtotime($slot->closing_hour);
          $minutes  = ($end - $start) / 60;
          $start1 = strtotime('01:00');
          $end1 = strtotime('00:00');
          $duration  = ($start1 - $end1) / 60;
          $slots = $minutes /$duration;
          $s =  $slot->opening_hour;
          $now = \Carbon\Carbon::today();
          
           for ($i = 01; $i <= $slots; $i++){
             $checkBookSlot=DB::table('agent_slot_not_available')->where('day_id', $slot->id)->where('slot_id',$i)->where('status',1)->whereDate('created_at',$now)->get();
             $checkSlot=DB::table('agent_slot_not_available')->where('day_id', $slot->id)->where('slot_id',$i)->where('status',0)->get();
             if(count($checkSlot)==0 && count($checkBookSlot)==0 ){
               $data['status'] = 'Available';
             }elseif(count($checkSlot)!=0){
                $data['status'] = 'Not Available';
             }elseif (count($checkBookSlot)!=0) {
               $data['status'] = 'Booked';
             }
             $data['agent_id'] = $slot->agent_id;
             $data['slot_id'] = $i;
             $duratn = 1;
             $timestamp = strtotime($s) + 60*60;
             $time = date('h:i A', $timestamp);
             $hour = date('H', $timestamp);
             $dayTerm = ($hour > 17) ? "3" : (($hour > 12) ? "2" : "1");

             $data['duration'] = $dayTerm;

             $data['time'] = $s.'-'.$time;
             $addSlots[] = $data;
             $s = $time;

           }
        } 
        return $this->sendResponse('Success.',$addSlots);    
             
    }
    public function vendorTopBooking(Request $request){
      $user = auth()->user();
       
     
      $booking =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->
          //leftJoin('customers', 'customer_booking.customer_id', '=', 'customers.id')
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
         //->leftJoin('doctors', 'customer_booking.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('vendor_id',$user->id)
        ->where('bookings.booking_status','8')
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->paginate(8);
        
        return $this->sendResponse('Success.',$booking);        
        
        $booking = [];

        if($data){

            foreach ($data as $key => $d) {

                $user  = User::select('username')->where('id',$d->user_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['product'] = $user->username;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               
               if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               $booking[$key]['mode'] = $mode;
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }else if($d->booking_status == '9') {
                  $status = 'Cancel Booking';
               }else if($d->booking_status == '10') {
                  $status = 'Rebooking';
               }

               $booking[$key]['status'] = $status;
               $booking[$key]['date'] = date('d-m-Y', strtotime($d->booking_date));
             
            }

        }

      // print_r($booking);

        return $this->sendResponse('Success.',$booking);
    }
    public function vendorDaudBooking(Request $request){
      $user = auth()->user();
       
      $data =  DB::table('bookings')->select('bookings.*','customers.user_id','customers.age','customers.gender','doctors.name','customer_booking_product.title')
        ->leftJoin('customers', 'bookings.customer_id', '=', 'customers.id')
        ->leftJoin('doctors', 'bookings.refered_by_doc', '=', 'doctors.id')
        ->leftJoin('customer_booking_product', 'bookings.id', '=', 'customer_booking_product.booking_id')
        
        ->orderBy('bookings.id','DESC')
        ->where('bookings.status','0 ')
        ->limit(5)
        ->get();
        $booking = [];

        if($data){

            foreach ($data as $key => $d) {

               $user  = User::select('username')->where('id',$d->user_id)->first();
               $vendor  = User::select('username')->where('id',$d->vendor_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['venor'] = $vendor->username;
               $booking[$key]['product'] = $d->title;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               $booking[$key]['mode'] = $mode;
               if($d->booking_status == '1'){
                  $status = 'Success';
               }else{
                  $status = 'Pending';
               }
               $booking[$key]['status'] = $status;
               $booking[$key]['date'] =  date('d-m-Y', strtotime($d->booking_date));
             
            }

        }

      // print_r($booking);
 
        return $this->sendResponse('Success.',$booking);
    }
    public function getBookingCount(Request $request){
       $user = auth()->user();
       //$completeBookings =[];
       $onGoingBookings =  DB::table('bookings')->where('vendor_id',$user->id)
        ->where('bookings.status','1')
        ->where('bookings.booking_status','0')
        ->where('created_at', '>=', Carbon::today())
        ->get();
       $todayBookings =  DB::table('bookings')->where('vendor_id',$user->id)
        ->where('bookings.status','1')
        //->where('bookings.booking_status','1')
        ->where('created_at', '>=', Carbon::today())
        ->get();
       $completeBookings =  DB::table('bookings')->where('vendor_id',$user->id)
        ->where('bookings.status','8')
        ->where('updated_at', '>=', Carbon::today())
              //  ->where('created_at', '>', Carbon::now()->startOfWeek())
        //->where('created_at', '<', Carbon::now()->endOfWeek())
        ->get();
       $data = array(
          'onGoingBookings' => count($onGoingBookings),
          'todayBookings' => count($todayBookings),
          'completeBookings' => count($completeBookings),
       );
       return $this->sendResponse('Success.',$data);
       
    }
      public function getTLBookingCount(Request $request){
       $user = auth()->user();
      $completed= 0;
      $todayBookings= 0;
      $onGoingBookings= 0;
      $unassignedBookings= 0;
       $getAgent = $user->assigned_agent;
       $expAgent = explode(',', $getAgent);

      $unassignedBookings =  DB::table('bookings')->where('tl_call',$user->id)
      ->whereNotIn('booking_status',[9,10])
      ->whereNotIn('booking_status',[1,3])
      ->whereDate('booking_date','>=',\Carbon\Carbon::now())
        ->get()->count();
       foreach ($expAgent as $key => $value) {

       $completed +=  DB::table('bookings')->where('assigned_agent',$value)
       ->where('booking_status',8)
       ->whereDate('booking_date','=',\Carbon\Carbon::now())
        ->get()->count();
       $todayBookings +=  DB::table('bookings')->where('assigned_agent',$value)
        ->whereDate('booking_date','=',\Carbon\Carbon::now())
        ->get()->count();
       $onGoingBookings +=  DB::table('bookings')->where('assigned_agent',$value)
       ->where('booking_status','>',0)
       ->where('booking_status','<',8)
        ->whereDate('booking_date', '=', \Carbon\Carbon::now())
        ->get()->count();

      }
       $data = array(
          'completed' => $completed,
          'todayBookings' => $todayBookings,
          'onGoingBookings' => $onGoingBookings,
          'unassignedBookings' => $unassignedBookings,
       );
       return $this->sendResponse('Success.',$data);
       
    }
    public function getOnGoingBookings(Request $request){
       $user = auth()->user();
      
      $req = $request->all();
      $filter = $req['filter']; 
      $data =  CustomerBookin::select('bookings.id','bookings.total_price','bookings.order_id','bookings.booking_status','bookings.assigned_agent','bookings.request_status', DB::raw("DATE_FORMAT(bookings.booking_date, '%d%b-%Y') as formatted_date"),'users.username')->
          //leftJoin('customers', 'customer_booking.customer_id', '=', 'customers.id')
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
         //->leftJoin('doctors', 'customer_booking.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        
        ->where('vendor_id',$user->id)
        
        ->where('bookings.status','1')
        ->whereNotIn('bookings.booking_status',['8','9','10'])
        ->whereNotIn('bookings.bookingRequest',['1','3']);
        if($filter['booking']){
          if($filter['booking'] == 'pending'){
              $data->where('bookings.assigned_agent', '0');
           }else{
              $data->where('users.username', 'like', '%' .$filter['booking']. '%'); 
           }
         }

       $booking =   $data->orderBy('bookings.id','DESC')->paginate(10);
      return VisitResource::collection($booking);

        //return $this->sendResponse('Success.',$booking);
         $booking = [];

        if($data){

            foreach ($data as $key => $d) {

               $user  = User::select('username')->where('id',$d->user_id)->first();
               $vendor  = User::select('username')->where('id',$d->vendor_id)->first();
                # code...
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user'] = $user->username;
               $booking[$key]['venor'] = $vendor->username;
               $booking[$key]['product'] = $user->username;
               $booking[$key]['doctor'] = $d->name;
               $booking[$key]['gender'] = $d->gender;
               if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }
               $booking[$key]['mode'] = $mode;
               $booking[$key]['status'] = $status;
               $booking[$key]['date'] =  date('d-m-Y', strtotime($d->booking_date));
             
            }

        }

        return $this->sendResponse('Success.',$booking);
    }
    public function addNewProductInBooking(Request $request){
      $data = $request->all();
      $total_price = 0;
      $discount = 0;
      foreach($data['product'] as $product){
        $query = CustomerBookingProduct::where('title', $product['productName'])->where('booking_id', $data['id'])->get();
        $booking =  Booking::where('id', $data['id'])->first();
        if(count($query) == 0 ){
            //$discount = Product::select('discount')->where('id', $product['value'])->first();
            $cart_data['booking_id'] = $data['id'];
            $cart_data['title'] = $product['productName'];
            $cart_data['price'] = $product['price'];
            $cart_data['discount'] = $product['discount'];
            CustomerBookingProduct::create($cart_data);
            $total_price += $product['price'] ;
            $discount += $product['discount'];
         
        }

      }
      $bookingData = array(
        'sub_total' => $booking->sub_total + $total_price,
        'discount' => $booking->discount + $discount,
        'total_price' => $booking->total_price + $total_price - $discount,
      );
      Booking::where('id', $data['id'])->update($bookingData);
      $detail['total_price'] = $total_price;
      $detail['discount'] = $discount;

      return $this->sendResponse('Success.',$detail);
    }
    public function fetchBookingDetails($id){
     
       
      $data =  Booking::select('bookings.*','users.username','users.email','users.id as user_ids','customers.phone','customers.medical_condition','customers.age','customers.gender','doctors.name as doc_name')->
          leftJoin('customers', 'bookings.user_id', '=', 'customers.user_id')
          ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
         ->leftJoin('doctors', 'bookings.refered_by_doc', '=', 'doctors.id')
         //->leftJoin('customer_booking_product', 'bookings.product_id', '=', 'customer_booking_product.id')
        
        ->where('bookings.id',$id)
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->get();
         $booking = [];

        if($data){

            foreach ($data as $key => $d) {

               // $user  = User::select('*')->where('id',$d->user_id)->first();
               //$vendor  = User::select('username')->where('id',$d->vendor_id)->first();
                # code...
              $query  = BookingQuery::where('booking_id',$id)->get()->toArray();
              $agent  = User::select('username')->where('id',$d->assigned_agent)->first();
              $time_slot  = AgentSlots::select('slot')->where('id',$d->time_slot)->first();
             $prod  =  CustomerBookingProduct::select('id','title','price','discount')->where('booking_id',$id)->get()->toArray();
              if($agent){
              if($d->request_status == '1'){
              $request_status = "Your request has been confirmed";
              $assigned_agent = $agent->username;
              }else{
              $assigned_agent = '';
              $request_status = ''; 
              }
             
              }else{
              $assigned_agent = '';
              $request_status = ''; 
              }
              $now = \Carbon\Carbon::today();
              $cdate = strtotime($now);
              $new_date = date('y-m-d', $cdate);
              $bookDate = date('y-m-d', strtotime($d->booking_date));
              if($bookDate >=  $new_date){
                $booking[$key]['dateStatus'] = 1; 
              }else{
                $booking[$key]['dateStatus'] = 2;
              }
               $booking[$key]['assigned_id'] = $d->assigned_agent;
               $booking[$key]['request_status'] = $request_status;
               $booking[$key]['assigned_agent'] = $assigned_agent;
               $booking[$key]['time_slot'] = $time_slot->slot;
               $booking[$key]['order_id'] = $d->order_id;
               
               $booking[$key]['id'] = $id;
               $booking[$key]['user_name'] = $d->username;
               $booking[$key]['user_email'] = $d->email;
               $booking[$key]['user_phone'] = $d->phone;
               $booking[$key]['user_ids'] = $d->user_ids;
               $booking[$key]['refered_by_doc'] = $d->refered_by_doc;
               
                $booking[$key]['user_age'] = $d->age;
                $booking[$key]['user_gender'] = $d->gender;
                $booking[$key]['user_condition'] = $d->medical_condition;
                $booking[$key]['doc_ref'] = $d->doc_name;
                $booking[$key]['convenience'] = $d->convenience;
              
               // $booking[$key]['products'] = $d->pro_title;   
               // $booking[$key]['pro_des'] = $d->pro_des;   
               // $booking[$key]['product_code'] = $d->product_code;   
                $booking[$key]['date'] = date('d M Y', strtotime($d->created_at));  
                $booking[$key]['booking_date'] = date('d M Y', strtotime($d->booking_date));  
                $booking_date= $d->booking_date;
                $timestamp = strtotime($booking_date);
                $booking_day = date('l', $timestamp);
               $booking[$key]['booking_day'] = $booking_day;  
               $booking[$key]['address'] = $d->address;  
               $booking[$key]['city'] = $d->city;  
               $booking[$key]['state'] = $d->state;  
               $booking[$key]['zip'] = $d->zip;  
               $booking[$key]['address1'] = $d->address1;  
               $booking[$key]['address2'] = $d->address2;  
                $booking[$key]['neighbourhood'] = $d->neighbourhood;  
                $booking[$key]['landmark'] = $d->landmark;  
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }else if($d->booking_status == '9') {
                  $status = 'Booking Cancelled ';
               }else if($d->booking_status == '10') {
                  $status = 'Rebooking';
               }
               $booking[$key]['bookingStatus'] = $d->booking_status;  
               $booking[$key]['status'] = $status;  
                 
               $booking[$key]['sub_total'] = $d->sub_total;  
               $booking[$key]['discount'] = $d->discount;  
               if($d->payment_status == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }
               $booking[$key]['booking_status'] = $status;
               $booking[$key]['booking_status_num'] = $d->booking_status;
               $booking[$key]['payment_mode'] = $d->payment_mode;  
               $booking[$key]['total_price'] = $d->total_price; 
               if($d->payment_status == '0'){
                  $payment_status = 'Pending';
               }else if($d->payment_status == '1') {
                  $payment_status = 'Already Paid';
               }else if($d->payment_status == '2') {
                  $payment_status = 'Payment Collected by Agent';
               }else if($d->payment_status == '3') {
                  $payment_status = 'Payment Received';
               }else if($d->payment_status == '4') {
                  $payment_status = 'Paid';
               } 
               $booking[$key]['payment_status'] = $payment_status;  
               $booking[$key]['products'] = $prod;  
               $booking[$key]['query'] = $query;  

            }

        }
        //print_r($booking);
        return $this->sendResponse('Success.',$booking);
    }
       public function fetchRunnerDetails($id){
           $data =  Qrcode::where('booking_id',$id)->first();
           $runner = [];
           if($data){
           if(!empty($data->runner_id)){ 
             $user = User::find($data->runner_id);
             $runner['username'] = $user->username;
           }
           $runner['booking_status'] = $data->booking_status;
           $runner['qr'] = $data->code;
            return $this->sendResponse('Success.',$runner);
          }
           return $this->sendResponse('Success.');
       }

        public function fetchTLBookingDetails($id){
     
       
      $data =  Booking::select('bookings.*','users.username','users.email','customers.phone','customers.age','customers.gender','customers.medical_condition','doctors.name as doc_name')->
          leftJoin('customers', 'bookings.user_id', '=', 'customers.user_id')
          ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
         ->leftJoin('doctors', 'bookings.refered_by_doc', '=', 'doctors.id')
         //->leftJoin('customer_booking_product', 'bookings.product_id', '=', 'customer_booking_product.id')
        
        ->where('bookings.id',$id)
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->get();
         $booking = [];

        if($data){

            foreach ($data as $key => $d) {
              $agent  = User::select('username')->where('id',$d->assigned_agent)->first();
              $time_slot  = AgentSlots::select('slot')->where('id',$d->time_slot)->first();
             $prod  =  CustomerBookingProduct::select('id','title','price','discount')->where('booking_id',$id)->get()->toArray();
              if($agent){
              if($d->request_status == '1'){
              $request_status = "Your request has been confirmed";
              $assigned_agent = $agent->username;
              }else{
              $assigned_agent = '';
              $request_status = ''; 
              }
             
              }else{
              $assigned_agent = '';
              $request_status = ''; 
              }
              $now = \Carbon\Carbon::today();
              $cdate = strtotime($now);
              $new_date = date('y-m-d', $cdate);
              $bookDate = date('y-m-d', strtotime($d->booking_date));
              if($bookDate >=  $new_date){
                $booking[$key]['dateStatus'] = 1; 
              }else{
                $booking[$key]['dateStatus'] = 2;
              }
               $booking[$key]['book_slot'] = $d->time_slot;
               $booking[$key]['assigned_id'] = $d->assigned_agent;
               $booking[$key]['request_status'] = $request_status;
               $booking[$key]['assigned_agent'] = $assigned_agent;
               $booking[$key]['time_slot'] = $time_slot->slot;
               
               $booking[$key]['id'] = $id;
               $booking[$key]['user_name'] = $d->username;
               $booking[$key]['user_email'] = $d->email;
               $booking[$key]['user_phone'] = $d->phone;
               
                $booking[$key]['user_age'] = $d->age;
                $booking[$key]['user_gender'] = $d->gender;
                $booking[$key]['user_condition'] = $d->medical_condition;
                $booking[$key]['doc_ref'] = $d->doc_name;
                $booking[$key]['convenience'] = $d->convenience; 
                $booking[$key]['date'] = $d->booking_date;  
                $booking[$key]['booking_date'] = date('d M Y', strtotime($d->booking_date));  
                $booking_date= $d->booking_date;
                $timestamp = strtotime($booking_date);
                $booking_day = date('l', $timestamp);
               $booking[$key]['booking_day'] = $booking_day;  
               $booking[$key]['address'] = $d->address;  
               $booking[$key]['city'] = $d->city;  
               $booking[$key]['state'] = $d->state;  
               $booking[$key]['zip'] = $d->zip;  
               $booking[$key]['address1'] = $d->address1;  
               $booking[$key]['address2'] = $d->address2;  
                $booking[$key]['neighbourhood'] = $d->neighbourhood;  
                $booking[$key]['landmark'] = $d->landmark;  
            if($d->booking_status == '0'){
                  $status = 'Visit Booked';
               }else if($d->booking_status == '1') {
                  $status = 'On the way';
               }else if($d->booking_status == '2') {
                  $status = 'Sample collected';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Dispatched';
               }else if($d->booking_status == '4') {
                  $status = 'Sample Delivered';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Recieved';
               }else if($d->booking_status == '6') {
                  $status = 'Testing in progress ( will require later)';
               }else if($d->booking_status == '7') {
                  $status = 'Report ready(later)';
               }else if($d->booking_status == '8') {
                  $status = 'Completed(later)';
               }else if($d->booking_status == '9') {
                  $status = 'Rebooking';
               }else if($d->booking_status == '10') {
                  $status = 'Cancel booking';
               }
               $booking[$key]['bookingStatus'] = $d->booking_status;  
               $booking[$key]['status'] = $status;  
                 
               $booking[$key]['sub_total'] = $d->sub_total;  
               $booking[$key]['discount'] = $d->discount;  
               if($d->payment_status == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               $booking[$key]['booking_status_num'] = $d->booking_status;
               $booking[$key]['payment_mode'] = $d->payment_mode;  
               $booking[$key]['total_price'] = $d->total_price; 
               if($d->payment_status == '0'){
                  $payment_status = 'Pending';
               }else if($d->payment_status == '1') {
                  $payment_status = 'Payment done online';
               }else if($d->payment_status == '2') {
                  $payment_status = 'Payment collected by Scientific officer';
               }else if($d->payment_status == '3') {
                  $payment_status = 'Payment Recieved';
               }else if($d->payment_status == '4') {
                  $payment_status = 'Paid';
               } 
               $booking[$key]['payment_status'] = $payment_status;  
               $booking[$key]['products'] = $prod;

            }

        }
        //print_r($booking);
        return $this->sendResponse('Success.',$booking);
    }

    public function changeBookingStatus(Request $request){
      $data = $request->all();
      Booking::where('id',$data['id'])->update(['booking_status' => $data['status']]);
      return $this->sendResponse('Success.');
    }

    public function removeProductInBooking(Request $request){
      $data = $request->all();
      $booking = Booking::where('id', $data['id'])->first();
      $bookingData = array(
        'sub_total' => $booking->sub_total - $data['price'],
        'discount' => $booking->discount - $data['discount'],
        'total_price' => $booking->total_price - $data['price'] + $data['discount'],
      );
      Booking::where('id', $data['id'])->update($bookingData);
      CustomerBookingProduct::where('id', $data['productId'])->where('booking_id',$data['id'])->delete();
      return $this->sendResponse('Success.');
    }

    public function fetchProductDetails($id){
     
     $dataproduct =  DB::table('bookings')->select('bookings.*','customer_booking_product.title' ,'customer_booking_product.price' ,'customer_booking_product.labName')
        ->leftJoin('customer_booking_product', 'bookings.id', '=', 'customer_booking_product.booking_id')
        // ->leftJoin('doctors', 'bookings.refered_by_doc', '=', 'doctors.id')
        // ->leftJoin('products', 'bookings.product_id', '=', 'products.id')
        ->where('bookings.id',$id)
        ->get();
        
         $product = [];

        if($dataproduct){

            foreach ($dataproduct as $key => $d) {
              $product[$key]['products'] = $d->title;
              $product[$key]['price'] = $d->price;
              $product[$key]['labName'] = $d->labName;

            }

        }
        //print_r($booking);
        //return $this->sendResponse('Success.',$product);
    }

    public function updateStatus(Request $request){
      $data = $request->all();
      $booking = Booking::find($data['id']);
      if($booking->request_status == 1){
      CustomerBookin::where('id',$data['id'])->update(['booking_status'=> $data['status']]);
      return $this->sendResponse('Successfully Updated.',$data);
      }
       return $this->sendResponse('Flebbo is not assingned yet.');
    }

    public function updatagents(Request $request){
      $data = $request->all();
      $booking = Booking::find($data['id']);
      $slot = AgentSlots::find($booking->time_slot);
      $vendor = User::find($booking->vendor_id);
      $customer = Customer::where('user_id',$booking->user_id)->first();
      $patient = User::find($booking->user_id);
      $agents = User::find($request->assigned_agent);
      $update = ['assigned_agent' => $request->assigned_agent,'request_sent' => $request->request_sent,'request_status' => $request->request_status,'tl_call' => NULL];
      CustomerBookin::where('id',$data['id'])->update($update);
              $lab =array();
              $arr = preg_split('/[,\ \.;]/', $vendor->username);
              $keywords = array_unique($arr);
              $i=0;
              foreach ($keywords as $keyword){
              if($i < 3){
              $lab[] = $keyword;
              }
              $i++;
              }
              $labimp = implode(' ', $lab);

                $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$customer->phone.'&message=Dear '.$patient->username.', Your Sample collection for '.$labimp.' on Dated '.$booking->booking_date.' at '.$slot->slot.' has been assigned to our scientific Officer '.$agents->username.' '.$agents->phone.'. Please make yourself available on time in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
                $smsUrl = str_replace(' ', '%20', $sms);
                $curl = curl_init();

                curl_setopt_array($curl, [
                CURLOPT_URL => $smsUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                ]);

                $response = curl_exec($curl);
      return $this->sendResponse('Successfully Updated.',$data);
    }
    public function getUser(Request $request){
      $user = auth()->user();
      return $this->sendResponse('Success.',$user);
    }
    public function logout(Request $request)
    {
        $user  = JWTAuth::toUser($request->token);
        $user->device_token = NULL;
        $user->save();
        return $this->sendResponse('Success.');
    }
    public function uploadReport(Request $request){
        $this->validate($request, [
            'Upload_report' => 'required|mimes:pdf'
        ]);
        $data = $request->all();
        $booking = Booking::find($data['booking_id']);
        if($booking->request_status == 1){
        if($booking->booking_status == 8){
        $file = $request->file('Upload_report') ;
        $fileNameWithExt = $request->file('Upload_report')->getClientOriginalName();
        $fileNameWithExt = str_replace(" ", "_", $fileNameWithExt);
        $filename = preg_replace("/[^a-zA-Z0-9\s]/", "", $fileNameWithExt);
        $filename = urlencode($filename);
        $extension = $request->file('Upload_report')->getClientOriginalExtension();
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        $destinationPath = public_path().'/Upload_report';
        $file->move($destinationPath,$fileNameToStore);
       // print_r($fileNameToStore );
        $check = CustomerReport::where('booking_id', $data['booking_id'])->get();
        $arr = array(
            'booking_id' => $data['booking_id'],
            'report' => $fileNameToStore,
        );
        if(count($check) == 0 ){
         CustomerReport::create($arr);
        }else{
          CustomerReport::where('booking_id', $data['booking_id'])->update(['report' => $fileNameToStore ]);
        }
        $booking = Booking::find($data['booking_id']);
        $user = User::find($booking->user_id);
        $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$user->phone.'&message=Thank you for giving us the opportunity to serve you, Your Reports are ready to review. Please visit your reports section to review/Print the reports. Wish you a healthy life always! Team Nexx Wave -uDAUD';
        $smsUrl = str_replace(' ', '%20', $sms);
        $curl = curl_init();

        curl_setopt_array($curl, [
        CURLOPT_URL => $smsUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        ]);
        $response = curl_exec($curl);
        return $this->sendResponse('Report Successfully Uploaded.',$arr);
        }
        return $this->sendResponse('Please Complete booking status before upload the report');
      }
        return $this->sendResponse('Flebbo is not assingned yet.');

    }
      public function fetchTLAgent($id){
           $booking = Booking::find($id);
           $agent = User::closeToBooking($booking->lat, $booking->lang)->limit(10)->get();
           if($agent){
             $agents= [];
              foreach ($agent as $key => $value) {
                $agents[$key]['id'] = $value->id;
                $agents[$key]['username'] = $value->username;

              }
               return $this->sendResponse('Agent Listing.',$agents);
           }
           return $this->sendResponse('Agent Listing.');


     }
               public function fetchSlotsTL(Request $request){
              $slot = AgentSlots::all(); 
         
             if($slot){
              $slots = [];
              foreach ($slot as $key => $value) {
                $slots[$key]['id'] = $value->id;
                $slots[$key]['slot'] = $value->slot;

              }
               return $this->sendResponse('Slots.',$slots);
             }
               return $this->sendResponse('Slots.');
         
         


     }
     public function assignAgents(Request $request){
      $bookingData = array(
      'assigned_agent' => $request->assigned_agent,
      'tl_call' => NULL,
      'request_sent' => $request->assigned_agent,
      );
      Booking::where('id', $request->id)->update($bookingData);
      $AgentBookedSlots = new AgentBookedSlots();
      $AgentBookedSlots->slot_id = $request->slot_id;
      $AgentBookedSlots->agent_id = $request->assigned_agent;
      $AgentBookedSlots->booking_date = $request->bookingDate;
      $AgentBookedSlots->save();
      $booking = Booking::find($request->id);
      $patient = User::find($booking->user_id);
      $vendor = User::find($booking->vendor_id);
      $customer = Customer::where('user_id',$booking->user_id)->first();
      $slot = AgentSlots::find($booking->time_slot);
      $agents = User::find($request->assigned_agent);
          $lab =array();
        $arr = preg_split('/[,\ \.;]/', $vendor->username);
        $keywords = array_unique($arr);
        $i=0;
        foreach ($keywords as $keyword){
        if($i < 3){
        $lab[] = $keyword;
        }
        $i++;
        }
        $labimp = implode(' ', $lab);
      $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$customer->phone.'&message=Dear '.$patient->username.', Your Sample collection for '.$labimp.' on Dated '.$booking->booking_date.' at '.$slot->slot.' has been assigned to our scientific Officer '.$agents->username.' '.$agents->phone.'. Please make yourself available on time in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
      $smsUrl = str_replace(' ', '%20', $sms);
      $curl = curl_init();
        curl_setopt_array($curl, [
        CURLOPT_URL => $smsUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
      ]);

      $response = curl_exec($curl);
      $user = auth()->user();

          $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->whereNotIn('bookings.bookingRequest',[1,3])
        ->whereNull('order_id')
       ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->paginate(10);

         return UnassignedBookingResource::collection($data);
     }

     public function assignUdaud(Request $request){
      $bookingData = array(
      'assigned_agent' => $request->assigned_agent,
      'tl_call' => NULL,
      'request_sent' => $request->assigned_agent,
      );
      Booking::where('id', $request->id)->update($bookingData);
      $AgentBookedSlots = new AgentBookedSlots();
      $AgentBookedSlots->slot_id = $request->slot_id;
      $AgentBookedSlots->agent_id = $request->assigned_agent;
      $AgentBookedSlots->booking_date = $request->bookingDate;
      $AgentBookedSlots->save();
      $booking = Booking::find($request->id);
      $patient = User::find($booking->user_id);
      $vendor = User::find($booking->vendor_id);
      $customer = Customer::where('user_id',$booking->user_id)->first();
      $slot = AgentSlots::find($booking->time_slot);
      $agents = User::find($request->assigned_agent);
        $lab =array();
        $arr = preg_split('/[,\ \.;]/', $vendor->username);
        $keywords = array_unique($arr);
        $i=0;
        foreach ($keywords as $keyword){
        if($i < 3){
        $lab[] = $keyword;
        }
        $i++;
        }
        $labimp = implode(' ', $lab);
      $sms = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$customer->phone.'&message=Dear '.$patient->username.', Your Sample collection for '.$labimp.' on Dated '.$booking->booking_date.' at '.$slot->slot.' has been assigned to our scientific Officer '.$agents->username.' '.$agents->phone.'. Please make yourself available on time in the interest of next patient who are waiting fasting. Team Nexx Wave -uDAUD';
      $smsUrl = str_replace(' ', '%20', $sms);
      $curl = curl_init();
        curl_setopt_array($curl, [
        CURLOPT_URL => $smsUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
      ]);

      $response = curl_exec($curl);
      $user = auth()->user();

          $data =  Booking::select('bookings.*','users.username')->
         leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.tl_call',$user->id)
        ->whereNotIn('bookings.booking_status',[9,10])
        ->whereNotIn('bookings.bookingRequest',[1,3])
        ->whereNotNull('order_id')
       ->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->orderBy('bookings.id','DESC')
        ->paginate(10);

         return UnassignedBookingResource::collection($data);
     }
    public function bookingQuery(Request $request){
      $user = auth()->user();
      $data = BookingQuery::where('status',0)->get();
      $query = [];
      foreach ($data as $key => $value) {
        $booking = Booking::where('id',$value->booking_id)->first();
        $slots = AgentSlots::find($booking->time_slot);
        $user = User::find($booking->user_id);
        if($booking->assigned_agent){  
          $agents = User::find($booking->assigned_agent);
          $agent = $agents->username;
        }else{
           $agent = "Agent not assigned yet";
        }
        $query[$key]['id'] = $value->id;
        $query[$key]['user'] = $user->username;
        $query[$key]['agent_name'] = $agent;
        $query[$key]['booking_date'] = $booking->booking_date;
        $query[$key]['slot'] = $slots->slot;
        $query[$key]['query'] = $value->query;
      }
       return ApiResponse::success('Query list',$query);
    }

      public function queryCount(Request $request){
      $user = auth()->user();
      $data = BookingQuery::where('status',0)->get()->count();
       return ApiResponse::success('Query list',$data);
    }
    public function queryMark(Request $request){
      $user = auth()->user();
       $bookingData = array(
        'status' => $request->status,
      );
      BookingQuery::whereIn('id',$request->id)->update($bookingData);
      $data = BookingQuery::where('status',0)->get();
      $query = [];
      foreach ($data as $key => $value) {
        $booking = Booking::where('id',$value->booking_id)->first();
        $slots = AgentSlots::find($booking->time_slot);
        $user = User::find($booking->user_id);
        if($booking->assigned_agent){  
          $agents = User::find($booking->assigned_agent);
          $agent = $agents->username;
        }else{
           $agent = "Agent not assigned yet";
        }
        $query[$key]['id'] = $value->id;
        $query[$key]['user'] = $user->username;
        $query[$key]['agent_name'] = $agent;
        $query[$key]['booking_date'] = $booking->booking_date;
        $query[$key]['slot'] = $slots->slot;
        $query[$key]['query'] = $value->query;
      }
       return ApiResponse::success('Query list',$query);
    }
    
    public function getCharges(Request $request){
       $user = auth()->user();
       $id = $user->id;
       $data = Vendor::where('user_id', $id)->first();
       return ApiResponse::success('Success',$data);
    }
    public function fetchBookingProduct($id){
              echo "$id";
    }
    public function updatSlot(Request $request){
      $data = $request->all();
    
      $update = ['time_slot' => $request->time_slot];
      Booking::where('id',$data['id'])->update($update);
      return $this->sendResponse('Successfully Updated.',$data);
    }
}
