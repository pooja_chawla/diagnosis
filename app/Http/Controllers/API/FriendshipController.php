<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Helpers\ApiResponse;
use App\Http\Requests\FriendshipRequest;
use App\Http\Requests\AcceptFriendshipRequest;
use App\Http\Requests\NotificationDetailRequest;
// use App\Http\Requests\SearchFriendRequest;
// use App\Http\Requests\AcceptFriendshipRequest;
// use App\Http\Requests\PendingFriendRequest;
use App\Models\User;
use App\Models\Vendor;
use App\Helpers\PaymentApp;
use App\Http\Requests\LabFilterRequest;
use App\Models\CustomerBookingRequest;
use App\Models\Booking;
use DB;
use App\Helpers\FcmNotification;
use App\Http\Resources\UserResource;
Use \Carbon\Carbon;
// use Friendable;
/**
 * Class CarController
 * @package App\Http\Controllers\API
 */

class FriendshipController extends AppBaseController
{
    public function requestAccept(AcceptFriendshipRequest $request)
    {
        $sender = User::find($request->sender_id);
        $user  = $request->User();
         $user_update_get = Booking::where('id',$request->booking_id)->where('assigned_agent',$user->id)->first();
         if($user_update_get){
      if($request->status == User::REQUEST_ACCEPTED){
       
        if($user_update_get->request_status == 1){
             return $this->sendResponse(__('already_accpeted')); 
        }
        $rejected_users =$user_update_get->rejected_users;
        $rejected_users_explode = explode(',', $rejected_users);

        if (($key = array_search($user->id, $rejected_users_explode)) !== false) {
          unset($rejected_users_explode[$key]);
          $rejected_users_implode = implode(',', $rejected_users_explode);
           Booking::where('id',$request->booking_id)->update(['rejected_users' => $rejected_users_implode]);
        }
        

        $request_accept = Booking::where('id',$request->booking_id)->update(['request_status' => 1,'assigned_agent' => $user->id]);
        return $this->sendResponse(__('request_accepted'), $user);
      }
      if($request->comment){       
        DB::table('rejected_users')->insert([
        'booking_id' => $request->booking_id,
        'user_id' => $user->id,
        'comment' => $request->comment
        ]);
      }
      if($user_update_get->request_status == 1){
        return $this->sendResponse(__('already_accpeted')); 
      }
      $get_rejected_users =  Booking::where('id',$request->booking_id)->first();
      $rejected_users =$get_rejected_users->rejected_users;
      if($rejected_users){
        $rejected_users = $rejected_users.','.$user->id;
      }else{
         $rejected_users = $user->id;
      }
      // return $rejected_users;
      Booking::where('id',$request->booking_id)
      ->update(['rejected_users' => $rejected_users,'request_sent'=>NULL, 'assigned_agent'=> NULL ]);
      return $this->sendResponse(__('request_rejected'), $user);
    }else{
       return $this->sendResponse('Request has been expired');
    }
  }

      public function getFriendList(Request $request)
    {
        $now = \Carbon\Carbon::today();
        $date = strtotime($now);
        $new_date = date('d', $date);

         $user = $request->user();
         $pending = Booking::where('request_status','0')->where('assigned_agent',$user->id)->whereDate('booking_date','>=',\Carbon\Carbon::now())->get();
         $accepeted = Booking::where('request_status','1')->where('assigned_agent',$user->id)->whereDate('booking_date','=',\Carbon\Carbon::now())->get();
         $get_total = Booking::where('assigned_agent',$user->id)->whereDate('booking_date','=',\Carbon\Carbon::now())->get()->count(); 

         if(!$pending->isEmpty()){
         foreach ($pending as $key => $value) {
            $arr[$key]['type']= "request";
            $arr[$key]['booking_id']= $value['id'];
            $arr[$key]['booking_date']= $value['booking_date'];
            $arr[$key]['phone']=  Booking::byPhone($value['user_id']);
            $arr[$key]['pickup_id'] = rand(100000, 999999);
            $arr[$key]['lab_name'] = User::byVendor($value['vendor_id']);
            $arr[$key]['distance'] = round(PaymentApp::distance($value['lat'], $value['lang'], $user->latitude, $user->longitude),2);
            $arr[$key]['time_slot']= Booking::slot($value['time_slot']);
            if($key == 0){
            $arr[$key]['total_pickup'] = $get_total;
            }
        }
        $i = ($key+1);
        }else{
        $i = 0;
        }
        if(!$accepeted->isEmpty()){
         foreach ($accepeted as $key => $value) {
            $arr[$i]['type']= "accepeted";
            $arr[$i]['booking_id']= $value['id'];
            $arr[$i]['booking_date']= $value['booking_date'];
            $arr[$i]['phone']= Booking::byPhone($value['user_id']);
            $arr[$i]['pickup_id'] = rand(100000, 999999);
            $arr[$i]['lab_name'] = User::byVendor($value['vendor_id']);
            $arr[$i]['distance'] = round(PaymentApp::distance($value['lat'], $value['lang'], $user->latitude, $user->longitude),2);
            $arr[$i]['time_slot']= Booking::slot($value['time_slot']);
            if($pending->isEmpty()){
              if($i == 0){
              $arr[$i]['total_pickup'] = $get_total;
              }
            }
            $i++;
        }
    }
    if($pending->isEmpty() && $accepeted->isEmpty()){
        return $this->sendResponse(__('request_list'));
    }
    
   		return $this->sendResponse(__('request_list'), $arr);
    }

    public function getPickup(Request $request)
    {
        $user = $request->user();
        $accepeted = Booking::where('request_status','1')->where('assigned_agent',$user->id)->whereDate('booking_date','>=',\Carbon\Carbon::now())->get();
        if(!$accepeted->isEmpty()){
         foreach ($accepeted as $key => $value) {
            $arr[$key]['booking_id']= $value['id'];
            $arr[$key]['booking_date']= $value['booking_date'];
            $arr[$key]['phone']=  Booking::byPhone($value['user_id']);
            $arr[$key]['pickup_id'] = rand(100000, 999999);
            $arr[$key]['lab_name'] = User::byVendor($value['vendor_id']);
            $arr[$key]['distance'] = round(PaymentApp::distance($value['lat'], $value['lang'], $user->latitude, $user->longitude),2);
            $arr[$key]['time_slot']= Booking::slot($value['time_slot']);

        }
         return $this->sendResponse(__('pickup_list'), $arr);
        }
        return $this->sendResponse(__('pickup_list'));

    }


    public function pickupFilter(LabFilterRequest $request) 
    {
      $user = $request->user();
        $type = $request->type;
        if($type == 'a'){
         $accepeted = Booking::select('bookings.*','bookings.id as booking_id')->where('request_status','1')->where('assigned_agent',$user->id)->whereDate('bookings.booking_date','>=',\Carbon\Carbon::now())
        ->leftJoin("vendors", "bookings.vendor_id", "=", "vendors.id")
         ->orderBy('vendors.title')->get();
        }
        if($type == 'b'){
            $accepeted = Booking::where('request_status','1')->where('assigned_agent',$user->id)->whereDate('created_at','>=',\Carbon\Carbon::now())->orderBy('created_at', 'ASC')->get();
        }
        if($type == 'c'){      
            $accepeted = Booking::where('request_status','1')->where('assigned_agent',$user->id)->whereDate('created_at','>=',\Carbon\Carbon::now())->orderBy('created_at', 'DESC')->get();
        }
                  
        if(!$accepeted->isEmpty()){
         foreach ($accepeted as $key => $value) {
            $arr[$key]['booking_id']= $value['booking_id'];
            $arr[$key]['booking_date']= $value['booking_date'];
            $arr[$key]['phone']=  Booking::byPhone($value['user_id'],);
            $arr[$key]['pickup_id'] = rand(100000, 999999);
            $arr[$key]['lab_name'] = User::byVendor($value['vendor_id']);
            $arr[$key]['distance'] = round(PaymentApp::distance($value['lat'], $value['lang'], $user->latitude, $user->longitude),2);
            $arr[$key]['time_slot']= Booking::slot($value['time_slot']);

        }
         return $this->sendResponse(__('pickup_list'), $arr);
        }
        return $this->sendResponse(__('pickup_list'));

    }

    public function searchFriend(SearchFriendRequest $request)
    {
      $del_val = $request->user()->id;
      $search = User::Where('email', 'like', '%' . $request->search . '%')->orWhere('phone', 'like', '%' . $request->search . '%')->pluck('id');
    foreach ($search as $key => $value) {
      if($value == $del_val){
        unset($search[$key]);
      }
   }
    $data = User::whereIn('id', $search)->get();
      return $this->sendResponse('success',UserResource::collection($data));
    }


    public function notificationDetail(NotificationDetailRequest $request)
    {
        $user = $request->user();
        $accepeted = Booking::find($request->booking_id);
            $arr['booking_id']= $accepeted->id;
            // $arr['customer_id']= $accepeted->customer_id;
            $arr['phone']=  Booking::byPhone($accepeted->user_id);
            $arr['pickup_id'] = rand(100000, 999999);
            $arr['lab_name'] = User::byVendor($accepeted->vendor_id);
            $arr['booking_date'] = $accepeted->booking_date;
            $arr['distance'] = round(PaymentApp::distance($accepeted->lat , $accepeted->lang, $user->latitude, $user->longitude),2);
            $arr['time_slot']= Booking::slot($accepeted->time_slot);
         return $this->sendResponse(__('pickup_list'), $arr);
    }

}
