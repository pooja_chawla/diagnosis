<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\UserResource;
use App\Http\Resources\PatientResource;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\{ LoginRequest, RegisterRequest ,OtpRequest,LoginPhoneRequest,LoginOtpRequest,UpdateProfileRequest};
use App\Repositories\UserRepository;
use App\Helpers\ApiResponse;
use App\Helpers\PaymentApp;
use App\Helpers\SmsHelper;
use App\Models\User;
use App\Models\Documents;
use App\Models\ProductInventory;
use App\Models\PackageTests;
use App\Models\Inventory;
use App\Models\Booking;
use App\Models\Product;
use App\Models\CustomerBookingProduct;
use Twilio\Rest\Client;
use App\Models\WorkSlot;
use App\Http\Requests\DocumentsUploadLoginRequest;


class AuthController extends AppBaseController
{

    private $repo;
    private $twilio;

    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
        $account_sid = config('services.twilio.account_sid');
        $auth_token  = config('services.twilio.auth_token');
        $this->twilio = new Client($account_sid, $auth_token);
        
    }
    
    public function login(LoginRequest $request) {      
        $credentials = $request->validated();
        
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
                return $this->sendError(__('invalid_login'));
            }
        } catch (JWTAuthException $e) {
            return $this->sendError(__('token_create_failed'));
        }
        
        $user = $request->user();
    
        
        $user = $this->repo->update($request->only([
          'device_type', 'device_token', 'latitude', 'longitude'
        ]), $user->id);

        $user = new UserResource($user, $token);
        return $this->sendResponse('Success', $user);
    }



    public function loginWithOtp(LoginPhoneRequest $request) 
    {  

      // return ("tfgv");
        // $this->validate($request, [
        //   'phone'        => 'required|exists:users,phone'
        // ]);    

        $user = $this->repo->findOneBy(['phone' => $request->phone]);

        // send OTP
        $otp         = "123456";
        // $otp         = PaymentApp::otp();
        $message     = 'Your '.config('app.name').' OTP is: ' . $otp;
        $phone       = '+' .$user->phone_code .$request->phone; 
        $messageData = [ ['to' => $request->phone, 'body' => $message ] ];

        // SmsHelper::send($messageData);
        
        $user->login_otp  = $otp;
        $user->save();

        return $this->sendResponse('OTP has been sent on your registered phone number.');
    }


    public function loginOtpVerification(LoginOtpRequest $request)
    {
      $user = $this->repo->findOneBy(['login_otp' => $request->otp,'phone' => $request->phone ]);
      if(!$user){
        return $this->sendResponse(__('Not a valid Otp'));
      }
      $user->login_otp = NULL;
      $user->save();
      $token = JWTAuth::fromUser($user);
      $user = new UserResource($user, $token);
      return $this->sendResponse(__('otp_verified'), $user);
    }

    /**
     * Register a User
     *
     * @param  Request $request
     * @return JSON
     */

    public function register(RegisterRequest $request)
    {
        return DB::transaction(function() use ($request) {

            $data = $request->validated();
            // $data['otp'] = "123456";           
            $data['otp'] = PaymentApp::otp();           
            $data['status'] = "Inactive";           
            $data['password'] = bcrypt($data['password']);           
            
          $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$data['phone'].'&message=Dear Customer, Welcome to uDAUD. Please use '.$data['otp'].' OTP to login your account. Live Healthy with uDAUD. Team Nexx Wave -uDAUD';


          $smsUrl = str_replace(' ', '%20', $homepage);
            $curl = curl_init();

            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);

            $user = $this->repo->create($data);
            // $user->setRole(User::PASSENGER_ROLE);
            $user->assignRole($data['role']);
    
            if($request->hasFile('avatar')) {
              $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
              $request->avatar->storeAs('public/users', $avatarName);              
              $user->image = 'users/'.$avatarName;
              $user->save();
            }
  

            event(new Registered($user));



            // $user->createAsStripeCustomer();
            // $user->createAsStripeAccount();

            $user = new UserResource($user);
            return $this->sendResponse(__('We have sent you confirmation email and OTP.'), $user);
        });                    
                                   
    }

    // public function register(RegisterRequest $request)
    // {
    //     return DB::transaction(function() use ($request) {

    //         $data = $request->validated();
    //         $data['otp'] = PaymentApp::otp();           
    //         $data['status'] = "Inactive";           
    //         $data['password'] = bcrypt($data['password']);           
            
    //         $user = $this->repo->create($data);
    //         // $user->setRole(User::PASSENGER_ROLE);
    //         $user->assignRole($data['role']);
    
    //         if($request->hasFile('avatar')) {
    //           $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
    //           $request->avatar->storeAs('public/users', $avatarName);              
    //           $user->avatar = 'users/'.$avatarName;
    //           $user->save();
    //         }

    //          // $this->twilio->messages->create($data['phone'], // to
    //          //          array("from" => config('services.twilio.from'), "body" => 'Your '.config('app.name').' App OTP is: ' . $data['otp'])
    //          // );
            
    //         $message = 'Your '.config('app.name').' OTP is: ' . $data['otp'];
            
    //         $phone = '+' .$user->phone_code.$user->phone;

    //         $messageData = [
    //           ['to' => $phone, 'body' => $message ]
    //         ];

    //         // SmsHelper::send($messageData);

    //         event(new Registered($user));



    //         // $user->createAsStripeCustomer();
    //         // $user->createAsStripeAccount();

    //         $user = new UserResource($user);
    //         return $this->sendResponse(__('We have sent you confirmation email and OTP.'), $user);
    //     });                    
                                   
    // }

    public function getAuthUser(Request $request)
    {
        // $user  = JWTAuth::toUser($request->token); 
        $user = auth()->user();
        $user = new UserResource($user);
        return $this->sendResponse('Success.', $user);
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = $request->user();
        $user->name = $request->name;
        $user->address = $request->address;
        // $user->dob = $request->dob;
        // $user->paypal_id = $request->paypal_id;
        if($request->hasFile('avatar')) {
          $oldPic = $user->image;
          $oldPicPath = storage_path('app/public/'.$user->image);
          $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
          $store = $request->avatar->storeAs('public/users', $avatarName);
          if($store && $oldPic) {
            // delete old pic
            if (file_exists($oldPicPath)) {
              @unlink($oldPicPath);
            }
          }
          $user->image = 'users/'.$avatarName;
        }

        if($request->phone !== $user->phone){

            $otp = '123456' ;

            $message = 'Your '.config('app.name').' OTP is: ' . $otp;
            
            $phone = '+' .$request->phone_code.$request->phone;

            $messageData = [
              ['to' => $phone, 'body' => $message ]
            ];

            $user->phone_code = $request->phone_code;
            $user->phone = $request->phone;
        }

        $user->save();
        $user = new UserResource($user);
        return $this->sendResponse(__('profile_udpated'), $user); 
    } 


    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();
        $oldpassword = $request->oldpassword;     
        $newpassword = bcrypt($request->newpassword);
        if (!\Hash::check($oldpassword, $user->password)) {
           return $this->sendError(__('pwd_doesnt_match'));
        }
        $user->password = $newpassword;
        $user->save();
        $user = new UserResource($user);
        return $this->sendResponse(__('password_updated'), $user); 
    }


    public function otpVerification(OtpRequest $request)
    {
      // $user = $this->repo->findOneBy(['phone' => $request->phone,'otp' => $request->otp]);
       $user = $this->repo->findOneBy(['otp' => $request->otp]);
        if(!$user){

          return $this->sendResponse(__('Not a valid Otp'));
        }
        $user->otp = NULL;
        $user->save();
        $token = JWTAuth::fromUser($user);
        $user = new UserResource($user, $token);


          return $this->sendResponse(__('otp_verified'), $user);
    }

    public function otpResend(Request $request)
    {
      $user = auth()->user();
      $this->validate($request, [
      'phone' => 'required|exists:users,phone'
      ]);
      $otp = PaymentApp::otp();   
      $user->otp  = $otp;
      $user->save(); 
      $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$phone.'&message=Dear Customer, Welcome to uDAUD. Please use '.$otp.' OTP to login your account. Live Healthy with uDAUD. Team Nexx Wave -uDAUD';
      $smsUrl = str_replace(' ', '%20', $homepage);
      $curl = curl_init();
      curl_setopt_array($curl, [
      CURLOPT_URL => $smsUrl,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      ]);
      $response = curl_exec($curl);
      return $this->sendResponse('OTP has been sent on your registered phone number.');
    }

    /**
     * Refresh the token
     *
     * @return JSON
     */
    public function refresh(Request $request)
    {
        $token = JWTAuth::parseToken()->refresh();
        $user = new UserResource($request->user(), $token);
        return $this->sendResponse('success', $user);
    }

    public function resendVerificationEmail(Request $request){
      
      $email = $request->input('email');

      $user = User::where('email', $email)->first();

      abort_if(!$user, 400, __('This email address does not exist.'));
      abort_if($user->hasVerifiedEmail(), 403, __('This email address has been already verified.'));

      $user->sendEmailVerificationNotification();
      return $this->sendResponse('success', $user);
    }

    public function logout(Request $request)
    {
        // $user  = JWTAuth::fromUser($request->token);
        $user = auth()->user();
        $user->device_token = NULL;
        $user->save();
        return $this->sendResponse('Success.');
    }

    public function slot(Request $request)
    {
      
        $workday_id = $request->get('q');

        return WorkSlot::where('day_id', $workday_id)->get(['id', DB::raw('CONCAT(opening_hour,"-", closing_hour) as text')]);
    }
       
     public function uploadDocumentslogin(DocumentsUploadLoginRequest $request)
     {
        $user = User::find($request->id);
        $documents = new Documents;
        if($request->hasFile('vehicle_image')) {
            $vehicle = $user->id.'_vehicle'.time().'.'.$request->vehicle_image->getClientOriginalExtension();
            $request->vehicle_image->storeAs('public/users', $vehicle);              
            $documents->vehicle_image = 'users/'.$vehicle;
            $user->save();
        }
        $documents->vehicle_number = $request->vehicle_number;
        $documents->education = $request->education;
        $documents->account = $request->account;
        $documents->user_id = $user->id;
        $documents->adhar_card = $request->adhar_card;
        $documents->pan_card = $request->pan_card;
        $documents->save();
        if($documents){
        User::where('id', $request->id)->update(['document_status' => 1]);
        }
        return ApiResponse::success('success');
     }

    public function UserDetailsTest(Request $request)
      {
         $prod = [];
        $packages = [];
         $this->validate($request, [
             'id' => 'required|exists:bookings,id'
         ]);
         $user = Booking::find($request->id);
        $user_details = new PatientResource($user);
        $product = CustomerBookingProduct::where('booking_id',$request->id)->get();
        foreach ($product as $key => $value) {
            $productid = Product::where('title',$value->title)->where('type',2)->first();
          if($productid){
              $prod[$key]['type'] = 'individual';
              $prod[$key]['name'] = $value->title;
              if($productid){
              $inventry = ProductInventory::where('product_id',$productid->id)->first();
  
                $invid = $inventry->inventory_id;
                $getCode = Inventory::find($invid);
                $prod[$key]['tube'] = $getCode->name;
              
            }
          }else{
            $getpackages = Product::where('title',$value->title)->first();
            if($getpackages){
               $prod[$key]['type'] = 'package';
               $prod[$key]['name'] = $value->title;
                $getpackageNma = ProductInventory::where('product_id',$getpackages->id)->first();
                $getCode = Inventory::find($getpackageNma->inventory_id);
                $prod[$key]['tube'] = $getCode->name;
             
              $prod[$key]['test'] = PackageTests::select('name')->where('product_id',$getpackages->id)->get()->pluck('name');
           
            }
            }
          }
        
         return ApiResponse::mulitsuccess('success',$user_details,$prod);

      }

          public function UserDetails(Request $request)
      {
        $prod = [];
         $this->validate($request, [
             'id' => 'required|exists:bookings,id'
         ]);
         $user = Booking::find($request->id);
        $user_details = new PatientResource($user);
        $product = CustomerBookingProduct::where('booking_id',$request->id)->get();
        foreach ($product as $key => $value) {
           $prod[$key]['id'] = $value->id;
          $prod[$key]['name'] = $value->title;
          $productid = Product::where('title',$value->title)->first();
          $inventry = ProductInventory::where('product_id',$productid->id)->first();
        if($inventry){
          $invid = $inventry->inventory_id;
          $getCode = Inventory::find($invid);

          $prod[$key]['tube'] = $getCode->name;
        }
         
        }
         return ApiResponse::mulitsuccess('success',$user_details,$prod);

      } 
      public function bookingComment(Request $request)
      {
        Booking::where('id', $request->id)->update(['comment' => $request->comment]);
        return ApiResponse::success('success');
      }
}