<?php
namespace App\Http\Controllers\API;

use DB;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Resources\ProductResource;
use App\Http\Controllers\Controller;


class ProductController extends Controller
{

    public function __construct()
    {
       
    }
  
    /**
     * @return JSON
     */
    public function index(Request $request)
    {

        $products = Product::where('status','1')->orderBy('order','desc')->get();

        // $data = [];

        // if($products){

        //     foreach ($products as $key => $product) {
        //         # code...
        //         $data['id'] = $product->title;
        //         $data['title'] = $product->title;
        //         $data['title'] = $product->title;
        //         $data['title'] = $product->title;
        //         $data['title'] = $product->title;

        //     }
        // }


        return ApiResponse::success('success', $products);
    }

    public function show(Request $request)
    {

        $productId  = $request->input('product_id');
        $product    = Product:: where('id',$productId)->first();
        if($product){
            return ApiResponse::success('success', $product);
        }
        return ApiResponse::success('error'); 
    }

     public function getProducts(Request $request)
    {

        $product    = Product:: get();
        if($product){
            return ApiResponse::success('success', $product);
        }
        return ApiResponse::success('error'); 
    }

    public function fetchProducts(Request $request){

        $product_ids = $request->input('selected');
        $data =[];
        if($product_ids){
            foreach ($product_ids as $key => $product_id) {
                
                $data[$key] = Product::where('id',$product_id)->first();
            }
        }
        return ApiResponse::success('success', $data);


    }
    


}
