<?php

namespace App\Http\Controllers\API;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\ForgotPasswordRequest;

class ForgotPasswordController extends Controller
{
  use SendsPasswordResetEmails;


   public function sendResetLinkEmail(ForgotPasswordRequest $request) {
        $response = $this->broker()->sendResetLink(
        $request->only('email')
        );
        
        if($request->input('email')) {

          return $response == Password::RESET_LINK_SENT
          ? ApiResponse::success('Reset link sent to your email.')
          : ApiResponse::error('Unable to send reset link.');

        }

    }
}