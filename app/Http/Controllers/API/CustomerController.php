<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\UserResource;
use App\Http\Resources\PatientResource;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\{ LoginRequest, RegisterRequest ,OtpRequest,LoginPhoneRequest,LoginOtpRequest,UpdateProfileRequest};
use App\Repositories\UserRepository;
use App\Helpers\ApiResponse;
use App\Helpers\PaymentApp;
use App\Helpers\SmsHelper;
use App\Models\Customer;
use App\Models\User;
use App\Models\Documents;
use App\Models\ProductInventory;
use App\Models\PackageTests;
use App\Models\Inventory;
use App\Models\DocConsultation;
use App\Models\Booking;
use App\Models\Product;
use App\Models\CustomerBookingProduct;
use Twilio\Rest\Client;
use App\Models\WorkSlot;
use App\Http\Requests\DocumentsUploadLoginRequest;

class CustomerController extends Controller
{

    private $repo;

    public function __construct()
    {

        // $this->repo = $repo;
    }
    
     /**
     * Fetch Agents
     *
     * @param  Request $request
     * @return JSON
     */
    public function index(Request $request)
    {


        $customers = User::role('customer')->where('status','Active')->get();

        return ApiResponse::success('success', $customers);
    }

    
    public function logout(Request $request)
    {
        $user  = JWTAuth::toUser($request->token);
        $user->device_token = NULL;
        $user->save();
        return $this->sendResponse('Success.');
    }

    public function sendMessage($phone,$otp,$username)
    {
       if($otp == 0){
           $url = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$phone.'&message=Dear '.$username.', Welcome to the world of healthy Habits to keeps yourself Fit and aware. You are successfully registered with us. Team Nexx Wave -uDAUD';
       }else{
           $url = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$phone.'&message=Dear Customer, Welcome to uDAUD. Please use '.$otp.' OTP to login your account. Live Healthy with uDAUD. Team Nexx Wave -uDAUD';
       }
      

          $smsUrl = str_replace(' ', '%20', $url);
            $curl = curl_init();
            curl_setopt_array($curl, [
            CURLOPT_URL => $smsUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ]);

            $response = curl_exec($curl);
            return true;

     
    }
    public function customerOtpLogin(Request $request)
    {
        $userCount = User::where('phone',$request->phone)->where('login_type','3')->get()->count();
        if($userCount > 0){
          $user = User::where('phone',$request->phone)->first();
          $otp = PaymentApp::otp(); 
          User::where('id', $user->id)->update(['otp' => $otp]);
          $msg = $this->sendMessage($request->phone , $otp, $user->username);
          if($msg){
               return ApiResponse::success('success', $request->phone);   
          }
        }else{
          return ApiResponse::success('success','1');
        }
    }
    public function addPatient(Request $request){
   
      $this->validate($request, [
          'first_name' => 'required|string',
          'email' => 'required|email|unique:users',
          'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users',
       ]); 
      $otp = PaymentApp::otp();
       $data = $request->all();
       $detail = array(
        'name' => $data['first_name'],
        'username' => $data['first_name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'login_otp' => $otp,
        'login_type' => 3,
        'status' => 1,
       );
      
        $user = User::create($detail)->id;  
        $data = array(
        'user_id' => $user,
        'phone' => $data['phone'],
        );
        Customer::create($data);
        $userFind = User::find($user);
        $otp = 0 ;
        $msg = $this->sendMessage($data['phone'] , $otp,$userFind->username);    
         
          
        return ApiResponse::success('Success',1);
    }
    public function loginOtpVerification(Request $request){
      $data = $request->all();
      $user = User::where(['otp' => $data['otp'],'phone' => $data['phone'] ])->first();
      if(!$user){
        return ApiResponse::success(__('Not a valid Otp'),1);
      }
      $user->otp = NULL;
      $user->save();
      $data = \Auth::login($user);
      return ApiResponse::success(__('otp_verified'), $data);

    }
 public function sendDocConsultation(Request $request){
      $data = $request->all();
       $detail = array(
        'name' => $data['name'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'age' => $data['age']
      );
      $user = DocConsultation::create($detail)->id;
      // $pay = '1';
      // header("Location: http://localhost:8000/api/consultpayment/".$pay."/".$user."");
       return ApiResponse::success('success', $user);   
    }


     public function consultpayment ($pay,$id){
        $auth = DocConsultation::find($id);
       $mode = "PROD"; //<------------ Change to TEST for test server, PROD for production
       
         if ($mode == "PROD") {
          $url = "https://www.cashfree.com/checkout/post/submit";
        } else {
          //echo "string";
          $url = "https://test.cashfree.com/billpay/checkout/post/submit";
        }  
         $secretKey = "66bb9c5c6e199180e7fbff382a877aa71210d93e";
         $postOrderId = $id;
          $postData = array( 
          "appId" => '1615949011ad7f6a60afd08388495161', 
           "paymentMode" => 'upi',
          "orderId" => $postOrderId, 
          "orderAmount" => $pay, 
          "orderCurrency" => 'INR', 
          "orderNote" => 'Booking', 
          "customerName" => $auth->name, 
         
          "customerPhone" => $auth->phone, 
          "customerEmail" => $auth->email,
          "returnUrl" => 'http://localhost:8000/api/consultresponse', 
         // "returnUrl" => 'http://localhost:8000/api/response', 
          "notifyUrl" => ''
         
         
        );
        ksort($postData);
        $signatureData = "";
        foreach ($postData as $key => $value){
            $signatureData .= $key.$value;
        }
        

        
        $s = hash_hmac('sha256', $signatureData, $secretKey,true);

        $sing = base64_encode($s); 
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array( 
                  "signature" => $sing, 
                  "paymentMode" => 'upi',
                  "orderNote" => 'Booking',
                  "orderCurrency" => 'INR', 
                  "customerName" => $auth->name,
                  "customerPhone" => $auth->phone, 
                  "customerEmail" => $auth->email,
                  "orderAmount" => $pay, 
                  "notifyUrl" => '', 
                  // "returnUrl" => 'http://localhost:8000/api/response', 
                  "returnUrl" => 'http://localhost:8000/api/consultresponse', 
                  "appId" => '1615949011ad7f6a60afd08388495161', 
                  "orderId" => $postOrderId,
                  "notifyUrl" => '',
                )
        );
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        curl_close($ch);
        // CartSession::where('status',0)->where('user_id',$auth->id)->update(['status'=>1,'order_id'=> $postOrderId]);
         // $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$auth->phone.'&message=Dear Customer, Welcome to uDAUD. Your booking has been done. Live Healthy with uDAUD. Team Nexx Wave -uDAUD';


         //  $smsUrls = str_replace(' ', '%20', $homepage);
         //    $curls = curl_init();

         //    curl_setopt_array($curls, [
         //    CURLOPT_URL => $smsUrls,
         //    CURLOPT_RETURNTRANSFER => true,
         //    CURLOPT_ENCODING => "",
         //    CURLOPT_MAXREDIRS => 10,
         //    CURLOPT_TIMEOUT => 30,
         //    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         //    CURLOPT_CUSTOMREQUEST => "GET",
         //    ]);

         //    $responses = curl_exec($curls);
        return  $result;

    }

    public function consultresponse(Request $request){
       $data = $request->all();
       if($data['txStatus'] == 'SUCCESS'){
       DocConsultation::where('id',$data['orderId'])->update(['status'=>1,'txStatus'=>$data['txStatus']]);
       echo '<script>alert("Payment Successfully Done")
       window.location= "https://udaudindia.com/doctor-consultation"

       </script>';
     }else{
       DocConsultation::where('id',$data['orderId'])->update(['txStatus'=>$data['txStatus']]);
       echo '<script>alert("Payment failed ,please try again")
       window.location= "https://udaudindia.com/doctor-consultation"

       </script>';
     }
    }
  

}
