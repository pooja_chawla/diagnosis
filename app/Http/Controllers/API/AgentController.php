<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use App\Http\Resources\UserResource;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\API\{ LoginRequest, AgentRegisterRequest };
use App\Http\Requests\UpdateAgentProfileRequest;
use App\Http\Requests\API\{ ChangePasswordRequest };
use App\Helpers\PaymentApp;
use App\Models\Payment;
use App\Http\Resources\AgentResource;
use App\Http\Resources\AgentProfileResource;
use App\Helpers\SmsHelper;
use App\Http\Requests\VenderAddressRequest;
use Illuminate\Support\Facades\View;
Use \Carbon\Carbon;
use App\Models\AgentAvailability;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\AddPatientRequest;
use App\Models\Qrcode;
use App\Http\Requests\AgentTestRequest;
use App\Models\Product;
use App\Http\Requests\AgentScannerRequest;
use App\Models\Agent;
use App\Models\CustomerBookingProduct;
use App\Http\Resources\UpcomingBookingResource;
use App\Models\Customer;
use App\Models\User;
use App\Models\Booking;
use App\Models\AgentBookedSlots;
use App\Models\AgentSlots;

class AgentController extends AppBaseController
{

    private $repo;

    public function __construct(UserRepository $repo)
    {

        $this->repo = $repo;
    }
    
     /**
     * Fetch Agents
     *
     * @param  Request $request
     * @return JSON
     */
    public function index(Request $request)
    {

        $products = Agent::where('status','Active')->orderBy('order','desc')->get();

        return ApiResponse::success('success', $products);
    }

    public function show(Request $request)
    {

        $agent_id  = $request->input('id');
        $agent    = Agent:: where('id',$agent_id)->first();
        if($agent){
            return ApiResponse::success('success', $agent);
        }
        return ApiResponse::success('error'); 
    }

    public function getVenderProducts(Request $request)
      {
        try{
          $venderId = request('vender_id');
          if(!empty($venderId))
          {
            $venderProducts = Product::where('vendor', $venderId)->orderBy('order','desc')->get(['id','title']);
            return ApiResponse::success('success', $venderProducts);
          }
          return ApiResponse::error('error','vender id is required!');;
        } catch(\Exception $e) { 
            return ApiResponse::error('vender id does not exits',$e->getMessage());
        }
      }

    public function venderAddress(VenderAddressRequest $request)
      {
        DB::beginTransaction();
        try {
          $data = $request->all();
          $userData = User::where('id',$data['parent'])->first();
          $detail = array(
            'username' => $data['username'],
            'email' => $data['email'],
            'age' => $data['age'],
            'gender' => $data['gender'],
            'login_type' => 3,
            'patient_code' => rand().'-PT-000'.rand(),
            'parent' => $data['parent'],
            'address' => $userData->address,
            'latitude' => $userData->latitude,
            'longitude' => $userData->longitude,
            'phone' => $data['phone'],
            'address1' => $userData->address1,
            'address2' => $userData->address2,
            'city' => $userData->city,
            'state' => $userData->state,
            'zip' => $userData->zip,
            'neighbourhood' => $userData->neighbourhood,
          );
   
          $id = User::create($detail)->id;
          $cust = array(
            'user_id' => $id,
            'age' => $data['age'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
          );
          Customer::create($cust);
          
          DB::commit();
          return ApiResponse::success('success', $detail);
        } catch (\Exception $th) {

          DB::rollback();
          return ApiResponse::error('error', $th->getMessage());
        }
      }


    public function login(LoginRequest $request) {      
        $credentials = $request->validated();
        
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
                return $this->sendError(__('invalid_login'));
            }
        } catch (JWTAuthException $e) {
            return $this->sendError(__('token_create_failed'));
        }
        
        $user = $request->user();
        // Check User role
        // if(!$user->hasRole($request->user_role)) {
        //   auth()->logout();
        //   return $this->sendError(__('invalid_login'));
        // }

        // if($user->hasVerifiedEmail() == false) {
        //   auth()->logout();
        //   return $this->sendError(__('email_not_verified'));
        // }

        // if($user->hasVerifiedOTP() == false) {
        //   auth()->logout();
        //   return $this->sendError(__('otp_not_verified'), ['type' => 'otp_not_verified', 'phone' => (string) $user->phone]);
        // }


        if($user->isActive() == false) {
          auth()->logout();
          return $this->sendError(__('account_deact'));
        }
        
        $user = $this->repo->update($request->only([
          'device_type', 'device_token', 'latitude', 'longitude'
        ]), $user->id);

        $user = new UserResource($user, $token);
        return $this->sendResponse('Success', $user);
        
    }


  public function agentScanner(AgentScannerRequest $request)
  {
    $user_id  =  $request->user()->id;
    $qrcheck = Qrcode::where('code',$request->qrcode)->first();
    if($qrcheck){
       $qrcheckBooking = Qrcode::where('code',$request->qrcode)->where('booking_id','0')->first();
       if($qrcheckBooking){
       Qrcode::where('code',$request->qrcode)->update(['code' => $request->qrcode, 'booking_id' => $request->booking_id , 'booking_status' => 'recieved by agent']);
          return $this->sendResponse('Success');
        }else{
           return $this->sendResponse('Qrcode is already assigned to another visit');
        }
    }else{
       return ApiResponse::error('Qrcode is not valid');
    }
  }
  public function oldBooking(Request $request)
  {
     $user_id  =  $request->user()->id;
    $booking = Booking::where('request_status','1')->where('assigned_agent',$user_id)->whereRaw('DATE(booking_date) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)')->get();
    $reBooking = UpcomingBookingResource::collection($booking);
        return $this->sendResponse('Success', $reBooking);
  }
  

    /**
     * Register a User
     *
     * @param  Request $request
     * @return JSON
     */
    public function register(AgentRegisterRequest $request)
    {
        return DB::transaction(function() use ($request) {

            $data = $request->validated();     
            $data['password'] = bcrypt($data['password']);           
            
            $user = $this->repo->create($data);
            // $user->setRole(User::PASSENGER_ROLE);
    
            if($request->hasFile('avatar')) {
              $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
              $request->avatar->storeAs('public/users', $avatarName);              
              $user->avatar = 'users/'.$avatarName;
              $user->save();
            }

             // $this->twilio->messages->create($data['phone'], // to
             //          array("from" => config('services.twilio.from'), "body" => 'Your '.config('app.name').' App OTP is: ' . $data['otp'])
             // );
            
            $message = 'Your '.config('app.name').' OTP is: ' . $data['otp'];
            
            $phone = '+' .$user->phone_code.$user->phone;

            $messageData = [
              ['to' => $phone, 'body' => $message ]
            ];

            SmsHelper::send($messageData);

            event(new Registered($user));

            // $user->createAsStripeCustomer();
            // $user->createAsStripeAccount();

            $user = new UserResource($user);
            return $this->sendResponse(__('We have sent you confirmation email and OTP.'), $user);
        });
    }

    // public function getAuthUser(Request $request)
    // {
    //     $user  = JWTAuth::toUser($request->token); 
    //     $user = new UserResource($user);
    //     return $this->sendResponse('Success.', $user);
    // }
    

    public function logout(Request $request)
    {
        $user  = JWTAuth::toUser($request->token);
        $user->device_token = NULL;
        $user->save();
        return $this->sendResponse('Success.');
    }
      public function agentProfile(Request $request)
      {
        $user = $request->user(); 
        $user_resource = new AgentProfileResource($user);
        return ApiResponse::success('success',$user_resource);

      }

      public function agentProfileUpdate(UpdateAgentProfileRequest $request)
      {
        $user = $request->user();
        $user->username = $request->username;
        $user->address = $request->address;
        $user->age = $request->age;
        $user->gender = $request->gender;
        $user->description = $request->description;
        // $user->dob = $request->dob;
        // $user->paypal_id = $request->paypal_id;
        if($request->hasFile('avatar')) {
          $oldPic = $user->image;
          $oldPicPath = storage_path('app/public/'.$user->image);
          $avatarName = $user->id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
          $store = $request->avatar->storeAs('public/users', $avatarName);
          if($store && $oldPic) {
            // delete old pic
            if (file_exists($oldPicPath)) {
              @unlink($oldPicPath);
            }
          }
          $user->image = 'users/'.$avatarName;
        }
        $user->save();
        $user_resource = new AgentResource($user);
        return ApiResponse::success('success',$user_resource);

      }

        public function agentList(Request $request)
      {
        $user = $request->user();
        $assigned_agent = $user->assigned_agent;
        if($assigned_agent){   
          $userexp = explode(',', $assigned_agent);
          $getuser = User::whereIn('id',$userexp)->paginate(10);
        }else{
          $getuser = "";
        }
        return $user_resource = AgentResource::collection($getuser);
         
      }

      public function runnerList(Request $request)
      {
        $user = $request->user();
        $runner = User::whereHas('roles', function($q){ $q->where('name', 'runner');})->paginate(10);
        return $user_resource = AgentResource::collection($runner);
         
      }
    public function agentLocation(Request $request)
      {
        $user = $request->user();
        $assigned_agent = $user->assigned_agent;
        if($assigned_agent){   
          $userexp = explode(',', $assigned_agent);
          $getusers = User::whereIn('id',$userexp)->get();
          foreach ($getusers as $key => $value) {
           $getuser[$key]['lat'] = (float)$value->latitude;
           $getuser[$key]['lng'] = (float)$value->longitude;
           $getuser[$key]['label'] = $value->username;
          
          }
        }else{
          $getuser = "";
        }
        return ApiResponse::success('success',$getuser);
      }

      public function runnerLocation(Request $request)
      {
        $user = $request->user();
        $getuser = [];
        $runner = User::whereHas('roles', function($q){ $q->where('name', 'runner');})->get();
          foreach ($runner as $key => $value) {
           $getuser[$key]['lat'] = (float)$value->latitude;
           $getuser[$key]['lng'] = (float)$value->longitude;
           $getuser[$key]['label'] = $value->username;
          }
        return ApiResponse::success('success',$getuser);
      }
      
      

      public function fetchAgentDetails($id)
      {
         $data =  User::select('users.*','users.username','users.email','users.username','users.email','users.phone','users.address','users.age')
        ->where('users.id',$id)
        ->get();
         $booking = [];

        if($data){

            foreach ($data as $key => $d) {
               $booking[$key]['id'] = $d->id;
               $booking[$key]['user_name'] = $d->username;
               $booking[$key]['user_email'] = $d->email;
               $booking[$key]['user_phone'] = $d->phone;
                $booking[$key]['user_age'] = $d->age;
                $booking[$key]['user_gender'] = $d->gender;
            }

        }
        //print_r($booking);
        return $this->sendResponse('Success.',$booking);

      }

         public function fetchBookingDetails($id)
      {
          $data =  Booking::select('bookings.*','users.username','users.email','users.phone','bookings.booking_date','bookings.payment_mode','bookings.booking_status','bookings.payment_status')
          ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->where('bookings.assigned_agent',$id)
        // ->where('bookings.status','1')
        ->orderBy('bookings.id','DESC')
        ->get();
$booking = [];
        if($data){
            foreach ($data as $key => $d) {
               $booking[$key]['id'] = $d->id;
               $booking[$key]['patient_name'] = $d->username;
               $booking[$key]['patient_email'] = $d->email;
               $booking[$key]['patient_phone'] = $d->phone;
               $booking[$key]['booking_date'] = $d->booking_date;
              if($d->payment_mode == 'cod'){
                  $mode = 'Cash on collection';
               }else if($d->payment_mode == 'online'){
                  $mode = 'Online';
               }else if($d->payment_mode == 'paid'){
                  $mode = 'Already Paid';
               }else{
                 $mode = '';
               }
               if($d->booking_status == '0'){
                  $status = 'Booked';
               }else if($d->booking_status == '1') {
                  $status = 'Out for Pickup';
               }else if($d->booking_status == '2') {
                  $status = 'Collecting Sample';
               }else if($d->booking_status == '3') {
                  $status = 'Sample Taken';
               }else if($d->booking_status == '4') {
                  $status = 'Out for Delivery';
               }else if($d->booking_status == '5') {
                  $status = 'Sample Received';
               }else if($d->booking_status == '6') {
                  $status = 'Sample Examining';
               }else if($d->booking_status == '7') {
                  $status = 'Report Ready';
               }else if($d->booking_status == '8') {
                  $status = 'Completed';
               }
              $booking[$key]['mode'] = $mode;
              $booking[$key]['status'] = $status;
                  if($d->payment_status == '0'){
                  $payment_status = 'Pending';
               }else if($d->payment_status == '1') {
                  $payment_status = 'Already Paid';
               }else if($d->payment_status == '2') {
                  $payment_status = 'Payment Collected by Agent';
               }else if($d->payment_status == '3') {
                  $payment_status = 'Payment Received';
               }else if($d->payment_status == '4') {
                  $payment_status = 'Paid';
               } 
               $booking[$key]['payment_status'] = $payment_status;  
            }

        }
        //print_r($booking);
        return $this->sendResponse('Success.',$booking);

      }

      public function updateAgentslot(Request $request){

        $checkDate = AgentAvailability::where('date',$request->date)->where('agent_id',$request->agent_id)->whereDate('date','>=',\Carbon\Carbon::now())->get()->count();
        if($checkDate > 0){
          AgentAvailability::where('date',$request->date)->where('agent_id',$request->agent_id)->update($request->all());
        }else{
        $AgentAvailability = new AgentAvailability();
        $AgentAvailability->agent_id = $request->agent_id;
        $AgentAvailability->opening_hours = $request->opening_hours;
        $AgentAvailability->closing_hours = $request->closing_hours;
        $AgentAvailability->date = $request->date;
        $AgentAvailability->save();
        }

        $data = AgentAvailability::where('agent_id',$request->agent_id)->whereDate('date','>=',\Carbon\Carbon::now())->get();
        if($data){

        return $this->sendResponse('Success.',$data);
        }
        return $this->sendResponse('Success.');

      }

      public function fetchAgentSlot($id){
        $data = AgentAvailability::where('agent_id',$id)->whereDate('date','>=',\Carbon\Carbon::now())->get();
        if($data){

        return $this->sendResponse('Success.',$data);
        }
        return $this->sendResponse('Success.');

      }

      public function userRole($id)
      {
        $user = User::find($id);
        if($user->login_type == '3'){
           $data['role'] = '3';
           return ApiResponse::success('success',$data);
        }
         $role = DB::table('model_has_roles')->where('model_id',$id)->first();
         $data['role'] = $role->role_id;
         return ApiResponse::success('success',$data);

      }
     public function getAvailSlots(Request $request)
      {
        $booked = [];
        $agentBookedSlots = AgentBookedSlots::where('booking_date',$request->date)->get()->pluck('slot_id');
        $agentUnbookedSlots = AgentSlots::whereNotIn('id',$agentBookedSlots)->get();
        foreach ($agentUnbookedSlots as $key => $value) {
          $data[$key]['id'] = $value->id;
          $data[$key]['slot'] = $value->slot;
        }
      $agenSlots = AgentSlots::whereIn('id',$agentBookedSlots)->get();
         foreach ($agenSlots as $key => $value) {
          $booked[$key]['id'] = $value->id;
          $booked[$key]['slot'] = $value->slot;
        }
         return ApiResponse::mulitsuccess('success',$data,$booked);

      }
      public function addAgentSlot(Request $request)
      {
        $booked = [];
       $AgentBookSlots = new AgentBookedSlots();
       $AgentBookSlots->agent_id = $request->agent_id;
       $AgentBookSlots->booking_date = $request->booking_date;
       $AgentBookSlots->slot_id = $request->slot_id;
        $AgentBookSlots->save();
        $agentBookedSlots = AgentBookedSlots::where('booking_date',$request->booking_date)->get()->pluck('slot_id');
        $agentUnbookedSlots = AgentSlots::whereNotIn('id',$agentBookedSlots)->get();
        foreach ($agentUnbookedSlots as $key => $value) {
          $data[$key]['id'] = $value->id;
          $data[$key]['slot'] = $value->slot;
        }
      $agenSlots = AgentSlots::whereIn('id',$agentBookedSlots)->get();
         foreach ($agenSlots as $key => $value) {
          $booked[$key]['id'] = $value->id;
          $booked[$key]['slot'] = $value->slot;
        }
         return ApiResponse::mulitsuccess('success',$data,$booked);

      }

      public function removeAgentSlot(Request $request)
      {
        $booked = [];
        AgentBookedSlots::where('booking_date',$request->booking_date)->where('slot_id',$request->slot_id)->delete();
        $agentBookedSlots = AgentBookedSlots::where('booking_date',$request->booking_date)->get()->pluck('slot_id');
        $agentUnbookedSlots = AgentSlots::whereNotIn('id',$agentBookedSlots)->get();
        foreach ($agentUnbookedSlots as $key => $value) {
          $data[$key]['id'] = $value->id;
          $data[$key]['slot'] = $value->slot;
        }
      $agenSlots = AgentSlots::whereIn('id',$agentBookedSlots)->get();
         foreach ($agenSlots as $key => $value) {
          $booked[$key]['id'] = $value->id;
          $booked[$key]['slot'] = $value->slot;
        }
         return ApiResponse::mulitsuccess('success',$data,$booked);

      }
      public function updateAgentLocation(Request $request)
      {
          User::where('id',$request->id)->update($request->all());

         $user = User::find($request->id);
         $data['assigned_location'] = $user->assigned_location;
         return ApiResponse::success('success',$data);

      }

     public function fetchAgentLocation($id)
      {
       
         $user = User::find($id);
         $data['assigned_location'] = $user->assigned_location;
         return ApiResponse::success('success',$data);

      }

     public function loginuserRoles($id)
    {
         $role = DB::table('model_has_roles')->where('model_id',$id)->first();        
        return response()->json([
            'role' => $role->role_id,
        ]);

      }


      public function agentBookingStatus(Request $request)
      {
        Booking::where('id',$request->id)->update($request->all());
        $booking = Booking::find($request->id);
        $data = $booking->booking_status;
        return ApiResponse::success('success',$data);

      }

      public function agentPaymentStatus(Request $request)
      {
        Booking::where('id',$request->id)->update($request->all());
        $booking = Booking::find($request->id);
        // $status = $booking->payment_status;
        $data = User::byId($request->id);

        return ApiResponse::success('success',$data);

      }

      public function agentAddTest(AgentTestRequest $request)
      {
      $total_price = 0;
      $discount = 0;
     $products = explode(',',$request->product_id);
        foreach($products as $key=>$value){
          $product = Product::find($value);
          $cart_data['booking_id'] = $request->booking_id;
          $cart_data['title'] = $product->title;
          $cart_data['price'] = $product->price;
          $cart_data['discount'] = $product->discount;
          CustomerBookingProduct::create($cart_data);
          $total_price += $product['price'] ;
          $discount += $product['discount'];


        }
        $booking =  Booking::where('id', $request->booking_id)->first();
        $bookingData = array(
        'sub_total' => $booking->sub_total + $total_price,
        'discount' => $booking->discount + $discount,
        'total_price' => $booking->total_price + $total_price - $discount,
      );
      Booking::where('id', $request->booking_id)->update($bookingData);
        return ApiResponse::success('success');
      }

      public function getAgentProduct(Request $request){   

        $data = Product::where('status','1')->orderBy('order','desc')->get();

        
        $product= [];
        foreach($data as $key=>$a){
            $product[$key]['id'] = $a->id;
            $product[$key]['title'] = $a->title;
        } 
         return ApiResponse::success('success', $product);
    }


      public function getVendorProduct(Request $request){   

        $data = Product::where('status','1')->where('vendor',$request->vendor_id)->orderBy('order','desc')->get();

        
        $product= [];
        foreach($data as $key=>$a){
            $product[$key]['id'] = $a->id;
            $product[$key]['title'] = $a->title;
        } 
         return ApiResponse::success('success', $product);
    }

     public function deleteAvailability(Request $request){   

      AgentAvailability::where('agent_id',$request->agent)->whereDate('date',$request->date)->delete();
      AgentBookedSlots::where('agent_id',$request->agent)->whereDate('booking_date',$request->date)->delete();
      $data = AgentAvailability::where('agent_id',$request->agent)->whereDate('date','>=',\Carbon\Carbon::now())->get();
        if($data){
        return $this->sendResponse('Success.',$data);
        }
         return ApiResponse::success('success');
    }
    
   
    public function getCustomerLocation(Request $request){   

        $booking = Booking::find($request->id);
        $bookingInfo['order_id']= rand(10,10000);
        $bookingInfo['booking_id']= $booking->id;
        $bookingInfo['pickup_location']= $booking->address.','.$booking->address1.','.$booking->address2.','.$booking->neighbourhood.','.$booking->landmark;
        $bookingInfo['drop_location']= 'Mumbai';
        $bookingInfo['lat']= $booking->lat;
        $bookingInfo['long']= $booking->lang;
        $bookingInfo['customer_name']= User::byName($booking->user_id);
         return ApiResponse::success('success', $bookingInfo);
    }
      public function agentLocationapp(Request $request)
      {

        $user = $request->user();
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->save();
         return ApiResponse::success('success');

      }

      public function getBookedSlot(Request $request)
      {

        $bookedSlot = AgentBookedSlots::where('slot_id',$request->slot_id)->where('agent_id',$request->assigned_agent)->whereDate('booking_date',$request->bookingDate)->get()->count();
       
         return ApiResponse::success('success',$bookedSlot);

      }
    public function updateUserAgent(Request $request)
      {
         $this->validate($request, [
             'user_id' => 'required|exists:users,id',
              'username' => 'required',
               'age' => 'required',
                'gender' => 'required'
         ]);
        $userData = array(
        'username' => $request->username,
      );
      User::where('id', $request->user_id)->update($userData);
        $userData = array(
        'age' => $request->age,
        'gender' => $request->gender,
      );
      Customer::where('user_id', $request->user_id)->update($userData);
       return ApiResponse::success('success');
    }
        public function getTLAgentBookingCount($id){
      $completed= 0;
      $weekly= 0;
      $monthly= 0;
       $completed =  DB::table('bookings')->where('assigned_agent',$id)
       ->where('booking_status',8)
       ->whereDate('booking_date','=',\Carbon\Carbon::now())
        ->get()->count();
       $weekly =  DB::table('bookings')->where('assigned_agent',$id)
       ->where('booking_status',8)
        ->whereDate('booking_date','>',\Carbon\Carbon::now()->subDays(7))
        ->get()->count();
       $monthly =  DB::table('bookings')->where('assigned_agent',$id)
       ->where('booking_status',8)
        ->whereDate('booking_date', '>', \Carbon\Carbon::now()->subDays(30))
        ->get()->count();

      
       $data = array(
          'completed' => $completed,
          'weekly' => $weekly,
          'monthly' => $monthly,
       );
       return $this->sendResponse('Success.',$data);
       
    }

        public function payment (PaymentRequest $request){
       // $auth = auth()->user();
       $mode = "TEST"; //<------------ Change to TEST for test server, PROD for production
        
         if ($mode == "PROD") {
          $url = "https://www.cashfree.com/checkout/post/submit";
        } else {
          //echo "string";
          $url = "https://test.cashfree.com/billpay/checkout/post/submit";
        }  
         $secretKey = "TESTb0d9b76736e2cdc24a2225a00d22b089cbe9e8a2";
         $postOrderId = 'uDAUD-'.rand().'-'.$request->booking_id;
         $user = User::find($request->customer_id);
          $postData = array( 
          "appId" => '112220225f9fc7ded89e32dc4e022211', 
           "paymentMode" => 'upi',
          "orderId" => $postOrderId, 
          "orderAmount" => $request->amount,
          "orderCurrency" => 'INR', 
          "orderNote" => 'Booking', 
          "customerName" => $user->username, 
         
          "customerPhone" => $user->phone, 
          "customerEmail" => $user->email,
          "returnUrl" => url('/api/agent_response'), 
         // "returnUrl" => 'http://localhost:8000/api/response', 
          "notifyUrl" => ''
         
         
        );
  

        ksort($postData);
        $signatureData = "";
        foreach ($postData as $key => $value){
            $signatureData .= $key.$value;
        }
        

        
        $s = hash_hmac('sha256', $signatureData, $secretKey,true);

        $sing = base64_encode($s); 
        $ch = curl_init();
        $curlConfig = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => array( 
                  "signature" => $sing, 
                  "paymentMode" => 'upi',
                  "orderNote" => 'Booking',
                  "orderCurrency" => 'INR', 
                  "customerName" => $user->username,
                  "customerPhone" => $user->phone, 
                  "customerEmail" =>  $user->email,
                  "orderAmount" => $request->amount, 
                  "notifyUrl" => '', 
                 // "returnUrl" => 'http://localhost:8000/api/response', 
                  "returnUrl" => url('/api/agent_response'), 
                  "appId" => '112220225f9fc7ded89e32dc4e022211', 
                  "orderId" => $postOrderId,
                  "notifyUrl" => '',
                )
        );
        curl_setopt_array($ch, $curlConfig);
        $result = curl_exec($ch);
        $base_string = base64_encode($result);
        $data['url'] = url('/api/submit_payment/');
        $data['payement'] = $base_string;
        return ApiResponse::success('success' ,$data);
    }
    public function submitPayment($url){
       $data = base64_decode($url);
        return $data;
    }
    public function response(Request $request){
       $data = $request->all();
       if($data['txStatus'] == "SUCCESS"){
        $order_id = substr($data['orderId'], strrpos($data['orderId'], '-' )+1);
        Booking::where('id',$order_id)->update(['payment_status'=>2]);
         $detail = array(
            'orderId' => $data['orderId'],
            'booking_id' => $order_id,
            'orderAmount' => $data['orderAmount'],
            'referenceId' => $data['referenceId'],
            'paymentMode' => $data['paymentMode'],
            'txStatus' => $data['txStatus'],
            'txTime' => $data['txTime'],
         );
         Payment::create($detail);
        return ApiResponse::success('success' ,"Payement recieved successfully");
       }
        return ApiResponse::success('error' ,"Payemnt failed please try again");
    }

      public function addChildPatient(AddPatientRequest $request){
         $data = $request->all();
         $otp = PaymentApp::otp();
         $detail = array(
          'name' => $data['username'],
          'username' => $data['username'],
          'phone' => $data['phone'],
          'address' => $data['address'],
          'city' => $data['city'],
          'state' => $data['state'],
          'login_otp' => $otp,
          'login_type' => 3,
          'status' => 1,
         );
        User::create($detail);  
          $homepage = 'http://buzz.yogiinfocom.in/http-api.php?username=udaud&password=daud123&senderid=uDAUDA&route=2&number='.$data['phone'].'&message= Dear '.$data['username'].', Welcome to uDAUD. Please use ('.$otp.') OTP to login your account. Live Healthy with uDAUD.';
          $smsUrl = str_replace(' ', '%20', $homepage);
          $curl = curl_init();
          curl_setopt_array($curl, [
          CURLOPT_URL => $smsUrl,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          ]);
          $response = curl_exec($curl);
          return ApiResponse::success('Success');
      }
}
