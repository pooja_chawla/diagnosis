<?php
namespace App\Http\Controllers\API;

use DB;
use JWTAuth;
use Validator;
use JWTAuthException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\UserResource;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\{ LoginRequest, RegisterRequest ,OtpRequest,LoginPhoneRequest,LoginOtpRequest,UpdateProfileRequest};
use App\Repositories\UserRepository;
use App\Helpers\ApiResponse;
use App\Helpers\PaymentApp;
use App\Helpers\SmsHelper;
use App\Models\User;
use App\Models\Agent;



class AgentAuthController extends AppBaseController
{

    
    public function agentRegister(Request $request) {
           
        $validator = Validator::make($request->all(), [
          'username' => 'required|string', 
          'email' => 'required|email',
          'phone' => 'required',
          'password' => 'required|min:6',
          'confirm_password' => 'required_with:password|same:password|min:6',
          
       ]);

      if ($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::error($error);
      }
      $data = $request->all(); 
      $detail= array(
        'username' => $data['username'],
        'email' => $data['email'],
        'phone' => $data['phone'],
        'password' => bcrypt($data['password']),
        'login_type' => 1,
        'status' => 'Inactive'
      );
      $user = User::create($detail);
      Agent::create(['user_id' => $user->id ]);
      return ApiResponse::success('Success',$detail);
    }

    public function agentLogin(Request $request){
       $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:6',
       ]);

      if ($validator->fails()) {
          $error = $validator->messages()->first();
          return ApiResponse::error($error);
      }
      
      $data = $request->all();
      echo bcrypt($data['password']);
        die;
      $user = User::where('email', $data['email'])
                    //->where('password', bcrypt($data['password'])) 
                    ->where('login_type', 1)
                    ->get();
      print_r(count($user));

    }
}
