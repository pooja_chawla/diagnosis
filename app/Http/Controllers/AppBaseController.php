<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($message, $result = [])
    {
        return response()->json([
          'success' => true,
          'message' => $message,
          'data'    => $result
        ]);
    }

    public function sendError($error, $data = [], $code = 404)
    {
        return response()->json([
          'success' => false,
          'message' => $error
         // 'data'    => $data
        ]);
    }
}
