<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\VerifyEmailException;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Helpers\ApiResponse;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
   
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $token = $this->guard()->attempt($this->credentials($request));

        if (! $token) {
            return false;
        }

        $user = $this->guard()->user();
        if ($user instanceof MustVerifyEmail && ! $user->hasVerifiedEmail()) {
            return false;
        }

        $this->guard()->setToken($token);

        return true;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);
        $user = $this->guard()->user();
        $token = (string) $this->guard()->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');

        return response()->json([
            'user' => $user->id,
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration - time(),
        ]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $user = $this->guard()->user();
        if ($user instanceof MustVerifyEmail && ! $user->hasVerifiedEmail()) {
            throw VerifyEmailException::forUser($user);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failbed')],
        ]);
    }


     public function loginWithOtp(Request $request) 
    {  

        $this->validate($request, [
          'phone'        => 'required|exists:users,phone'
        ]);    

        $user = $this->repo->findOneBy(['phone' => $request->phone]);

        // send OTP
        $otp         = PaymentApp::otp();
        $message     = 'Your '.config('app.name').' OTP is: ' . $otp;
        $phone       = '+' .$user->phone_code .$request->phone; 
        $messageData = [ ['to' => $request->phone, 'body' => $message ] ];

        // SmsHelper::send($messageData);
        
        $user->login_otp  = $otp;
        $user->save();

        return $this->sendResponse('OTP has been sent on your registered phone number.');
    }

    public function loginOtpVerification(LoginOtpRequest $request)
    {
      $user = $this->repo->findOneBy(['login_otp' => $request->otp,'phone' => $request->phone ]);
      $user->login_otp = NULL;
      $user->save();
      $token = JWTAuth::fromUser($user);
      $user = new UserResource($user, $token);
      return $this->sendResponse(__('otp_verified'), $user);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = auth()->user();
        $user->device_token = NULL;
        $user->save();
        $this->guard()->logout();
        return ApiResponse::success('Logout Successfully.');

    }
}
