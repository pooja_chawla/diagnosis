<?php

namespace App\Admin\Actions\VendorProduct;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;

class ImportVendorProduct extends Action
{
    
     public $name = 'import data';
    protected $selector = '.import-vendor-product';

    // public function handle(Request $request)
    // {
    //     // $request ...

    //     return $this->response()->success('Success message...')->refresh();
    // }

    public function handle(Request $request)
    {
        // The following code gets the uploaded file, then uses the package `maatwebsite/excel` to process and upload your file and save it to the database.
        $request->file('file');

        return $this->response()->success('Import complete!')->refresh();
    }

    public function form()
    {
        $this->file('file', 'Please select file');
    }


    public function html()
    {
        return <<<HTML
        <a class="btn btn-sm btn-success import-vendor-product"><i class="fa fa-stack-overflow"></i>Import</a>
HTML;
    }
}