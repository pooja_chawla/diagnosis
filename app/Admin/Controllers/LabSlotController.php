<?php

namespace App\Admin\Controllers;

use App\Models\WorkDay;
use App\Models\WorkSlot;
use App\Models\SlotNotAvailabil;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class LabSlotController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'WorkDay';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkDay());

        $grid->column('day', __('Day'));
           
        // $grid->column('workslots.id');
        $grid->workslots()->opening_hour();
        $grid->workslots()->closing_hour();
        // $grid->column('workslots.id');
        $states = [
             'on' => ['value' => '1', 'text' => 'open', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'close', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkDay::findOrFail($id));

        $show->field('day', __('Day'));
        $show->field('status', __('Status'))->label();
        $show->workTimings('Products information', function ($timing) {
            $timing->disableCreation();
            // $timing->workDay('Day')->day();
            $timing->opening_hour('Opening Hour');
            $timing->closing_hour('Closing Hour');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkDay());
        $form->select('day', __('Day'))->options([ 
        'Monday'    => 'Monday',
        'Tuesday'   => 'Tuesday',
        'Wednesday' => 'Wednesday',
        'Thursday'  => 'Thursday',
        'Friday'    => 'Friday',
        'Saturday'  => 'Saturday',
        'Sunday'    => 'Sunday',
        ]);
        $form->time('workslots.opening_hour');
        $form->time('workslots.closing_hour');
        $states = [
            'on' => ['value' => '1', 'text' => 'open', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'close', 'color' => 'danger'],
        ];
        $form->switch('status')->states($states);





        return $form;
    }

}
