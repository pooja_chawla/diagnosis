<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\Vendor;
use App\Models\Charges;
use App\Models\Product;
use App\Models\Accreditation;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Models\Setting;

class VendorController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Vendor';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();

        $grid->model()->role('vendor');
        // $grid->column('name', __('Name'));
        $grid->column('username', __('Username'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('image', __('Avtar'))->image('','40','40');
        $grid->column('status', __('Status'))->label();
        $grid->column('created_at', __('Created at'))->date('Y-m-d');
        $grid->filter(function($filter){
            $filter->disableIdFilter();
              $filter->equal('id', 'Filter by Vendor')->select(User::whereHas('roles', function($q){ 
                    $q->where('name', 'vendor');
                           })->pluck('username', 'id'));
            $filter->equal('status')->select(['Active' => 'Active', 'Inactive' => 'Inactive', 'Suspended' => 'Suspended']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('name', __('Name'));
        $show->field('username', __('Username'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('password', __('Password'));
        $show->image()->image();
        $show->vendor('VENDOR INFORMATION', function ($user) {
            $user->panel()->tools(function ($tools) {
                $tools->disableEdit();
                $tools->disableList();
                $tools->disableDelete();
            });
            $user->title( __('Title'));
            $user->slug( __('Slug'));
            $user->type( __('Type'));
            $user->fax( __('Fax'));
            $user->GST( __('GST'));
            $user->latitude( __('Latitude'));
            $user->longitude( __('Longitude'));
            $user->description( __('Description'))->unescape();
            $user->address( __('Address'))->unescape();
            $user->gallery( __('Gallery'))->image();
        });

        $show->products('Products information', function ($vendorProduct) {
            $vendorProduct->disableCreation();
            // $vendorProduct->setResource('/admin/vendor');
            // $vendorProduct->id('Id');
            $vendorProduct->product('Product')->title();
            $vendorProduct->price('Product Price');
            $vendorProduct->discount('Product Discount');
        });

        $show->field('status', __('Status'))->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());
       

       $form->tab('Basic info', function ($form) {
           $form->text('name', __('Name'))->rules('required');
           $form->text('username', __('Username'));
           $form->email('email', __('Email'))->creationRules(['required', "unique:users"])
                                          ->updateRules(['required', "unique:users,email,{{id}}"]);
           
            $form->password('password', __('Password'))->creationRules('required|min:6');
            
            // $form->select('Charges')->options(Setting::all()->pluck('title', 'id'));
            $form->mobile('phone', __('Phone'))
                 ->creationRules(['required','min:10',"unique:users"])
                 ->updateRules(['required','min:10',"unique:users,phone,{{id}}"])
                 ->help('Phone must contain 10 numbers');
            $form->image('image', __('Avtar'));

           

       })->tab('Profile', function ($form) {
            
            $form->text('vendor.slug')->default('lab-'.rand());    
            $form->text('vendor.fax', __('Fax'));
            $form->text('vendor.GST', __('GST'));
            $form->textarea('address', __('Address'));
            $form->text('city', __('City'));
            $form->text('zip', __('Zip'));

        })->tab('Lab Info', function ($form) {
            $form->select('vendor.parent_id', __('Parent Vendor'))->options(Vendor::pluck('title','user_id'));
            $form->radio('vendor.partner_lab', __('Partner Lab'))->options(['1' => 'Yes', '0'=> 'No'])->default('0');
            $form->text('vendor.title', __('Title'));
           
            
            $form->ckeditor('vendor.description', __('Description'));
            $form->image('vendor.image', __('Logo'))->removable();
            $form->multipleImage('vendor.gallery', __('Gallery'))->removable();
            $form->multipleImage('vendor.certificate', __('Certificates'))->removable();
        })->tab('Includes', function ($form) {
                $form->number('vendor.other_chr',_('Other'))->default(0);
                $form->number('vendor.fasting_chr',_('Fasting'))->default(0);
                $form->number('vendor.nfasting_chr',_('Non Fasting'))->default(0);
                $form->number('vendor.after_meal_chr',_('2 Hours after meal'))->default(0);

                $form->number('vendor.child_other_chr',_('Other(Child)'))->default(0);
                $form->number('vendor.child_fasting_chr',_('Fasting(Child)'))->default(0);
                $form->number('vendor.child_nfasting_chr',_('Non Fasting(Child)'))->default(0);
                $form->number('vendor.child_after_meal_chr',_('2 Hours after meal(Child)'))->default(0);
                
                
                $form->multipleSelect('vendor.accreditation', __('Accreditation'))->options(Accreditation::pluck('name','id'));
             

        })->tab('SEO', function ($form) {

             $form->text('vendor.seo_title', __('Title')); 
             $form->text('vendor.seo_keyword', __('Keyword'));
             $form->textarea('vendor.seo_des', __('Description'))->rows(5);

        })->tab('Status', function ($form) {

             $form->select('status', __('Status'))->options(['Active' => 'Active', 'Inactive' => 'Inactive', 'Suspended' => 'Suspended'])->default('Inactive');

        }); 
        $form->saving(function (Form $form){
             if ($form->password == null) {
                $form->password = $form->model()->password;
            }
            if($form->password && $form->model()->password != $form->password)
            {
                $form->password = bcrypt($form->password);
            }

                
            if(\request()->isMethod('POST')) {
                 
               
                
            }

        });
        $form->model()->assignRole('vendor');

         $form->saved(function (Form $form) {
              return redirect('admin/vendors');
            //...
        });
        
       return $form;
    }


}
