<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\Prerequisite;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Habit;
use App\Models\Inventory;
use App\Models\Vendor;
use App\Models\Profile;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Models\AssignProfile;
use App\Models\PackageTests;
use Encore\Admin\Show;

class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());
        $grid->model()->where('vendor', '=', 0);
        $grid->model()->orderBy('created_at', 'desc');
        //$grid->quickSearch('title');
        $grid->sortable();
        

        $grid->column('title', __('Title'));
        $grid->categories(__('Categories'))->display(function ($owner_id) {
        return ($owner_id ? Category::find($owner_id)['name'] : null);
        });

        //$grid->column('vendor', __('vendor'));
        $grid->column('price', __('Price'));
        $grid->column('type', __('Test type'))->using(['1' => 'Package','5' => 'Admin-Package', '2' => 'Individual' ,'3' => 'Service','4' => 'Profile'])->label();

        
        /*$form->select('categories', __('Categories'))->options(Category::where('status','1')->pluck('name','id'));*/

        $grid->column('status', __('Status'))->using(['1' => 'Active','0' => 'InActive'])->label();
        $grid->column('created_at', __('Created at'))->date('Y-m-d');
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('title', __('Name'));
            $filter->equal('status')->select(['Active' => 'Active', 'Inactive' => 'Inactive']);
            $filter->equal('type')->select(['1' => 'Package','5' => 'Admin-Package', '2' => 'Individual' ,'3' => 'Service','4' => 'Profile']);
        });
        
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('slug', __('Slug'));
        $show->field('description', __('Description'))->unescape();
       
        $show->type('type')->using(['5' => 'Admin Package', '2' => 'Individual' ,'3' => 'Service','4' => 'Profile'])->label();
        $show->field('price', __('Price'));
        $show->field('discount', __('Discount'));
        $dd = request()->route()->parameters('product');

        $type = Product::find($dd['product']);
        if($type->type == '5'){
            $show->field('vendor',__('Vendor'))->as(function ($roles) {
            $dd = request()->route()->parameters('product');
            $userids = AssignProfile::where('product_id',$dd['product'])->first();
            $exp = explode(',',$userids->vendor_id);
            $allusers = User::whereIn('id',$exp)->pluck('username');
            if(!empty($allusers)){
            return $allusers;
            }else{
            return "";
            }
            })->label();
        }
        $profile = Profile::where('product_id',$dd)->get();
        
        $show->AssignProfile('Profile Info', function ($id) {
            
            $id->id();
            $id->name();
        });
       // $show->include('include')->using(['1' => 'Fasting', '2' => 'Non Fasting', '3' => '2 Hour after meal'])->label();
         $show->include('include')->multipleSelect(['1' => 'Fasting', '2' => 'Non Fasting', '3' => '2 Hour after meal' , '4'=> 'Other'])->label();
        $show->status('status')->using(['1' => 'Active', '0' => 'Inactive'])->label();

        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());
        
        $form->tab('Product', function ($form) {
            $form->select('categories', __('Categories'))->options(Category::where('status','1')->pluck('name','id'));
            $form->text('title', __('Title'))->rules('required');
            $form->hidden('slug');
            $form->select('type', __('Type'))->options(['5' => 'Package', '2' => 'Individual' ,'3' => 'Service'])->rules('required');
            $form->multipleSelect('profile_ids', __('Profile'))->options(Profile::pluck('name','id'));
            $form->hasMany('prerequisites', __('Test'), function(Form\NestedForm $form){
             $form->text('name', __('Test'));
            }); 
            $form->number('price', __('Price'))->default(0)->rules('required');
            $form->number('discount', __('Discount'))->default(0);
            $form->ckeditor('description', __('Description'))->rules('required');
           
            $form->hidden('product_code');

            $form->hidden('vendor');
           });
           $form->tab('Details', function ($form) {
            $form->multipleSelect('include', __('Includes'))->options([1 => 'Fasting', 2 => 'Non Fasting', 3 => '2 Hour after meal', 4 => 'Other'])->rules('required');
            $form->multipleSelect('inventories')->options(Inventory::all()->pluck('name', 'id'))->rules('required');
            $form->image('image', __('Image'))->removable();
            $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive'])->default('Inactive');
            $form->radio('available_at_door', __('Availability'))->options(['1' => 'Available at door'])->default('1');
            $form->radio('cost_free_package', __('Cost Free Package'))->options(['1' => 'Cost Free Package'])->default('0');

        })->tab('SEO', function ($form) {

             $form->text('seo_title', __('Title')); 
             $form->text('seo_keyword', __('Keyword'));
             $form->textarea('seo_des ', __('Description'))->rows(5);

        });   
         //... saving 
        $form->saving(function (Form $form){
            if(\request()->isMethod('POST')) {
                if ($form->slug == null) {
                    $slug = preg_replace('/(\d){1,}\.?(\d?){1,}\.?(\d?){1,}\.?(\d?){1,}/', '', $form->title);
                    $form->slug = \Str::slug($slug);
                }
                if ($form->product_code == null) {
                    $product_code = 'PRO-'.rand();
                    $form->product_code = $product_code;
                }
                if ($form->discount == null) {
                    $form->discount = '0';
                }
                if ($form->vendor == null) {
                    $form->vendor = '0';
                }
                
            }
        });
        // $form->saved(function (Form $form) {
        //       return redirect('/admin/products');
        //     //...
        // });

        return $form;
    }
}?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
 $('#tab-form-1 .row').css('display','none');
 $('#tab-form-2 .form-group:nth-child(6)').css('display','none');
        var selectedType = $('.type').val();
        if(selectedType == '5'){
            $('#tab-form-2 .form-group:nth-child(6)').css('display','block');
            $('#has-many-prerequisites').css('display','block');
        }else{
             $('#has-many-prerequisites').css('display','none');
             $('#tab-form-2 .form-group:nth-child(6)').css('display','none');
        }

        if(selectedType == '5'){
             $('.form-group:nth-child(5)').css('display','block');
        }else{
            $('.form-group:nth-child(5)').css('display','none');
        }
       
        $('.type').change(function() {
        var $option = $(this).find('option:selected');
        var value = $option.val();
    if(value == '5'){
        $('#tab-form-2 .form-group:nth-child(6)').css('display','block');
         $('#tab-form-1 #has-many-prerequisites').css('display','block');
        $('#tab-form-1 .row').css('display','block');
    }else{
             $('#tab-form-2 .form-group:nth-child(6)').css('display','none');
         $('#tab-form-1 #has-many-prerequisites').css('display','none');
        $('#tab-form-1 .row').css('display','none');
    }
    if(value == '5'){
         $('#tab-form-1 .form-group:nth-child(5)').css('display','block');
        $('#tab-form-1 .row').css('display','block');
    }else{
         $('#tab-form-1 .form-group:nth-child(5)').css('display','none');
        $('#tab-form-1 .row').css('display','none');
    }
        });
    })
</script>