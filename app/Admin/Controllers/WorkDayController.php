<?php

namespace App\Admin\Controllers;

use App\Models\WorkDay;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class WorkDayController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'WorkDay';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkDay());

        $grid->column('day', __('Day'));
           
        $states = [
            'on' => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
            'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkDay::findOrFail($id));

        $show->field('day', __('Day'));
        $show->field('status', __('Status'))->label();
        $show->workTimings('Products information', function ($timing) {
            $timing->disableCreation();
            // $timing->workDay('Day')->day();
            $timing->opening_hour('Opening Hour');
            $timing->closing_hour('Closing Hour');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkDay());
       
       $form->tab('Working Day', function ($form) {
       $form->date('working_date', __('Date time'))->default(date('Y-m-d'));  
       $form->select('day', __('Day'))->options([ 
                                                      'Monday'    => 'Monday',
                                                      'Tuesday'   => 'Tuesday',
                                                      'Wednesday' => 'Wednesday',
                                                      'Thursday'  => 'Thursday',
                                                      'Friday'    => 'Friday',
                                                      'Saturday'  => 'Saturday',
                                                      'Sunday'    => 'Sunday',
                                                  ])
                ->creationRules(['required', "unique:work_days"])
                ->updateRules(['required', "unique:work_days,day,{{id}}"]);
            
        })->tab('Time slots', function ($form) {
            
            $form->hasMany('workslots', function (Form\NestedForm $form) {

                $form->time('opening_hour', __('Start'))->format('HH:mm');
                $form->time('closing_hour', __('End'))->format('HH:mm');
                $states = [
                'on'  => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
                'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger']
            ];

            $form->switch('status', __('Status'))->states($states)->default('close');
                    
            });

        });

        $form->saved(function (Form $form) {
              return redirect('/admin/work-days');
            //...
        });




        return $form;
    }
}
