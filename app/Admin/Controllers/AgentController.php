<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\Agent;
use App\Models\WorkDay;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AgentController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Agent';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();
        $grid->model()->role('agent');

        $grid->column('username', __('Name'));
        // $grid->column('username', __('Username'));
        $grid->column('image', __('Avtar'))->image('',40,40);
        $grid->column('email', __('Email'))->color('#1e90ff');
        $grid->column('phone', __('Phone'))->color('#1e90ff');
       $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'))->sortable();
        
        // $grid->expandFilter();
        $grid->filter(function($filter){
            $filter->disableIdFilter();
              $filter->equal('id', 'Filter by Vendor')->select(User::whereHas('roles', function($q){ 
                    $q->where('name', 'agent');
                           })->pluck('username', 'id'));

            $filter->equal('status')->select(['1' => 'Active', '0' => 'Inactive']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('name', __('Name'));
        $show->field('title', __('Title'));
        $show->field('username', __('Username'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('phone', __('Phone'));
        $show->field('image', __('Avtar'))->image();
        $show->field('latitude', __('Latitude'));
        $show->field('longitude', __('Longitude'));
        $show->field('address', __('Address'));
        $show->agent('Agent INFORMATION', function ($user) {
            $user->panel()->tools(function ($tools) {
                $tools->disableEdit();
                $tools->disableList();
                $tools->disableDelete();
            });
            $user->qualification();
            $user->photo('Agent Photo')->image();
            $user->identity_type('Identity type')->label();
            $user->identity('Identity')->image();
            $user->identity_verification('Identity verification')->using(['1' => 'Yes', '0' => 'No'])->label();
            $user->driving_license_no('Driving license no');
            $user->driving_license_expired_at('Driving license expired at');
            $user->driving_proof('Driving proof')->image();
            $user->driving_proof_verification('Driving proof verification')->using(['1' => 'Yes', '0' => 'No'])->label();
        });
        $show->field('status', __('Status'))->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());


        $form->tab('Basic info', function ($form) {

            $form->text('name', __('Name'))->rules('required');
            $form->text('username', __('Username'))->rules('required');
            $form->email('email', __('Email'))->creationRules(['required', "unique:users"])
                                          ->updateRules(['required', "unique:users,email,{{id}}"]);
            $form->password('password', __('Password'));

            $form->mobile('phone', __('Phone'))
                 ->creationRules(['required','min:10',"unique:users"])
                 ->updateRules(['required','min:10',"unique:users,phone,{{id}}"])
                 ->help('Phone must contain 10 numbers');

            $form->image('image', __('Avtar'))->removable();
            $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive', '2' => 'Suspended'])->default('Inactive');
        })->tab('Profile', function ($form) {

            $form->text('agent.title', __('Title'))->rules('nullable|min:3')->help('Title must be atleast 3 characters');
            $form->hidden('agent.slug', __('Slug'));
             // $form->text('agent.latitude', __('Latitude')); 
            // $form->text('agent.longitude', __('Longitude')); 
            $form->textarea('agent.address', __('Address'))->rules('nullable|min:3');

        })->tab('Qualification', function ($form) {

            $form->text('agent.qualification', __('Qualification'));

        })->tab('Driving details', function ($form) {

            $form->text('agent.driving_license_no', __('Driving license no'));
            $form->date('agent.driving_license_expired_at', __('Driving license expired at'))->format('YYYY-MM-DD');
            $form->image('agent.driving_proof', __('Driving proof'))->removable()->help('Upload driving license');


        })->tab('Identity details', function ($form) {

            $form->image('agent.photo', __('Agent photo'))->removable();
            $form->text('agent.identity_type', __('Identity Type'))->help('Adhar Card, Voter Id etc.');
             $form->image('agent.identity', __('Identity proof'))->removable()->help('Upload Identity Proof');


        })->tab('Verification details', function ($form) {

            $form->select('agent.driving_proof_verification', __('Driving proof verified'))->options(['1' => 'Yes', '0' => 'No'])->default('0');
            $form->select('agent.identity_verification', __('Identity Verified'))->options(['1' => 'Yes', '0' => 'No'])->default('0');
        });

        $form->model()->assignRole('agent');
        
        
        $form->saving(function (Form $form){
            
            if(\request()->isMethod('POST')) {
                if ($form->slug == null) {
                    $slug = preg_replace('/(\d){1,}\.?(\d?){1,}\.?(\d?){1,}\.?(\d?){1,}/', '', $form->title);
                    $form->slug = \Str::slug($slug);
                }
            }
             if ($form->password == null) {
                $form->password = $form->model()->password;
            }

            if($form->password && $form->model()->password != $form->password)
            {
                $form->password = bcrypt($form->password);
            }

        });

        $form->saved(function (Form $form) {
              return redirect('/admin/agents');
            //...
        });

        return $form;
    }


    
}
