<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\Booking;
use App\Models\Qrcode;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Support\Facades\Hash;
use Encore\Admin\Show;

class QrController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Qrcode';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Qrcode());
        // $grid->model()->where('booking_id','!=', '');
        $grid->model()->orderBy('created_at', 'desc');
        $grid->column('code', __('Qrcode')); 
        // $grid->booking_id(__('Patient'))->display(function ($owner_id) {
        // $user_id = Booking::find($owner_id);
        // if($user_id){
        //     $user = User::find($user_id->user_id);
        //     return $user->username;
        // }else{
        //     return "";
        // }
        // });      
        $grid->column('created_at', __('Created at'))->sortable();
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('code', __('Qrcode'));
            $filter->like('booking_id', __('Booking ID'));
        });
 // $grid->disableCreation();
             $grid->actions(function ($actions) {
    $actions->disableDelete();
    $actions->disableEdit();

});
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Qrcode::findOrFail($id));

        $show->field('code', __('Code'));
        $show->field('runner_id', __('Runner'))->as(function ($id) {
        $user = User::find($id);
        if($user){
            return $user->username;
        }else{
            return "";
        }

      });
        $show->field('booking_status', __('Status'));

        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Qrcode());
        $form->text('code', __('Code')) ->creationRules(['required', "unique:qrcode"])
        ->updateRules(['required', "unique:qrcode,code,{{id}}"]);
       
        return $form;
    }


    
}
