<?php

namespace App\Admin\Controllers;

use App\Models\VendorProduct;
use App\Models\Product;
use App\Models\Vendor;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Habit;
use App\Models\User;
use App\Models\Inventory;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Encore\Admin\Widgets\Box;
use App\Admin\Extensions\Tools\ExcelImport;
use App\Admin\Extensions\ExcelExpoter;
use App\Admin\Imports\VendorProductImport;
use Excel;



class VendorMultipleProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Vendor Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());
        $grid->model()->where('vendor','!=',0);

        $grid->model()->orderBy('created_at', 'desc');

        // $grid->column('id', __('Id'));
        // $grid->column('vendor_id', __('Vendor id'));
        // $grid->product()->product_code('Product Code');
        // $grid->product()->title('Product Name');
        $grid->vendor_name()->username('Vendor Name');
        $grid->column('title', __('Product'));
        $states = [
            'on' => ['value' => 2, 'text' => 'Approve', 'color' => 'success'],
            'off' => ['value' => 3, 'text' => 'reject', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'));
    /*               $grid->actions(function ($actions) {
    $actions->disableEdit();

});*/
          $grid->filter(function($filter){
            $filter->disableIdFilter();
        $filter->equal('title', 'Filter by Product')->select(Product::where('vendor','!=',0)->pluck('title', 'title'));
            $filter->equal('vendor', 'Filter by Vendor')->select(User::whereHas('roles', function($q){ 
                    $q->where('name', 'vendor');
                           })->pluck('username', 'id'));
            // $filter->like('title', __('Name'));
          
        });
        return $grid;
    }   

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Product Name'));
        $show->field('price', __('Price'));
        $show->status('status')->using(['1' => 'Completed', '0' => 'Pending'])->label();
        $show->vendor_name('Vendor Info', function ($vendor) {
             $vendor->username();
             $vendor->email();
             $vendor->phone();
        $vendor->panel()
        ->tools(function ($tools) {
        $tools->disableList();
         $tools->disableDelete();
        $tools->disableEdit();
        });
         });
          $show->panel()
        ->tools(function ($tools) {
        $tools->disableList();
         $tools->disableDelete();
        $tools->disableEdit();
        });;
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */

       protected function form()
    {
        $form = new Form(new Product());
        
        $form->tab('Product', function ($form) {
            $form->text('title', __('Title'))->rules('required');
            $form->hidden('slug');
           /* $form->text('test_type', __('Test type'));*/
            $form->ckeditor('description', __('Description'));
            $form->number('price', __('Price'));
            $form->number('discount', __('Discount'));
            $form->text('product_code', __('Product code'))->creationRules(['required', "unique:products"])
                                                         ->updateRules(['required', "unique:products,product_code,{{id}}"]);
            $form->select('type', __('Type'))->options(['1' => 'Package', '2' => 'Individual' ,'3' => 'Service']);

        });
        $form->tab('Details', function ($form) {
            $form->multipleSelect('include', __('Includes'))->options([1 => 'Fasting', 2 => 'Non Fasting']);
            $form->multipleSelect('tags')->options(Tag::all()->pluck('name', 'id'));
            $form->multipleSelect('inventories')->options(Inventory::all()->pluck('name', 'id'));
            $form->multipleSelect('habits')->options(Habit::all()->pluck('name', 'id'));
            $form->select('categories', __('Categories'))->options(Category::where('status','1')->pluck('name','id'));


            $form->hasMany('prerequisites', __('Prerequisite'), function(Form\NestedForm $form){
                     $form->text('prerequisite', __('Prerequisite'));
                     
            });
           
            $form->textarea('equipment_info ', __('Equipment information'))->rows(5);

            $form->select('status', __('Status'))->options(['1' => 'Active', '0' => 'Inactive'])->default('Inactive');
            // $form->checkbox('available_at_door', __('Availability'))->options(['1' => 'Available at door step']);
            $form->radio('available_at_door', __('Availability'))->options(['1' => 'Available at door'])->default('1');

        })->tab('SEO', function ($form) {

             $form->text('seo_title', __('Title')); 
             $form->text('seo_keyword', __('Keyword'));
             $form->textarea('seo_des ', __('Description'))->rows(5);

        });   
         //... saving 
        $form->saving(function (Form $form){
            if(\request()->isMethod('POST')) {
                if ($form->slug == null) {
                    $slug = preg_replace('/(\d){1,}\.?(\d?){1,}\.?(\d?){1,}\.?(\d?){1,}/', '', $form->title);
                    $form->slug = \Str::slug($slug);
                }
            }
        });
        $form->saved(function (Form $form) {
              return redirect('/admin/vendor-products');
            //...
        });

        return $form;
    }
}

