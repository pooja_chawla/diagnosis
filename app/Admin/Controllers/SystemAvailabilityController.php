<?php

namespace App\Admin\Controllers;

use App\Models\WorkDay;
use App\Models\WorkSlot;
use App\Models\SlotNotAvailabil;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class SystemAvailabilityController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'WorkDay';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkDay());

        $grid->column('day', __('Day'));
           
        $states = [
            'on' => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
            'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkDay::findOrFail($id));

        $show->field('day', __('Day'));
        $show->field('status', __('Status'))->label();
        $show->workTimings('Products information', function ($timing) {
            $timing->disableCreation();
            // $timing->workDay('Day')->day();
            $timing->opening_hour('Opening Hour');
            $timing->closing_hour('Closing Hour');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkDay());
       
       $form->tab('Working Day', function ($form) {
       $form->date('working_date', __('Date time'))->default(date('Y-m-d'));  
       $form->select('day', __('Day'))->options([ 
                                                      'Monday'    => 'Monday',
                                                      'Tuesday'   => 'Tuesday',
                                                      'Wednesday' => 'Wednesday',
                                                      'Thursday'  => 'Thursday',
                                                      'Friday'    => 'Friday',
                                                      'Saturday'  => 'Saturday',
                                                      'Sunday'    => 'Sunday',
                                                  ])
                ->creationRules(['required', "unique:work_days"])
                ->updateRules(['required', "unique:work_days,day,{{id}}"]);
            
        })->tab('Time slots', function ($form) {
            
            $form->hasMany('workslots', function (Form\NestedForm $form) {

                $form->time('opening_hour', __('Start'))->format('HH:mm');
                $form->time('closing_hour', __('End'))->format('HH:mm');
                $states = [
                'on'  => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
                'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger']
            ];

            $form->switch('status', __('Status'))->states($states)->default('close');
                    
            });

        });

        $form->saved(function (Form $form) {
              return redirect('/admin/work-days');
            //...
        });




        return $form;
    }

    public function slots(){
        $day =  WorkDay::get();
        if(count($day)!=0){
            $duration = $day[0]->duration;
            $duratn = explode(':', $duration);
            $duratn = $duratn[0];
        }else{
            $duratn = 1;
        }
        $days =WorkDay::select("work_days.*", "work_slots.opening_hour","work_slots.closing_hour")
                        ->leftJoin("work_slots", "work_slots.day_id", "=", "work_days.id")
                        ->get();

               
        return view('admin/slots',compact('days','duratn','duration'));
    }
    public function updateDuration(Request $request){
        $data = $request->all();
        //print_r($data);
        WorkDay::where('status', 'open')->update($data);
        SlotNotAvailabil::truncate();
    } 
    public function updateslots(Request $request){
         $data = $request->all();
         $slots = WorkSlot::where('day_id',$data['day_id'])->get();
         if(count($slots) !=0){
            WorkSlot::where('day_id',$data['day_id'])->update(['opening_hour'=> $data['opening_hour'],'closing_hour'=> $data['closing_hour']]);
         }else{
                /*$data['status'] = 1;
                $start = strtotime($data['opening_hour']);
                $end = strtotime($data['closing_hour']);
                $minutes  = ($end - $start) / 60;
                $hours = floor($minutes / 60);
                $min = $minutes - ($hours * 60);

                $start1 = strtotime($data['duration']);
                $end1 = strtotime('00:00');
                $duration  = ($start1 - $end1) / 60;

                //print_r($duration.'-'.$minutes);
                print_r($minutes/$duration);
                die;*/
               WorkSlot::create($data);
         }
    } 
    public function slotNotAvailable(Request $request){
        $data = $request->all();
        SlotNotAvailabil::create($data);
    } 
}
