<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\Prerequisite;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Habit;
use App\Models\Inventory;
use App\Models\AssignPrice;
use App\Models\Vendor;
use App\Models\User;
use App\Models\AssignProfile;
use App\Models\AssignPackagePrice;
use App\Models\Profile;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class AssignPackageController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Assign Package';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());
        $grid->model()->where('type', '=', 5);
        $grid->model()->where('status', '=', 1);
        $grid->model()->orderBy('created_at', 'desc');
        //$grid->quickSearch('title');
        $grid->sortable();
        

        $grid->column('title', __('Package'));
         $grid->disableCreation();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
       
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());
         $id = request()->route()->parameters('assign_package');
         $id =  $id['assign_package'];
         $getData = AssignProfile::where('product_id',$id)->first();
         if($getData){
            $default[] = $getData->vendor_id;
             $a = $default[0];
                $s = explode(',', $a);
               $q= 1; 
             
            foreach ($s as $key=>$sid) {
                if($sid){
                    
                $vendor = User::find($sid);
                $amount = @AssignPrice::where(['product_id' =>$id, 'vendor_id' =>$sid])->pluck('amount')->first();
                $discount = @AssignPrice::where(['product_id' =>$id, 'vendor_id' =>$sid])->pluck('discount')->first();
                if($discount){
                    $discount = $discount;
                }else{
                    $discount = "0";
                }
                if($amount){
                    $amount = $amount;
                }else{
                    $amount = "0";
                } 
                $form->text('vendor', __('Vendor'))->default($vendor->username);
                $form->number('amount_'.$key, __('Amount'))->default($amount);
                $form->number('discount'.$key, __('Discount'))->default($discount);
                   
                  $form->saving(function (Form $form) use ($sid,$id,$key){
                    $amount = 'amount_'.$key;
                    $discount = 'discount'.$key;
                    $check = AssignPrice::where(['product_id' =>$id, 'vendor_id' =>$sid])->get();
                    if(count($check)==0){
                        AssignPrice::create(['product_id' =>$id, 'vendor_id' =>$sid,'amount'=> $form->{$amount},'discount'=> $form->{$discount}]);
                    }else{
                        AssignPrice::where(['product_id' =>$id, 'vendor_id' =>$sid])->update(['amount'=> $form->{$amount},'discount'=> $form->{$discount}]);
                    }
                       
                   });  
                    
                $idss[$key] = $sid;
            
                $vendor_ids = implode(',',$idss);

            $checkData = AssignProfile::where('product_id',$id)->get()->count();
            if($checkData > 0){
             $bookingData = array(
                'vendor_id' => $vendor_ids,
             );
             AssignProfile::where('product_id', $id)->update($bookingData);
            }
                
                  
                }
                $q++;
            }
             
            
         }else{
          $s = '';  
         }

          
        
         //var_dump($s);
         $form->multipleSelect('vendor_id', __('Assign Vendor'))->options(User::whereHas('roles', function($q){ 
                    $q->where('name', 'vendor');
                           })->pluck('username', 'id'))->default($s);

        

        $form->saving(function (Form $form) use ($id){
            $vendor_id = $form->vendor_id;
            $updVedor = AssignPrice::where('product_id',$id)->get()->pluck('vendor_id');
            foreach ($vendor_id as $k=>$vid) {
                $idss[$k] = $vid;
            }
            foreach ($updVedor as $key => $value) {
               if(!in_array($value, $vendor_id)){
                AssignPrice::where('product_id',$id)->where('vendor_id',$value)->delete();
               }
            }
                $vendor_ids = implode(',',$idss);

            $checkData = AssignProfile::where('product_id',$id)->get()->count();
            if($checkData > 0){
             $bookingData = array(
                'vendor_id' => $vendor_ids,
             );
             AssignProfile::where('product_id', $id)->update($bookingData);
            }else{
              $AssignProfile = new AssignProfile;
              $AssignProfile->product_id = $id;
              $AssignProfile->vendor_id = $vendor_ids;
              $AssignProfile->save();
            }
           //  $assignVendor = $form->vendor_id;
           //  foreach ($vendor_id as $k=>$vid) {

           //  }
           // $assignPrice = new AssignPackagePrice();
           // $assignPrice
            return redirect('/admin/assign-package');
             });
       return $form; 
       
    }
}?>
