<?php

namespace App\Admin\Controllers;

use App\Models\Booking;
use App\Models\Agent;
use App\Models\ProductRequest;
use App\Models\Product;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;


class ProductRequestController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product Request';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductRequest());
       
        $grid->model()->orderBy('created_at', 'desc');
         $grid->sortable();
        $grid->product_id(__('Product'))->display(function ($owner_id) {

        $product = Product::find($owner_id);
         if($product){
            return $product['title'];
        }else{
            return '';
        }

        });
        
      
        $grid->vendor(__('Vendor'))->display(function ($owner_id) {
         $user = User::find($owner_id);
         if($user){
            return $user['username'];
        }else{
            return '';
        }
        });
        $grid->column('price', __('Price'));
        $grid->column('discount', __('Discount'));
        
        $states = [
            'on' => ['value' => '2', 'text' => 'Accepted', 'color' => 'success'],
            'off' => ['value' => '3', 'text' => 'Rejected', 'color' => 'danger'],
        ];
        $grid->disableActions();
        $grid->column('status')->switch($states);
        $grid->disableCreation();
     // $grid->actions(function ($actions) {
     //    $actions->disableEdit();
     //    $actions->disableDelete();
     //    $actions->disableView();
     //    });
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->equal('vendor', 'Filter by Vendor')->select(User::whereHas('roles', function($q){ 
                    $q->where('name', 'vendor');
                           })->pluck('username', 'id'));
            $filter->equal('status')->select(['2' => 'Accepted', '3' => 'Rejected']);
            //$filter->like('title', __('Name'));
          
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */

    

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    { 
        $form = new Form(new ProductRequest());

        $form->select('status', __('Status'))->options(['2' => 'Accepted', '3' => 'Rejected'])->default('Pending');
        $form->hidden('cancel_status');
           $form->saving(function (Form $form){
                if ($form->status == 3) {
                    $form->cancel_status = 1;
                }
                 if ($form->status == 2) {
                    $form->cancel_status = 0;
                }
            
        });
        return $form;
    }
}


?>
