<?php

namespace App\Admin\Controllers;

use App\Models\Booking;
use App\Models\Agent;
use App\Models\CustomerBookingRequest;
use App\Models\Product;
use App\Models\Vendor;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
 use Encore\Admin\Show;

class LabCollectionController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Lab Collection';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CustomerBookingRequest());
        $grid->model()->where('booking_status', '=', 1);
        $grid->sortable();
       
        $grid->column('name', __('Name'));
        // $grid->column('dob', __('DOB'));
        // $grid->column('number');
        $grid->column('address');
        // $grid->column('subtotal', __('Subtotal'));
        // $grid->column('amount', __('Amount'));
        // $grid->column('discount', __('discount'));
        $grid->column('dateBooking', __('Booking Date'));
        $grid->column('slotBooking', __('Booking Slot'));
        $grid->actions(function ($actions) {
        $actions->disableEdit();
        });
        $grid->disableCreation();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CustomerBookingRequest::findOrFail($id));
         $show->booking('Booking Product information', function ($booking) {
            $booking->id();
            $booking->title();
            $booking->price();
            $booking->disableCreation();
            // $booking->actions(function ($actions) {
            // $actions->disableEdit();
            // $actions->disableDelete();
            // });
             $booking->disableActions();
            
        });
        $show->amounts('Booking Amount', function ($amounts) {
           $amounts->subtotal( __('Subtotal'));
            $amounts->discount( __('Discount'));
            $amounts->amount( __('Total Amount'));
            $amounts->disableCreation();
            // $amounts->actions(function ($actions) {
            // $actions->disableEdit();
            // $actions->disableDelete();
            // });
            $amounts->disableActions();

        });
        $show->panel()
        ->tools(function ($tools) {
        $tools->disableEdit();
        $tools->disableList();
        $tools->disableDelete();
        });
        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('dob', __('DOB'));
        $show->field('number');
        $show->field('dateBooking', __('Booking Date'));
        $show->field('slotBooking', __('Booking Slot'));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CustomerBookingRequest());

        $form->text('name', __('Name'));
        $form->date('dob', __('DOB'));
        $form->text('number', __('Number'));
        $form->text('address', __('Address'));
        $form->text('subtotal', __('Subtotal'));
        $form->text('amount', __('Amount'));
        $form->text('discount', __('discount'));
        $form->date('dateBooking', __('Booking Date'));
        $form->text('slotBooking', __('Booking Slot'));
        $form->hidden('status');

        return $form;
    }
}
