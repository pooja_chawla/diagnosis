<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\Prerequisite;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Habit;
use App\Models\Inventory;
use App\Models\Vendor;
use App\Models\User;
use App\Models\AssignProfile;
use App\Models\PackageTests;
use App\Models\Profile;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PackageController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product Profile';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Profile());
         $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();
        $grid->column('name', __('Profile'));
        
        $grid->product_id(__('Product [Package]'))->display(function ($owner_id) {

        $product = Product::find($owner_id);
         if($product){
            return $product['title'];
        }else{
            return '';
        }

        });
        // $grid->disableCreation();
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->equal('product_id', 'Filter by Product')->select(Product::where('type', 5)->where('status', 1)->pluck('title', 'id'));
         });
         
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $form = new Form(new Profile());
        $id = request()->route()->parameters('product_profile');

        $form->text('name', __('Profile'));
        /* $form->multipleSelect('vendor_sid.vendor_id', __('Assign Vendor'))->options(User::pluck('username', 'id')); */
        $getTests = PackageTests::where('profile_id',$id)->get();
         foreach ($getTests as $key=>$test) {
             $testId = $test->id;
             $form->text('test'.$key, __('Test'))->default($test->name);

         }
        
        return $form;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Profile());
       

        $form->text('name', __('Profile'));
        $form->hasMany('profilePackage', __('Test'), function(Form\NestedForm $form){
           $form->text('name', __('Test'));
          });
       return $form;
    }
}?>
