<?php

namespace App\Admin\Controllers;

use App\Models\SiteInfo;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SiteInfoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Site Info';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SiteInfo());

        // $grid->column('id', __('Id'));
        $grid->column('site_logo', __('Logo'))->image('',40,40);
        // $grid->column('slug', __('Slug'));
        $grid->column('site_number', __('Number'));
          $grid->column('site_email', __('Email'));
        // $grid->column('slug', __('Slug'));
        $grid->column('site_address', __('Address'));
          $grid->column('site_copyright', __('Copright'));
        $grid->column('cunsltation_fee', __('Consultation Fees'));
        $grid->column('site_description', __('Description'));
            $grid->disableCreation();
            $grid->actions(function ($actions) {
    $actions->disableDelete();

});
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SiteInfo::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('site_logo', __('Logo'))->image();
        $show->field('site_number', __('Number'));
        $show->field('site_email', __('Email'));
        $show->field('site_address', __('Address'));
        $show->field('site_copyright', __('Copright'));
        $show->field('cunsltation_fee', __('Consultation Fees'));
        $show->field('site_description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->panel()
        ->tools(function ($tools) {
        $tools->disableList();
        $tools->disableDelete();
        });;
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SiteInfo());
         // $form->image('site_logo', __('Logo'))->move($dir, $name);
        $form->image('site_logo', __('Logo'))->removable();
        $form->text('site_number', __('Number'));
         $form->text('site_email', __('Email'));
        $form->text('site_address', __('Address'));
         $form->text('site_copyright', __('Coptright'));
         $form->text('cunsltation_fee', __('Consultation Fees'));
        $form->textarea('site_description', __('Description'));
        return $form;
    }
}
