<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\CartSession;
use App\Models\Payment;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PaymentController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Payment';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Payment());
        $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();
        $grid->orderId(__('Name'))->display(function ($orderId) {
        $CartSession = CartSession::where('order_id',$orderId)->first();
        if($CartSession){
            $user = User::find($CartSession['user_id']);
            return $user->username;
        }else{
            return "";
        }
        });
         $grid->orderAmount(__('Amount'));
         $grid->txStatus(__('Status'));
        $grid->created_at(__('Created At'));
          $grid->actions(function ($actions) {
        $actions->disableEdit();
        });
        $grid->disableCreation();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Payment::findOrFail($id));
        $show->orderId('Name')->as(function ($orderId) {
             $data = array();
        $CartSession = CartSession::where('order_id',$orderId)->first();
        if($CartSession){
            $orderId = $CartSession['user_id'];
            $users = User::find($orderId);
            return $users->username;
        }else{
            return "";
        }
        });
    

        $show->field('orderAmount', __('Amount'));               
        $show->field('txStatus', __('Status'));
        $show->field('created_at', __('Created At'));
        $show->paymentDeatail('Customer information', function ($payment) {
        $payment->setResource('/admin/payments');
            $payment->id();
            $payment->user_id('Parent Customer');
            $payment->child('Child Customer');
            $payment->price();
            $payment->product_title('Product');
        });
     
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        return $form;
    }


    
}
