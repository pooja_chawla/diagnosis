<?php

namespace App\Admin\Controllers;

use App\Models\VendorProduct;
use App\Models\Product;
use App\Models\Vendor;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Encore\Admin\Widgets\Box;
// use App\Admin\Extensions\Tools\ExcelImport;
use App\Admin\Extensions\ExcelExpoter;
// use App\Admin\Imports\VendorProductImport;
use App\Admin\Actions\VendorProduct\ImportVendorProduct as ExcelImport;
use Excel;



class VendorProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Price Cap';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new VendorProduct());
         $grid->model()->orderBy('created_at', 'desc');
         $grid->sortable();
        // $grid->header(function ($query) {

        //     // $gender = $query->select(DB::raw('count(gender) as count, gender'))
        //     //     ->groupBy('gender')->get()->pluck('count', 'gender')->toArray();

        //     $csv = view('admin.csv');


        //     return new Box('Import CSV', $csv);
        // });

         $grid->tools(function ($tools) {
           $tools->append(new ExcelImport());
         });
           //Export
         // $grid->exporter(new ExcelExpoter());
         $grid->exporter(new ExcelExpoter());

        $grid->column('id', __('Id'));
        // $grid->column('vendor_id', __('Vendor id'));
        $grid->product()->product_code('Product Code');
        $grid->product()->title('Product Name');
        $grid->vendor()->title('Vendor');
        $grid->column('price', __('Vendor price'));
        // $grid->column('product_id', __('Product id'));
        $grid->column('type', __('Price Cap Type'))->editable('select', ['percentage' => 'Percentage', 'price' => 'Price']);
        $grid->column('price_cap', __('Price Cap'))->editable();
        $grid->column('offered_price', __('Price Offered'));
        // $grid->column('admin_discount', __('Admin discount'));
        // $grid->column('order', __('Order'));
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(VendorProduct::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('vendor_id', __('Vendor id'));
        $show->field('product_id', __('Product id'));
        $show->field('price', __('Price'));
        $show->field('admin_price', __('Admin price'));
        $show->field('discount', __('Discount'));
        $show->field('admin_discount', __('Admin discount'));
        $show->field('order', __('Order'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new VendorProduct());

        // $form->text('vendor_id', __('Vendor id'));
        $form->hidden('offered_price');
        $form->select('vendor_id', __('Vendor'))->options(Vendor::pluck('title','user_id'));
        $form->select('product_id', __('Product'))->options(Product::pluck('title','id'));
        $form->text('price', __('Vendor Price'));
        $form->text('discount', __('Vendor Discount'));
         $form->select('type', __('Type'))->options(['percentage' => 'Percentage', 'price' => 'Price']);
        $form->text('price_cap', __('Price Cap'));
        // $form->text('admin_discount', __('Admin discount'));
        // $form->text('order', __('Order'));
        $form->saving(function ($form) { 
            $price =  $form->model()->price;




            $price_cap =  $form->price_cap ? $form->price_cap : $form->model()->price_cap;
            $type = $form->type ? $form->type : $form->model()->type;

            if($type == 'price'){

                $form->offered_price = $price + $price_cap ;
            }elseif($type == 'percentage'){

                $percentage = ($price_cap / 100) * $price;
                $offered_price = $percentage + $price;
                $form->offered_price = round($offered_price,2) ;
            }





        });

        // $form->saved(function ($form) { 
        //     return redirect('/admin/price-caps');
        // });



        return $form;
    }

   


    public function importCsv(Request $request){

        if($request->file('csv_file')){
            return 1;
        }

        return 0;

        $path = $request->file('csv_file')->getRealPath();
        $data = array_map('str_getcsv', file($path));

        return $data;
        
       // 
    } 

    public function import(Request $request)
    {
      $files = $request->file('files');

      try {

          Excel::import(new VendorProductImport,$files);
          
      } catch (\Exception $e) {
          admin_toastr($e->getMessage(), 'error');
      }
      return back();
    }

    
}

