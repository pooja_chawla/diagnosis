<?php

namespace App\Admin\Controllers;

use App\Models\Booking;
use App\Models\Agent;
use App\Models\CustomerBookingProduct;
use App\Models\Product;
use App\Models\Vendor;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
 use Encore\Admin\Show;

class CustomerBookingProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Lab Collection';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CustomerBookingProduct());
        $grid->sortable();
        $grid->column('title', __('Title '));
        $grid->column('price', __('Price'));
        $grid->column('labName', __('Lab'));
        $grid->column('created_at');
        $grid->actions(function ($actions) {
        $actions->disableEdit();
        });
        $grid->disableCreation();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CustomerBookingProduct::findOrFail($id));
        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('price', __('Price'));
        $show->field('labName', __('Lab'));
        $show->field('created_at', __('created_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CustomerBookingProduct());

        $form->text('title', __('Title'));
        $form->text('price', __('Price'));
        $form->text('labName', __('Lab'));
          $form->date('created_at', __('created_at Date'));

        return $form;
    }
}
