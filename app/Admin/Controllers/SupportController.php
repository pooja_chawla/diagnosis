<?php

namespace App\Admin\Controllers;

use App\Models\User;
use App\Models\Agent;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Support\Facades\Hash;
use Encore\Admin\Show;

class SupportController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Support Panel';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());
        $grid->model()->orderBy('created_at', 'desc');
        
        $grid->sortable();
        $grid->model()->role('support');

        $grid->column('username', __('Name'));
        $grid->column('image', __('Avtar'))->image('',40,40);
        $grid->column('email', __('Email'))->color('#1e90ff');
        $grid->column('phone', __('Phone'))->color('#1e90ff');
        $grid->column('created_at', __('Created at'))->sortable();
        
        $grid->filter(function($filter)
        {
            $filter->disableIdFilter();
            $filter->like('name', __('Name'));
            $filter->equal('status')->select(['1' => 'Active', '0' => 'Inactive']);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));
        $show->field('name', __('Name'));
        $show->field('title', __('Title'));
        $show->field('username', __('Username'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('phone', __('Phone'));
        $show->field('assigned_agent', trans('admin.agents'))->as(function ($roles) {
            $dd = request()->route()->parameters('team_lead');
            $userids = User::find($dd['team_lead']);
            $exp = explode(',',$userids->assigned_agent);
            $allusers = User::whereIn('id',$exp)->pluck('username');
            if(!empty($allusers)){
                return $allusers;
            }else{
                return "";
            }
        })->label();
        $show->field('image', __('Avtar'))->image();
        $show->field('latitude', __('Latitude'));
        $show->field('longitude', __('Longitude'));
        $show->field('address', __('Address'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());
        $form->text('name', __('Name'))->rules('required');
        $form->text('username', __('Username'))->rules('required');
        $form->email('email', __('Email'))->creationRules(['required', "unique:users"]);
        $form->password('password',  trans('admin.password'))->creationRules(['required']);
        $form->mobile('phone', __('Phone'))
                ->creationRules(['required','min:10',"unique:users"])
                ->updateRules(['required','min:10',"unique:users,phone,{{id}}"])
                ->help('Phone must contain 10 numbers');

        $form->image('image', __('Avtar'))->removable();
        $agents = User::whereHas('roles', function($q){ 
            $q->where('name', 'agent');
        })->where('status','1')->get()->pluck('username', 'id');

        $dd = request()->route()->parameters('team-lead');
        if($dd){
            $userids = User::find($dd['team_lead']);
            $exp = explode(',',$userids->assigned_agent);
        }else{
            $exp = '';
        }
        $form->model()->assignRole('support');
        
        $form->saving(function (Form $form) 
        {
            if ($form->password == null)
            {
                $form->password = $form->model()->password;
            }
            if($form->password && $form->model()->password != $form->password)
            {
                $form->password = Hash::make($form->password);
            }
        });

        return $form;
    }    
}

