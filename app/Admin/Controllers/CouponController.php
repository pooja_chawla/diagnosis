<?php

namespace App\Admin\Controllers;
use Encore\Admin\Controllers\AdminController;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;


class CouponController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Coupons';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Coupon());
        
        $grid->sortable();
        $grid->column('name', __('Name'));
        $grid->column('expiry_date', __('Expire Date'));
        $grid->column('code', __('Coupon Code'));
        $grid->column('amount', __('Amount'));
         
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Coupon::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
          $show->field('code', __('Coupon Code'));
          $show->field('expiry_date', __('Expire Date'));
        $show->field('amount', __('Amount'));
       
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Coupon());
        $form->text('name', __('Name'));
        $form->text('code', __('Coupon Code'));
        $form->select('type', __('Type'))->options(['1' => 'Free Packages', '2' => 'Amount', '3' => 'Shipping Coupon'])->rules('required');
        $form->number('amount', __('Amount'));
        $form->date('expiry_date', __('Coupon Expire'));
        $form->multipleSelect('user_ids', __('Users'))->options(User::where('login_type',3)->pluck('username', 'id'));
        $form->multipleSelect('package_ids', __('Package'))->options(Product::where('type',5)->pluck('title', 'id'));
         $form->multipleSelect('free_packages_ids', __('Free Packages'))->options(Product::where('type',5)->where('cost_free_package',1)->pluck('title', 'id'));
            $form->saving(function (Form $form){
            if(\request()->isMethod('POST')) {
                if ($form->amount == null) {
                    $form->amount = '0';
                }
                if ($form->free_packages_ids == null) {
                    $form->free_packages_ids = '';
                }                
            }
        });
        return $form; 
    }
}?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
            $('.form-group:nth-child(4)').css('display','none');
            $('.form-group:nth-child(8)').css('display','none');
        var selectedType = $('.type').val();
        if(selectedType == '2'){
            $('.form-group:nth-child(8)').css('display','none');
            $('.form-group:nth-child(4)').css('display','block');
        }else if(selectedType == '3'){
             $('.form-group:nth-child(4)').css('display','block');
               $('.fields-group .form-group:nth-child(6)').css('display','none');
                // $('.fields-group .form-group:nth-child(7)').css('display','none');
                $('.fields-group .form-group:nth-child(8)').css('display','none');
        }else{
            $('.form-group:nth-child(4)').css('display','none');
            $('.form-group:nth-child(8)').css('display','block');
        }

        $('.type').on('change',function() {
            var $option = $(this).find('option:selected');
            var value = $option.val();
            if(value == '1'){
                $("#amount ").val('');
                $(".free_packages_ids ").prop('disabled', false); 
                $('.fields-group .form-group:nth-child(8)').css('display','block');
                $('.fields-group .form-group:nth-child(4)').css('display','none');
                  $('.fields-group .form-group:nth-child(6)').css('display','block');
                 $('.fields-group .form-group:nth-child(7)').css('display','block');
            }else if(value == '3'){
                $('.form-group:nth-child(4)').css('display','block');
                $('.fields-group .form-group:nth-child(6)').css('display','none');
                // $('.fields-group .form-group:nth-child(7)').css('display','block');
                $('.fields-group .form-group:nth-child(8)').css('display','none');
            }else{
                $(".free_packages_ids ").prop('disabled', true); 
                $('.fields-group .form-group:nth-child(4)').css('display','block');
                 $('.fields-group .form-group:nth-child(6)').css('display','block');
                 $('.fields-group .form-group:nth-child(7)').css('display','block');
                $('.fields-group .form-group:nth-child(8)').css('display','none');
            }
          
        });
    });
</script>
