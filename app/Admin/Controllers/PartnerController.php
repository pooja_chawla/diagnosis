<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\Prerequisite;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Habit;
use App\Models\Inventory;
use App\Models\Vendor;
use App\Models\User;
use App\Models\Partner;
use App\Models\AssignProfile;
use App\Models\Profile;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PartnerController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Partners';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Partner());
        
        $grid->sortable();
        $grid->column('name', __('Partner'));
        $grid->column('logo', __('Logo'))->image('',40,40);
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Partner::findOrFail($id));

        $show->field('id', __('Id'));
       // $show->image()->image();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Partner());
        $form->text('name', __('Partner'));
        $form->image('image', __('Logo'))->rules('mimes:jpg,png,jpeg')->removable();
        // $form->image('vendor.image', __('Logo'))->removable();
       // $form->image('logo', __('Logo'))->move('partners');
        return $form;
    }
}?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
