<?php

namespace App\Admin\Controllers;

use App\Models\WorkDay;
use App\Models\WorkSlot;
use App\Models\AgentWorkSlot;
use App\Models\Agent;
use App\Models\SlotNotAvailabil;
use App\Models\AgentSlotNotAvailabil;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class AgentAvailabilityController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'WorkDay';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WorkDay());

        $grid->column('day', __('Day'));
           
        $states = [
            'on' => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
            'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WorkDay::findOrFail($id));

        $show->field('day', __('Day'));
        $show->field('status', __('Status'))->label();
        $show->workTimings('Products information', function ($timing) {
            $timing->disableCreation();
            // $timing->workDay('Day')->day();
            $timing->opening_hour('Opening Hour');
            $timing->closing_hour('Closing Hour');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WorkDay());
       
       $form->tab('Working Day', function ($form) {
       $form->date('working_date', __('Date time'))->default(date('Y-m-d'));  
       $form->select('day', __('Day'))->options([ 
                                                      'Monday'    => 'Monday',
                                                      'Tuesday'   => 'Tuesday',
                                                      'Wednesday' => 'Wednesday',
                                                      'Thursday'  => 'Thursday',
                                                      'Friday'    => 'Friday',
                                                      'Saturday'  => 'Saturday',
                                                      'Sunday'    => 'Sunday',
                                                  ])
                ->creationRules(['required', "unique:work_days"])
                ->updateRules(['required', "unique:work_days,day,{{id}}"]);
            
        })->tab('Time slots', function ($form) {
            
            $form->hasMany('workslots', function (Form\NestedForm $form) {

                $form->time('opening_hour', __('Start'))->format('HH:mm');
                $form->time('closing_hour', __('End'))->format('HH:mm');
                $states = [
                'on'  => ['value' => 'open', 'text' => 'open', 'color' => 'success'],
                'off' => ['value' => 'close', 'text' => 'close', 'color' => 'danger']
            ];

            $form->switch('status', __('Status'))->states($states)->default('close');
                    
            });

        });

        $form->saved(function (Form $form) {
              return redirect('/admin/work-days');
            //...
        });




        return $form;
    }

    public function slots(){
        $day =  WorkDay::get();
        $duration=1;
        $duratn = 1;
        
        $agents = Agent::select("agents.user_id", "users.name","agents.id")
                        ->leftJoin("users", "users.id", "=", "agents.user_id")
                        ->get();

        $days =WorkDay::select("work_days.*", "agent_work_slot.opening_hour","agent_work_slot.closing_hour")
                        ->leftJoin("agent_work_slot", "agent_work_slot.day_id", "=", "work_days.id")
                        ->get();

               
        return view('admin/agent-slot',compact('days','duratn','duration','agents'));
    }
    public function agentSlots(Request $request){
         $data = $request->all();
         $day =  WorkDay::get();
         $duration=1;
         $duratn = 1;
         $days =WorkDay::select("work_days.*", "agent_work_slot.opening_hour","agent_work_slot.closing_hour")
                        ->leftJoin("agent_work_slot", "agent_work_slot.day_id", "=", "work_days.id")
                      //  ->where('agent_work_slot.agent_id',$data['id'])
                        ->get();
         return view('admin/add-agent-slot',compact('days','duratn','duration'));               
        // print_r($days);
    }
    public function updateslots(Request $request){
         $data = $request->all();
         $slots = AgentWorkSlot::where('day_id',$data['day_id'])->where('agent_id',$data['agent_id'])->get();
         if(count($slots) !=0){
            AgentWorkSlot::where('day_id',$data['day_id'])->where('agent_id',$data['agent_id'])->update(['opening_hour'=> $data['opening_hour'],'closing_hour'=> $data['closing_hour']]);
         }else{
                /*$data['status'] = 1;
                $start = strtotime($data['opening_hour']);
                $end = strtotime($data['closing_hour']);
                $minutes  = ($end - $start) / 60;
                $hours = floor($minutes / 60);
                $min = $minutes - ($hours * 60);

                $start1 = strtotime($data['duration']);
                $end1 = strtotime('00:00');
                $duration  = ($start1 - $end1) / 60;

                //print_r($duration.'-'.$minutes);
                print_r($minutes/$duration);
                die;*/
               AgentWorkSlot::create($data);
         }
    } 
    public function slotNotAvailable(Request $request){
        $data = $request->all();
        $dataa = AgentSlotNotAvailabil::where('day_id',$data['day_id'])->where('slot_id',$data['slot_id'])->get();
        //print_r($dataa);
        
        if(count($dataa)==0){
            AgentSlotNotAvailabil::create($data);
            return 1;
        }else{
            AgentSlotNotAvailabil::where('day_id',$data['day_id'])->where('slot_id',$data['slot_id'])->delete();

            return 0;
        }
       // 
    } 
}
