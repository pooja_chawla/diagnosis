<?php

namespace App\Admin\Controllers;

use App\Models\Booking;
use App\Models\Agent;
use App\Models\Product;
use App\Models\Vendor;
use App\Models\AgentSlots;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;


class BookingController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Booking';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Booking());
        $grid->model()->whereNull('order_id');
        $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();
        // $grid->column('id', __('Id'));
        $grid->user_id(__('Name'))->display(function ($owner_id) {
        return ($owner_id ? User::find($owner_id)['username'] : null);
        });
        // $grid->agent()->name('Agent');
        // $grid->selectedproduct()->title('title');
        $grid->vendor()->username('Vendor');
        $grid->column('booking_date', __('Booking Dates'));
         $grid->time_slot(__('Time Slots'))->display(function ($time_slot) {
        return ($time_slot ? AgentSlots::find($time_slot)['slot'] : null);
        });
        // $grid->column('address', __('Address')); 
        // $grid->column('description', __('Description'));
        // $grid->column('status', __('Status'))->label();
        $states = [
            'on' => ['value' => '1', 'text' => 'Completed', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Pending', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->assigned_agent(__('Assigned Agent'))->display(function ($owner_id) {
        return ($owner_id ? User::find($owner_id)['username'] : null);
    });
        $grid->column('address', __('Address'));
        // $grid->column('updated_at', __('Updated at'));
        // $grid->expandFilter();
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->equal('vendor_id', 'Filter by Vendors')->select(User::whereHas('roles', function($q){ 
                    $q->where('name', 'vendor');
                           })->pluck('username', 'id'));
           $filter->equal('assigned_agent', 'Filter by Agents')->select(User::whereHas('roles', function($q){ 
            $q->where('name', 'agent');
                   })->pluck('username', 'id'));
            
            $filter->between('booking_date', 'Date')->datetime();

            $filter->equal('status')->select(['1' => 'Complete', '0' => 'Pending']);
        });

        return $grid;
   }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Booking::findOrFail($id));

        $show->field('id', __('Id'));
        // $show->field('agent_id', __('Agent id'));
        // $show->field('product_id', __('Product id'));
        // $show->field('customer_id', __('Customer id'));
        $show->vendorDeatail('Vendor information', function ($vendor) {

            $vendor->setResource('/admin/vendors');

            $vendor->id();
            $vendor->username(__('name'));
            $vendor->email();
        });

        $show->runnerDeatail('QR information', function ($vendor) {

            $vendor->setResource('/admin/runner');

            $vendor->id();
            $vendor->booking_status(__('Booking status'));
            $vendor->code(__('QR'));
            $vendor->runner_id();
        });

        $show->assignedAgent('Agent information', function ($agent) {

            $agent->setResource('/admin/agents');

            $agent->id();
            $agent->username(__('name'));
            $agent->email();
        });

        $show->customer('Customer information', function ($customer) {

            // $customer->setResource('/admin/customers');

            // $customer->id();
            $customer->name();
            $customer->number();
        });


        $show->selectedproduct('Test information', function ($customer) {

            // $customer->setResource('/admin/customers');

            // $customer->id();
            $customer->title();
            // $customer->number();
        });

        //  $show->product('Test information', function ($product) {

        //     $product->setResource('/admin/products');

        //     $product->id();
        //     $product->name();
        //     $product->email();
        // });


        $show->field('booking_date', __('Booking Date'));
        $show->field('address', __('Address'));
        $show->field('description', __('Description'));
        // $show->field('status', __('statusstatus'));
         $show->status('status')->using(['1' => 'Completed', '0' => 'Pending'])->label();
        $show->field('created_at', __('Created at'));
        $users = User::all()->toArray();
        $usersArray = [];
        foreach ($users as $item) {
        $usersArray[$item['id']] = $item['username'];
        }
        $show->assigned_agent(__('Assigned Agent'))->using($usersArray);
        $show->request_status()->as(function ($bookingid) {
        $id = request()->route()->parameters('booking');
        $id = $id['booking'];
            $request_statuss = Booking::find($id);
             $request_statuss['request_status'];
            if($request_statuss['request_status'] == '1'){
                return "Request has been accepted";
            }elseif($request_statuss['request_status'] == '0' && !empty($request_statuss['assigned_agent']) ){
                return "Request is not accepted yet"; 
            }else{
                 return "Request sent to agent but not accepted yet"; 
            }
        });
        $show->rejeceted_users()->as(function ($bookingid) {
        $id = request()->route()->parameters('booking');
        $id = $id['booking'];
        $data = array();
            $request_statuss = Booking::find($id);
            $rejected = explode(',', $request_statuss['rejected_users']);
       
            $users = User::whereIn('id',$rejected)->get();
            // dump($users);
            foreach ($users as $key => $value) {
              $data[] = $value->username;
            }
            if($data){

            return implode(',', $data);
        }else{
            return "";
        }
          
        });
        $show->field('updated_at', __('Updated at'));

        return $show;
        }
    

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    { 
        $form = new Form(new Booking());

        // $form->number('agent_id', __('Agent id'));
        // $form->number('product_id', __('Product id'));
        // $form->select('agent_id', __('Agent'))->options(Agent::all()->sortBy('name')->pluck('name', 'id'));
        // $products = User::all()->toArray();
        // $productsArray = [];
        // foreach ($products as $product) {
        // $productsArray[$product['id']] = $product['name'];
        // }
         // $form->text('selectedproduct.title',__('Product'))->readonly();
        // $form->text('product_id', __('Product'))->options(Product::all()->sortBy('title')->pluck('title', 'id'));
        // $form->select('vendor_id', __('Vendor'))->options(Vendor::all()->sortBy('name')->pluck('name', 'id'));
        // $form->select('customer_id', __('Customer'))->options(User::all()->sortBy('name')->pluck('name', 'id'));
        $form->text('booking_date', __('Date time'))->default(date('Y-m-d'));
        $form->text('time_slot', __('Slot'))->default(date('H:i:s'));
        $id = request()->route()->parameter('booking');
        $booking = Booking::find($id);
        $agents = User::whereHas('roles', function($q){ 
                    $q->where('name', 'agent');
                           })->where('status','1')->closeTo($booking->lat, $booking->lang)->get()->pluck('username', 'id');
        // dump($agents);
        $form->select('assigned_agent', __('Assigned Agent'))->options($agents);
        $form->hidden('request_sent');
        $form->hidden('request_status');
        $form->ckeditor('description', __('Description'));
        $form->select('status', __('Status'))->options(['1' => 'Completed', '0' => 'Pending'])->default('Pending');
        return $form;
    }
}


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.assigned_agent ').change(function() {
        var $option = $(this).find('option:selected');
        var value = $option.val();
        $('.request_sent').val(value);
        $('.request_status').val(0);
        });
    })
</script>