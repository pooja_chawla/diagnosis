<?php

namespace App\Admin\Controllers;

use App\Models\Inventory;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class InventoryController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Inventory';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Inventory());
        $grid->model()->orderBy('created_at', 'desc');
        $grid->sortable();
        // $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];
        $grid->column('status')->switch($states);
        $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Inventory::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('description', __('Description'));
        $show->field('image', __('Image'));
        $show->field('status', __('Status'))->using(['1' => 'Active','0' => 'Inactive'])->label();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Inventory());

        $form->text('name', __('Name'));
        $form->textarea('description', __('Description'))->rows(5);
        $form->image('image', __('Image'));
        $states = [
            'on' => ['value' => '1', 'text' => 'Active', 'color' => 'success'],
            'off' => ['value' => '0', 'text' => 'Inactive', 'color' => 'danger'],
        ];;
        $form->switch('status', __('Status'))->states($states)->default('1');

        return $form;
    }
}
