<?php

namespace App\Admin\Extensions;

// use Encore\Admin\Grid\Exporters\AbstractExporter;
Use Encore\Admin\Grid\Exporters\ExcelExporter;
// use Maatwebsite\Excel\Facades\Excel;

class ExcelExpoter extends ExcelExporter
{
    protected $fileName = 'Vendor Product list.xlsx';

    protected $columns = [
        'id' => 'ID',
        'vendor_id' => 'Vendor',
        'product_id' => 'Product',
        'price' => 'Price',
        'type' => 'Type',
        'price_cap' => 'Price cap',
        'offered_price' => 'Offered price',
        'discount' => 'Discount',
    ];
    
}

