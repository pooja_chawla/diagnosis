
<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('vendors', VendorController::class);
    $router->resource('products', ProductController::class);
    $router->resource('agents', AgentController::class);
    $router->resource('customers', CustomerController::class);
    $router->resource('bookings', BookingController::class);
     $router->resource('lab-collection', LabCollectionController::class);
    $router->resource('home-collection', HomeCollectionController::class);
    $router->resource('support', SupportController::class);
    $router->resource('site-info', SiteInfoController::class);
    $router->resource('team-lead', TeamLeadController::class);
    $router->resource('payments', PaymentController::class);
    $router->resource('blogs', BlogController::class);
    $router->resource('coupons', CouponController::class);
    $router->resource('runner', RunnerController::class);
    $router->resource('qrcode', QrController::class);
    $router->resource('customer_booking_product', CustomerBookingProductController::class);
    $router->resource('work-days', WorkDayController::class);
    $router->resource('system-availability', SystemAvailabilityController::class);  
    $router->get('slots', 'SystemAvailabilityController@slots');
    $router->get('agent-slots', 'AgentAvailabilityController@slots');
    $router->post('updateDuration', 'SystemAvailabilityController@updateDuration');
    $router->post('updateslots', 'SystemAvailabilityController@updateslots');
    $router->post('updateAgentSlot', 'AgentAvailabilityController@updateslots');
    $router->post('agentSlotNotAvailable', 'AgentAvailabilityController@slotNotAvailable');
    $router->post('agentSlots', 'AgentAvailabilityController@agentSlots');
    $router->post('slotNotAvailable', 'SystemAvailabilityController@slotNotAvailable');

    $router->resource('tags', TagController::class);
    $router->resource('categories', CategoryController::class);
    $router->resource('habits', HabitController::class);
    $router->resource('inventories', InventoryController::class);
    $router->resource('accreditation', AccreditationController::class);
    $router->resource('testimonial', TestimonialController::class);
    
    $router->resource('product-request', ProductRequestController::class);
    $router->resource('product-profile', ProductProfileController::class);
    $router->resource('partners', PartnerController::class);
    $router->resource('assign-package', AssignPackageController::class);
     $router->resource('vendor-products', VendorMultipleProductController::class);
    $router->resource('price-caps', VendorProductController::class);
    $router->get('offer-price', 'VendorProductController@offerPrice');
    $router->any('vendor-import', 'VendorProductController@import');
    $router->resource('settings', SettingController::class);
    $router->resource('includes', IncludeController::class);
    $router->resource('admin-packages', PackageController::class);

    $router->resource('customer-prescriptions', CustomerPrescriptionController::class);    





});




