<?php
  
namespace App\Admin\Imports;
  
use App\Models\VendorProduct;
use Maatwebsite\Excel\Concerns\ToModel;
  
class VendorProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        return new VendorProduct([
          'vendor_id' => $row[0], 
          'product_id' => $row[1],
          'price' => $row[2],
          'type' => $row[3],
          'price_cap' => $row[4],
          'offered_price' => $row[5],
          'discount' => $row[6],
        ]);
    }
}