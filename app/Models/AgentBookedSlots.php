<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AgentBookedSlots extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'agent_booked_slot';
    protected $fillable = [
        'id',
        'slot_id',
        'agent_id',
        'booking_date'
    ];
}
