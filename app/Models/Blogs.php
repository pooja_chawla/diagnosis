<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Blogs extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'blogs';

    protected $fillable = [
        'id',
        'title',
        'image',
        'description',
        'status'
    ];
   
}
