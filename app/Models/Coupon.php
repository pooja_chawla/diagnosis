<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
       protected $table = 'coupons';
     protected $fillable = [
        'name',
        'code',
        'amount',
        'package_ids',
    ];

    public function getPackageidsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setPackageidsAttribute($value)
    {
        $this->attributes['package_ids'] = implode(',', $value);
    }

    public function getUseridsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setUseridsAttribute($value)
    {
        $this->attributes['user_ids'] = implode(',', $value);
    }

       public function getFreePackagesidsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setFreePackagesidsAttribute($value)
    {
        $this->attributes['free_packages_ids'] = implode(',', $value);
    }
    
}
