<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Customer extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

        protected $fillable = [
        'user_id',
        'age',
        'gender',
        'phone',
        'medical_condition',
        
    ];


   

}
