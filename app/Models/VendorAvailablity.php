<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\VendorProduct;
use App\Models\Vendor;
use App\Models\User;

class VendorAvailablity extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $fillable = [
        'user_id',
        'day',
        'start_time',
        'end_time',
        'order',
    ];
   
    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id');
    }
}
