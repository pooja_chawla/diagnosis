<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Bokking;
use App\Models\Agent;
use App\Models\Booking;
use App\Models\Charge;
use App\Models\Customer;
use App\Models\Product;
use App\Models\VendorProduct;
use App\Models\VendorAvailablity;
use App\Models\AgentDay;
use Illuminate\Database\Eloquent\Builder;
use Rennokki\Rating\Traits\Rate;
use Rennokki\Rating\Contracts\Rating;
use DB;

class User extends Authenticatable implements JWTSubject ,Rating//, MustVerifyEmail
{
    use Notifiable,
        HasRoles,
        HasFactory,
         Rate;

   const REQUEST_ACCEPTED    = 'accepted';
    
    const REQUEST_REJECTED    = 'rejected';

    const AGENT_ROLE = 'agent';
    const RUNNER_ROLE = 'runner';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'phone_code',
        'phone',
        'otp',
        'login_otp',
        'device_type',
        'image',
        'status',
        'device_token',
        'login_type',
        'latitude',
        'longitude',
        'address',
        'address1',
        'address2',
        'city',
        'state',
        'zip',
        'neighbourhood',
        'parent',
        'age',
        'patient_code',
        'gender',
        'landmark'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'photo_url',
    ];

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getPhotoUrlAttribute()
    {
        return vsprintf('https://www.gravatar.com/avatar/%s.jpg?s=200&d=%s', [
            md5(strtolower($this->email)),
            $this->name ? urlencode("https://ui-avatars.com/api/$this->name") : 'mp',
        ]);
    }

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // public function roles()
    // {
    //     return $this->belongsToMany('App\Models\Role');
    // }
    
    public function booking()
    {
        return $this->hasMany(Booking::class,'customer_id');
    }

    public function product()
    {
        return $this->hasMany(VendorProduct::class,'vendor_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

     public function getProductsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProductsAttribute($value)
    {
        $this->attributes['products'] = implode(',', $value);
    }

    public function availablity()
    {
        return $this->hasOne(VendorAvailablity::class,'vendor_id');
    }

    public function agentdays()
    {
        return $this->hasMany(AgentDay::class,'agent_id');
    }

    public function agent()
    {
        return $this->hasOne(Agent::class,'user_id');
    }

    public function vendor()
    {
        return $this->hasOne(Vendor::class);
    }


    public function customer()
    {
        return $this->hasOne(Customer::class,'user_id');
    }

    public function charge()
    {
        return $this->hasMany(Charge::class,'user_id');
    }

    public function userRole()
    {
        if($this->hasRole('customer')){ return 'customer';}
        if($this->hasRole('agent')) { return 'agent'; }
        if($this->hasRole('vendor')){ return 'vendor'; }
        if($this->hasRole('runner')){ return 'runner'; }
    }

    public function scopeByAge(Builder $query, $id)
    {
            $customer = Customer::where('user_id', $id)->first();
            if($customer){
                return $customer->age;
            }else{
                return "";
            }
            
    }
    public function scopeByGender(Builder $query, $id)
    {
            $customer = Customer::where('user_id', $id)->first();
            if($customer){
                return $customer->gender;
            }else{
                return "";
            }
            
    }
       public function scopeByPhone(Builder $query, $id)
    {
            $customer = Customer::where('user_id', $id)->first();
            if($customer){
                return $customer->phone;
            }else{
                return "";
            }
            
    }

    public function scopeByVendor(Builder $query, $id)
    {
            $vendor = User::where('id', $id)->first();
            if($vendor){
                return $vendor->username;
            }else{
                return "";
            }
            
    }

        public function scopeByMedical(Builder $query, $id)
    {
            $customer = Customer::where('user_id', $id)->first();
            if($customer->medical_condition){
                return $customer->medical_condition;
            }else{
                return "";
            }
            
    }
    
    public function scopeById(Builder $query, $id)
    {
            $d = Booking::where('id', $id)->first();
            if($d->payment_status == '0'){
            $pay_status = 'Pending';
            }else if($d->payment_status == '1'){
            $pay_status = 'Payment done online';
            }else if($d->payment_status == '2'){
            $pay_status = 'Payment collected by Scientific officer';
            }else if($d->payment_status == '3'){
            $pay_status = 'Payment Recieved';
            }else if($d->payment_status == '4'){
            $pay_status = 'Paid';
            }
            return $pay_status;
    }
       public function scopeByName(Builder $query, $id)
    {
            $customer = User::where('id', $id)->first();
            if($customer){
                return $customer->username;
            }else{
                return "";
            }
            
    } 
     public function scopeByConditions(Builder $query, $id)
    {
            $customer = Customer::where('user_id', $id)->first();
            if($customer){
                return explode(',', $customer->medical_tests);
            }else{
                return "";
            }
            
    } 

    public function scopeCloseTo(Builder $query, $latitude, $longitude)
    {  
        $users          =  User::whereHas('roles', function($q){ $q->where('name', 'agent');})->where('status','1');
        $users          =       $users->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
          * cos(radians(agent_lat)) * cos(radians(agent_long) - radians(" . $longitude . "))
          + sin(radians(" .$latitude. ")) * sin(radians(agent_lat))) AS distance"));
        $users          =       $users->having('distance', '<', 6.1);

        return $users          =       $users->orderBy('distance', 'asc');
    }

    public function scopeCloseToBooking(Builder $query, $latitude, $longitude)
    {  
     $users =  User::whereHas('roles', function($q){ $q->where('name', 'agent');})->where('status','1');
        $users          =       $users->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
          * cos(radians(agent_lat)) * cos(radians(agent_long) - radians(" . $longitude . "))
          + sin(radians(" .$latitude. ")) * sin(radians(agent_lat))) AS distance"));
        $users          =       $users->having('distance', '<', 10.1);

        return $users          =       $users->orderBy('distance', 'asc');
    }

    public function scopeCloseToLab(Builder $query, $latitude, $longitude)
    {  
     $users =  User::whereHas('roles', function($q){ $q->where('name', 'vendor');})->where('status','1');
        $users          =       $users->select("*", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
          * cos(radians(agent_lat)) * cos(radians(agent_long) - radians(" . $longitude . "))
          + sin(radians(" .$latitude. ")) * sin(radians(agent_lat))) AS distance"));
        $users          =       $users->having('distance', '<', 100);

        return $users          =       $users->orderBy('distance', 'asc');
    }

    public function assigned_agent()
    {
        return $this->belongsToMany(User::class);
    }
 
 public function getTagsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setTagsAttribute($value)
    {
        $this->attributes['assigned_agent'] = implode(',', $value);
    }




}
