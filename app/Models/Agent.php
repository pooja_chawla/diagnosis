<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Agent extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

      protected $fillable = [
        'id',
        'user_id'
        
    ];

}
