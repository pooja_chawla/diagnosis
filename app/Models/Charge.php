<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\User;

class Charge extends Model
{
    use HasFactory,DefaultDatetimeFormat;
    protected $table = 'patient_charges';
     protected $fillable = [
        'user_id',
        'patient_charge',

    ];

}
