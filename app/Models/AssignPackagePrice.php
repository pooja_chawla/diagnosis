<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignPackagePrice extends Model
{
    use HasFactory;
    protected $table = 'assign_package_price';
    protected $fillable = [
    	'id',
        'vendor_id',
        'product_id',
        'amount' 
    ];
}
