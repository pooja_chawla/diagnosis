<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class BackCall extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'back_call';

    protected $fillable = [
        'id',
        'phone'
    ];
   
}
