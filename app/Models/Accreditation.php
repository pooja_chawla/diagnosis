<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class Accreditation extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'accreditation';
    protected $fillable = [
        'id',
        'name',
        'position'
    ];
}
		