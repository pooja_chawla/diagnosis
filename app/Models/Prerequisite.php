<?php

namespace App\Models;

use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Prerequisite extends Model
{
   
    use HasFactory ,DefaultDatetimeFormat;
    protected $table = 'product_prerequisites';
     protected $fillable = [
        'product_id',
        'prerequisite',

    ];

}
