<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\AssignProfile;


class Includes extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'include';

    protected $fillable = [
    	'id',
        'name',
        'charge'
        
    ];
    


}
