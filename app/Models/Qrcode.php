<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qrcode extends Model
{
    use HasFactory;
    protected $table = 'qrcode';

    protected $fillable = [
    	'id',
        'code',
        'booking_id',
        'booking_status',
        'runner_id',
    ];
}
