<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class Partner extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'partner';

    protected $fillable = [
    	'id',
        'name',
        'image'
        
        
    ];
   /* public function assignProduct()
    {
        return $this->hasMany(PackageTests::class);

    }*/
}
