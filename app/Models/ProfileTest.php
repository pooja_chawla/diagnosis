<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class ProfileTest extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'profile_test';

    protected $fillable = [
    	'id',
        'profile_id',
        'name',
        
    ];
}
