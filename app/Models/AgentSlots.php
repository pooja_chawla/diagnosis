<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AgentSlots extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'agent_slots';
    protected $fillable = [
        'id',
        'slot',
        'day_slot',
        'status'
    ];
}
