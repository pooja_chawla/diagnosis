<?php

namespace App\Models;

use App\Models\Prerequisite;
use App\Models\Habbit;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class NewProduct extends Model 
{
    use HasFactory,DefaultDatetimeFormat;
      protected $table = 'products';
        protected $fillable = [
        'id',
        'title',
        'slug',
        'product_code',
        'description',
        'price',
        'discount',
        'include',
        'type',
        'status',
        'vendor'
        
    ];
		
}
