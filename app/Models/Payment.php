<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\CartSession;
use App\Models\User;

class Payment extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'payment';
    protected $fillable = [
        'id',
        'orderId',
        'orderAmount',
        'referenceId',
        'txStatus',
        'paymentMode',
        'txTime',
        'status',
        'created_at',
        'updated_at'
        
    ];

    public function paymentDeatail()
    {
        return $this->hasMany(CartSession::class,'order_id','orderId');
    }
}
