<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AgentWorkSlot extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'agent_work_slot';
    protected $fillable = [
        'day_id',
        'opening_hour',
        'closing_hour',
        'agent_id'
    ];
}
