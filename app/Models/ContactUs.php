<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class ContactUs extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'contact_us';

    protected $fillable = [
    	'id',
        'name',
        'email',
        'phone',
        'subject',
        'msg',
    ];
}
