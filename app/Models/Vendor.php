<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Vendor extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'parent_id',
        'title',
        'slug',
        'type',
        'fax',
        'GST',
        'description',
        'gallery',
        'order',
        'image',
        'status',
        'device_token',
        'seo_title',
        'seo_keyword',
        'seo_des',
        'other_chr',
        'fasting_chr',
        'nfasting_chr',
        'after_meal_chr',
        'perkm',
        'fix_price',
        'flat_price_perkm',
        'patient_price',
        'accreditation',
    ];

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public function setGalleryAttribute($gallery)
	{
	    if (is_array($gallery)) {
	        $this->attributes['gallery'] = json_encode($gallery);
	    }
	}

	public function getGalleryAttribute($gallery)
	{
	    return json_decode($gallery, true);
	}

    public function getCertificateAttribute($certificate){
        if (is_string($certificate)) {
            return json_decode($certificate, true);
        }

        return $certificate;
    }

    public function setCertificateAttribute($certificate)
    {
        if (is_array($certificate)) {
            $this->attributes['certificate'] = json_encode($certificate);
        }
    }

    public function getTabAttribute($value)
    {
        return array_values(json_decode($value, true) ?: []);
    }

    public function setTabAttribute($value)
    {
        $this->attributes['tab'] = json_encode(array_values($value));
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
 

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

     public function getProductsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProductsAttribute($value)
    {
        $this->attributes['products'] = implode(',', $value);
    }
    public function getAccreditationAttribute($value)
    {
        return explode(',', $value);
    }
     public function setAccreditationAttribute($value)
    {
        $this->attributes['accreditation'] = implode(',', $value);
    }

    // public function getTabAttribute($value)
    // {
    //     return array_values(json_decode($value, true) ?: []);
    // }

    // public function setTabAttribute($value)
    // {
    //     $this->attributes['tab'] = json_encode(array_values($value));
    // }

}
