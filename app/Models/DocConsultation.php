<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class DocConsultation extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'doc_consultation';

    protected $fillable = [
    	'id',
        'name',
        'email',
        'age',
        'phone',
        'orderAmount',
        'referenceId',
        'txStatus',
        'txTime',
        'status',
        'created_at',
        'updated_at'
        
        
    ];
   /* public function assignProduct()
    {
        return $this->hasMany(PackageTests::class);

    }*/
}
