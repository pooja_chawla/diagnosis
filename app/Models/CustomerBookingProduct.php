<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class CustomerBookingProduct extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

       protected $fillable = [

        'id',
     	'booking_id',
     	'title',
     	'price',
        'lab_id',
     	'discount',
     	'labName',
        'user_id',
        'order_id',
        'charges',
        
    ];
     protected $table = 'customer_booking_product';


   

}
