<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class ProductInventory extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'inventory_product';

    protected $fillable = [
    	'id',
        'product_id',
        'inventory_id',
        

    ];
}
