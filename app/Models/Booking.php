<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Vendor;
use App\Models\AgentSlots;
use App\Models\Agent;
use App\Models\Qrcode;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Product;
use App\Models\Customer;
use App\Models\User;

class Booking extends Model implements Sortable
{       
    use HasFactory, SortableTrait,DefaultDatetimeFormat;
      protected $fillable = [

        'id',
        'user_id',
        'price',
        'sub_total',
        'order_id',
        'child_id',
        'address',
        'booking_status',
        'address1',
        'address2',
        'neighbourhood',
        'payment_status',
        'landmark',
        'payment_mode',
        'total_price',
        'parent_id',
        'booking_date',
        'time_slot',
        'description',
        'order',
        'status',
        'vendor_id',
        'product_id',
        'customer_id',
        'refered_by_doc',
        'city',
        'state',
        'zip',
        'support_booking',
        'lat',
        'lang',
        'assigned_agent',
        

        
    ];

     /*public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];*/

    public function vendor()
    {
        return $this->belongsTo(User::class,'vendor_id');
    }
    public function vendorDeatail()
    {
    return $this->belongsTo(User::class,'vendor_id');
    }
    public function runnerDeatail()
    {
    return $this->belongsTo(Qrcode::class,'id','booking_id');
    }
    public function agent()
    {
        return $this->belongsTo(Agent::class,'agent_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }

    public function assignedAgent()
    {
        return $this->belongsTo(User::class,'assigned_agent');
    }
    public function scopeByVendor(Builder $query, $id)
    {
            $vendor = Vendor::where('id', $id)->first();
            if($vendor){
                return $vendor->title;
            }else{
                return "";
            }
            
    } 
    public function scopeSlot(Builder $query, $id)
    {
            $vendor = AgentSlots::where('id', $id)->first();
            if($vendor){
                return $vendor->slot;
            }else{
                return "";
            }
            
    } 
     public function scopeByPhone(Builder $query, $id)
    {
            $user = Customer::where('user_id', $id)->first();
            if($user){
                return $user->phone;
            }else{
                return "";
            }
            
    } 
}
