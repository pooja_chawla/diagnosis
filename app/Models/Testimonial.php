<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class Testimonial extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'testimonial';
    protected $fillable = [
        'id',
        'name',
        'age',
        'city',
        'image',
        'description',
    ];
}
		