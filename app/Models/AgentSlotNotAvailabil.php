<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AgentSlotNotAvailabil extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'agent_slot_not_available';

    protected $fillable = [
    	'id',
        'day_id',
        'slot',
        'status',
    ];
}
