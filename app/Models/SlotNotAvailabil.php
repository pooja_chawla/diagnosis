<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class SlotNotAvailabil extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'slot_not_available';

    protected $fillable = [
    	'id',
        'day_id',
        'slot_id',
    ];
}
