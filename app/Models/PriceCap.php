<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Vendor;
use App\Models\User;
use App\Models\Product;

class PriceCap extends Model 
{
    use HasFactory,DefaultDatetimeFormat;


   

    protected $fillable = [
        'vendor_id',
        'product',
        'admin_price',
        'price_cap',
        'type',
        'discount',
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','user_id');
    }
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
