<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class CustomerPrescription extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'customer_prescription';

    protected $fillable = [
    	'id',
        'f_name',
        'l_name',
        'number',
        'image',
    ];
}
