<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class PackageTests extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'product_package_test';

    protected $fillable = [
    	'id',
        'product_id',
        'name',
        
    ];
}
