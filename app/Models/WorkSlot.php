<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class WorkSlot extends Model 
{
    use HasFactory,DefaultDatetimeFormat;


    protected $fillable = [
        'day_id',
        'opening_hour',
        'closing_hour',
    ];
}
