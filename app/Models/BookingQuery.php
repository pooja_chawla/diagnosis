<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class BookingQuery extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'booking_query';

    protected $fillable = [
    	'id',
        'booking_id',
        'query',

    ];
}
