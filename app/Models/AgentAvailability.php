<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AgentAvailability extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'agent_availability';
    protected $fillable = [
        'id',
        'agent_id',
        'opening_hours',
        'closing_hours',
        'date'
    ];
}
