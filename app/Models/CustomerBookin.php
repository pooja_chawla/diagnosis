<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class CustomerBookin extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

       protected $fillable = [

        'id',
        'booking_date',
        'time_slot',
        'description',
        // 'order',
        'status',
        'vendor_id',
        'user_id',
        'customer_id',
        'refered_by_doc',
        'price',
        'discount',
        'payment_mode',
        'total_price',
        'sub_total',
        'convenience',
        'address',
        'city',
        'state',
        'zip',
        'lat',
        'lang',
        'address1',
        'address2',
        'neighbourhood',
        'landmark',
        'status',
        'booking_status',
        'payment_status',
        'bookingRequest',
        'charges',

        
    ];
     protected $table = 'bookings';


   

}
