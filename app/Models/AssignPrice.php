<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AssignPrice extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'assign_package_price';

    protected $fillable = [
    	'id',
        'vendor_id',
        'product_id',
        'amount',
        'discount'
        
        
    ];
   /* public function assignProduct()
    {
        return $this->hasMany(PackageTests::class);

    }*/
}
