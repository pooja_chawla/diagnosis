<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\CustomerBookingProduct;
use App\Models\Vendor;
use App\Models\Agent;
use App\Models\Product;
class CustomerBookingRequest extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;

    const CANCELED_AFTER_SECS = 150;
       protected $fillable = [

        'id',
        'vendor_id',
     	'name',
     	'dob',
     	'phone',
     	'pincode',
     	'state',
     	'city',
     	'address',
        'address1',
     	'dateBooking',
     	'slotBooking',
        'subtotal',
        'amount',
        'discount',
         'lat',
         'lon'
        
    ];
     protected $table = 'customer_booking';


    public function booking()
    {
        return $this->hasMany(CustomerBookingProduct::class, 'booking_id');
    }

    public function customer()
    {
        return $this->belongsTo(CustomerBookingRequest::class,'id');
    }

        public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function selectedproduct()
    {
        return $this->hasMany(CustomerBookingProduct::class,'booking_id');
    }

    public function vendorss()
    {
        return $this->belongsTo(Vendor::class,'vendor_id');
    }
       public function vendor()
    {
        return $this->belongsTo(User::class,'vendor_id');
    }
     public function amounts()
    {
          return $this->hasMany(CustomerBookingRequest::class,'id');
    }
      public function agent()
    {
        return $this->belongsTo(User::class,'assigned_agent');
    }

    public function assignedAgent()
    {
        return $this->belongsTo(User::class,'assigned_agent');
    }


}
