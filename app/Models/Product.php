<?php

namespace App\Models;

use App\Models\Prerequisite;
use App\Models\Habbit;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Profile;
use App\Models\AssignProfile;
use App\Models\PackageTests;
use App\Models\Tag;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Product extends Model 
{
    use HasFactory,DefaultDatetimeFormat;
      protected $table = 'products';
		
    public function vendor()
    {
        return $this->belongsTo(User::class);
    }
    public function AssignProfile()
    {
       return $this->hasMany(Profile::class,'product_id');
    }
     public function product_package_test()
    {
       return $this->hasMany(PackageTests::class,'product_id');
    }
    public function getIncludeAttribute($value)
    {
        return explode(',', $value);
    }
       public function vendor_name()
    {
        return $this->belongsTo(User::class,'vendor');
    }

    public function setIncludeAttribute($value)
    {
        $this->attributes['include'] = implode(',', $value);
    }
    
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function getTagsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = implode(',', $value);
    }

    public function prerequisites()
    {
        return $this->hasMany(PackageTests::class,'product_id');
    }
    public function package()
    {
        return $this->hasMany(AssignProfile::class,'product_id');
    }
    
    public function profile()
    {
        return $this->hasMany(Profile::class,'product_id');
    }
    public function habits()
    {
        return $this->belongsToMany(Habit::class);
    }
     public function getHabitsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setHabitsAttribute($value)
    {
        $this->attributes['habits'] = implode(',', $value);
    }
    
    public function inventories()
    {
        return $this->belongsToMany(Inventory::class);
    }
     public function getProfileAttribute($value)
    {
        return explode(',', $value);
    }
    public function setProfileAttribute($value)
    {
        $this->attributes['profile'] = implode(',', $value);
    }


     public function getInventoriesAttribute($value)
    {
        return explode(',', $value);
    }

    public function setInventoriesAttribute($value)
    {
        $this->attributes['inventories'] = implode(',', $value);
    }
    public function getProfileidsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setProfileidsAttribute($value)
    {
        $this->attributes['profile_ids'] = implode(',', $value);
    }
    // public function tags()
    // {
    //     return $this->belongsToMany();
    // }
}
