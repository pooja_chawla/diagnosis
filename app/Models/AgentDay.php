<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Agent;
use App\Models\WorkDay;

class AgentDay extends Model implements Sortable
{
    use HasFactory, SortableTrait,DefaultDatetimeFormat;


    // public $sortable = [
    //     'order_column_name' => 'order',
    //     'sort_when_creating' => true,
    // ];

    protected $fillable = [
        'agent_id',
        'workday_id',
        'order',
    ];

    public function agent()
    {
        return $this->belongsTo(Agent::class,'agent_id');
    }

    public function workday()
    {
        return $this->belongsTo(WorkDay::class,'wordday_id');
    }
}
