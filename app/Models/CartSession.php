<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class CartSession extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $table = 'cart_session';
    protected $fillable = [
        'id',
        'user_id',
        'child',
        'product_id',
        'product_title',
        'price',
        'actual_price',
        'lab_name',
        'lab_id',
        'slot_id',
        'booking_date',
        'status',
        'package',
        'order_id'
    ];
}
