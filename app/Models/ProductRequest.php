<?php

namespace App\Models;

use App\Models\Prerequisite;
use App\Models\Habbit;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class ProductRequest extends Model 
{
    use HasFactory,DefaultDatetimeFormat;
      protected $table = 'product_request';
        protected $fillable = [
        'id',
        'product_id',
        'vendor',
        'price',
        'discount',
        'status',
        'cancel_status'

    ];
		
}
