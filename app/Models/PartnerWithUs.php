<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class PartnerWithUs extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'partner_with_us';

    protected $fillable = [
    	'id',
        'lab',
        'patient',
        'address',
        'message',
        'email',
        'phone',
        
        
    ];
   /* public function assignProduct()
    {
        return $this->hasMany(PackageTests::class);

    }*/
}
