<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\AssignProfile;


class Profile extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'product_package_profile';

    protected $fillable = [
    	'id',
        'product_id',
        'name',
        
    ];
    public function profilePackage()
    {
        return $this->hasMany(PackageTests::class,'profile_id');
    }
    public function profileTest()
    {
        return $this->hasMany(ProfileTest::class,'profile_id');
    }

    public function vendor_sid()
    {
        return $this->hasMany(AssignProfile::class,'profile_id');
    }
    public function getTagsAttribute($value)
    {
        return explode(',', $value);
    }

    public function setTagsAttribute($value)
    {
        $this->attributes['vendor_id'] = implode(',', $value);
    }


}
