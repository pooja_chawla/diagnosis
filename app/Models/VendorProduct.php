<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\Vendor;
use App\Models\User;
use App\Models\Product;

class VendorProduct extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    protected $fillable = [
        'vendor_id',
        'product_id',
        'price',
        'offered_price',
        'discount',
        'price_cap',
        'type',
        
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id','user_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'vendor_id','id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
