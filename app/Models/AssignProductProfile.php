<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;


class AssignProductProfile extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

  protected $table = 'assign_product_profile';

    protected $fillable = [
    	'id',
        'product_id',
        'profile_id',
        
        
    ];
}
