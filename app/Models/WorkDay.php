<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Models\VendorProduct;
use App\Models\WorkSlot;

class WorkDay extends Model 
{
    use HasFactory,DefaultDatetimeFormat;

    public function workslots()
    {
        return $this->hasMany(WorkSlot::class,'day_id');
    }
}
