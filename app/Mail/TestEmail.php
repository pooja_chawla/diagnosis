<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $vendor_email;

    public function __construct($data,$vendor_email)
    {
        $this->data = $data;
        $this->vendor_email = $vendor_email;
    }

    public function build()
    {
        $address = 'noreply@udaudindia.com';
        $subject = 'Your Appointment Schedule - Home Sample collection.';
        $name = 'Udaud';

        return $this->view('emails.test')
                    ->from($address, $name)
                    ->subject($subject)
                    ->with([ 'test_message' => $this->data['message'] ]);
    }
}