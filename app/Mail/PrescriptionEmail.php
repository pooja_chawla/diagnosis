<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrescriptionEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $attach;

    public function __construct($data,$attach)
    {
        $this->data = $data;
        $this->vendor_email = $attach;
    }

    public function build()
    {
        $address = 'info@udaudindia.com';
        $subject = 'Prescription';
        $name = 'Udaud';

        return $this->view('emails.test')
                    ->from($address, $name)
                    ->subject($subject)
                    ->attach($this->vendor_email)
                    ->with([ 'test_message' => $this->data['message'] ]);
    }
}