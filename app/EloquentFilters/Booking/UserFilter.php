<?php
namespace App\EloquentFilters\Booking;
use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filter

{
  /**
     * Apply the age condition to the query.
     *
     * @param Builder $builder
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply(Builder $builder, $value): Builder
    {
        return $builder->where(function($query) use($value){
            $query->where('username', 'like', '%' .$value. '%');
        });
    }
}