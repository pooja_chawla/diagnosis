<?php
namespace App\Helpers;
use TCG\Voyager\Models\Page;


class PaymentApp {
 
	public static function otp()
	{
	     $otp = rand(100000, 999999);
	     $check = \App\Models\User::where('otp', $otp)->first();
	     if($check) {
	      return self::otp();
	     }
	     return $otp;
	}
	
 public static function distance($latitude1, $longitude1, $latitude2, $longitude2, $distanceIn = 'K')
 {
    if (($latitude1 == $latitude2) && ($longitude1 == $longitude2)) { return 0; } // distance is zero because they're the same point
    $p1 = deg2rad($latitude1);
    $p2 = deg2rad($latitude2);
    $dp = deg2rad($latitude2 - $latitude1);
    $dl = deg2rad($longitude2 - $longitude1);
    $a = (sin($dp/2) * sin($dp/2)) + (cos($p1) * cos($p2) * sin($dl/2) * sin($dl/2));
    $c = 2 * atan2(sqrt($a),sqrt(1-$a));
    $r = 6371008; // Earth's average radius, in meters
    $d = $r * $c; // Distance in meters
    switch ($distanceIn) {
      case 'K': // Kilometers
        return $d / 1000;
        break;
      case 'M': // Miles
        return $d / 1609.34;
        break;
      case 'Y': // Yards
        return $d * 1760;
        break;
      case 'F': // Feet
        return $d * 5280;
        break;
      default:
        return $d;
        break;
    }
 }
}