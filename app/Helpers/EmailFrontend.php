<?php

namespace App\Helpers;

class EmailFrontend
{
    public static function sendEmailFrontend($user,$subject,$temp)
    {
        // 1means attachet file 0 means not attachet file
        // $val = 1;
        // if(empty($attachment)){
        //     $val = 0;
        // }
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://msg.icloudsms.com/rest/services/sendEmail/email?AUTH_KEY=ed5887c912d42d5b72188ee4bbca8922",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"routeId":15,"fromEmail":"noreply@udaudindia.com","fromName":"Udaud","toEmailSet":[{"email":"'.$user.'","personName":"Udaud"}],"contentType":"html","subject":"'.$subject.'","mailContent":'.json_encode($temp).'}',
            // CURLOPT_POSTFIELDS => '{"routeId":15,"fromEmail":"noreply@udaudindia.com","fromName":"Udaud","toEmailSet":[{"email":"'.$user.'","personName":"Udaud"}],"contentType":"html","subject":"'.$subject.'","mailContent":'.json_encode($temp).',"attachmentType": "'.$val.'","attachments":[ {"fileType":"text/plain", "fileName":"'.$attachment.'","fileData":"dGV4dCBmaWxl" }]}',
            CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }
}